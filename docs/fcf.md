# FCF generation

The repair strategy of ledc-sigma is based on the *set-cover* approach.
Basically, when a dirty object needs repair, we look for groups of attributes that "cover" all failing rules.
The main reason for using this approach, is that it allows to find *minimal-cost* repairs quite efficiently.
However, in order for this approach to work, the set of rules need to meet some conditions.
Simply put, it needs to be verified whether or not there are some "hidden" constraints that should be added to the set of rules.
Such hidden constraints are characterized by the fact that there is one attribute (the generating attribute or generator) that is eliminated in the "hidden" constraint.
When a set of rules has all these "hidden" constraints, the set is called **sufficient**.
The production of a sufficient set is done by the *FCF generator*.

```mermaid
graph TB

  in["Input.rules"];
  
  impl("Implicator");
  
  subgraph FCF
    fcf("FCF Generator");
  end

  suff["Sufficient.rules"];

  in-->fcf;
  impl-->fcf;
  fcf-->suff;
  

```

Finding a sufficient set for $`\sigma`$-rules can be a computationally intensive task, especially if the number of rules at input side becomes high.
To mitigate the computation time, the FCF generator tries to optimize the *implicator* module it uses.
The main idea hereby is that if the given $`\sigma`$-rules are bound by some constraints, the implicator can use these constraints for faster computation.
For example, if all attributes have an [ordinal datatype](sigma-rules.md#contractors) and edit rules have [constant atoms](sigma-rules.md#atoms) only, there is a very efficient algorithm to find sufficient sets.
Besides that, sets of rules can sometimes be *partitioned* into several parts such that two rules from different parts never have an attribute in common.
In that case, the FCF algorithm can be applied to each part separately and this will effectively reduce the computational effort.



