# Repair

Whenever a [dataset](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#dataset) contains an erroneous object `o` (i.e., an object that fails some rules), a *repair engine* can be used to propose modifications to `o` in such a way it becomes valid.
Such a generated valid object is called *repair* for `o`.

As an example, consider the [census example](sigma-rules.md#file-format) we introduced before.
Now suppose we are given a dataset as follows:

| age | marital_status | relationship |
|-----|----------------|--------------|
| <font color="#D12C08"><b>17</b></font>  | <font color="#D12C08"><b>Separated</b></font> | Own-child    |

For the single object in this dataset, the set of rules that are failed, is given by:
```
NOT age < 18 & marital_status != 'Never-married'
```
One possible repair would be to change the value for `age` into 18 or higher.
Another repair would be to change the value for `marital_status` into 'Never-married'.
We now need to decide which one is best...
Making that decision is not easy.
In general, there are multiple repairs that are possible.
To ensure that the proposed modifications are *reasonable*, ledc-sigma offers a wide range of **cost models**.
A cost model allows to express the cost of changes made to a [data object](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#dataobject).
Repair engines then take a cost model as input and search for repairs that are *minimal* in terms of this cost model.

# Cost models

When searching for repairs like in the example above, ledc-sigma uses the notion of a *cost model*.
Such a cost model is basically a mapping that assigns a **cost function** to each attribute that might be involved in a repair operation.
A cost function is hereby a function that computes, for some given original value `v` and some repair value `v'` the integer-valued cost to change `v` into `v'`.
A cost function is bound to be *positive definite*: the cost can only be equal to 0 if `v=v'` and is strictly positive otherwise.
This models the simple fact that *keeping* a value for any attribute has no cost at all and *changing* a value for any attribute always has some cost.
These simple rules are visually summarized in the diagram below.

```mermaid
graph TD
  A[Cost-model:<br>1 cost function for each attribute]-->B[Cost function:<br>cost to change v into v'<br>Cost 0 only if v=v'];
  B-->C(Constant cost:<br>v and v' do not matter);
  B-->D(Non-constant cost:<br>v and v' matter);

  style A fill:#ffffff
  style B fill:#ffffff
  style C fill:#ffffff, stroke:#2bbde9
  style D fill:#ffffff, stroke:#2bbde9
```

As you can see from the diagram, we distinguish between two types of cost functions: *constant* cost functions and *non-constant* cost functions.
We briefly explain these below.

## Constant cost functions

A cost function is declared as **constant** if the cost to change some value `v` into another value `v'`, is a constant positive integer.
The idea is simple: the cost does not depend on `v` or `v'` and is entirely determined once you know the attribute for which a change is made.
Variations in costs between attributes actually informs a repair engine that some attributes are more preferable to be changed than others, for example, because they are less reliable.

In our census example above, we could assign a constant cost of `1` to each of the attributes.
In that case, there is no preference among attributes when repairing.
The resulting behaviour will be that a repair should change as few attributes as possible.
If, however, we know that the information in attribute `marital_status` is more *reliable*, we could increase the cost to 2 for `marital_status`.
When doing so in our example above, we create a preference towards changing `age` rather than `marital_status`.

In the original framework of edit rules by Fellegi and Holt, only constant cost functions were considered.
Constant cost functions are very simple, but come with the great advantage that the [set cover method](fcf.md) can be applied in a straightforward manner and that finding minimal repairs is very fast.

## Non-constant cost functions

A cost function is declared as **non-constant** whenever the cost to change some value `v` into another value `v'` is dependent on the values `v` and `v'`.
By this dependency, the assignment of cost to some changes becomes much more flexible.
This allows to model a rich variety of repair strategies.
The usage of non-constant cost functions and the ability to find repairs that are minimal under these cost functions is one of the main innovations that ledc-sigma offers.
A few scenarios in which non-constant functions emerge are listed below.

Sometimes, a dataset is susceptible to specific types of *errors*.
One example is a single digit error in integer numbers, where a single digit is changed into another (e.g., `1269` changed into `1279`).
When some error mechanisms are know to be prevalent in a dataset, a non-constant cost model allows to assign a lower cost to those changes that can be *explained* by that error mechanism.
<br><br>In our census example, we could for example model the fact that single digit errors are often made in the attribute `age`.
The cost to change `17` into `18` is then lower than the cost to change `17` into `26`, simply because with the first repair, the error that was made when inputting the `17` can be explained as a single-digit error.
Ledc-sigma comes with some built-in error mechanisms (see table below) that can be plugged into non-constant cost models immediately.
The Flight example illustrates how these can be used.
Of course, you can extend this logic with any type of error mechanism you want.

| Datatype | Error mechanism name (short name)      | Description                                  | Example value    | Example repair   |
|----------|----------------------------|----------------------------------------------|------------------|------------------|
| Integer  | SingleDigitError (sd)          | A single digit has changed                   | 12**2**00    | 12**3**00            |
| Integer  | AdjacentTranspositionError (at)| Two adjacent digits swapped                  | 12**20**0    | 12**02**0            |
| Integer  | TwinError (t)| Two equal adjacent digits changed equally         | 1**22**00         | 1**44**00            |
| Integer  | PhoneticError (p)  | Two digits changed into similarly sounding digits | 12**20**0         | 12**12**0            |
| Integer  | SignError (s) | The sign of a digit changed                       | **1**2200            | **-1**2200           |
| Integer  | RoundingError (r) | Significant digits are omitted                    | 122**00**            | 122**93**            |
| Datetime | DayMonthSwith (dms) | Digits for day and month are swapped              | **01/09**/2021 10:00 | **09/01**/2021 10:00 |
| Datetime | OneMonthOff (1mo)   | Difference is exactly one month                   | 01/**09**/2021 10:00 | 01/**10**/2021 10:00 |
| Datetime | OneDayOff (1do)     | Difference is exactly one day                     | **01/09**/2021 10:00 | **31/08**/2021 10:00 |
| Datetime | OneHourOff (1ho)    | Difference is exactly one hour                    | 01/09/2021 **10**:00 | 01/09/2021 **11**:00 |
| Datetime | OneMinuteOff (1mio)   | Difference is exactly one minute                  | 01/09/2021 10:**00** | 01/09/2021 10:**01** |
| Datetime | MonthError (me)    | Difference only in month field                    | 01/**09**/2021 10:00 | 01/**05**/2021 10:00 |
| Datetime | DayError (de)      | Difference only in day of month field             | **01**/09/2021 10:00 | **12**/09/2021 10:00 |
| Datetime | HourError (he)     | Difference only in hour of day field              | 01/09/2021 **10**:00 | 01/09/2021 **15**:00 |
| Datetime | MinuteError (mie)    | Difference only in minute field                   | 01/09/2021 10:**00** | 01/09/2021 10:**15** |
| Datetime | SecondError (se)   | Difference only in second field                   | 01/09/2021 10:00:**02** | 01/09/2021 10:00:**08** |


A second scenario where non-constant cost models can be handy, is when there are clear preferences to specific values.
Using preferences are particularly useful in any scenario that includes (very) high risk for some repairs.
A very good illustration of this principle can be found in the allergen example.

Finally, when data are numeric, the cost of changing `v` into `v'` can be dependent on `|v-v'|` to keep changes small.
For example, in our census example, we could prefer to change `17` into `18` more than changing it into `80`.

The main disadvantage of non-constant cost models, is that we cannot use the set cover method anymore in the traditional way.
Behind the scenes, set covers will be inspected and consider value-combinations for them in an order of *Lowest Cost First* (LCF).
Because of these iteration procedures, repairing with non-constant cost functions tends to be slower than with constant cost functions.

# Repair engines

Now that we have the concept of a cost model explained, we are ready to present the repair engines that are currently available in ledc-sigma.
The distinction between constant and non-constant cost models is of course important and the diagram below gives an overview of the repair engines you can use.

```mermaid

graph TB

  R("Repair engine");
  C("Constant");
  NC("Non-constant");

  R-->C;
  R-->NC;

  C-->seq;
  C-->joint;
  C-->condseq;

  NC-->loco;
  NC-->parker;

  seq["Sequential"];
  joint["Joint"];
  condseq["Conditional sequential"];
  
  
  
    loco["LoCo"];
    parker["Parker"];


  style seq fill:#ffffff, stroke:#2bbde9
  style joint fill:#ffffff, stroke:#2bbde9
  style condseq fill:#ffffff, stroke:#2bbde9
  style loco fill:#ffffff, stroke:#2bbde9
  style parker fill:#ffffff, stroke:#2bbde9
```

One common feature of all these repair engines, is their strategy to deal with missing data.
This strategy can be communicated when the repair engine is created and basically, there are three strategies:

* **NO_REPAIR**: NULL values will not be repaired
* **ALWAYS_REPAIR**: NULL values will always be repaired
* **DOMAIN_REPAIR**: NULL values are repaired only when there is at least one domain constraint in the rule set for the attribute
* **CONSTRAINED_REPAIR**: NULL values are repaired only when there is at least one domain constraint in the rule set for the attribute *and* there is least one selection rule that involves the attribute plus some other attribute. This is the default behavior.

The rationale behind these strategies is that we try to repair NULL values for an attribute *only* if some minimal amount of information is available.
When the strategy is DOMAIN_REPAIR, we require that at least some restrictions are known on the values the attribute can take.
If we choose CONSTRAINED_REPAIR, then in addition, we require that the attribute that features a NULL value is involved in some selection rule where some other attribute is also involved.

## Constant repair

In a constant repair engine, we require that a cost model is constant and therefore that **all** cost functions are constant.
Under this assumption, there are few ways in which a repair can be chosen and some of them have been implemented as a repair engine.

### Sequential repair

Sequential repair is a repair strategy that works with a constant cost model and changes attributes one at a time.
This repair strategy is the first of two strategies coined by Fellegi and Holt in their paper on edit rules from 1969.
That means the repair engine has the following strategy:
1. From all minimal set covers of the failing rules, choose one cover is *at random*
2. For each attribute in the chosen cover, choose a repair value among the permitted values

In step 2, the permitted values are determined by (i) those attributes that don't change and (ii) those attributes that already have been repaired.
Because of the second determinant, the *order* in which attributes are repaired plays a role in how the repaired object will look like.
There is however no guarantee on this order: attributes are repaired in a random order. 

In their original paper, Fellegi and Holt made no specifications on how to choose a value among the available alternatives.
The default implementation in ledc-sigma uses a frequency-driven selection strategy.
From the available clean data, the frequency of each of the permitted values for an attribute is measured.
Next, we sample one value from the permitted ones, where the probability of selecting a value is proportionate to its observed frequency.
Different strategies can be implemented to model any selection strategy that picks, from the set of permitted values, a suitable repair value.

### Joint repair

Sequential repair is particularly fast because it changes attributes one by one.
One problem hereby, is that joint distributions of attributes might not be preserved.
When this happens, an alternative is the joint repair engine, which works in a similar way as the sequantial repair engine, except that all attributes are repaired in one go.
The strategy is therefore as follows:
1. From all minimal set covers of the failing rules, choose one cover is *at random*
2. Choose permitted values for the selected attributes in one step.

The choice among permitted value combinations is done by looking for a donor-tuple among the clean data objects.
The repair is achieved by copying the relevant attribute values from the selected donor.
Selection of the donor is again done on a frequency-driven basis: more frequent donors are more likely to be selected.
If no donors can be found, an exception will be thrown.
Such an exception can be caught, in which a fallback strategy like sequential repair can be used.

### Conditional sequential repair

The third constant repair engine is in fact an extension of the sequential repair engine that works in mostly the same way: it chooses a set cover and then repairs attributes one by one.
ConditionalSequentialRepair differs from the regular SequentialRepair in the fact that selection of a set cover among the all minimal set covers is not random.
Random selection of set covers has been criticized on several occasions because it can lead in some case to repairs that would be very infrequent.
The risk is hereby that extensive repairing inflates frequencies of rare events.
To deal with this issue, the selection of a set cover accounts for the *lift* (a notion of correlation) we would observe if some attributes are *kept*.

## Non-constant repair

As soon as one of the cost functions in a cost model is non-constant, constant repair engines can not be used anymore because they can no longer ensure that a proposed repair will be minimal.
The current version of ledc-sigma provides two repair engines that can deal with non-constant cost functions: loco and parker.

### Loco repair

The first engine that deals with non-constant cost functions is loco.
In fact, it accepts *only* cost functions that are non-constant.
Currently, the efficient combination of both constant and non-constant cost functions is a feature that is not yet available in ledc-sigma.

The main problem with non-constant cost functions, is that the regular set cover approach will not work to find minimal solutions.
Instead, for some set cover $`C`$, the algorithm of loco uses a Lowest-Cost First (LCF) iteration over possible value combinations for $`C`$.
That means: cheaper combinations of values will be considered first, even if such solutions might not satisfy all rules.
In this iteration, several pruning strategies can be used to keep searching efficient.

The loco engine can be a powerful tool, when used in the right setting.
In the worst case, it will need to perform an iteration over *all* value-combinations and when dealing with non-nominal data, this will quickly become non-tractable.
This worst case happens when there is no (or very little) differentiation between values in terms of cost.
So the key to using loco, is to use non-constant cost models that provide good differentiation and will propose reasonable repair values with low cost.

### Parker repair

The second repair engine that deals with non-constant cost functions, is parker.
It is the most advanced repair engine in ledc-sigma.
The main addition with respect to loco, is that parker considers an additional *partial key* constraint on top of the regular selection rules.
To illustrate this, assume in our census example that we have multiple sources that provide information about the same person.
Our dataset could then look like this.

|#ssn | age | marital_status | relationship |
|-----|-----|----------------|--------------|
| 1236| **17** | **Separated**| Own-child   |
| 1236| 17 | Never-married  | Own-child     |

We now have two rows providing us information about a person with `#ssn=1236`.
Suppose we know that the surveys were taken approximately at the same time, then we would expect the information about this person to be the same.
This can be modelled by saying that `#ssn` is a (partial) key.
In other words, the following functional dependency must hold:

$`\# ssn \rightarrow age, marital\_status, relationship`$

This constraint now enforces that in a clean dataset, different tuples with the same `#ssn` should have the same values for `age`, `marital_status` and `relationship`.
Because all attributes in our dataset are determined in this way, the key constraint is a *full* key constraint.
In general, it is possible that only some attributes are determined and then we call it a *partial* key constraint.
In addition to this key constraint, the selection rules we defined before must also hold.

Parker is a repair engine that searches for minimal repairs where constraints are a combination of a partial key constraint and a set of selection rules.
It might seem restrictive to consider only one functional dependency, but the expressiveness of this system is much higher compared to selection rules only.
In addition, *because* we only consider one additional key constraint, the algorithm of the loco engine can be used (in a slightly modified version) to search for minimal repairs efficiently.
In fact, the current algorithm will find *all* minimal repairs, from which it can then make a choice.
As such, the combination of a simple (partial) key constraint together with a set of selection rules, offers a good balance between *expressiveness* (which errors can we capture?) and *efficiency* (how fast can we find minimal repairs?).

The [example section](../examples/index.md) offer some further insights and code to get you started with parker.
