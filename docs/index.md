# Introduction

## An overview of ledc-sigma

The goal of ledc-sigma is to find and repair *errors* in basic entity-describing objects in a database (e.g., rows of a table, nodes of a graph).
To do that, it uses a set of rules.
An object is called *clean* if it passes all rules and is called *dirty* if at least one rule fails.
The basic idea is to first verify an object and if it's dirty, propose a "reasonable" repair.
The concept "reasonable" is formalized by means of cost functions.

The general flow of using ledc-sigma, is depicted below.
Click on the white nodes in the diagram for more information on that particular component.

```mermaid
graph TB

  subgraph input
    in["Input.rules"];
    data[("Data")];
    cost("Cost model");
  end
  
  subgraph ledc-sigma
    fcf("FCF Generator");
    suff["Sufficient.rules"];
    repair("Repair engine");
  end

  clean[("Clean data")];

  in-->fcf;
  fcf-->suff;
  
  suff-->repair;
  data-->repair;
  cost-->repair;
  repair-->clean;
  
  click in "https://gitlab.com/ddcm/ledc-sigma/-/blob/master/docs/sigma-rules.md#file-format"
  click fcf "https://gitlab.com/ddcm/ledc-sigma/-/blob/master/docs/fcf.md"
  click cost "https://gitlab.com/ddcm/ledc-sigma/-/blob/master/docs/repair.md#cost-models"
  click repair "https://gitlab.com/ddcm/ledc-sigma/-/blob/master/docs/repair.md#repair-engines"
  
  style in fill:#ffffff;
  style fcf fill:#ffffff;
  style cost fill:#ffffff;
  style repair fill:#ffffff;

```
As can be seen in this flow chart, a set of [rules](sigma-rules.md#file-format) is first processed by a component called the [FCF generator](fcf.md).
This particular is step is necessary because we make use of the set cover method to find optimal repairs.
After that, the set of rules is transformed into a so-called *sufficient set*.
Such a sufficient set is then combined with a particular [cost model](repair.md#cost-models) to define how ideal repairs should be computed.
A [repair engine](repair.md#repair-engines) is then able to compute, for some given dirty object, the most optimal repairs for the given dirty object.

To see this workflow in action, have a look at our [example use cases](examples/index.md).
