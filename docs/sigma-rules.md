# Constraint model

The error model used in ledc-sigma is based on a generic tuple-level constraint known as a *sigma rule* or *selection rule*
The name comes from the fact that the expressiveness is *almost equal* to that of the selection operator $`\sigma`$ in the relational model for databases.
We say 'almost' here for a good reason.
The original idea was to develop a system of rules that was, in terms of expressiveness, equal to relational selection.
However, it turns out that to ensure the [FCF](fcf.md) method still works, the expressivess of rules needs to be slightly higher.

## Contractors

Before we can explain the concept of a selection rule, we first need to explain a few things about the data model on which a selection rule operates.
A selection rules is basically a *predicate* that acts on a [data object](https://gitlab.com/ledc/ledc-core/-/blob/master/docs/data-model.md#dataobject) which is implemented in the [core](https://gitlab.com/ledc/ledc-core) library of ledc.
However, to make correct computations, a selection rule require some additional information about the data on which it acts.
This additional information is comes in the form of a *contractor* that is specified for each attribute present in a data object.

In the very essense, a contractor declares that an attribute is of a certain *datatype* and encodes how to apply a number of *standard operations*  on instances of that datatype.
The main reason why we need contractors is that the algorithms we use to clean data make an abstraction of certain notions like "the next value", "the previous value" or "the value obtained after skipping three values".
These notions are dependent on the datatype, but even for a given datatype, there might be different truncation levels.
For example, it might be unclear what the next value is for the timestamp `26-10-1983 02:00:00`.
Is it a timestamp you get by adding a second?
A minute?
A day?
Truncation levels help here to give meaning to the notions like 'next' and 'previous'.
Contractors also help to distinguish between *nominal* data and *ordinal* data.
When data are pure nominal, notions like 'next' and 'previous' are not applicable.
When data are ordinal, there exists a total order on the domain and we can use this order to define notions like 'next', 'previous' and even 'the gap between two values'.
An overview of the contractors available in the current version of ledc-sigma is shown in the table below

| Contractor name     | Datatype   | Contractor type |
|---------------------|------------|-----------------|
| string              | String     | Nominal         |
| integer             | Integer    | Ordinal         |
| long                | Long       | Ordinal         |
| boolean             | Boolean    | Ordinal         |
| decimal_scale_0     | BigDecimal | Ordinal         |
| decimal_scale_1     | BigDecimal | Ordinal         |
| decimal_scale_2     | BigDecimal | Ordinal         |
| decimal_scale_3     | BigDecimal | Ordinal         |
| date_in_days        | Date       | Ordinal         |
| date_in_months      | Date       | Ordinal         |
| date_in_years       | Date       | Ordinal         |
| time_in_seconds     | Time       | Ordinal         |
| time_in_minutes     | Time       | Ordinal         |
| time_in_hours       | Time       | Ordinal         |
| datetime_in_seconds | Datetime   | Ordinal         |
| datetime_in_minutes | Datetime   | Ordinal         |
| datetime_in_hours   | Datetime   | Ordinal         |
| datetime_in_days    | Datetime   | Ordinal         |
| datetime_in_months  | Datetime   | Ordinal         |
| datetime_in_years   | Datetime   | Ordinal         |
| duration_in_seconds | Duration   | Ordinal         |
| duration_in_minutes | Duration   | Ordinal         |
| instant_in_seconds  | Instant    | Ordinal         |

## Atoms

Now that we have defined the notion of a contractor as a way to settle the datatype *and* basic operations for some attribute, we can define simple constraints for attributes.
Those simple constraints are known as *atoms* and form the basic building blocks from which we can compose selection rules.
In the current version of ledc-sigma, we distinguish three such atoms:

- **Constant atoms** The first type of atom simply compares the value of an attribute `a` with a constant value `v` by means of some operator.
The generic formula for this atom is therefore `a op v`, where `a` is the name of an attribute, `v` is a constant value and  `op` is an operator.
An example of a constant atom is the formula `age <= 18`.

- **Set atoms** The second type of atom compares the value of an attribute `a` with a set of values `S` by means of some operator.
The generic formula for this atom is therefore `a op S`, where `a` is the name of an attribute, `S` is a set of constant values and  `op` is an operator.
An example of a set atom is the formula `age in {12,15,18}`.

- **Variable atoms** The third type of atom compares the values of two attribute `a` and `b` with each other by means of some operator, where the second attribute can be *shifted* with an integer amount of units.
More precisely, in the case of ordinal attributes, we allow to take the n-th power of the successor function for the second attribute.
The generic formula for this atom is therefore `a op S^i(b)`, where `a` and `b` are the names of attributes, `i` is an integer number and `S` denotes the successor function.
Note that when `i=0`, we get the identify function.
In that case, we can also denote the atom by `a op b`.
If `i<0`, we take the n-th power of the *inverse* successor function.

A first example of a variable atom, not using shifting, is the formula `birthdate <= date_of_death`.
A second example that uses (left) shifting, is the formula `time_of_departure <= S^-15(time_of_arrival)`.
Note in this second example the importance of contractors: the meaning of the number of units (`15`) is entirely determined by the contractor that is specified for the attribute `time_of_arrival`.

The operators that can be used in the differen types of atoms, are summarized in the table below.

| Operator symbol | Constant atoms | Variable atoms | Set atoms |
|-----------------|:--------------:|:--------------:|:---------:|
| <               |     Ordinal    |     Ordinal    |     No    |
| <=              |     Ordinal    |     Ordinal    |     No    |
| ==              |       Yes      |       Yes      |     No    |
| !=              |       Yes      |       Yes      |     No    |
| >=              |     Ordinal    |     Ordinal    |     No    |
| >               |     Ordinal    |     Ordinal    |     No    |
| in              |       No       |       No       |    Yes    |
| notin           |       No       |       No       |    Yes    |

## Selection rules

The definition of a selection rules can now be given straightforwardly.
A selection rule is a *negation* of a *conjunction* of atoms that describes a condition for *valid* data.

First of all, atoms are always combined conjunctively. 
Negations of atoms can always be avoided by rewriting the atom and disjunctions of atoms can be avoided by splitting into different rules.
Second, a selection rule describes data that should *not* occur.
To illustrate this, consider the selection rule `NOT age < 18 & marital_status == 'Married'`.
This rule basically states that a minor cannot be married. 
The way it states this, is by defining two conditions (atoms) that, when both true, yield an invalid situation.

A special type of selection rule is one that involves exactly one attribute.
An example is `NOT age < 0`, which is a selection rule that says the attribute `age` needs to be positively valued.
Rules involving exactly one attribute basically provide constraint on the values that this attribute can take.
Therefore, such rules are called *domain rules*.

## File format

For a given dataset, the choice of contractors and the selection rules that must be satisfied can be communicated to ledc-sigma by means of a simple file format.
In this file format, we first declare all attributes and assign them a contractor.
This is done by contractor clause where we first have the name of an attribute, prefixed with `@`, followed by a colon `:` and finally the name of a valid [contractor](#contractors).
Next we simply list all selection rules that need to be satisfied.
When printing atoms, string values should be quoted with single quotes (`'`).
In addition, when the symbol `&` occurs in a string constant, it should be escaped by preceding it with a backslash.
In other words, any appearance of `&` should be replaced with `\&`.

Multiple atoms are combined with `&`.
Comments can be added by prefixing a line with `--`.
A good habbit, although not necessary, is to separate domain rules from the other selection rules, with a preference to writing domain rules first.
If a selection rule involves **more than one** atom, it **must** always start with the negation operator `NOT`.
If a selection rule has **exactly one** atom, it **can** start with the negation operator.
The latter allows to write down domain constraints more intuitively.
A simple example of a configuration that can used in a census survey is given below.

```
-- Attribute contractors
@marital_status:string
@relationship:string
@age:integer

--Domain rules: can start with NOT
relationship in {'Own-child', 'Husband', 'Not-in-family', 'Unmarried', 'Wife', 'Other-relative'}
NOT age > 150
NOT age < 0
marital_status in {'Divorced', 'Married-spouse-absent', 'Widowed', 'Separated', 'Married'}

-- Multivariate selection rules: always start with NOT
NOT age < 18 & marital_status != 'Never-married'
NOT marital_status == 'Married' & relationship == 'Unmarried'
NOT age < 18 &  relationship in {'Husband', 'Wife'}
```

Note in this example that the domain constraints for attributes `relationship` and `marital_status` are written as non-negations, whereas the domain constraints for `age` are written as negations.
