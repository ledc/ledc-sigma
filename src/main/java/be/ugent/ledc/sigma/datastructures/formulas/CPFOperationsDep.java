package be.ugent.ledc.sigma.datastructures.formulas;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorUtil;
import be.ugent.ledc.sigma.datastructures.operators.ComparableOperator;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;

import java.util.*;
import java.util.stream.Collectors;

/**
 * This class provides static operations on CPFs (union, intersection,...)
 */
@Deprecated
public class CPFOperationsDep {

    public static CPF reduce(CPF cpf) throws CPFException {

        if (!cpf.isSimplified()) {
            throw new CPFException("Reducing a CPF can only be done if all atoms in CPF " + cpf + " are simplified.");
        }

        // If the CPF features an ALWAYS_FALSE atom, it is a contradiction and should be reduced to a CPF containing a
        // single ALWAYS_FALSE atom
        // If the CPF features an ALWAYS_TRUE atom, this atom should be removed (i.e., ignored) during the reduction process
        if(cpf.getAtoms()
            .stream()
            .anyMatch(atom -> atom.equals(AbstractAtom.ALWAYS_FALSE)))
        {
            return new CPF(Collections.singleton(AbstractAtom.ALWAYS_FALSE));
        }

        try {

            Set<AbstractAtom<?, ?, ?>> reducedAtoms = new HashSet<>();

            Set<VariableAtom<?, ?, ?>> variableAtoms = cpf.getVariableAtoms();
            Set<VariableAtom<?, ?, ?>> reducedVariableAtoms = new HashSet<>();

            if (!variableAtoms.isEmpty()) {
                reducedVariableAtoms = intersectVariable(new ArrayList<>(variableAtoms));
            }

            Set<ConstantOrdinalAtom<?>> constantOrdinalAtoms = cpf.getConstantOrdinalAtoms();
            Set<SetOrdinalAtom<?>> setOrdinalAtoms = cpf.getSetOrdinalAtoms();

            Set<AbstractAtom<?, ?, ?>> reducedNonVariableOrdinalAtoms = new HashSet<>();

            if (!constantOrdinalAtoms.isEmpty() || !setOrdinalAtoms.isEmpty()) {
                reducedNonVariableOrdinalAtoms = reduceNonVariableOrdinal(constantOrdinalAtoms, setOrdinalAtoms);
            }

            Set<SetNominalAtom<?>> setNominalAtoms = cpf.getSetNominalAtoms();
            Set<SetNominalAtom<?>> reducedSetNominalAtoms = new HashSet<>();

            if (!setNominalAtoms.isEmpty()) {
                reducedSetNominalAtoms = reduceSetNominal(setNominalAtoms);
            }

            if (!reducedVariableAtoms.isEmpty() && !reducedNonVariableOrdinalAtoms.isEmpty()) {
                reducedNonVariableOrdinalAtoms = reduceNonVariableOrdinalVariable(reducedNonVariableOrdinalAtoms, reducedVariableAtoms);
            }

            if (!reducedVariableAtoms.isEmpty() && !reducedSetNominalAtoms.isEmpty()) {
                reducedSetNominalAtoms = reduceSetNominalVariable(reducedSetNominalAtoms, reducedVariableAtoms);
            }

            reducedVariableAtoms = removeImpliedVariableAtoms(reducedVariableAtoms, reducedNonVariableOrdinalAtoms, reducedSetNominalAtoms);

            reducedAtoms.addAll(reducedNonVariableOrdinalAtoms);
            reducedAtoms.addAll(reducedSetNominalAtoms);
            reducedAtoms.addAll(reducedVariableAtoms);

            return new CPF(reducedAtoms);

        } catch (AtomException ex) {
            // If the reduction process results in a contradiction, a CPF containing a single ALWAYS_FALSE atom should
            // be returned
            return new CPF(Collections.singleton(AbstractAtom.ALWAYS_FALSE));
        }

    }

    public static <T extends Comparable<? super T>> boolean atomIsImpliedByCPF(CPF cpf, AbstractAtom<?, ?, ?> impliedAtom) throws CPFException
    {

        if (!(cpf.isSimplified() && impliedAtom.isSimplified()))
        {
            throw new CPFException("Checking implication of an atom against a CPF"
                + " can only be done if all atoms in CPF "
                + cpf
                + " and atom "
                + impliedAtom
                + " are simplified.");
        }

        if (impliedAtom
            .getAttributes()
            .anyMatch(a1 -> cpf
                .getAtoms()
                .stream()
                .anyMatch(aa -> aa
                    .getAttributes()
                    .anyMatch(a2 -> a1.equals(a2)
                        && !aa.getContractor().equals(impliedAtom.getContractor()))))
        ) {
            throw new CPFException("Contractor of "
                + impliedAtom
                + " should be equal to contractor of "
                + cpf
                + " for same attributes.");
        }

        CPF reducedCPF = reduce(cpf);

        if (reducedCPF.isContradiction()) {
            return true;
        }

        if (reducedCPF.isTautology()) {
            return impliedAtom.equals(AbstractAtom.ALWAYS_TRUE);
        }

        Set<VariableAtom<?, ?, ?>> reducedVariableAtoms = reducedCPF.getVariableAtoms();
        Set<ConstantOrdinalAtom<?>> reducedConstantOrdinalAtoms = reducedCPF.getConstantOrdinalAtoms();
        Set<SetOrdinalAtom<?>> reducedSetOrdinalAtoms = reducedCPF.getSetOrdinalAtoms();
        Set<SetNominalAtom<?>> reducedSetNominalAtoms = reducedCPF.getSetNominalAtoms();

        if (impliedAtom.getAtomType().equals(AtomType.VARIABLE))
        {

            Set<AbstractAtom<?, ?, ?>> reducedNonVariableOrdinalAtoms = new HashSet<>(reducedConstantOrdinalAtoms);
            reducedNonVariableOrdinalAtoms.addAll(reducedSetOrdinalAtoms);

            return
                variableIsImpliedByVariable(
                    reducedVariableAtoms,
                    (VariableAtom<?, ?, ?>) impliedAtom) ||
                        (SigmaContractorUtil.isOrdinalContractor(impliedAtom.getContractor()) && variableIsImpliedByNonVariableOrdinal(reducedNonVariableOrdinalAtoms, (VariableOrdinalAtom<T>) impliedAtom)) ||
                        (SigmaContractorUtil.isNominalContractor(impliedAtom.getContractor()) && variableIsImpliedBySetNominal(reducedSetNominalAtoms, (VariableNominalAtom<T>) impliedAtom));

        }
        else if (SigmaContractorUtil.isOrdinalContractor(impliedAtom.getContractor()) && (
                impliedAtom.getAtomType().equals(AtomType.CONSTANT) || impliedAtom.getAtomType().equals(AtomType.SET)))
        {
            return nonVariableOrdinalIsImpliedByNonVariableOrdinal(
                reducedConstantOrdinalAtoms,
                reducedSetOrdinalAtoms,
                (AbstractAtom<T, ?, ?>) impliedAtom);
        }
        else if (SigmaContractorUtil.isNominalContractor(impliedAtom.getContractor()) && impliedAtom.getAtomType().equals(AtomType.SET))
        {
            return setNominalIsImpliedBySetNominal(
                reducedSetNominalAtoms,
                (SetNominalAtom<T>) impliedAtom);
        }
        else if (impliedAtom.equals(AbstractAtom.ALWAYS_FALSE))
        {
            return false;
        }
        else if (impliedAtom.equals(AbstractAtom.ALWAYS_TRUE))
        {
            return true;
        }

        throw new CPFException("Checking implication of an atom against a CPF"
                + " can only be done if reduced implied atom "
                + impliedAtom
                + " is not a constant nominal atom.");

    }

    /**
     * INTERSECTION OF VARIABLE ATOMS AGAINST VARIABLE ATOMS
     **/

    private static <T extends Comparable<? super T>> Set<VariableAtom<?, ?, ?>> intersectVariable(List<VariableAtom<?, ?, ?>> variableAtoms) throws AtomException {

        VariableAtomGraph graph = new VariableAtomGraph(variableAtoms);

        Set<VariableAtom<?, ?, ?>> intersection = new HashSet<>();

        // Check for each variable atom, whether it features a contradiction in conjunction with the other variable atoms,
        // if not, check whether the operator should be tightened
        // Algorithm depends on the type of variable atom: nominal or ordinal
        for (VariableAtom<?, ?, ?> variableAtom : variableAtoms) {

            if (SigmaContractorUtil.isNominalContractor(variableAtom.getContractor())) {
                VariableNominalAtom<T> intersectedAtom = intersectVariableNominal((VariableNominalAtom<T>) variableAtom, graph);
                intersection.add(intersectedAtom);
            } else if (SigmaContractorUtil.isOrdinalContractor(variableAtom.getContractor()))
            {
                Set<VariableOrdinalAtom<T>> intersectedAtoms = intersectVariableOrdinal((VariableOrdinalAtom<T>) variableAtom, graph);
                intersection.addAll(intersectedAtoms);
            }

        }

        return new HashSet<>(intersection);

    }

    private static <T extends Comparable<? super T>> VariableNominalAtom<T> intersectVariableNominal(VariableNominalAtom<T> variableNominalAtom, VariableAtomGraph graph) throws AtomException {

        // Get all paths starting at the left attribute and ending at the right attribute
        List<List<VariableAtomGraph.Edge>> sequences = graph.getAllPaths(variableNominalAtom.getLeftAttribute(), variableNominalAtom.getRightAttribute());
        Set<ComparableOperator> transitiveOperators = new HashSet<>();

        for (List<VariableAtomGraph.Edge> sequence : sequences) {

            // For each path, get the resulting operator (e.g.: A != B & B = C -> A != C)
            // null is returned when no transitive operator can be derived (e.g. A != B & B != C -> A ? C)
            ComparableOperator transitiveOperator = ComparableOperator.getTransitiveOperatorVariable(
                    sequence.stream().map(VariableAtomGraph.Edge::getOperator).collect(Collectors.toList())
            );

            if (transitiveOperator != null) {
                transitiveOperators.add(transitiveOperator);
            }

        }

        // Combine operators of each path from left to right attribute (e.g. A == B & A != B -> disjoint, A == B & A == B -> A == B, A != B & A != B -> A != B)
        if (transitiveOperators.size() > 1) {
            StringBuilder message = new StringBuilder("Intersection of variable atom sequences ");
            for (List<VariableAtomGraph.Edge> sequence : sequences) {
                message.append(sequence).append(" ");
            }
            message.append("is disjoint.");
            throw new AtomException(message.toString());
        }

        return variableNominalAtom;

    }

    private static <T extends Comparable<? super T>> Set<VariableOrdinalAtom<T>> intersectVariableOrdinal(VariableOrdinalAtom<T> variableOrdinalAtom, VariableAtomGraph graph) throws AtomException {

        Set<VariableOrdinalAtom<T>> intersection = new HashSet<>();

        // Get all paths starting at the left attribute and ending at the right attribute
        List<List<VariableAtomGraph.Edge>> sequences = graph.getAllPaths(variableOrdinalAtom.getLeftAttribute(), variableOrdinalAtom.getRightAttribute());

        Set<VariableOrdinalAtom<T>> sequenceAtoms = new HashSet<>();

        for (List<VariableAtomGraph.Edge> sequence : sequences) {

            // For each path, get the resulting variable atom (e.g.: A < B & B < C -> A < C)
            // null is returned when no transitive operator can be derived (e.g. A < B & B > C -> A ? C)
            VariableOrdinalAtom<T> transitiveVariableOrdinal = getTransitiveVariableOrdinal(variableOrdinalAtom.getContractor(), variableOrdinalAtom.getLeftAttribute(), variableOrdinalAtom.getRightAttribute(), sequence);

            if (transitiveVariableOrdinal != null) {
                sequenceAtoms.add(transitiveVariableOrdinal);
            }

        }

        // Split the resulting set of variable atoms into atoms with < and atoms with >
        Set<VariableOrdinalAtom<T>> lowerThanAtoms = sequenceAtoms
                .stream()
                .filter(a -> a.getOperator()
                        .equals(InequalityOperator.LT)
                )
                .collect(Collectors.toSet());

        Set<VariableOrdinalAtom<T>> greaterThanAtoms = sequenceAtoms
                .stream()
                .filter(a -> a.getOperator()
                        .equals(InequalityOperator.GT)
                )
                .collect(Collectors.toSet());

        Set<VariableOrdinalAtom<T>> equalAtoms = sequenceAtoms
                .stream()
                .filter(a -> a.getOperator()
                        .equals(EqualityOperator.EQ)
                )
                .collect(Collectors.toSet());

        if (equalAtoms.size() > 1) {
            StringBuilder message = new StringBuilder("Intersection of variable atom sequences ");
            for (List<VariableAtomGraph.Edge> sequence : sequences) {
                message.append(sequence).append(" ");
            }
            message.append("is disjoint.");
            throw new AtomException(message.toString());
        }

        Optional<VariableOrdinalAtom<T>> equalAtom = equalAtoms.stream().findAny();

        VariableOrdinalAtom<T> lowerThanAtom = null;
        VariableOrdinalAtom<T> greaterThanAtom = null;

        // Tighten lower than atoms to the atom with the lowest right constant (e.g. A < B - 1 & A < B - 2 -> A < B - 2)
        if (!lowerThanAtoms.isEmpty()) {

            lowerThanAtom = Collections.min(lowerThanAtoms, Comparator.comparing(VariableOrdinalAtom::getRightConstant));

            if (equalAtom.isPresent()) {

                if (lowerThanAtom.getRightConstant() - equalAtom.get().getRightConstant() > 0) {
                    lowerThanAtom = null;
                } else {
                    StringBuilder message = new StringBuilder("Intersection of variable atom sequences ");
                    for (List<VariableAtomGraph.Edge> sequence : sequences) {
                        message.append(sequence).append(" ");
                    }
                    message.append("is disjoint.");
                    throw new AtomException(message.toString());
                }

            }

        }

        // Tighten greater than atoms to the atom with the highest right constant (e.g. A > B - 1 & A > B - 2 -> A > B - 1)
        if (!greaterThanAtoms.isEmpty()) {

            greaterThanAtom = Collections.max(greaterThanAtoms, Comparator.comparing(VariableOrdinalAtom::getRightConstant));

            if (equalAtom.isPresent()) {

                if (greaterThanAtom.getRightConstant() - equalAtom.get().getRightConstant() < 0) {
                    greaterThanAtom = null;
                } else {
                    StringBuilder message = new StringBuilder("Intersection of variable atom sequences ");
                    for (List<VariableAtomGraph.Edge> sequence : sequences) {
                        message.append(sequence).append(" ");
                    }
                    message.append("is disjoint.");
                    throw new AtomException(message.toString());
                }

            }

        }

        // Check for disjoint atoms (e.g. A < B & A > B or A < B & A > B - 1)
        if (lowerThanAtom != null && greaterThanAtom != null) {

            if (lowerThanAtom.getRightConstant() - greaterThanAtom.getRightConstant() <= 1) {
                StringBuilder message = new StringBuilder("Intersection of variable atom sequences ");
                for (List<VariableAtomGraph.Edge> sequence : sequences) {
                    message.append(sequence).append(" ");
                }
                message.append("is disjoint.");
                throw new AtomException(message.toString());
            }

            if (lowerThanAtom.getRightConstant() - greaterThanAtom.getRightConstant() == 2) {
                return Collections.singleton(new VariableOrdinalAtom<>(lowerThanAtom.getContractor(), lowerThanAtom.getLeftAttribute(), EqualityOperator.EQ, lowerThanAtom.getRightAttribute(), lowerThanAtom.getRightConstant() - 1));
            }

        }

        equalAtom.ifPresent(intersection::add);
        if (lowerThanAtom != null) intersection.add(lowerThanAtom);
        if (greaterThanAtom != null) intersection.add(greaterThanAtom);

        return intersection;

    }

    private static <T extends Comparable<? super T>> VariableOrdinalAtom<T> getTransitiveVariableOrdinal(OrdinalContractor<T> contractor, String leftAttribute, String rightAttribute, List<VariableAtomGraph.Edge> sequence) {

        // Get the resulting variable atom for all atom sequences from left to right attribute (e.g.: A < B & B < C -> A < C)
        // null is returned when no transitive operator can be derived (e.g. A < B & B > C -> A ? C)
        ComparableOperator transitiveOperator = ComparableOperator.getTransitiveOperatorVariable(sequence.stream().map(VariableAtomGraph.Edge::getOperator).collect(Collectors.toList()));

        if (transitiveOperator == null) {
            return null;
        }

        // Find the minimum difference in values between the left and the right attribute (e.g. A < B & B < C + 2 -> A < C + 1)
        int minDiff = 0;

        for (int i = 0; i < sequence.size(); i++) {

            VariableAtomGraph.Edge edge = sequence.get(i);

            if (edge.getOperator().equals(EqualityOperator.EQ)) {
                minDiff += edge.getConstant() * -1;
            } else if (edge.getOperator().equals(InequalityOperator.LT)) {
                minDiff += edge.getConstant() * -1 + 1;
            } else if (edge.getOperator().equals(InequalityOperator.GT)) {
                minDiff += edge.getConstant() * -1 - 1;
            }

        }

        int constant = 0;

        if (transitiveOperator.equals(EqualityOperator.EQ)) {
            constant = minDiff * -1;
        } else if (transitiveOperator.equals(InequalityOperator.LT)) {
            constant = minDiff * -1 + 1;
        } else {
            constant = minDiff * -1 - 1;
        }

        return new VariableOrdinalAtom<>(
                contractor,
                leftAttribute,
                transitiveOperator,
                rightAttribute,
                constant);

    }

    /**
     * INTERSECTION OF NON-VARIABLE ORDINAL ATOMS AGAINST NON-VARIABLE ORDINAL ATOMS
     **/

    private static Set<AbstractAtom<?, ?, ?>> reduceNonVariableOrdinal(Set<ConstantOrdinalAtom<?>> constantOrdinalAtoms, Set<SetOrdinalAtom<?>> setOrdinalAtoms) throws AtomException {

        // Intersect non-variable ordinal atoms for each attribute (e.g., A < 4 & A IN {1,2,3,5} -> A IN {1,2,3})
        Set<AbstractAtom<?, ?, ?>> reducedNonVariableOrdinalAtoms = new HashSet<>();

        Set<String> attributes = constantOrdinalAtoms
                .stream()
                .flatMap(ConstantAtom::getAttributes)
                .collect(Collectors.toSet());

        attributes.addAll(setOrdinalAtoms
                .stream()
                .flatMap(SetAtom::getAttributes)
                .collect(Collectors.toSet()));

        for (String attribute : attributes) {
            reducedNonVariableOrdinalAtoms.addAll(intersectNonVariableOrdinal(attribute, constantOrdinalAtoms, setOrdinalAtoms));
        }

        return reducedNonVariableOrdinalAtoms;

    }

    private static <T extends Comparable<? super T>> Set<AbstractAtom<?, ?, ?>> intersectNonVariableOrdinal(String attribute, Set<ConstantOrdinalAtom<?>> constantOrdinalAtoms, Set<SetOrdinalAtom<?>> setOrdinalAtoms) throws AtomException {

        Set<AbstractAtom<?, ?, ?>> intersection = new HashSet<>();

        Optional<ConstantOrdinalAtom<?>> coAtomForAttribute = constantOrdinalAtoms
                .stream().filter(a -> a.getAttribute().equals(attribute)).findFirst();
        Optional<SetOrdinalAtom<?>> sAtomForAttribute = setOrdinalAtoms
                .stream().filter(a -> a.getAttribute().equals(attribute)).findFirst();

        // An empty set of atoms is returned in case no atoms for given attribute are passed
        if (coAtomForAttribute.isEmpty() && sAtomForAttribute.isEmpty()) {
            return new HashSet<>();
        }

        OrdinalContractor<T> contractor = coAtomForAttribute.isPresent() ?
                (OrdinalContractor<T>) coAtomForAttribute.get().getContractor() :
                (OrdinalContractor<T>) sAtomForAttribute.get().getContractor();

        Set<SetAtom<?, ?>> setAtoms = new HashSet<>(setOrdinalAtoms);

        // Get interval represented by constant ordinal atoms and set of constants represented by set atoms
        Interval<T> interval = getConstantOrdinalInterval(attribute, constantOrdinalAtoms);
        Pair<SetOperator, Set<T>> constants = getSetConstants(attribute, setAtoms);

        if (constants == null) {
            if (interval.getLeftBound() == null && interval.getRightBound() == null) {
                return new HashSet<>();
            } else {
                if (interval.getRightBound() != null && interval.getLeftBound() != null && interval.getRightBound().compareTo(interval.getLeftBound()) == 0) {
                    intersection.add(new SetOrdinalAtom<>(contractor, attribute, SetOperator.IN, Collections.singleton(interval.getLeftBound())));
                } else {
                    if (interval.getRightBound() != null) {
                        intersection.add(new ConstantOrdinalAtom<>(contractor, attribute, InequalityOperator.LEQ,  interval.getRightBound()));
                    }
                    if (interval.getLeftBound() != null) {
                        intersection.add(new ConstantOrdinalAtom<>(contractor, attribute, InequalityOperator.GEQ,  interval.getLeftBound()));
                    }
                }
                return intersection;
            }
        } else {
            if (interval.getLeftBound() == null && interval.getRightBound() == null) {
                intersection.add(new SetOrdinalAtom<>(contractor, attribute, constants.getFirst(), constants.getSecond()));
                return intersection;
            } else {
                intersection.addAll(intersectOrdinalConstantsInterval(constants, interval, contractor, attribute));
            }
        }

        return intersection;

    }

    private static <T extends Comparable<? super T>> Interval<T> getConstantOrdinalInterval(String attribute, Set<ConstantOrdinalAtom<?>> constantOrdinalAtoms) throws AtomException {

        // Get only the constant ordinal atoms for the given attribute
        Set<ConstantOrdinalAtom<T>> atomsForAttribute = constantOrdinalAtoms
                .stream()
                .filter(a -> a.getAttribute().equals(attribute))
                .map(a -> (ConstantOrdinalAtom<T>) a)
                .collect(Collectors.toSet());

        // Find the smallest overlapping interval for the set of constant ordinal atoms
        // Start with the interval ]-infty, +infty[
        Interval<T> interval = new Interval<>();

        for (ConstantOrdinalAtom<T> coa : atomsForAttribute) {

            // Check whether current coa constant is lower than current upper bound and greater than current lower bound
            boolean lowerThanUpperBound = interval.getRightBound() == null || coa.getConstant().compareTo(interval.getRightBound()) <= 0;
            boolean greaterThanLowerBound = interval.getLeftBound() == null || coa.getConstant().compareTo(interval.getLeftBound()) >= 0;

            if (coa.getOperator().equals(InequalityOperator.LEQ)) {

                if (lowerThanUpperBound && greaterThanLowerBound) {
                    interval.setRightBound(coa.getConstant());
                    interval.setRightOpen(false);
                } else if (lowerThanUpperBound) {
                    throw new AtomException("Intersection of given atoms " + constantOrdinalAtoms + " for attribute " + attribute + " is disjoint.");
                }
            }

            if (coa.getOperator().equals(InequalityOperator.GEQ)) {

                if (lowerThanUpperBound && greaterThanLowerBound) {
                    interval.setLeftBound(coa.getConstant());
                    interval.setLeftOpen(false);
                } else if (greaterThanLowerBound) {
                    throw new AtomException("Intersection of given atoms " + constantOrdinalAtoms + " for attribute " + attribute + " is disjoint.");
                }
            }

        }

        return interval;

    }

    private static <T extends Comparable<? super T>> Set<AbstractAtom<?, ?, ?>> intersectOrdinalConstantsInterval(Pair<SetOperator, Set<T>> constants, Interval<T> interval, OrdinalContractor<T> contractor, String attribute) throws AtomException {

        Set<AbstractAtom<?, ?, ?>> result = new HashSet<>();

        if (constants.getFirst().equals(SetOperator.IN)) {
            constants.getSecond().removeIf(c -> !interval.contains(c));
            if (constants.getSecond().isEmpty()) {
                throw new AtomException("Intersection of given atoms for attribute " + attribute + " is disjoint.");
            }
            result.add(new SetOrdinalAtom<>(contractor, attribute, SetOperator.IN, constants.getSecond()));
            return result;
        }

        constants.getSecond().removeIf(c -> !interval.contains(c));

        T newLeft = interval.getLeftBound();
        T newRight = interval.getRightBound();

        if (!constants.getSecond().isEmpty()) {

            List<T> sortedC = constants.getSecond().stream().sorted().collect(Collectors.toList());

            T x = sortedC.get(0);

            if (interval.getLeftBound() != null && x.compareTo(interval.getLeftBound()) == 0) {

                while (sortedC.contains(contractor.next(x)) && (interval.getRightBound() == null || contractor.next(x).compareTo(interval.getRightBound()) <= 0)) {
                    x = contractor.next(x);
                }

                if (interval.getRightBound() != null && x.compareTo(interval.getRightBound()) == 0) {
                    throw new AtomException("Intersection of given atoms for attribute " + attribute + " is disjoint.");
                } else {
                    newLeft = contractor.next(x);
                }

            }

            x = sortedC.get(sortedC.size()-1);

            if (interval.getRightBound() != null && x.compareTo(interval.getRightBound()) == 0) {

                while (contractor.hasPrevious(x) && sortedC.contains(contractor.previous(x)) && (interval.getLeftBound() == null || contractor.previous(x).compareTo(interval.getLeftBound()) >= 0)) {
                    x = contractor.previous(x);
                }

                if(contractor.hasPrevious(x)) {
                    newRight = contractor.previous(x);
                }

            }

        }

        if (newLeft != null && newRight != null && newLeft.compareTo(newRight) == 0) {
            result.add(new SetOrdinalAtom<>(contractor, attribute, SetOperator.IN, Collections.singleton(newLeft)));
            T finalNewLeft = newLeft;
            constants.getSecond().removeIf(c -> c.compareTo(finalNewLeft) != 0);
        } else {
            if (newRight != null) {
                result.add(new ConstantOrdinalAtom<>(contractor, attribute, InequalityOperator.LEQ,  newRight));
                T finalNewRight = newRight;
                constants.getSecond().removeIf(c -> c.compareTo(finalNewRight) > 0);
            }
            if (newLeft != null) {
                result.add(new ConstantOrdinalAtom<>(contractor, attribute, InequalityOperator.GEQ,  newLeft));
                T finalNewLeft = newLeft;
                constants.getSecond().removeIf(c -> c.compareTo(finalNewLeft) < 0);
            }
        }

        if (!constants.getSecond().isEmpty()) {
            result.add(new SetOrdinalAtom<>(contractor, attribute, SetOperator.NOTIN, new HashSet<>(constants.getSecond())));
        }

        return result;

    }

    /**
     * INTERSECTION OF SET NOMINAL ATOMS AGAINST SET NOMINAL ATOMS
     **/

    private static Set<SetNominalAtom<?>> reduceSetNominal(Set<SetNominalAtom<?>> setNominalAtoms) throws AtomException {

        Set<SetNominalAtom<?>> reducedSetNominalAtoms = new HashSet<>();

        Set<String> attributes = setNominalAtoms.stream().flatMap(SetAtom::getAttributes).collect(Collectors.toSet());

        // Intersect constant nominal atoms for each attribute (e.g. A NOTIN {'4', '5'} & A IN {'3', '4'} -> A IN {'3'})
        for (String attribute : attributes) {

            SetNominalAtom<?> reducedSetNominalAtom = intersectSetNominal(attribute, setNominalAtoms);

            if (reducedSetNominalAtom != null) {
                reducedSetNominalAtoms.add(reducedSetNominalAtom);
            }


        }

        return reducedSetNominalAtoms;

    }

    private static <T extends Comparable<? super T>> SetNominalAtom<T> intersectSetNominal(String attribute, Set<SetNominalAtom<?>> setNominalAtoms) throws AtomException {

        Optional<SetNominalAtom<?>> sAtomForAttribute = setNominalAtoms.stream().filter(a -> a.getAttribute().equals(attribute)).findFirst();

        if (sAtomForAttribute.isEmpty()) {
            return null;
        }

        NominalContractor<T> contractor = (NominalContractor<T>) sAtomForAttribute.get().getContractor();

        Set<SetAtom<?, ?>> setAtoms = setNominalAtoms.stream().map(a -> (SetAtom<?, ?>) a).collect(Collectors.toSet());
        Pair<SetOperator, Set<T>> constants = getSetConstants(attribute, setAtoms);

        if (constants == null) {
            return null;
        }

        else if (constants.getFirst().equals(SetOperator.IN)) {
            return new SetNominalAtom<>(contractor, attribute, SetOperator.IN, constants.getSecond());
        }

        else {
            return new SetNominalAtom<>(contractor, attribute, SetOperator.NOTIN, constants.getSecond());
        }

    }

    private static <T extends Comparable<? super T>, C extends SigmaContractor<T>> Pair<SetOperator, Set<T>> getSetConstants(String attribute, Set<SetAtom<?, ?>> setAtoms) throws AtomException {

        // Get only the set atoms for the given attribute
        Set<SetAtom<T, C>> atomsForAttribute = setAtoms
                .stream()
                .filter(a -> a.getAttribute().equals(attribute))
                .map(a -> (SetAtom<T, C>) a)
                .collect(Collectors.toSet());

        // In case the set of set atoms is empty, a null value is returned
        if (setAtoms.isEmpty()) {
            return null;
        }

        // Get all set atoms with an IN operator
        List<Set<T>> inConstants = atomsForAttribute
                .stream()
                .filter(a -> a.getOperator().equals(SetOperator.IN))
                .map(SetAtom::getConstants)
                .collect(Collectors.toList());

        Set<T> reducedInConstants = new HashSet<>();

        // The set of reduced IN constants is equal to the intersection of the original IN constants sets
        if (!inConstants.isEmpty()) {

            reducedInConstants.addAll(inConstants.get(0));

            for (int i = 1; i < inConstants.size(); i++) {
                reducedInConstants.retainAll(inConstants.get(i));
            }

            // If this intersection is disjoint, CPF is a contradiction
            if (reducedInConstants.isEmpty()) {
                throw new AtomException("Intersection of given atoms " + setAtoms + " for attribute " + attribute + " is disjoint.");
            }

        }

        // Get all set atoms with a NOTIN operator
        // The set of reduced NOTIN constants is equal to the union of the original NOTIN constants sets
        Set<Set<T>> notInConstants = atomsForAttribute
                .stream()
                .filter(a -> a.getOperator().equals(SetOperator.NOTIN))
                .map(SetAtom::getConstants)
                .collect(Collectors.toSet());

        Set<T> reducedNotInConstants = notInConstants.stream().flatMap(Collection::stream).collect(Collectors.toSet());

        if (!reducedInConstants.isEmpty()) {

            // If IN constants and NOTIN constants exist, then the result equals the set of IN constants \ the set of
            // NOTIN constants
            reducedInConstants.removeAll(reducedNotInConstants);

            if (!reducedInConstants.isEmpty()) {
                return new Pair<>(SetOperator.IN, reducedInConstants);
            } else {
                throw new AtomException("Intersection of given atoms " + setAtoms + " for attribute " + attribute + " is disjoint.");
            }

        } else {
            return new Pair<>(SetOperator.NOTIN, reducedNotInConstants);
        }

    }

    /**
     * INTERSECTION OF NON VARIABLE ORDINAL ATOMS AGAINST VARIABLE ATOMS
     **/

    private static <T extends Comparable<? super T>> Set<AbstractAtom<?, ?, ?>> reduceNonVariableOrdinalVariable(Set<AbstractAtom<?, ?, ?>> reducedNonVariableOrdinalAtoms, Set<VariableAtom<?, ?, ?>> reducedVariableAtoms) throws AtomException {

        // Get only the non-variable ordinal atoms that can be reduced by means of matching variable atoms,
        // i.e., non-variable ordinal atoms featuring an attribute that appears in a variable atom
        Set<String> variableAttributes = reducedVariableAtoms.stream().flatMap(VariableAtom::getAttributes).collect(Collectors.toSet());
        Set<AbstractAtom<?, ?, ?>> remainingNonVariableOrdinalAtoms = reducedNonVariableOrdinalAtoms
                .stream()
                .filter(oa -> oa.getAttributes().noneMatch(variableAttributes::contains))
                .collect(Collectors.toSet());

        // As long as the set of intersected non-variable ordinal atoms changes, keep on reducing
        while (true) {

            Set<AbstractAtom<?, ?, ?>> intersectedNonVariableOrdinalAtoms = new HashSet<>();

            for (String attribute : variableAttributes) {

                // Get the variable atoms containing given attribute, there should be at least one
                List<VariableAtom<?, ?, ?>> variableAtomsForAttribute = reducedVariableAtoms
                        .stream()
                        .filter(va -> va.getAttributes().anyMatch(a -> a.equals(attribute)))
                        .collect(Collectors.toList());

                OrdinalContractor<T> contractor = (OrdinalContractor<T>) variableAtomsForAttribute.get(0).getContractor();
                Set<String> remainingAttributes = variableAttributes.stream().filter(a -> !a.equals(attribute)).collect(Collectors.toSet());
                intersectedNonVariableOrdinalAtoms.addAll(intersectNonVariableOrdinalWithVariable(attribute, remainingAttributes, contractor, reducedNonVariableOrdinalAtoms, reducedVariableAtoms));

            }

            if (intersectedNonVariableOrdinalAtoms.equals(reducedNonVariableOrdinalAtoms)) {
                break;
            } else {
                reducedNonVariableOrdinalAtoms = intersectedNonVariableOrdinalAtoms;
            }

        }

        reducedNonVariableOrdinalAtoms.addAll(remainingNonVariableOrdinalAtoms);

        return reducedNonVariableOrdinalAtoms;

    }

    private static <T extends Comparable<? super T>> Set<AbstractAtom<?, ?, ?>> intersectNonVariableOrdinalWithVariable(String attribute, Set<String> remainingAttributes, OrdinalContractor<T> contractor, Set<AbstractAtom<?, ?, ?>> reducedNonVariableOrdinalAtoms, Set<VariableAtom<?, ?, ?>> reducedVariableAtoms) throws AtomException {

        // Get only the set ordinal atoms for the given attribute
        Set<SetOrdinalAtom<?>> setAtomsForAttribute = reducedNonVariableOrdinalAtoms
                .stream()
                .filter(a -> a instanceof SetOrdinalAtom<?>)
                .map(a -> (SetOrdinalAtom<?>) a)
                .filter(a -> a.getAttribute().equals(attribute))
                .collect(Collectors.toSet());

        // Get only the constant ordinal atoms for the given attribute
        Set<ConstantOrdinalAtom<?>> constantAtomsForAttribute = reducedNonVariableOrdinalAtoms
                .stream()
                .filter(a -> a instanceof ConstantOrdinalAtom<?>)
                .map(a -> (ConstantOrdinalAtom<?>) a)
                .filter(a -> a.getAttribute().equals(attribute))
                .collect(Collectors.toSet());

        // Create a variable atom graph containing the reduced variable atoms
        VariableAtomGraph graph = new VariableAtomGraph(reducedVariableAtoms);

        // Get all paths from the given attribute to each of the remaining attributes
        for (String remainingAttribute : remainingAttributes) {

            Set<SetOrdinalAtom<T>> setInAtomsForRemainingAttribute = reducedNonVariableOrdinalAtoms
                    .stream()
                    .filter(a -> a instanceof SetOrdinalAtom<?>)
                    .map(a -> (SetOrdinalAtom<T>) a)
                    .filter(a -> a.getAttribute().equals(remainingAttribute) && a.getOperator().equals(SetOperator.IN))
                    .collect(Collectors.toSet());

            Set<SetOrdinalAtom<T>> setNotInAtomsForRemainingAttribute = reducedNonVariableOrdinalAtoms
                    .stream()
                    .filter(a -> a instanceof SetOrdinalAtom<?>)
                    .map(a -> (SetOrdinalAtom<T>) a)
                    .filter(a -> a.getAttribute().equals(remainingAttribute) && a.getOperator().equals(SetOperator.NOTIN))
                    .collect(Collectors.toSet());

            Set<ConstantOrdinalAtom<T>> constantAtomsForRemainingAttribute = reducedNonVariableOrdinalAtoms
                    .stream()
                    .filter(a -> a instanceof ConstantOrdinalAtom<?>)
                    .map(a -> (ConstantOrdinalAtom<T>) a)
                    .filter(a -> a.getAttribute().equals(remainingAttribute))
                    .collect(Collectors.toSet());

            List<List<VariableAtomGraph.Edge>> sequences = graph.getAllPaths(attribute, remainingAttribute);

            // For each path, check whether the constant ordinal atoms should be reduced
            for (List<VariableAtomGraph.Edge> sequence : sequences) {

                // For each path, get the resulting variable atom (e.g.: A < B & B < C -> A < C)
                // null is returned when no transitive operator can be derived (e.g. A < B & B > C -> A ? C),
                // in this case, transitive variable atom can not apply for reduction of constant atoms
                VariableOrdinalAtom<T> transitiveVariableOrdinal = getTransitiveVariableOrdinal(contractor, attribute, remainingAttribute, sequence);

                if (transitiveVariableOrdinal == null) {
                    continue;
                }

                ComparableOperator transitiveOperator = transitiveVariableOrdinal.getOperator();
                int rightConstant = transitiveVariableOrdinal.getRightConstant();

                // Introduce new constant ordinal atom for attribute depending on the operator of the transitive variable atom
                if (transitiveOperator.equals(InequalityOperator.LT)) {

                    Optional<SetOrdinalAtom<T>> saOpt = setInAtomsForRemainingAttribute.stream().findFirst();

                    if (saOpt.isPresent()) {
                        SetOrdinalAtom<T> sa = saOpt.get();
                        T max = Collections.max(sa.getConstants());
                        constantAtomsForAttribute.add(
                                new ConstantOrdinalAtom<>(sa.getContractor(), attribute, InequalityOperator.LEQ, sa.getContractor().previous(sa.getContractor().add(max, rightConstant)))
                        );
                    }

                    Optional<ConstantOrdinalAtom<T>> caOpt = constantAtomsForRemainingAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.LEQ)).findFirst();

                    if (caOpt.isPresent()) {
                        ConstantOrdinalAtom<T> ca = caOpt.get();
                        constantAtomsForAttribute.add(
                                new ConstantOrdinalAtom<>(ca.getContractor(), attribute, InequalityOperator.LEQ, ca.getContractor().previous(ca.getContractor().add(ca.getConstant(), rightConstant)))
                        );
                    }

                } else if (transitiveOperator.equals(InequalityOperator.GT)) {

                    Optional<SetOrdinalAtom<T>> saOpt = setInAtomsForRemainingAttribute.stream().findFirst();

                    if (saOpt.isPresent()) {
                        SetOrdinalAtom<T> sa = saOpt.get();
                        T min = Collections.min(sa.getConstants());
                        constantAtomsForAttribute.add(
                                new ConstantOrdinalAtom<>(sa.getContractor(), attribute, InequalityOperator.GEQ, sa.getContractor().next(sa.getContractor().add(min, rightConstant)))
                        );
                    }

                    Optional<ConstantOrdinalAtom<T>> caOpt = constantAtomsForRemainingAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.GEQ)).findFirst();

                    if (caOpt.isPresent()) {
                        ConstantOrdinalAtom<T> ca = caOpt.get();
                        constantAtomsForAttribute.add(
                                new ConstantOrdinalAtom<>(ca.getContractor(), attribute, InequalityOperator.GEQ, ca.getContractor().next(ca.getContractor().add(ca.getConstant(), rightConstant)))
                        );
                    }
                } else if (transitiveOperator.equals(EqualityOperator.EQ)) {

                    Optional<SetOrdinalAtom<T>> siaOpt = setInAtomsForRemainingAttribute.stream().findFirst();

                    if (siaOpt.isPresent()) {
                        SetOrdinalAtom<T> sia = siaOpt.get();
                        Set<T> constants = sia.getConstants().stream().map(c -> sia.getContractor().add(c, rightConstant)).collect(Collectors.toSet());
                        setAtomsForAttribute.add(
                                new SetOrdinalAtom<>(sia.getContractor(), attribute, SetOperator.IN, constants)
                        );
                    }

                    Optional<SetOrdinalAtom<T>> sniaOpt = setNotInAtomsForRemainingAttribute.stream().findFirst();

                    if (sniaOpt.isPresent()) {
                        SetOrdinalAtom<T> snia = sniaOpt.get();
                        Set<T> constants = snia.getConstants().stream().map(c -> snia.getContractor().add(c, rightConstant)).collect(Collectors.toSet());
                        setAtomsForAttribute.add(
                                new SetOrdinalAtom<>(snia.getContractor(), attribute, SetOperator.NOTIN, constants)
                        );
                    }

                    Optional<ConstantOrdinalAtom<T>> cleqaOpt = constantAtomsForRemainingAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.LEQ)).findFirst();

                    if (cleqaOpt.isPresent()) {
                        ConstantOrdinalAtom<T> cleqa = cleqaOpt.get();
                        constantAtomsForAttribute.add(
                                new ConstantOrdinalAtom<>(cleqa.getContractor(), attribute, InequalityOperator.LEQ,cleqa.getContractor().add(cleqa.getConstant(), rightConstant))
                        );
                    }

                    Optional<ConstantOrdinalAtom<T>> cgeqaOpt = constantAtomsForRemainingAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.GEQ)).findFirst();

                    if (cgeqaOpt.isPresent()) {
                        ConstantOrdinalAtom<T> cgeqa = cgeqaOpt.get();
                        constantAtomsForAttribute.add(
                                new ConstantOrdinalAtom<>(cgeqa.getContractor(), attribute, InequalityOperator.GEQ,cgeqa.getContractor().add(cgeqa.getConstant(), rightConstant))
                        );
                    }

                }
            }
        }

        // Apply intersection
        return intersectNonVariableOrdinal(attribute, constantAtomsForAttribute, setAtomsForAttribute);

    }

    /**
     * INTERSECTION OF SET NOMINAL ATOMS AGAINST VARIABLE ATOMS
     **/

    private static Set<SetNominalAtom<?>> reduceSetNominalVariable(Set<SetNominalAtom<?>> reducedSetNominalAtoms, Set<VariableAtom<?, ?, ?>> reducedVariableAtoms) throws AtomException {

        // Get only the set nominal atoms that can be reduced by means of matching variable atoms,
        // i.e. set nominal atoms featuring an attribute that appears in a variable atom
        Set<String> variableAttributes = reducedVariableAtoms.stream().flatMap(VariableAtom::getAttributes).collect(Collectors.toSet());
        Set<SetNominalAtom<?>> remainingSetNominalAtoms = reducedSetNominalAtoms.stream().filter(a -> !variableAttributes.contains(a.getAttribute())).collect(Collectors.toSet());

        // As long as the set of intersected set nominal atoms changes, keep on reducing
        while (true) {

            Set<SetNominalAtom<?>> intersectedSetNominalAtoms = new HashSet<>();

            for (String attribute : variableAttributes) {

                // Get the variable atoms containing given attribute, there should be at least one
                List<VariableAtom<?, ?, ?>> variableAtomsForAttribute = reducedVariableAtoms.stream().filter(va -> va.getAttributes().anyMatch(a -> a.equals(attribute))).collect(Collectors.toList());

                if (variableAtomsForAttribute.get(0) instanceof VariableNominalAtom) {
                    Set<String> remainingAttributes = variableAttributes.stream().filter(a -> !a.equals(attribute)).collect(Collectors.toSet());

                    SetNominalAtom<?> intersectedSetNominalAtom = intersectSetNominalWithVariable(attribute, remainingAttributes, reducedSetNominalAtoms, reducedVariableAtoms);

                    if (intersectedSetNominalAtom != null) {
                        intersectedSetNominalAtoms.add(intersectedSetNominalAtom);
                    }

                }

            }

            if (intersectedSetNominalAtoms.equals(reducedSetNominalAtoms)) {
                break;
            } else {
                reducedSetNominalAtoms = intersectedSetNominalAtoms;
            }

        }

        reducedSetNominalAtoms.addAll(remainingSetNominalAtoms);

        return reducedSetNominalAtoms;

    }

    private static <T extends Comparable<? super T>> SetNominalAtom<T> intersectSetNominalWithVariable(String attribute, Set<String> remainingAttributes, Set<SetNominalAtom<?>> reducedSetNominalAtoms, Set<VariableAtom<?, ?, ?>> reducedVariableAtoms) throws AtomException {

        // Get only the set nominal atoms for the given attribute
        Set<SetNominalAtom<?>> setNominalAtomsForAttribute = reducedSetNominalAtoms.stream().filter(ca -> ca.getAttribute().equals(attribute)).collect(Collectors.toSet());

        // Create a variable atom graph containing the reduced variable atoms
        VariableAtomGraph graph = new VariableAtomGraph(reducedVariableAtoms);

        // Get all paths from the given attribute to each of the remaining attributes
        for (String remainingAttribute: remainingAttributes) {

            Set<SetNominalAtom<T>> setNominalAtomsForRemainingAttribute = reducedSetNominalAtoms
                    .stream()
                    .filter(a -> a.getAttribute().equals(remainingAttribute))
                    .map(a -> (SetNominalAtom<T>) a)
                    .collect(Collectors.toSet());

            List<List<VariableAtomGraph.Edge>> sequences = graph.getAllPaths(attribute, remainingAttribute);

            for (List<VariableAtomGraph.Edge> sequence : sequences) {

                // For each path, get the resulting operator (e.g.: A != B & B = C -> A != C)
                // null is returned when no transitive operator can be derived (e.g. A != B & B != C -> A ? C)
                // in this case, transitive variable atom can not apply for reduction of set atoms
                ComparableOperator transitiveOperator = ComparableOperator.getTransitiveOperatorVariable(sequence.stream().map(VariableAtomGraph.Edge::getOperator).collect(Collectors.toList()));

                // Introduce new set nominal atom for attribute depending on the operator of the transitive variable atom
                if (transitiveOperator != null && transitiveOperator.equals(EqualityOperator.EQ)) {

                    for (SetNominalAtom<T> a : setNominalAtomsForRemainingAttribute) {
                        setNominalAtomsForAttribute.add(new SetNominalAtom<>(a.getContractor(), attribute, a.getOperator(), a.getConstants()));
                    }

                } else if (transitiveOperator != null && transitiveOperator.equals(EqualityOperator.NEQ)) {

                    setNominalAtomsForRemainingAttribute
                            .stream()
                            .filter(a -> a.getOperator().equals(SetOperator.IN) && a.getConstants().size() == 1)
                            .findAny()
                            .ifPresent(
                                    a -> setNominalAtomsForAttribute.add(
                                            new SetNominalAtom<>(a.getContractor(), attribute, SetOperator.NOTIN, a.getConstants())
                                    )
                            );

                }

            }

        }

        return intersectSetNominal(attribute, setNominalAtomsForAttribute);

    }

    /**
     * REDUCTION OF NON-VARIABLE ATOMS AGAINST VARIABLE ATOMS
     **/

    private static <T extends Comparable<? super T>> Set<VariableAtom<?, ?, ?>> removeImpliedVariableAtoms(Set<VariableAtom<?, ?, ?>> reducedVariableAtoms, Set<AbstractAtom<?, ?, ?>> reducedNonVariableOrdinalAtoms, Set<SetNominalAtom<?>> reducedSetNominalAtoms) {

        Iterator<VariableAtom<?, ?, ?>> reducedVariableAtomIterator = reducedVariableAtoms.iterator();

        // For each variable atom, check whether is implied by the other variable atoms or by the set of constant atoms
        while (reducedVariableAtomIterator.hasNext()) {

            VariableAtom<?, ?, ?> impliedVariableAtom = reducedVariableAtomIterator.next();

            if ((variableIsImpliedByVariable(reducedVariableAtoms.stream().filter(va -> !va.equals(impliedVariableAtom)).collect(Collectors.toSet()), impliedVariableAtom))
                    ||  (impliedVariableAtom.getContractor() instanceof OrdinalContractor && variableIsImpliedByNonVariableOrdinal(reducedNonVariableOrdinalAtoms, (VariableOrdinalAtom<T>) impliedVariableAtom))
                    ||  (impliedVariableAtom.getContractor() instanceof NominalContractor && variableIsImpliedBySetNominal(reducedSetNominalAtoms, (VariableNominalAtom<T>) impliedVariableAtom))) {
                reducedVariableAtomIterator.remove();
            }

        }

        return new HashSet<>(reducedVariableAtoms);

    }

    /**
     * CHECK FOR IMPLIED ATOMS
     **/

    private static <T extends Comparable<? super T>> boolean variableIsImpliedByVariable(Set<VariableAtom<?, ?, ?>> variableAtoms, VariableAtom<?, ?, ?> impliedVariableAtom) {

        if (variableAtoms.isEmpty()) {
            return false;
        }

        // Create a variable atom graph for the remaining variable atoms
        VariableAtomGraph graph = new VariableAtomGraph(variableAtoms);

        // Check whether there exists a sequence of variable atoms that implies the variable atom under investigation
        List<List<VariableAtomGraph.Edge>> sequences = graph.getAllPaths(
            impliedVariableAtom.getLeftAttribute(),
            impliedVariableAtom.getRightAttribute());

        for (List<VariableAtomGraph.Edge> sequence : sequences) {

            if (SigmaContractorUtil.isNominalContractor(impliedVariableAtom.getContractor())) {

                ComparableOperator transitiveOperator = ComparableOperator
                    .getTransitiveOperatorVariable(sequence
                        .stream()
                        .map(VariableAtomGraph.Edge::getOperator)
                        .collect(Collectors.toList()));

                if (transitiveOperator != null && transitiveOperator.getImpliedOperators().contains(impliedVariableAtom.getOperator())) {
                    return true;
                }

            } else if (SigmaContractorUtil.isOrdinalContractor(impliedVariableAtom.getContractor()))
            {
                VariableOrdinalAtom<T> impliedVariableOrdinalAtom = (VariableOrdinalAtom<T>) impliedVariableAtom;
                VariableOrdinalAtom<T> transitiveVariableOrdinal = getTransitiveVariableOrdinal(impliedVariableOrdinalAtom.getContractor(), impliedVariableOrdinalAtom.getLeftAttribute(), impliedVariableOrdinalAtom.getRightAttribute(), sequence);

                if (transitiveVariableOrdinal != null) {
                    if (transitiveVariableOrdinal.getOperator().equals(InequalityOperator.LT)
                    && impliedVariableOrdinalAtom.getOperator().equals(InequalityOperator.LT)
                    && transitiveVariableOrdinal.getRightConstant() <= impliedVariableOrdinalAtom.getRightConstant())
                    {
                        return true;
                    }

                    if (transitiveVariableOrdinal.getOperator().equals(InequalityOperator.GT)
                    && impliedVariableOrdinalAtom.getOperator().equals(InequalityOperator.GT)
                    && transitiveVariableOrdinal.getRightConstant() >= impliedVariableOrdinalAtom.getRightConstant())
                    {
                        return true;
                    }

                    if (transitiveVariableOrdinal.getOperator().equals(EqualityOperator.EQ)
                            && impliedVariableOrdinalAtom.getOperator().equals(InequalityOperator.LT)
                            && transitiveVariableOrdinal.getRightConstant() < impliedVariableOrdinalAtom.getRightConstant())
                    {
                        return true;
                    }

                    if (transitiveVariableOrdinal.getOperator().equals(EqualityOperator.EQ)
                            && impliedVariableOrdinalAtom.getOperator().equals(InequalityOperator.GT)
                            && transitiveVariableOrdinal.getRightConstant() > impliedVariableOrdinalAtom.getRightConstant())
                    {
                        return true;
                    }

                    if (transitiveVariableOrdinal.getOperator().equals(EqualityOperator.EQ)
                            && impliedVariableOrdinalAtom.getOperator().equals(EqualityOperator.EQ)
                            && transitiveVariableOrdinal.getRightConstant() == impliedVariableOrdinalAtom.getRightConstant())
                    {
                        return true;
                    }

                }
            }
        }

        return false;

    }

    private static <T extends Comparable<? super T>> boolean variableIsImpliedByNonVariableOrdinal(Set<AbstractAtom<?, ?, ?>> reducedNonVariableOrdinalAtoms, VariableOrdinalAtom<T> impliedVariableAtom) {

        if (reducedNonVariableOrdinalAtoms.isEmpty()) {
            return false;
        }

        Set<SetOrdinalAtom<T>> setInAtomsForLeftAttribute = reducedNonVariableOrdinalAtoms
            .stream()
            .filter(a -> a instanceof SetOrdinalAtom<?>)
            .map(a -> (SetOrdinalAtom<T>) a)
            .filter(a -> a.getAttribute().equals(impliedVariableAtom.getLeftAttribute()) && a.getOperator().equals(SetOperator.IN))
            .collect(Collectors.toSet());

        Set<ConstantOrdinalAtom<T>> constantAtomsForLeftAttribute = reducedNonVariableOrdinalAtoms
                .stream()
                .filter(a -> a instanceof ConstantOrdinalAtom<?>)
                .map(a -> (ConstantOrdinalAtom<T>) a)
                .filter(a -> a.getAttribute().equals(impliedVariableAtom.getLeftAttribute()))
                .collect(Collectors.toSet());

        Set<SetOrdinalAtom<T>> setInAtomsForRightAttribute = reducedNonVariableOrdinalAtoms
                .stream()
                .filter(a -> a instanceof SetOrdinalAtom<?>)
                .map(a -> (SetOrdinalAtom<T>) a)
                .filter(a -> a.getAttribute().equals(impliedVariableAtom.getRightAttribute()) && a.getOperator().equals(SetOperator.IN))
                .collect(Collectors.toSet());

        Set<ConstantOrdinalAtom<T>> constantAtomsForRightAttribute = reducedNonVariableOrdinalAtoms
                .stream()
                .filter(a -> a instanceof ConstantOrdinalAtom<?>)
                .map(a -> (ConstantOrdinalAtom<T>) a)
                .filter(a -> a.getAttribute().equals(impliedVariableAtom.getRightAttribute()))
                .collect(Collectors.toSet());

        if (impliedVariableAtom.getOperator().equals(InequalityOperator.LT))
        {

            T maxLeft = null;
            T minRight = null;

            Optional<SetOrdinalAtom<T>> lsaOpt = setInAtomsForLeftAttribute.stream().findFirst();

            if (lsaOpt.isPresent()) {
                maxLeft = Collections.max(lsaOpt.get().getConstants());
            }

            Optional<ConstantOrdinalAtom<T>> lcaOpt = constantAtomsForLeftAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.LEQ)).findFirst();

            if (lcaOpt.isPresent()) {
                maxLeft = lcaOpt.get().getConstant();
            }

            Optional<SetOrdinalAtom<T>> rsaOpt = setInAtomsForRightAttribute.stream().findFirst();

            if (rsaOpt.isPresent()) {
                minRight = Collections.min(rsaOpt.get().getConstants());
            }

            Optional<ConstantOrdinalAtom<T>> rcaOpt = constantAtomsForRightAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.GEQ)).findFirst();

            if (rcaOpt.isPresent()) {
                minRight = rcaOpt.get().getConstant();
            }

            return maxLeft != null && minRight != null && maxLeft.compareTo(impliedVariableAtom.getContractor().add(minRight, impliedVariableAtom.getRightConstant())) < 0;

        }
        else if (impliedVariableAtom.getOperator().equals(InequalityOperator.GT))
        {

            T minLeft = null;
            T maxRight = null;

            Optional<SetOrdinalAtom<T>> lsaOpt = setInAtomsForLeftAttribute.stream().findFirst();

            if (lsaOpt.isPresent()) {
                minLeft = Collections.min(lsaOpt.get().getConstants());
            }

            Optional<ConstantOrdinalAtom<T>> lcaOpt = constantAtomsForLeftAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.GEQ)).findFirst();

            if (lcaOpt.isPresent()) {
                minLeft = lcaOpt.get().getConstant();
            }

            Optional<SetOrdinalAtom<T>> rsaOpt = setInAtomsForRightAttribute.stream().findFirst();

            if (rsaOpt.isPresent()) {
                maxRight = Collections.max(rsaOpt.get().getConstants());
            }

            Optional<ConstantOrdinalAtom<T>> rcaOpt = constantAtomsForRightAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.LEQ)).findFirst();

            if (rcaOpt.isPresent()) {
                maxRight = rcaOpt.get().getConstant();
            }

            return minLeft != null && maxRight != null && minLeft.compareTo(impliedVariableAtom.getContractor().add(maxRight, impliedVariableAtom.getRightConstant())) > 0;
        }
        else if (impliedVariableAtom.getOperator().equals(EqualityOperator.EQ))
        {

            T minLeft = null;
            T maxLeft = null;
            T minRight = null;
            T maxRight = null;

            Optional<SetOrdinalAtom<T>> lsaOpt = setInAtomsForLeftAttribute.stream().findFirst();

            if (lsaOpt.isPresent()) {
                minLeft = Collections.min(lsaOpt.get().getConstants());
                maxLeft = Collections.max(lsaOpt.get().getConstants());
            }

            Optional<ConstantOrdinalAtom<T>> lcgeqaOpt = constantAtomsForLeftAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.GEQ)).findFirst();

            if (lcgeqaOpt.isPresent()) {
                minLeft = lcgeqaOpt.get().getConstant();
            }

            Optional<ConstantOrdinalAtom<T>> lcleqaOpt = constantAtomsForLeftAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.LEQ)).findFirst();

            if (lcleqaOpt.isPresent()) {
                maxLeft = lcleqaOpt.get().getConstant();
            }

            boolean leftOneValue = minLeft != null && minLeft.equals(maxLeft);

            Optional<SetOrdinalAtom<T>> rsaOpt = setInAtomsForRightAttribute.stream().findFirst();

            if (rsaOpt.isPresent()) {
                minRight = Collections.min(rsaOpt.get().getConstants());
                maxRight = Collections.max(rsaOpt.get().getConstants());
            }

            Optional<ConstantOrdinalAtom<T>> rcleqaOpt = constantAtomsForRightAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.LEQ)).findFirst();

            if (rcleqaOpt.isPresent()) {
                maxRight = rcleqaOpt.get().getConstant();
            }

            Optional<ConstantOrdinalAtom<T>> rcgeqaOpt = constantAtomsForRightAttribute.stream().filter(a -> a.getOperator().equals(InequalityOperator.GEQ)).findFirst();

            if (rcgeqaOpt.isPresent()) {
                maxRight = rcgeqaOpt.get().getConstant();
            }

            boolean rightOneValue = minRight != null && minRight.equals(maxRight);

            return leftOneValue && rightOneValue && minLeft.compareTo(impliedVariableAtom.getContractor().add(maxRight, impliedVariableAtom.getRightConstant())) == 0;
        }

        return false;

    }

    private static <T extends Comparable<? super T>> boolean variableIsImpliedBySetNominal(Set<SetNominalAtom<?>> reducedSetNominalAtoms, VariableNominalAtom<T> impliedVariableAtom) {

        if (reducedSetNominalAtoms.isEmpty()) {
            return false;
        }

        Set<SetNominalAtom<T>> atomsForLeftAttribute = reducedSetNominalAtoms
                .stream()
                .filter(sna -> sna
                        .getAttribute()
                        .equals(impliedVariableAtom.getLeftAttribute()))
                .map(sna -> (SetNominalAtom<T>) sna)
                .collect(Collectors.toSet());

        Set<SetNominalAtom<T>> atomsForRightAttribute = reducedSetNominalAtoms
                .stream()
                .filter(sna -> sna.
                        getAttribute()
                        .equals(impliedVariableAtom.getRightAttribute()))
                .map(sna -> (SetNominalAtom<T>) sna)
                .collect(Collectors.toSet());

        Optional<Set<T>> leftAttributeNotInConstants = atomsForLeftAttribute
                .stream()
                .filter(ca -> ca.getOperator().equals(SetOperator.NOTIN))
                .map(SetNominalAtom::getConstants)
                .findAny();

        Optional<Set<T>> rightAttributeNotInConstants = atomsForRightAttribute
                .stream()
                .filter(ca -> ca.getOperator().equals(SetOperator.NOTIN))
                .map(SetNominalAtom::getConstants)
                .findAny();

        Optional<Set<T>> leftAttributeInConstants = atomsForLeftAttribute
                .stream()
                .filter(ca -> ca.getOperator().equals(SetOperator.IN))
                .map(SetNominalAtom::getConstants)
                .findAny();

        Optional<Set<T>> rightAttributeInConstants = atomsForRightAttribute
                .stream()
                .filter(ca -> ca.getOperator().equals(SetOperator.IN))
                .map(SetNominalAtom::getConstants)
                .findAny();

        if (impliedVariableAtom.getOperator().equals(EqualityOperator.EQ))
        {
            return leftAttributeInConstants.isPresent()
                    && rightAttributeInConstants.isPresent()
                    && leftAttributeInConstants.get().size() == 1
                    && rightAttributeInConstants.get().size() == 1
                    && leftAttributeInConstants.get().stream().findFirst()
                    .get()
                    .compareTo(rightAttributeInConstants.get().stream().findFirst().get()) == 0;
        }
        else
        {
            return (leftAttributeInConstants.isPresent() && rightAttributeNotInConstants.isPresent() && rightAttributeNotInConstants.get().containsAll(leftAttributeInConstants.get())) ||
                    (rightAttributeInConstants.isPresent() && leftAttributeNotInConstants.isPresent() && leftAttributeNotInConstants.get().containsAll(rightAttributeInConstants.get())) ||
                    (leftAttributeInConstants.isPresent() && rightAttributeInConstants.isPresent() && leftAttributeInConstants.get().stream().noneMatch(c -> rightAttributeInConstants.get().contains(c)));

        }

    }

    private static <T extends Comparable<? super T>> boolean nonVariableOrdinalIsImpliedByNonVariableOrdinal(Set<ConstantOrdinalAtom<?>> constantOrdinalAtoms, Set<SetOrdinalAtom<?>> setOrdinalAtoms, AbstractAtom<T, ?, ?> impliedNonVariableOrdinalAtom) {

        if (constantOrdinalAtoms.isEmpty() && setOrdinalAtoms.isEmpty()) {
            return false;
        }

        String attribute = impliedNonVariableOrdinalAtom.getAttributes().findFirst().get();

        Set<SetAtom<?, ?>> setAtoms = new HashSet<>(setOrdinalAtoms);

        Interval<T> interval = getConstantOrdinalInterval(attribute, constantOrdinalAtoms);
        Pair<SetOperator, Set<T>> constants = getSetConstants(attribute, setAtoms);

        if (impliedNonVariableOrdinalAtom instanceof ConstantOrdinalAtom) {
            return constantOrdinalIsImpliedByNonVariableOrdinal(interval, constants, (ConstantOrdinalAtom<T>) impliedNonVariableOrdinalAtom);
        } else {
            return setOrdinalIsImpliedByNonVariableOrdinal(interval, constants, (SetOrdinalAtom<T>) impliedNonVariableOrdinalAtom);
        }

    }

    private static <T extends Comparable<? super T>> boolean constantOrdinalIsImpliedByNonVariableOrdinal(Interval<T> interval, Pair<SetOperator, Set<T>> constants, ConstantOrdinalAtom<T> impliedConstantOrdinalAtom) {

        T gteqImpliedConstant = impliedConstantOrdinalAtom
                .getOperator()
                .equals(InequalityOperator.GEQ)
                ? impliedConstantOrdinalAtom.getConstant()
                : null;

        T lteqImpliedConstant = impliedConstantOrdinalAtom
                .getOperator()
                .equals(InequalityOperator.LEQ)
                ? impliedConstantOrdinalAtom.getConstant()
                : null;

        Interval<T> impliedInterval = new Interval<>(
                gteqImpliedConstant,
                lteqImpliedConstant,
                gteqImpliedConstant == null,
                lteqImpliedConstant == null);

        boolean coaImplication = impliedInterval.allenEquals(interval)
                || impliedInterval.allenStartedBy(interval)
                || impliedInterval.allenFinishedBy(interval)
                || impliedInterval.allenContains(interval);

        if (constants != null && constants.getFirst().equals(SetOperator.IN)) {
            return constants.getSecond().stream().allMatch(impliedInterval::contains);
        }

        return coaImplication;

    }

    private static <T extends Comparable<? super T>> boolean setOrdinalIsImpliedByNonVariableOrdinal(Interval<T> interval, Pair<SetOperator, Set<T>> operatorConstants, SetOrdinalAtom<T> impliedSetOrdinalAtom) {

        SetOperator impliedOperator = impliedSetOrdinalAtom.getOperator();
        Set<T> impliedConstants = impliedSetOrdinalAtom.getConstants();

        if (interval.getLeftBound() == null && interval.getRightBound() == null) {

            if (operatorConstants == null) {
                return false;
            }

            SetOperator operator = operatorConstants.getFirst();
            Set<T> constants = operatorConstants.getSecond();

            if (operator.equals(SetOperator.IN)) {
                return (impliedOperator.equals(SetOperator.IN) && impliedConstants.containsAll(constants))
                        || (impliedOperator.equals(SetOperator.NOTIN) && impliedConstants.stream().noneMatch(constants::contains));
            } else {
                return (impliedOperator.equals(SetOperator.NOTIN) && constants.containsAll(impliedConstants));
            }

        } else {

            if (impliedOperator.equals(SetOperator.IN)) {

                T x = interval.getLeftBound();

                if (x == null || !impliedConstants.contains(x) || interval.getRightBound() == null) {
                    return false;
                }

                OrdinalContractor<T> contractor = impliedSetOrdinalAtom.getContractor();

                List<T> sortedC = impliedConstants.stream().sorted().collect(Collectors.toList());

                while (
                        (sortedC.contains(contractor.next(x)) &&
                                (contractor.next(x).compareTo(interval.getRightBound()) <= 0)
                        ) || (operatorConstants != null &&
                                operatorConstants.getFirst().equals(SetOperator.NOTIN) &&
                                operatorConstants.getSecond().contains(x)
                        )
                ) {
                    x = contractor.next(x);
                }

                return x.compareTo(interval.getRightBound()) == 0;

            } else {
                return impliedConstants.stream()
                        .allMatch(c ->
                                !interval.contains(c) ||
                                        (operatorConstants != null &&
                                                operatorConstants.getFirst().equals(SetOperator.NOTIN) &&
                                                operatorConstants.getSecond().contains(c))
                        );
            }

        }
    }

    private static <T extends Comparable<? super T>> boolean setNominalIsImpliedBySetNominal(Set<SetNominalAtom<?>> reducedSetNominalAtoms, SetNominalAtom<T> impliedSetNominalAtom) {

        if (reducedSetNominalAtoms.isEmpty()) {
            return false;
        }

        String attribute = impliedSetNominalAtom.getAttribute();

        Set<SetNominalAtom<T>> atomsForAttribute = reducedSetNominalAtoms
                .stream()
                .filter(sna -> sna.getAttribute().equals(attribute))
                .map(sna -> (SetNominalAtom<T>) sna)
                .collect(Collectors.toSet());

        Optional<Set<T>> notInConstants = atomsForAttribute
                .stream()
                .filter(ca -> ca.getOperator().equals(SetOperator.NOTIN))
                .map(SetNominalAtom::getConstants)
                .findAny();

        Set<T> impliedNotInConstants = impliedSetNominalAtom
                .getOperator()
                .equals(SetOperator.NOTIN)
                ? impliedSetNominalAtom.getConstants()
                : null;

        Optional<Set<T>> inConstants = atomsForAttribute
                .stream()
                .filter(ca -> ca.getOperator().equals(SetOperator.IN))
                .map(SetNominalAtom::getConstants)
                .findAny();

        Set<T> impliedInConstants = impliedSetNominalAtom
                .getOperator()
                .equals(SetOperator.IN)
                ? impliedSetNominalAtom.getConstants()
                : null;

        if (impliedNotInConstants != null) {
            return notInConstants.isPresent() && notInConstants.get().containsAll(impliedNotInConstants) ||
                    inConstants.isPresent() && inConstants.get().stream().noneMatch(impliedNotInConstants::contains);
        } else if (impliedInConstants != null) {
            return inConstants.isPresent() && impliedInConstants.containsAll(inConstants.get());
        }

        return false;

    }

}
