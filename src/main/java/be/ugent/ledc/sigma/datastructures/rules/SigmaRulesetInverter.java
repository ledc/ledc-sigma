package be.ugent.ledc.sigma.datastructures.rules;

import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.formulas.CPFImplicator;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SigmaRulesetInverter {

    public static Set<CPF> invert(SigmaRuleset ruleset) {

        Set<CPF> result = new HashSet<>(Collections.singleton(new CPF(AbstractAtom.ALWAYS_TRUE)));

        Set<SigmaRule> sigmaRules = ruleset.getRules();

        for (SigmaRule sigmaRule : sigmaRules) {
            result = processSigmaRule(sigmaRule, result);
            result = removeRedundancy(result);
        }

        return result;

    }

    // TODO: method to add rule to existing inverted set
    // TODO: partition non-overlapping rules

    private static Set<CPF> processSigmaRule(SigmaRule sigmaRule, Set<CPF> CPFs) {

        Set<CPF> extendedCPFs = new HashSet<>();

        Set<AbstractAtom<?, ?, ?>> atoms = sigmaRule.getAtoms();

        for (AbstractAtom<?, ?, ?> atom : atoms) {

            AbstractAtom<?, ?, ?> inverseAtom = atom.getInverse();

            for (CPF cpf : CPFs) {

                Set<AbstractAtom<?, ?, ?>> cpfAtoms = new HashSet<>(cpf.getAtoms());
                cpfAtoms.add(inverseAtom);

                CPF extendedCPF = CPFImplicator.imply(new CPF(cpfAtoms));
                extendedCPFs.add(extendedCPF);

            }

        }

        return extendedCPFs;

    }

    private static Set<CPF> removeRedundancy(Set<CPF> CPFs) {

        return CPFs
                .stream()
                .filter(cpf1 -> CPFs.stream().noneMatch(cpf2 -> !cpf1.equals(cpf2) && cpf1.implies(cpf2)))
                .collect(Collectors.toSet());

    }

}
