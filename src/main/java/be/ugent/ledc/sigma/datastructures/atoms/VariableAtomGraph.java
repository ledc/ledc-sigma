package be.ugent.ledc.sigma.datastructures.atoms;

import be.ugent.ledc.sigma.datastructures.operators.ComparableOperator;

import java.util.*;
import java.util.stream.Collectors;

public class VariableAtomGraph {

    private final Map<String, List<Edge>> adjacencyList;

    public VariableAtomGraph(Collection<VariableAtom<?, ?, ?>> variableAtoms) {

        adjacencyList = new HashMap<>();

        Set<String> attributes = variableAtoms.stream().flatMap(VariableAtom::getAttributes).collect(Collectors.toSet());

        for (String attribute : attributes) {
            adjacencyList.put(attribute, new LinkedList<>());
        }

        for (VariableAtom<?, ?, ?> variableAtom : variableAtoms) {
            addEdge(variableAtom);
        }

    }

    private void addEdge(VariableAtom<?, ?, ?> variableAtom) {
        if (variableAtom instanceof VariableNominalAtom) {
            adjacencyList.get(variableAtom.getLeftAttribute()).add(new Edge(variableAtom.getLeftAttribute(), variableAtom.getRightAttribute(), variableAtom.getOperator(), null));
            adjacencyList.get(variableAtom.getRightAttribute()).add(new Edge(variableAtom.getRightAttribute(), variableAtom.getLeftAttribute(), variableAtom.getOperator().getReversedOperator(), null));
        } else if (variableAtom instanceof VariableOrdinalAtom<?> variableCountableAtom) {
            adjacencyList.get(variableCountableAtom.getLeftAttribute()).add(new Edge(variableCountableAtom.getLeftAttribute(), variableCountableAtom.getRightAttribute(), variableCountableAtom.getOperator(), variableCountableAtom.getRightConstant()));
            adjacencyList.get(variableCountableAtom.getRightAttribute()).add(new Edge(variableCountableAtom.getRightAttribute(), variableCountableAtom.getLeftAttribute(), variableCountableAtom.getOperator().getReversedOperator(), variableCountableAtom.getRightConstant() * -1));
        }
    }

    public List<List<Edge>> getAllPaths(String source, String destination) {

        if (!(adjacencyList.containsKey(source) && adjacencyList.containsKey(destination))) {
            return new ArrayList<>();
        }

        List<List<Edge>> allPaths = new ArrayList<>();
        depthFirstSearch(source, destination, null, new HashSet<>(), new ArrayList<>(), allPaths);
        return allPaths;

    }

    private void depthFirstSearch(String originalSource, String originalDestination, Edge currentEdge, Set<String> visited, List<Edge> currentPath, List<List<Edge>> allPaths) {

        String source;

        if (currentEdge == null) {
            source = originalSource;
        } else {
            source = currentEdge.getDestination();
        }

        if (visited.contains(source)) {
            return;
        }

        visited.add(source);

        if (currentEdge != null) {
            currentPath.add(currentEdge);
        }

        if (source.equals(originalDestination)) {
            allPaths.add(new ArrayList<>(currentPath));
            visited.remove(source);
            currentPath.remove(currentPath.size() - 1);
            return;
        }

        for (Edge nextEdge : adjacencyList.get(source)) {
            depthFirstSearch(originalSource, originalDestination, nextEdge, visited, currentPath, allPaths);
        }

        if (currentEdge != null) {
            currentPath.remove(currentPath.size() - 1);
            visited.remove(source);
        }

    }

    public static class Edge {

        private final String source;
        private final String destination;
        private final ComparableOperator operator;
        private final Integer constant;

        public Edge(String source, String destination, ComparableOperator operator, Integer constant) {
            this.source = source;
            this.destination = destination;
            this.operator = operator;
            this.constant = constant;
        }

        public String getSource() {
            return source;
        }

        public String getDestination() {
            return destination;
        }

        public ComparableOperator getOperator() {
            return operator;
        }

        public int getConstant() { return constant; }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Edge edge = (Edge) o;
            return Objects.equals(source, edge.source) && Objects.equals(destination, edge.destination) && Objects.equals(operator, edge.operator) && Objects.equals(constant, edge.constant);
        }

        @Override
        public int hashCode() {
            int result = Objects.hashCode(source);
            result = 31 * result + Objects.hashCode(destination);
            result = 31 * result + Objects.hashCode(operator);
            result = 31 * result + Objects.hashCode(constant);
            return result;
        }

        @Override
        public String toString() {

            if (constant == null || constant == 0) {
                return source + " " + operator.getSymbol() + " " + destination;
            } else if (constant > 0) {
                return source + " " + operator.getSymbol() + " " + destination + " + " + constant;
            } else {
                return source + " " + operator.getSymbol() + " " + destination + " - " + constant * -1;
            }
        }
    }

}
