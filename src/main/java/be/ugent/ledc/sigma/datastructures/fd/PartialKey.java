package be.ugent.ledc.sigma.datastructures.fd;

import be.ugent.ledc.fundy.datastructures.FD;
import java.util.Set;

public class PartialKey extends FD
{
    public PartialKey(Set<String> key, Set<String> determined)
    {
        super(key, determined);
    }

    public Set<String> getKey()
    {
        return super.getLeftHandSide();
    }

    public Set<String> getDetermined()
    {
        return super.getRightHandSide();
    }
}
