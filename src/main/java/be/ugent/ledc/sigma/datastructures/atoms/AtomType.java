package be.ugent.ledc.sigma.datastructures.atoms;

public enum AtomType
{
    DUMMY,
    CONSTANT,
    SET,
    VARIABLE
}
