package be.ugent.ledc.sigma.datastructures.values.attributevalues;

import java.util.Map;
import java.util.Objects;

/**
 * An AttributeValueMapping object is an object in which attributes are mapped to their corresponding ValueIterator object.
 * Note: such an object can represent a set of DataObject objects, a set of CPFs...
 *
 * @author Toon Boeckling
 * @param <V> Type of ValueIterator
 */
public abstract class AttributeValueMapping<V extends ValueIterator<?>> {

    public Map<String, V> mapping;

    /**
     * Create a new AttributeValueMapping object.
     *
     * @param mapping Map of attributes to their corresponding ValueIterator object
     */
    public AttributeValueMapping(Map<String, V> mapping) {
        this.mapping = mapping;
    }

    /**
     * Get the map of attributes to their corresponding ValueIterator object.
     *
     * @return map of attributes to their corresponding ValueIterator object
     */
    public Map<String, V> getMapping() {
        return this.mapping;
    }

    /**
     * Verifies whether given attribute exists in the map.
     *
     * @param attribute Attribute for which it is verified whether it exists in the map
     * @return true if given attribute exists in the map, false otherwise
     */
    public boolean hasMappingForAttribute(String attribute) { return this.mapping.containsKey(attribute); }

    /**
     * Get the corresponding ValueIterator object of the given attribute.
     *
     * @param attribute attribute for which to retrieve the corresponding ValueIterator object
     * @return corresponding ValueIterator object of the given attribute, null if attribute is not in the map
     */
    public V getMappingForAttribute(String attribute) {
        return this.mapping.get(attribute);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AttributeValueMapping<?> that = (AttributeValueMapping<?>) o;
        return Objects.equals(mapping, that.mapping);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mapping);
    }

    @Override
    public String toString() {
        return "AttributeValueMapping{" +
                "mapping=" + mapping +
                '}';
    }
}
