package be.ugent.ledc.sigma.datastructures.values.attributevalues;

import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.values.NominalValueIterator;

import java.util.*;

/**
 * This class represents an AttributeValueMapping where the ValueIterator objects are NominalValueIterator objects and
 * correspond with a enumerations of values.
 *
 * @author Toon Boeckling
 */
public class EnumerationMapping extends AttributeValueMapping<NominalValueIterator<?>> {

    public EnumerationMapping(Map<String, NominalValueIterator<?>> mapping) {
        super(mapping);
    }

    /**
     * Inner class that can be used to transform an EnumerationMapping object to another kind of object
     * (e.g., a set of CPFs).
     */
    public static class Transformer {

        /**
         * Transform the given EnumerationMapping object to a CPF object that represent this object,
         * based on the given contractors.
         * Note that the CPF can only feature SetAtom objects with the IN operator.
         *
         * @param eMapping EnumerationMapping object to transform to a CPF object
         * @param contractors Contractors that are used to build the CPF object
         * @return a CPF object that represent the given EnumerationMapping object
         */
        public static CPF transformToCPF(EnumerationMapping eMapping, Map<String, SigmaContractor<?>> contractors) {

            Set<AbstractAtom<?, ?, ?>> atoms = new HashSet<>();

            for (String attribute : eMapping.getMapping().keySet()) {

                if (!contractors.containsKey(attribute)) {
                    throw new TransformerException("No contractor in set of contractors " + contractors + " exists for " +
                            "attribute " + attribute + ". Therefore, it is not possible to construct a CPF that involves " +
                            "this attribute.");
                }

                SetAtom<?, ? extends SigmaContractor<?>> atom;

                if (contractors.get(attribute) instanceof NominalContractor) {
                    atom = new SetNominalAtom((NominalContractor<?>) contractors.get(attribute), attribute, SetOperator.IN, eMapping.getMappingForAttribute(attribute).getValues());
                } else {
                    atom = new SetOrdinalAtom((OrdinalContractor<?>) contractors.get(attribute), attribute, SetOperator.IN, eMapping.getMappingForAttribute(attribute).getValues());
                }

                atoms.add(atom);
            }

            return new CPF(atoms);

        }

        /**
         * Transform the given set of SetAtom objects to an EnumerationMapping object.
         * Note that, for simplicity reasons, only SetAtoms that feature the IN operator are allowed here.
         * Besides that, a set of SetAtom objects can contain at most one SetAtom with a certain attribute.
         *
         * @param atomset set of SetAtom objects to transform to an EnumerationMapping object

         * @return EnumerationMapping object that represent the given set of SetAtom objects
         */
        public static EnumerationMapping transformFromAtomset(Set<SetAtom<?, ?>> atomset) {

            if (atomset.stream().anyMatch(a -> !a.getOperator().equals(SetOperator.IN))) {
                throw new TransformerException("Given set of SetAtom objects " + atomset + " contains a SetAtom object featuring a NOTIN operator.");
            }

            Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

            for (SetAtom<?, ?> atom : atomset) {

                NominalValueIterator<?> result = mapping.putIfAbsent(atom.getAttribute(), new NominalValueIterator<>(atom.getConstants()));

                if (result != null) {
                    throw new TransformerException("Given set of SetAtom objects contains multiple SetAtom objects with attribute " + atom.getAttribute() + ".");
                }

            }

            return new EnumerationMapping(mapping);

        }

    }

}
