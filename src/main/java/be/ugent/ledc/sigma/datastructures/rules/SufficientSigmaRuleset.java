package be.ugent.ledc.sigma.datastructures.rules;

import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class SufficientSigmaRuleset extends SigmaRuleset {
    private SufficientSigmaRuleset(Map<String, SigmaContractor<?>> contractors, Set<SigmaRule> sigmaRules) {
        super(contractors, sigmaRules);
    }

    public static SufficientSigmaRuleset create(SigmaRuleset ruleset, FCFGenerator generator) {
        return new SufficientSigmaRuleset(
                ruleset.getContractors(),
                generator.generateSufficientSet(ruleset)
        );
    }

    public static SufficientSigmaRuleset create(SigmaRuleset ruleset) {
        return new SufficientSigmaRuleset(
                ruleset.getContractors(),
                ruleset.getRules());
    }

    @Override
    public SufficientSigmaRuleset project(Set<String> attributes) {
        return new SufficientSigmaRuleset(
                getContractors() //Project the contractors
                        .entrySet()
                        .stream()
                        .filter(e -> attributes.contains(e.getKey()))
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue)),
                getRules() //Project the rules
                        .stream()
                        .filter(rule -> attributes.containsAll(rule.getInvolvedAttributes()))
                        .collect(Collectors.toSet()));
    }
    
    public SufficientSigmaRuleset merge(SufficientSigmaRuleset other) {

        Map<String, SigmaContractor<?>> contractors =
                Stream.of(this.getContractors(), other.getContractors())
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (v1, v2) -> v1));

        Set<SigmaRule> sigmaRules = new HashSet<>(this.getRules());
        sigmaRules.addAll(other.getRules());

        return new SufficientSigmaRuleset(contractors, sigmaRules);

    }
}
