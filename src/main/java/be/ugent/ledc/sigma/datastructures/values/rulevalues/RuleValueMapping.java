package be.ugent.ledc.sigma.datastructures.values.rulevalues;

import be.ugent.ledc.sigma.datastructures.atoms.AtomException;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;

public abstract class RuleValueMapping<V> {

    private final String attribute;
    private final SigmaRule rule;
    private final V coveredAttributeValues;

    public RuleValueMapping(String attribute, SigmaRule rule) {

        if (rule.getAtoms()
                .stream()
                .filter(a -> a.getAttributes().anyMatch(at -> at.equals(attribute)))
                .anyMatch(a -> !a.isSimplified())) {
            throw new AtomException("All atoms with involved attribute " + attribute + " in rule " +  rule + " should be simplified for being used in a RuleValueMapping object");
        }

        this.attribute = attribute;
        this.rule = rule;
        this.coveredAttributeValues = calculateCoveredAttributeValues();
    }

    public String getAttribute() {
        return this.attribute;
    }

    public SigmaRule getRule() {
        return this.rule;
    }

    public V getCoveredAttributeValues() {
        return this.coveredAttributeValues;
    }

    protected abstract V calculateCoveredAttributeValues();

}
