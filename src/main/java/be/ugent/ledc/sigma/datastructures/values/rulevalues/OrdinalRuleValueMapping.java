package be.ugent.ledc.sigma.datastructures.values.rulevalues;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;

import java.util.*;
import java.util.stream.Collectors;

public class OrdinalRuleValueMapping<T extends Comparable<? super T>> extends RuleValueMapping<Set<Interval<T>>> {

    public OrdinalRuleValueMapping(String attribute, SigmaRule rule) {
        super(attribute, rule);
    }

    @Override
    protected Set<Interval<T>> calculateCoveredAttributeValues() {

        Set<Interval<T>> coveredValues = new HashSet<>();

        Set<ConstantOrdinalAtom<T>> constantAttributeAtoms = getRule().getConstantOrdinalAtoms().stream().map(r -> (ConstantOrdinalAtom<T>) r).filter(a -> a.getAttribute().equals(getAttribute())).collect(Collectors.toSet());
        Optional<SetOrdinalAtom<T>> setInAttributeAtomOpt = getRule().getSetOrdinalAtoms().stream().map(r -> (SetOrdinalAtom<T>) r).filter(a -> a.getAttribute().equals(getAttribute()) && a.getOperator().equals(SetOperator.IN)).findFirst();
        Optional<SetOrdinalAtom<T>> setNotInAttributeAtomOpt = getRule().getSetOrdinalAtoms().stream().map(r -> (SetOrdinalAtom<T>) r).filter(a -> a.getAttribute().equals(getAttribute()) && a.getOperator().equals(SetOperator.NOTIN)).findFirst();

        if (setInAttributeAtomOpt.isEmpty()) {

            Interval<T> interval = new Interval<>();

            if (!constantAttributeAtoms.isEmpty()) {

                Optional<T> lowerBound = constantAttributeAtoms.stream().filter(coa -> coa.getOperator().equals(InequalityOperator.GEQ)).map(ConstantAtom::getConstant).findAny();
                Optional<T> upperBound = constantAttributeAtoms.stream().filter(coa -> coa.getOperator().equals(InequalityOperator.LEQ)).map(ConstantAtom::getConstant).findAny();

                if (lowerBound.isPresent()) {
                    interval.setLeftBound(lowerBound.get());
                    interval.setLeftOpen(false);
                }

                if (upperBound.isPresent()) {
                    interval.setRightBound(upperBound.get());
                    interval.setRightOpen(false);
                }

            }

            if (setNotInAttributeAtomOpt.isPresent()) {

                SetOrdinalAtom<T> setNotInAttributeAtom = setNotInAttributeAtomOpt.get();
                OrdinalContractor<T> contractor = setNotInAttributeAtom.getContractor();
                List<T> notInConstants = setNotInAttributeAtom.getConstants().stream().sorted().collect(Collectors.toList());

                for (int i = 0; i < notInConstants.size(); i++) {

                    if (i == 0 && (interval.getLeftBound() == null || contractor.previous(notInConstants.get(i)).compareTo(interval.getLeftBound()) >= 0)) {
                        coveredValues.add(new Interval<>(interval.getLeftBound(), contractor.previous(notInConstants.get(i)), interval.getLeftBound() == null, false));
                    }

                    if (i > 0 & i <= notInConstants.size() - 1 && contractor.previous(notInConstants.get(i)).compareTo(contractor.next(notInConstants.get(i-1))) >= 0) {
                        coveredValues.add(new Interval<>(contractor.next(notInConstants.get(i-1)), contractor.previous(notInConstants.get(i)), false, false));
                    }

                    if (i == notInConstants.size() - 1 && (interval.getRightBound() == null || interval.getRightBound().compareTo(contractor.next(notInConstants.get(i))) >= 0)) {
                        coveredValues.add(new Interval<>(contractor.next(notInConstants.get(i)), interval.getRightBound(), false, interval.getRightBound() == null));
                    }

                }

                return coveredValues;

            }

            return Collections.singleton(interval);

        } else {

            List<T> inConstants = setInAttributeAtomOpt.get().getConstants().stream().sorted().collect(Collectors.toList());
            OrdinalContractor<T> contractor = setInAttributeAtomOpt.get().getContractor();

            for (int i = 0; i < inConstants.size(); i++) {

                T currentLeftBound = inConstants.get(i);
                T currentInConstant = inConstants.get(i);

                while (contractor.hasNext(currentInConstant) && inConstants.contains(contractor.next(currentInConstant)) && i < inConstants.size()) {
                    i++;
                    currentInConstant = inConstants.get(i);
                }

                coveredValues.add(new Interval<>(currentLeftBound, currentInConstant, false, false));

            }

            return coveredValues;
        }

    }

}
