package be.ugent.ledc.sigma.datastructures.values.rulevalues;

import java.util.Set;

public class NominalRuleValues<T> {

    private final Set<T> coveredValues;
    private final Set<T> uncoveredValues;

    public NominalRuleValues(Set<T> coveredValues, Set<T> uncoveredValues) {
        this.coveredValues = coveredValues;
        this.uncoveredValues = uncoveredValues;
    }

    public Set<T> getCoveredValues() {
        return this.coveredValues;
    }

    public Set<T> getUncoveredValues() {
        return this.uncoveredValues;
    }

    public boolean hasCoveredValues() {
        return this.coveredValues != null;
    }

    public boolean hasUncoveredValues() {
        return this.uncoveredValues != null;
    }


}
