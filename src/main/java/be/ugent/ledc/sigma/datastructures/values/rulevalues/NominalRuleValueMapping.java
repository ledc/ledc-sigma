package be.ugent.ledc.sigma.datastructures.values.rulevalues;

import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleException;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.FullRuleImplicator;

import java.util.Optional;

public class NominalRuleValueMapping<T extends Comparable<? super T>> extends RuleValueMapping<NominalRuleValues<T>> {

    public NominalRuleValueMapping(String attribute, SigmaRule rule) {
        super(attribute, rule);
    }

    @Override
    protected NominalRuleValues<T> calculateCoveredAttributeValues() {

        Optional<SetNominalAtom<T>> setAttributeAtomsOpt = getRule().getSetNominalAtoms().stream().map(r -> (SetNominalAtom<T>) r).filter(a -> a.getAttribute().equals(getAttribute())).findFirst();

        if (setAttributeAtomsOpt.isPresent()) {

            SetNominalAtom<T> setAttributeAtom = setAttributeAtomsOpt.get();

            if (setAttributeAtom.getOperator().equals(SetOperator.IN)) {
                return new NominalRuleValues<>(setAttributeAtom.getConstants(), null);
            } else {
                return new NominalRuleValues<>(null, setAttributeAtom.getConstants());
            }

        }

        throw new SigmaRuleException("Attribute " + getAttribute() + " is not involved in a set nominal atom in sigma rule " + getRule());

    }

}
