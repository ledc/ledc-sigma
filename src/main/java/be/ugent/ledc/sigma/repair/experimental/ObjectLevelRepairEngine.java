package be.ugent.ledc.sigma.repair.experimental;

import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.CostModel;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;

import java.util.Set;

public abstract class ObjectLevelRepairEngine<M extends CostModel<?>> extends CPFRepairEngine<M> {

    private final CPFRepairSelection repairSelection;

    public ObjectLevelRepairEngine(SigmaRuleset sigmaRuleset, M costModel, NullBehavior nullBehavior, CPFRepairSelection repairSelection) throws RepairException {
        super(sigmaRuleset, costModel, nullBehavior);
        this.repairSelection = repairSelection;
    }

    public ObjectLevelRepairEngine(SigmaRuleset sigmaRuleset, M costModel, CPFRepairSelection repairSelection) throws RepairException {
        super(sigmaRuleset, costModel);
        this.repairSelection = repairSelection;
    }

    public abstract Set<CPF> getMinCostChangeExpressions(DataObject dataObject) throws RepairException;

    @Override
    protected Dataset buildRepair(Dataset dataset) throws RepairException {

        Dataset repairedDataset = new SimpleDataset();

        for(DataObject dataObject: dataset.getDataObjects())
        {
            if(needsRepair(dataObject))
                repairedDataset.addDataObject(repair(dataObject));
            else
                repairedDataset.addDataObject(dataObject);
        }

        return repairedDataset;

    }

    private DataObject repair(DataObject dataObject) throws RepairException {

        if(VERBOSITY > 0)
            System.out.println("Repairing " + dataObject);

        Set<CPF> minCostChangeExpressions = getMinCostChangeExpressions(dataObject);

        return repairSelection.selectRepair(minCostChangeExpressions, dataObject);

        // TODO: sample repair values from minCostChangeExpressions, impute in data object and return repaired data object
        // Take into account null behavior

    }

}
