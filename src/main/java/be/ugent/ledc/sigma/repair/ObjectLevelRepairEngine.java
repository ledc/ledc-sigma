package be.ugent.ledc.sigma.repair;

import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.CostModel;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;

/**
 * A RepairEngine that works on a row-per-row basis. For these types of repair engines,
 * the strategy is basically to check, for each DataObject, it it needs repair, accounting for
 * the behaviour towards null values. If a DataObject needs repair, we apply a function
 * to that object.
 * @param <M> The type of cost model used by this repair engine
 */
public abstract class ObjectLevelRepairEngine<M extends CostModel<?>> extends RepairEngine<M>
{
    public ObjectLevelRepairEngine(SufficientSigmaRuleset rules, M costModel, NullBehavior nullBehavior)
    {
        super(rules, costModel, nullBehavior);
    }

    public ObjectLevelRepairEngine(SufficientSigmaRuleset rules, M costModel)
    {
        super(rules, costModel);
    }

    @Override
    public Dataset buildRepair(Dataset dataset) throws RepairException
    {
        Dataset repairedDataset = new SimpleDataset();

        for(DataObject object: dataset.getDataObjects())
        {
            if(needsRepair(object))
                repairedDataset.addDataObject(repair(object));
            else
                repairedDataset.addDataObject(object);
        }

        return repairedDataset;
    }

    public abstract DataObject repair(DataObject dirty) throws RepairException;
}
