package be.ugent.ledc.sigma.repair.experimental;

import be.ugent.ledc.core.cost.CostModel;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetInverter;

import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public abstract class CPFRepairEngine<M extends CostModel<?>> {

    private final Map<String, SigmaContractor<?>> contractors;
    private final Set<CPF> cpfs;
    private final M costModel;
    private final NullBehavior nullBehavior;

    public static int VERBOSITY = 0;

    public CPFRepairEngine(SigmaRuleset sigmaRuleset, M costModel, NullBehavior nullBehavior) throws RepairException {
        this.contractors = sigmaRuleset.getContractors();
        this.cpfs = SigmaRulesetInverter.invert(sigmaRuleset);

        if (this.cpfs.stream().allMatch(CPF::isContradiction)) {
            throw new RepairException("Set of sigma rules " + sigmaRuleset.getRules() + " is unsatisfiable.");
        }

        this.costModel = costModel;
        this.nullBehavior = nullBehavior;
    }

    public CPFRepairEngine(SigmaRuleset sigmaRuleset, M costModel) throws RepairException {
        this(sigmaRuleset, costModel, NullBehavior.CONSTRAINED_REPAIR);
    }

    public  Map<String, SigmaContractor<?>> getContractors() {
        return contractors;
    }

    public  SigmaContractor<?> getContractor(String attribute) {
        return contractors.get(attribute);
    }

    public Set<CPF> getCPFs() {
        return cpfs;
    }

    public M getCostModel() {
        return costModel;
    }

    public NullBehavior getNullBehavior() {
        return nullBehavior;
    }

    public Dataset repair(Dataset dataset) throws RepairException {
        applyRuleContracts(dataset);
        return buildRepair(dataset);
    }

    protected boolean needsRepair(DataObject dataObject) {

        Set<String> nonNullAttributes = dataObject
                .getAttributes()
                .stream()
                .filter(a -> dataObject.get(a) != null)
                .collect(Collectors.toSet());

        if(cpfs.stream().noneMatch(cpf -> cpf.project(nonNullAttributes).isSatisfied(dataObject)))
            return true;

        return dataObject
                .getAttributes()
                .stream()
                .anyMatch(a -> dataObject.get(a) == null &&
                        getNullBehavior().repairIfNull(cpfs, a));

    }

    private <T extends Comparable<? super T>> void applyRuleContracts(Dataset dataset) {

        Set<String> ruleAttributes = contractors.keySet();

        for (String attribute : ruleAttributes) {
            SigmaContractor<T> ctr = (SigmaContractor<T>) getContractor(attribute);

            for (DataObject dataObject : dataset) {
                dataObject.set(attribute, ctr.get((T) dataObject.get(attribute)));
            }
        }

    }

    protected abstract Dataset buildRepair(Dataset dataset) throws RepairException;

}
