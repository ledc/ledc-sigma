package be.ugent.ledc.sigma.repair.experimental;


import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.atoms.AtomOperations;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class CPFRandomRepairSelection implements CPFRepairSelection {

    @Override
    public <T extends Comparable<? super T>> DataObject selectRepair(Set<CPF> minCostChangeExpressions, DataObject originalObject) {

        CPF selectedCPF = selectRandomCPF(minCostChangeExpressions);

        Set<String> repairAttributes = selectedCPF.getAttributes();
        Set<String> otherAttributes = originalObject.getAttributes().stream().filter(at -> !repairAttributes.contains(at)).collect(Collectors.toSet());

        DataObject repairObject = originalObject.project(otherAttributes);

        for (String attribute : repairAttributes) {
            T value = selectRandomValue(selectedCPF, attribute);
            repairObject.set(attribute, value);
        }

        return repairObject;

    }

    private CPF selectRandomCPF(Set<CPF> cpfs) {

        if (cpfs == null || cpfs.isEmpty()) {
            throw new IllegalArgumentException("The set of CPFs cannot be empty.");
        }

        List<CPF> cpfsList = new ArrayList<>(cpfs);

        int randomIndex = new Random().nextInt(cpfsList.size());
        return cpfsList.get(randomIndex);

    }

    private <T extends Comparable<? super T>> T selectRandomValue(CPF cpf, String attribute) {

        if (cpf.getContractor(attribute) instanceof OrdinalContractor) {

            OrdinalContractor<T> contractor = (OrdinalContractor<T>) cpf.getContractor(attribute);

            Set<Interval<T>> intervals = AtomOperations.getIntervalsFromOrdinalAtoms(cpf.getAtoms(), attribute);

            if (intervals == null || intervals.isEmpty()) {
                throw new IllegalArgumentException("The set of intervals " + intervals + " cannot be empty in order to select a random value from it.");
            }

            List<Interval<T>> intervalsList = new ArrayList<>(intervals);

            int randomIntervalIndex = new Random().nextInt(intervalsList.size());
            Interval<T> selectedInterval = intervalsList.get(randomIntervalIndex);

            long randomValueIndex = new Random().nextLong(contractor.cardinality(selectedInterval));

            boolean leftOpen = selectedInterval.isLeftOpen();

            T selectedValue = leftOpen ? contractor.next(selectedInterval.getLeftBound()) : selectedInterval.getLeftBound();

            int i = 0;

            while (i != randomValueIndex) {
                selectedValue = contractor.next(selectedValue);
                i++;
            }

            return selectedValue;

        } else {

            Set<T> valueset = AtomOperations.getValuesetFromNominalAtoms(cpf.getAtoms(), attribute);

            if (valueset.isEmpty()) {
                throw new IllegalArgumentException("The value set " + valueset + " cannot be empty in order to select a random value from it.");
            }

            List<T> valuelist = new ArrayList<>(valueset);

            int randomIntervalIndex = new Random().nextInt(valuelist.size());
            return valuelist.get(randomIntervalIndex);

        }

    }


}
