package be.ugent.ledc.sigma.repair.experimental;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.formulas.CPFImplicator;

import java.util.*;
import java.util.stream.Collectors;

public class ChangeExpressionSearch {

    public static Map<CPF, Set<CPF>> search(DataObject dataObject, Set<CPF> cpfs, NullBehavior nullBehavior) {

        Map<CPF, Set<CPF>> changeExpressions = new HashMap<>();

        for (CPF cpf : cpfs) {
            Set<AbstractAtom<?, ?, ?>> cpfFailedAtoms = getFailedAtoms(cpf, dataObject);
            Set<Set<String>> cpfAttributesToChange = getAttributesToChange(cpfFailedAtoms, dataObject, cpfs, nullBehavior);
            Set<CPF> cpfChangeExpressions = getChangeExpressions(cpfAttributesToChange, cpf, dataObject);
            changeExpressions.put(cpf, cpfChangeExpressions);
        }

        return changeExpressions;

    }

    private static Set<AbstractAtom<?, ?, ?>> getFailedAtoms(CPF cpf, DataObject dataObject) {

        Set<String> nonNullAttributes = dataObject
                .getAttributes()
                .stream()
                .filter(a -> dataObject.get(a) != null)
                .collect(Collectors.toSet());

        return cpf.project(nonNullAttributes)
                .getAtoms()
                .stream()
                .filter(a -> !a.test(dataObject))
                .collect(Collectors.toSet());
    }

    private static Set<Set<String>> getAttributesToChange(Set<AbstractAtom<?, ?, ?>> failedAtoms, DataObject dataObject, Set<CPF> cpfs, NullBehavior nullBehavior) {

        Set<Set<String>> currentResult = new HashSet<>();
        currentResult.add(new HashSet<>());

        for (AbstractAtom<?, ?, ?> atom : failedAtoms) {

            Set<String> newAttributes = atom.getAttributes().collect(Collectors.toSet());

            Set<Set<String>> newResult = new HashSet<>();

            for (Set<String> currentSet : currentResult) {
                for (String newAttribute : newAttributes) {
                    Set<String> newSet = new HashSet<>(currentSet);
                    newSet.add(newAttribute);
                    newResult.add(newSet);
                }
            }

            currentResult = new HashSet<>(newResult);

        }

        return addNullAttributes(currentResult, dataObject, cpfs, nullBehavior);

    }

    private static Set<Set<String>> addNullAttributes(Set<Set<String>> attributesToChange, DataObject dataObject, Set<CPF> cpfs, NullBehavior nullBehavior) {

        Set<String> nullAttributes = dataObject
                .getAttributes()
                .stream()
                .filter(at -> dataObject.get(at) == null && nullBehavior.repairIfNull(cpfs, at))
                .collect(Collectors.toSet());

        for (Set<String> attributes : attributesToChange) {
            attributes.addAll(nullAttributes);
        }

        return attributesToChange;

    }

    private static Set<CPF> getChangeExpressions(Set<Set<String>> attributesToChange, CPF cpf, DataObject dataObject) {

        Set<CPF> changeExpressions = new HashSet<>();

        for (Set<String> attributes : attributesToChange) {

            Set<AbstractAtom<?, ?, ?>> changeExpressionAtoms = new HashSet<>();

            for (AbstractAtom<?, ?, ?> atom : cpf.getAtoms()) {


                if (atom.getAttributes().allMatch(at -> attributes.stream().anyMatch(at::equals))) {
                    changeExpressionAtoms.add(atom);
                } else if (atom.getAttributes().anyMatch(at -> attributes.stream().anyMatch(at::equals))) {

                    String nonMatchingAttribute = atom.getAttributes().filter(at -> !attributes.contains(at)).findFirst().get();
                    DataObject project = dataObject.project(nonMatchingAttribute);

                    if (atom instanceof VariableAtom<?, ?, ?>) {
                        changeExpressionAtoms.add(atom.fix(project));
                    }
                }

            }

            changeExpressions.add(CPFImplicator.imply(new CPF(changeExpressionAtoms)));

        }

        return changeExpressions;

    }

}
