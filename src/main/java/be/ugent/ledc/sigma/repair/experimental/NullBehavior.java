package be.ugent.ledc.sigma.repair.experimental;

import be.ugent.ledc.sigma.datastructures.formulas.CPF;

import java.util.Set;
import java.util.function.BiFunction;

public enum NullBehavior {

    NO_REPAIR((cpfs, a) -> false),
    ALWAYS_REPAIR((cpfs, a) -> true),
    CONSTRAINED_REPAIR((cpfs, a) -> cpfs.stream().anyMatch(cpf -> cpf.getAttributes().contains(a)));

    private final BiFunction<Set<CPF>, String, Boolean> needsRepairWhenNullTest;

    NullBehavior(BiFunction<Set<CPF>, String, Boolean> needsRepairWhenNullTest) {
        this.needsRepairWhenNullTest = needsRepairWhenNullTest;
    }

    public boolean repairIfNull(Set<CPF> cpfs, String attribute) {
        return needsRepairWhenNullTest.apply(cpfs, attribute);
    }

}
