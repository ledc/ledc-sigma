package be.ugent.ledc.sigma.repair;

import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleException;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.values.NominalValueIterator;
import be.ugent.ledc.sigma.datastructures.values.OrdinalValueIterator;
import be.ugent.ledc.sigma.datastructures.values.attributevalues.ValueIterator;
import be.ugent.ledc.sigma.datastructures.values.rulevalues.NominalRuleValueMapping;
import be.ugent.ledc.sigma.datastructures.values.rulevalues.OrdinalRuleValueMapping;
import be.ugent.ledc.sigma.datastructures.values.rulevalues.RuleValueMapping;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class RepairOperations
{
    public static <T extends Comparable<? super T>> ValueIterator<T> getPermittedValues(String attribute, SigmaRuleset rules)
    {        
        Map<String, SigmaContractor<?>> contract = new HashMap<>();
        contract.put(attribute, rules.getContractors().get(attribute));

        SigmaRuleset proj = new SigmaRuleset(
            contract,
            rules.getDomainConstraints(attribute)
        );
        
        return getValueIterator(proj, attribute, null);
    }
    
    public static <T extends Comparable<? super T>> ValueIterator<T> getPermittedValues(String attributeToTreat, T current, SigmaRuleset selectedRules, DataObject repair) throws RepairException
    {
        //We fix those rules selected
        Set<SigmaRule> fixedRules = selectedRules
            .getRules()
            .stream()
            .map(rule -> rule.fix(repair))
            .collect(Collectors.toSet());

        //Sanity check: if some rule contains only ALWAYS_TRUE atoms, the rule will always fail...
        if (fixedRules.stream().anyMatch(rule -> rule.getAtoms().isEmpty() || rule.getAtoms()
                .stream()
                .allMatch(atom -> atom.equals(AbstractAtom.ALWAYS_TRUE)))) {
            throw new RepairException("Repair encoutered a situation where no repairs are possible:\n"
                    + "Attribute to change: " + attributeToTreat + "\n"
                    + "Repair so far: " + repair + "\n"
                    + "Rules: " + selectedRules
                    .getRules()
                    .stream()
                    .map(SigmaRule::toString)
                    .collect(Collectors.joining("\n"))
            );
        }

        fixedRules.removeIf(rule -> rule
            .getAtoms()
            .stream()
            .allMatch(atom -> atom.equals(AbstractAtom.ALWAYS_FALSE))
        );

        //Filter out atoms containing no attributes
        for (SigmaRule rule : fixedRules)
        {
            rule.getAtoms().removeIf(atom -> atom.getAttributes().findAny().isEmpty());
        }

        Map<String, SigmaContractor<?>> cleanedContractors = new HashMap<>(selectedRules.getContractors());

        repair.getAttributes().forEach(cleanedContractors::remove);

        SigmaRuleset fixed = new SigmaRuleset(cleanedContractors, fixedRules);

        return getValueIterator(
            fixed.project(SetOperations.set(attributeToTreat)),
            attributeToTreat,
            current
        );
    }
    
    public static Map<String, ValueIterator<?>> getPermittedValues(DataObject dirty, Set<String> attributes, Set<String> conditionals, SigmaRuleset rules) throws RepairException
    {
        Map<String, ValueIterator<?>> permittedValues = new HashMap<>();

        for(String attribute: attributes)
        {
            Set<SigmaRule> selectedRules = rules
                .getRules()
                .stream()
                .filter(rule -> rule
                    .getInvolvedAttributes()
                    .stream()
                    .allMatch(a -> a.equals(attribute) || conditionals.contains(a)))
                .collect(Collectors.toSet());

            SigmaRuleset selectedRuleset = new SigmaRuleset(
                rules
                    .getContractors()
                    .entrySet()
                    .stream()
                    .filter(e -> e.getKey().equals(attribute) || conditionals.contains(e.getKey()))
                    .collect(Collectors.toMap(
                            Map.Entry::getKey,
                            Map.Entry::getValue)
                    ),
                selectedRules);

            permittedValues.put(
                attribute,
                getPermittedValues(
                    attribute,
                    (Comparable)dirty.get(attribute),
                    selectedRuleset,
                    dirty.project(conditionals))
            );
        }

        return permittedValues;
    }

    private static <T extends Comparable<? super T>> ValueIterator<T> getValueIterator(SigmaRuleset selectedRules, String attributeToTreat, T current)
    {
        Set<SigmaRule> simplifiedReduced = selectedRules
            .getRules()
            .stream()
            .flatMap(rule -> rule.simplifyAndReduce(true).stream())
            .collect(Collectors.toSet());

        SigmaContractor<?> contractor = selectedRules
            .getContractors()
            .get(attributeToTreat);

        if (contractor instanceof NominalContractor) {

            //Compose a set of atoms
            Set<NominalRuleValueMapping<T>> rvMapping = simplifiedReduced
                .stream()
                .filter(rule -> rule.getInvolvedAttributes().size() == 1 && rule.involves(attributeToTreat))
                .map(rule -> new NominalRuleValueMapping<>(attributeToTreat, rule))
                .map(rvm -> (NominalRuleValueMapping<T>) rvm)
                .collect(Collectors.toSet());

            //Compute the union
            SetNominalAtom<T> unionAtom = getUnionSetNominalAtom(attributeToTreat, rvMapping);

            if (unionAtom == null || !unionAtom.getOperator().equals(SetOperator.NOTIN)) {
                throw new SigmaRuleException("A nominal datatype should always have a domain restriction...");
            }

            Set<T> permittedValues = unionAtom.getConstants();

            permittedValues.remove(current);

            return new NominalValueIterator<>(permittedValues);

        } else if (contractor instanceof OrdinalContractor) {
            OrdinalContractor<T> cContractor = (OrdinalContractor<T>) contractor;

            //Compose a set of atoms
            Set<OrdinalRuleValueMapping<T>> rvMapping = simplifiedReduced
                    .stream()
                    .filter(rule -> rule.getInvolvedAttributes().size() == 1 && rule.involves(attributeToTreat))
                    .map(rule -> new OrdinalRuleValueMapping<>(attributeToTreat, rule))
                    .map(rvm -> (OrdinalRuleValueMapping<T>) rvm)
                    .collect(Collectors.toSet());

            //Initialize the list of intervals
            List<Interval<T>> iList =
                current == null
                ? Stream.of(
                    new Interval<>(cContractor.first(), cContractor.last(), false, false)
                ).collect(Collectors.toList())

                : Stream.of(
                    new Interval<>(cContractor.first(), cContractor.previous(current), false, false),
                    new Interval<>(cContractor.next(current), cContractor.last(), false, false)
                ).collect(Collectors.toList());

            if(!rvMapping.isEmpty())
            {

                Set<Interval<T>> nonPermittedIntervals = rvMapping.stream().map(RuleValueMapping::getCoveredAttributeValues).flatMap(Set::stream).collect(Collectors.toSet());

                if (nonPermittedIntervals.stream().allMatch(i -> i.getLeftBound() == null && i.getRightBound() == null)) {
                    return new OrdinalValueIterator<>(new ArrayList<>(), cContractor);
                }

                for (Interval<T> i : nonPermittedIntervals) {
                    List<Interval<T>> lList = convert(i, cContractor);

                    iList = merge(iList, lList);
                }
            }

            return new OrdinalValueIterator<>(iList, cContractor);
        } else
            throw new SigmaRuleException("Unknown type of contractor: expected either nominal or ordinal data");
    }

    public static <T extends Comparable<? super T>> List<Interval<T>> merge(List<Interval<T>> current, List<Interval<T>> added)
    {
        List<Interval<T>> merged = new ArrayList<>();

        for (Interval<T> a : current) {
            for (Interval<T> b : added) {
                if (!a.isDisjunctWith(b))
                    merged.add(a.intersect(b));
            }
        }

        return merged;
    }

    private static <T extends Comparable<? super T>> List<Interval<T>> convert(Interval<T> interval, OrdinalContractor<T> contractor)
    {

        if (interval.getRightBound() != null && interval.getLeftBound() != null) {

            Interval<T> i1 = new Interval<>(null, contractor.previous(interval.getLeftBound()), true, false);
            Interval<T> i2 = new Interval<>(contractor.next(interval.getRightBound()), null, false, true);

            return Stream.of(i1, i2).collect(Collectors.toList());

        } else if (interval.getRightBound() == null && interval.getLeftBound() == null){
            return List.of(new Interval<>(null, null));
        } else {

            List<Interval<T>> list = new ArrayList<>();

            if (interval.getLeftBound() != null) {
                list.add(new Interval<>(null, contractor.previous(interval.getLeftBound()), true, false));
            }

            if (interval.getRightBound() != null) {
                list.add(new Interval<>(contractor.next(interval.getRightBound()), null, false, true));
            }

            return list;

        }

    }
    
    private static <T extends Comparable<? super T>> SetNominalAtom<T> getUnionSetNominalAtom(String attribute, Set<NominalRuleValueMapping<T>> rvMapping)
    {

        NominalContractor<T> contractor = null;

        Set<T> inConstants = new HashSet<>();
        Set<T> notInConstants = new HashSet<>();

        boolean first = true;

        for (NominalRuleValueMapping<T> current : rvMapping) {

            if (contractor == null) {
                contractor = (NominalContractor<T>) current.getRule().getContractor(attribute);
            }

            // Get all constants appearing as constant in a 'notin' atom
            Set<T> currentNotInConstants = current.getCoveredAttributeValues().getUncoveredValues();

            // If 'notin' constants exist, add them to the set of all 'notin'  constants (first time) or keep only the constants in the set of all 'notin' constants that also appear in this set
            if (currentNotInConstants != null) {

                if (first) {
                    notInConstants.addAll(currentNotInConstants);
                    first = false;
                } else {
                    notInConstants.retainAll(currentNotInConstants);

                    // If the set of all 'notin' constants is empty, the union covers the entire domain
                    if (notInConstants.isEmpty()) {
                        return null;
                    }
                }
                // Otherwise, add 'in' constants to the set of all 'in' constants.
            } else {

                Set<T> currentInConstants = current.getCoveredAttributeValues().getCoveredValues();
                inConstants.addAll(currentInConstants);

                // If the set of all 'notin' constants equals the set of 'in constants, the union covers the entire domain
                if (!notInConstants.isEmpty() && inConstants.containsAll(notInConstants)) {
                    return null;
                }
            }
        }

        if (!notInConstants.isEmpty()) {
            notInConstants.removeAll(inConstants);
            return new SetNominalAtom<>(contractor, attribute, SetOperator.NOTIN, notInConstants);
        } else {
            return new SetNominalAtom<>(contractor, attribute, SetOperator.IN, inConstants);
        }

    }
    
    public static Map<String, ValueIterator<?>> shrink(Map<String, ValueIterator<?>> permittedValues, SigmaRuleset rules)
    {
        Map<String, ValueIterator<?>> shrunkIterators = new HashMap<>();
        
        for(String a: permittedValues.keySet())
        {
            //Check for rules that allow simplification
            Set<SigmaRule> filterRules = rules
                .stream()
                .filter(rule -> rule.getAtoms().size() == 1)        //There can be only one atom
                .filter(rule -> rule.involves(a))                   //The rule must involve a
                .filter(rule -> !rule.getVariableAtoms().isEmpty()) //The rule must have variable atoms
                .collect(Collectors.toSet());
            
            Set<?> aValues = new HashSet<>(permittedValues
                .get(a)
                .convert()
                .getValues());
            
            for(SigmaRule fr: filterRules)
            {
                VariableAtom<?, ?, ?> vAtom = fr
                    .getVariableAtoms()
                    .stream()
                    .findFirst()
                    .get();
                
                //An assertion that two attributes must cannot be equal (must be different)
                //does not offer us information
                if(vAtom.getOperator().equals(EqualityOperator.EQ))
                    continue;
                
                //We ensure attribute a is the left attribute
                if(vAtom.getRightAttribute().equals(a))
                {
                    vAtom = vAtom.flip();
                }

                //Find the other attribute in the atom
                String b = vAtom.getRightAttribute();
                
                Set<?> bValues = RepairOperations
                    .getPermittedValues(b, rules)
                    .convert()
                    .getValues();
                
                if(vAtom.getOperator().equals(EqualityOperator.NEQ))
                {
                    aValues.removeIf(av -> !bValues.contains(av));
                    continue;
                }
                
                VariableOrdinalAtom vOrdAtom = (VariableOrdinalAtom)vAtom;

                if(vOrdAtom.getOperator().equals(InequalityOperator.LEQ)
                || vOrdAtom.getOperator().equals(InequalityOperator.LT))
                {
                    //Compute minimum value of b and apply constant shift
                    Object thr = bValues
                            .stream()
                            .map(bv -> (Comparable)bv)
                            .min(Comparator.naturalOrder())
                            .get();
                    
                    //Use the threshold to filter values
                    aValues.removeIf(av -> vOrdAtom.test(new DataObject().set(a, av).set(b, thr)));
                }
                else if(vOrdAtom.getOperator().equals(InequalityOperator.GEQ)
                || vOrdAtom.getOperator().equals(InequalityOperator.GT))
                {
                    //Compute minimum value of b and apply constant shift
                    Object thr = bValues
                        .stream()
                        .map(bv -> (Comparable)bv)
                        .max(Comparator.naturalOrder())
                        .get();
                    
                    //Use the threshold to filter values
                    aValues.removeIf(av -> vOrdAtom.test(new DataObject().set(a, av).set(b, thr)));
                }
            }
            
            shrunkIterators.put(a, new NominalValueIterator<>(aValues));
        }
        
        return shrunkIterators;
    }
}
