package be.ugent.ledc.sigma.repair.experimental;

import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

public class ConstantCostRepairEngine extends ObjectLevelRepairEngine<ConstantCostModel> {

    public ConstantCostRepairEngine(SigmaRuleset sigmaRuleset, ConstantCostModel costModel, NullBehavior nullBehavior, CPFRepairSelection repairSelection) throws RepairException {
        super(sigmaRuleset, costModel, nullBehavior, repairSelection);
    }

    public ConstantCostRepairEngine(SigmaRuleset sigmaRuleset, ConstantCostModel costModel,CPFRepairSelection repairSelection) throws RepairException {
        super(sigmaRuleset, costModel, repairSelection);
    }

    @Override
    public Set<CPF> getMinCostChangeExpressions(DataObject dataObject) {

        Map<CPF, Set<CPF>> changeExpressionsMap = ChangeExpressionSearch.search(dataObject, getCPFs(), getNullBehavior());
        Set<CPF> changeExpressions = changeExpressionsMap.values().stream().flatMap(Set::stream).collect(Collectors.toSet());
        changeExpressions.removeIf(ce1 -> changeExpressions.stream().anyMatch(ce2 -> !ce1.equals(ce2) && ce1.implies(ce2)));

        Set<CPF> minCostChangeExpressions = new HashSet<>();
        int minCost = Integer.MAX_VALUE;

        for (CPF changeExpression : changeExpressions) {

            Set<String> involvedAttributes = changeExpression.getAttributes();
            int currentCost = involvedAttributes.stream().mapToInt(ia -> getCostModel().getCostFunction(ia).getFixedCost()).sum();

            if (currentCost < minCost) {
                minCostChangeExpressions = new HashSet<>();
                minCostChangeExpressions.add(changeExpression);
                minCost = currentCost;
            } else if (minCost == currentCost) {
                minCostChangeExpressions.add(changeExpression);
            }

        }

        return minCostChangeExpressions;

    }

}
