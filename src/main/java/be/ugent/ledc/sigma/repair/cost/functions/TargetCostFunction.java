package be.ugent.ledc.sigma.repair.cost.functions;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;

/**
 * A cost function that uses a target value to assess the goodness of the repair value.
 * @author abronsel
 * @param <T>
 * @param <C> 
 */
public class TargetCostFunction <T extends Comparable<? super T>, C extends OrdinalContractor<T>> extends AbstractIterableCostFunction<T>
{
    private final C contractor;
    
    private final T target;

    public TargetCostFunction(C contractor, T target)
    {
        this.contractor = contractor;
        this.target = target;
    }

    @Override
    public int cost(T originalValue, T repairedValue, DataObject originalObject)
    {        
        if(repairedValue == null)
            return Integer.MAX_VALUE;
        
        if(originalValue == null)
            return 1;
        
        if(originalValue.equals(repairedValue))
            return 0;
        
        return target.compareTo(repairedValue) <= 0
            ? (int) contractor.cardinality(new Interval<>(target, repairedValue)) + 1
            : (int) contractor.cardinality(new Interval<>(repairedValue, target)) + 1;
    }
}
