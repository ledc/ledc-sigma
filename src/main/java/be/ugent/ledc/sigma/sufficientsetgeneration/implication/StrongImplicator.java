package be.ugent.ledc.sigma.sufficientsetgeneration.implication;

import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class StrongImplicator extends SimpleImplicator
{
    @Override
    public Set<SigmaRule> getAllENRules(String generator, SigmaContractor<?> generatorContractor, Set<SigmaRule> contributors)
    {
        Set<?> generatorDomain = getGeneratorValues(getDomainRules(contributors));
        
        Set<SigmaRule> candidateContributors = super.getCandidateContributors(contributors);
        
        int domainSize = generatorDomain.size();

        //Check if the strong generator theorem can be applied
        boolean weakGenerators =
            generatorDomain //Condition 1: not all values are involved as singleton conditions
            .stream()
            .allMatch(v ->
                candidateContributors
                    .stream()
                    .anyMatch(rule ->
                        super.buildValues(rule, generator).contains(v) &&
                        super.buildValues(rule, generator).size() == 1)
            )
        &&
            candidateContributors
            .stream()
            .anyMatch(rule ->
                super.buildValues(rule, generator).size() > 1 &&
                super.buildValues(rule, generator).size() < domainSize - 1   
            );

        if (weakGenerators)
        {
            System.out.println("Warning: weak generators are possible for generator "  + generator + ". Switching to simple implication.");
            return super.getAllNecessaryRules(generator, generatorContractor, contributors);
        }
        
        List<SigmaRule> contributorsList = new ArrayList<>(candidateContributors);
        
        Set<SigmaRule> allNewRules = new HashSet<>();
        
        for (int i = 0; i < contributorsList.size(); i++)
        {
            SigmaRule rule1 = contributorsList.get(i);

            Set<?> values1 = buildValues(rule1, generator);
            
            for (int j = i + 1; j < contributorsList.size(); j++)
            {
                SigmaRule rule2 = contributorsList.get(j);

                Set<?> values2 = buildValues(rule1, generator);
                
                //One of both rules should have domain size minus one for the entering attribute
                if (values1.size() + 1 == domainSize || values2.size() + 1 == domainSize)
                    allNewRules.add(super.imply(rule1, rule2, generatorDomain, generator));
            }
        }
        
        return allNewRules;
    }
    
}
