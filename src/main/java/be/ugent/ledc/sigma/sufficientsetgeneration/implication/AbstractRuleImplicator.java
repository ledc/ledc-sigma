package be.ugent.ledc.sigma.sufficientsetgeneration.implication;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractRuleImplicator implements RuleImplicator<SigmaRule> {

    public Set<SigmaRule> getAllNecessaryRules(String generator, SigmaContractor<?> generatorContractor, Set<SigmaRule> candidateContributors) {
        return removeRedundant(this.getAllENRules(generator, generatorContractor, candidateContributors));
    }

    public abstract Set<SigmaRule> getAllENRules(String generator, SigmaContractor<?> generatorContractor, Set<SigmaRule> candidateContributors);

    protected <T extends Comparable<? super T>> Set<SigmaRule> implyConstantNominal(String generator, NominalContractor<T> generatorContractor, Set<SigmaRule> impliedContributors, Set<T> uncoveredGeneratorValues) {

        SigmaRule impliedRule;

        Set<AbstractAtom<?, ?, ?>> nonGeneratorAtoms = impliedContributors.stream().map(SigmaRule::getAtoms).flatMap(Set::stream).filter(a -> !(a instanceof SetNominalAtom && ((SetNominalAtom<T>) a).getAttribute().equals(generator))).collect(Collectors.toSet());

        if (uncoveredGeneratorValues.isEmpty()) {
            impliedRule = new SigmaRule(nonGeneratorAtoms);
        } else {
            Set<AbstractAtom<?, ?, ?>> allAtoms = new HashSet<>(nonGeneratorAtoms);
            for (T impliedGeneratorValue : uncoveredGeneratorValues) {
                allAtoms.add(new ConstantNominalAtom<>(generatorContractor, generator, EqualityOperator.NEQ, impliedGeneratorValue));
            }
            impliedRule = new SigmaRule(allAtoms);
        }

        return impliedRule.simplifyAndReduce(true);

    }

    protected <T extends Comparable<? super T>> Set<SigmaRule> implyNonVariableOrdinal(String generator, OrdinalContractor<T> generatorContractor, Set<SigmaRule> impliedContributors, Set<Set<Interval<T>>> impliedGeneratorIntervals) {

        Set<SigmaRule> impliedRules = new HashSet<>();

        Set<AbstractAtom<?, ?, ?>> nonGeneratorAtoms = impliedContributors
            .stream()
            .map(SigmaRule::getAtoms)
            .flatMap(Set::stream)
            .filter(a -> (a.getAttributes().noneMatch(at -> at.equals(generator)) || a instanceof VariableAtom))
            .collect(Collectors.toSet());

        Set<Interval<T>> impliedGeneratorIntervalsFlattened = impliedGeneratorIntervals.stream().flatMap(Set::stream).collect(Collectors.toSet());

        Set<Interval<T>> generatorUnion = getGeneratorUnion(generatorContractor, impliedGeneratorIntervalsFlattened);

        if (generatorUnion.size() == 1 && generatorUnion.stream().allMatch(i -> i.getLeftBound() == null && i.getRightBound() == null)) {
            impliedRules.add(new SigmaRule(nonGeneratorAtoms));
        }

        for (Interval<T> impliedGeneratorInterval : generatorUnion) {

            Set<AbstractAtom<?, ?, ?>> allAtoms = new HashSet<>(nonGeneratorAtoms);

            if (impliedGeneratorInterval.getLeftBound() != null) {
                allAtoms.add(new ConstantOrdinalAtom<>(generatorContractor, generator, InequalityOperator.GEQ, impliedGeneratorInterval.getLeftBound()));
            }

            if (impliedGeneratorInterval.getRightBound() != null) {
                allAtoms.add(new ConstantOrdinalAtom<>(generatorContractor, generator, InequalityOperator.LEQ, impliedGeneratorInterval.getRightBound()));
            }

            impliedRules.add(new SigmaRule(allAtoms));

        }

        return impliedRules.stream().map(r -> r.simplifyAndReduce(true)).flatMap(Set::stream).collect(Collectors.toSet());

    }

    private <T extends Comparable<? super T>> Set<Interval<T>> getGeneratorUnion(OrdinalContractor<T> generatorContractor, Set<Interval<T>> generatorIntervals) {

        Stack<Interval<T>> stack = new Stack<>();

        List<Interval<T>> sortedIntervals = generatorIntervals.stream().sorted(Interval.leftBoundComparator()).collect(Collectors.toList());

        stack.push(sortedIntervals.get(0));

        for (int i = 1; i < sortedIntervals.size(); i++) {

            Interval<T> top = stack.peek();
            Interval<T> currentInterval = sortedIntervals.get(i);

            if (top.getRightBound() != null
                    && currentInterval.getLeftBound() != null
                    && generatorContractor.next(top.getRightBound()).compareTo(currentInterval.getLeftBound()) < 0) {
                stack.push(currentInterval);
            } else if (top.getRightBound() != null
                    && (currentInterval.getRightBound() == null || top.getRightBound().compareTo(currentInterval.getRightBound()) < 0)) {
                Interval<T> newInterval = new Interval<>(top.getLeftBound(), currentInterval.getRightBound(), top.isLeftOpen(), currentInterval.isRightOpen());
                stack.pop();
                stack.push(newInterval);
            }

        }

        return new HashSet<>(stack);

    }

    public Set<SigmaRule> removeRedundant(Set<SigmaRule> rules) {
        return rules
                .stream()
                .filter(r1 -> rules
                        .stream()
                        .noneMatch(r2 -> !r1.equals(r2)
                                && isRedundantTo(r1,r2)
                        )
                )
                .collect(Collectors.toSet());
    }
    
    public abstract boolean isRedundantTo(SigmaRule first, SigmaRule second);

    public boolean isDominantOver(SigmaRule first, SigmaRule second)
    {
        return isRedundantTo(second, first);
    }

}
