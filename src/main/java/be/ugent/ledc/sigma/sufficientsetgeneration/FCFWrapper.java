package be.ugent.ledc.sigma.sufficientsetgeneration;

import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetOperations;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.FullManager;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.ImplicationManager;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.SimpleManager;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.StrongManager;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

public class FCFWrapper
{
    private final Map<ImplicationManager, String> implicationManagers;
    
    private final boolean silent;
    
    public FCFWrapper()
    {
        this(true);
    }
    
    public FCFWrapper(boolean silent)
    {
        this.silent = silent;
        
        implicationManagers = new LinkedHashMap<>();
        
        implicationManagers.put(new StrongManager(), "strong implication manager");
        implicationManagers.put(new SimpleManager(), "simple implication manager");
        implicationManagers.put(new FullManager(), "full implication manager");
    }
    
    public SufficientSigmaRuleset build(SigmaRuleset ruleset)
    {
        //Create partitions of sigma rules
        if(!silent)
        {
            System.out.println("Computing rule partition...");
        }
        
        Set<SigmaRuleset> partition = SigmaRulesetOperations.partition(ruleset);
        
        if(!silent)
        {
            System.out.println("Rule partition size: " + partition.size());
        }
        
        SigmaRuleset sufficientRuleset = new SigmaRuleset(new HashMap<>(), new HashSet<>());
        
        for(SigmaRuleset partitionClass: partition)
        {
            if (partitionClass.getRules().size() <= 1)
            {
                //Merge
                sufficientRuleset = sufficientRuleset.merge(partitionClass);
            }
            else
            {
                ImplicationManager manager = null;

                for(ImplicationManager im: implicationManagers.keySet())
                {
                    if(im.test(partitionClass))
                    {
                        if(!silent)
                        {
                            System.out.println("FCF will be executed with the " + implicationManagers.get(im));
                        }
                        manager = im;
                        break;
                    }
                }

                if(manager == null)
                    throw new RuntimeException("Unexpected exception: implication is null.");

                //Make a generator
                FCFGenerator generator = new FCFGenerator(manager);

                //Create a sufficient set with this generator
                SufficientSigmaRuleset ssr = SufficientSigmaRuleset.create(
                    partitionClass,
                    generator
                );

                //Create a sufficient rule and merge it
                sufficientRuleset = sufficientRuleset.merge(ssr);

            }
        }
    
        //Return
        return SufficientSigmaRuleset.create(sufficientRuleset);
    }
}
