package be.ugent.ledc.sigma.sufficientsetgeneration;

import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.AbstractRuleImplicator;

public abstract class AbstractSufficientSetGenerator implements SufficientSetGenerator<SigmaRule, SigmaRuleset> {

    private final AbstractRuleImplicator ruleImplicator;

    public AbstractSufficientSetGenerator(AbstractRuleImplicator ruleImplicator) {
        this.ruleImplicator = ruleImplicator;
    }

    public AbstractRuleImplicator getRuleImplicator() {
        return ruleImplicator;
    }
}
