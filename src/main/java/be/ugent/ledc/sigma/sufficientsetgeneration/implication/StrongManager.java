package be.ugent.ledc.sigma.sufficientsetgeneration.implication;

import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetException;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * The strong manager extends upon the simple manager and works with implicative rules
 * of the from a_1=c_1 AND a_2=c_2 AND ... AND a_n-1 = c_n-1 AND a_n != c_n
 * where all a_i are attributes and all c_i are constant values.
 * 
 * Under this form, the strong generator theorem allows to detect efficiently when implication can be
 * done by a double for loop (and thus produces quadratic complexity)
 * @author abronsel
 */
public class StrongManager extends SimpleManager
{
    @Override
    public void verify(SigmaRuleset ruleset) throws SigmaRulesetException
    {
        super.verify(ruleset);
        
        Set<SigmaRule> nonDomainRules = ruleset
            .stream()
            .filter(rule -> rule.getInvolvedAttributes().size() > 1)
            .collect(Collectors.toSet());
        
        if(nonDomainRules
            .stream()
            .flatMap(rule -> rule.getAtoms().stream())
            .anyMatch(atom ->  atom.getOperator() != EqualityOperator.EQ
                            && atom.getOperator() != EqualityOperator.NEQ))
        {
            throw new SigmaRulesetException("Strong implication expects atoms with operators "
                + EqualityOperator.EQ + " or "
                + EqualityOperator.NEQ + " only.");
        }
       
        
        if(nonDomainRules
            .stream()
            .anyMatch(rule -> rule
                .getAtoms()
                .stream()
                .filter(atom -> atom.getOperator() == EqualityOperator.NEQ)
                .count() != 1))
        {
            throw new SigmaRulesetException("Strong implication requires each rule to have exactly one atom with operator " + EqualityOperator.NEQ);
        }
    }
    @Override
    public SigmaRuleset transformRuleset(SigmaRuleset ruleset)
    {
        return super.transformRuleset(ruleset);
    }

    @Override
    public AbstractRuleImplicator getRuleImplicator()
    {
        return new StrongImplicator();
    }
    
}
