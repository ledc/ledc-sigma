package be.ugent.ledc.sigma.sufficientsetgeneration;

import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.FullManager;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.ImplicationManager;

import java.util.*;
import java.util.stream.Collectors;

public final class FCFGenerator extends AbstractSufficientSetGenerator {

    private final ImplicationManager implicationManager;
    
    public FCFGenerator(ImplicationManager implicationManager)
    {
        super(implicationManager.getRuleImplicator());
        this.implicationManager = implicationManager;
    }

    public FCFGenerator()
    {
        this(new FullManager());
    }

    @Override
    public Set<SigmaRule> generateSufficientSet(SigmaRuleset ruleset) {

        implicationManager.verify(ruleset);

        Set<SigmaRule> rules = implicationManager
            .transformRuleset(ruleset)
            .getRules();
        
        rules = getRuleImplicator().removeRedundant(rules);
        
        rules.addAll(generateSufficientSetWithFCF(
            rules,
            rules,
            ruleset.getContractors(),
            new ArrayList<>(ruleset.getContractors().keySet()),
            new HashSet<>()));

        rules = getRuleImplicator().removeRedundant(rules);

        return rules;
    }

    private Set<SigmaRule> generateSufficientSetWithFCF(Set<SigmaRule> allMaxRules, Set<SigmaRule> currentBranchRules, Map<String, SigmaContractor<?>> sigmaContractors, List<String> remainingAttributes, Set<String> nonInvolvedAttributes) {

        Iterator<String> attributeIterator = remainingAttributes.iterator();

        // Loop over all remaining child attributes
        while (attributeIterator.hasNext()) {

            // Select generator
            String generator = attributeIterator.next();
            attributeIterator.remove();
            
            // Select all edit rules in the current branch with the generator involved and the nonInvolvedAttributes not involved
            Set<SigmaRule> candidateContributors = getRuleImplicator().getCandidateContributors(currentBranchRules, generator);
            candidateContributors.removeIf(e -> nonInvolvedAttributes.stream().anyMatch(e::involves));
            
            //If the generator has a nominal contract AND
            //there are no domain constraints, we NEVER have new rules
//            if(sigmaContractors.get(generator) instanceof NominalContractor
//            && candidateContributors
//                .stream()
//                .noneMatch(rule ->
//                    rule.getInvolvedAttributes().size() == 1
//                &&  rule.involves(generator)))
//            {
//                continue;
//            }
            
            if (candidateContributors.size() > 1)
            {

                // Generate all new rules
                Set<SigmaRule> newRules = getRuleImplicator().getAllNecessaryRules(
                    generator,
                    sigmaContractors.get(generator),
                    candidateContributors);

                // Prepare for recursive call
                if (!newRules.isEmpty()) {

                    // Replace redundant edit rules in newRules by their dominant edit rules in allMaxRules
                    Map<SigmaRule, SigmaRule> newRulesDominanceMapping = mapOnDominant(newRules, allMaxRules);
                    newRules = newRules
                        .stream()
                        .map(newRulesDominanceMapping::get)
                        .collect(Collectors.toSet());

                    // Replace redundant edit rules in allMaxRules by their dominant edit rules in newRules
                    Map<SigmaRule, SigmaRule> allMaxRulesDominanceMapping = mapOnDominant(allMaxRules, newRules);
                    allMaxRules = allMaxRules
                        .stream()
                        .map(allMaxRulesDominanceMapping::get)
                        .collect(Collectors.toSet());

                    // Replace redundant edit rules in currentBranchRules by their dominant edit rules in newRules
                    Map<SigmaRule, SigmaRule> currentBranchRulesDominanceMapping = mapOnDominant(currentBranchRules, newRules);
                    currentBranchRules = currentBranchRules
                        .stream()
                        .map(currentBranchRulesDominanceMapping::get)
                        .collect(Collectors.toSet());

                    // Add all generated essentially new rules to the set of allMaxRules
                    allMaxRules.addAll(newRules);

                    // Pass the currentBranchRules together with the generated essentially new edits (or their dominating rule) for the next recursive call
                    Set<SigmaRule> nextLevelRules = new HashSet<>(currentBranchRules);
                    nextLevelRules.addAll(newRules);

                    if (!remainingAttributes.isEmpty() && nextLevelRules.size() >= 2) {

                        // Add the current generator the set of attributes that are not supposed to enter during the next recursive calls
                        Set<String> newNonEnteringAttributes = new HashSet<>(nonInvolvedAttributes);
                        newNonEnteringAttributes.add(generator);

                        // Call method recursively
                        allMaxRules = generateSufficientSetWithFCF(
                            allMaxRules,
                            nextLevelRules,
                            sigmaContractors,
                            new ArrayList<>(remainingAttributes),
                            newNonEnteringAttributes);

                    }
                }
            }
        }

        return allMaxRules;

    }

    private Map<SigmaRule, SigmaRule> mapOnDominant(Set<SigmaRule> potentiallyRedundant, Set<SigmaRule> potentiallyDominant) {

        Map<SigmaRule, SigmaRule> dominanceMapping = new HashMap<>();

        for (SigmaRule ruleA : potentiallyRedundant) {

            boolean isMapped = false;

            for (SigmaRule ruleB : potentiallyDominant) {
                if (getRuleImplicator().isRedundantTo(ruleA, ruleB)) {
                    dominanceMapping.put(ruleA, ruleB);
                    isMapped = true;
                    break;
                }
            }

            if (!isMapped) {
                dominanceMapping.put(ruleA, ruleA);
            }

        }

        return dominanceMapping;

    }
}
