package be.ugent.ledc.sigma.sufficientsetgeneration.implication;

import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetException;

public interface ImplicationManager
{
    void verify(SigmaRuleset ruleset) throws SigmaRulesetException;
    
    AbstractRuleImplicator getRuleImplicator();
    
    SigmaRuleset transformRuleset(SigmaRuleset ruleset);
    
    default boolean test(SigmaRuleset ruleset)
    {
        try
        {
            verify(ruleset);
        }
        catch(SigmaRulesetException ex)
        {
            return false;
        }
        return true;
        
    }
    
}
