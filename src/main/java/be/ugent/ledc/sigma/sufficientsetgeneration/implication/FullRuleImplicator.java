package be.ugent.ledc.sigma.sufficientsetgeneration.implication;

import be.ugent.ledc.core.dataset.contractors.Contractor;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.datastructures.Pair;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorUtil;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.formulas.CPFImplicator;
import be.ugent.ledc.sigma.datastructures.operators.ComparableOperator;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetException;
import be.ugent.ledc.sigma.datastructures.values.rulevalues.NominalRuleValueMapping;
import be.ugent.ledc.sigma.datastructures.values.rulevalues.NominalRuleValues;
import be.ugent.ledc.sigma.datastructures.values.rulevalues.OrdinalRuleValueMapping;
import be.ugent.ledc.sigma.datastructures.values.rulevalues.RuleValueMapping;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class FullRuleImplicator extends AbstractRuleImplicator {

    @Override
    public Set<SigmaRule> getAllENRules(String generator, SigmaContractor<?> generatorContractor, Set<SigmaRule> candidateContributors) throws SigmaRulesetException {

        if (candidateContributors.size() == 1) {
            return new HashSet<>();
        }

        Set<SigmaRule> newRules;

        if (SigmaContractorUtil.isNominalContractor(generatorContractor)) {
            newRules = getAllENRulesNominal(generator, (NominalContractor<?>) generatorContractor, candidateContributors);
        } else {
            newRules = getAllENRulesOrdinal(generator, (OrdinalContractor<?>) generatorContractor, candidateContributors);
        }

        Set<SigmaRule> simplifiedAndReducedRules = new HashSet<>();

        for (SigmaRule newRule : newRules) {
            simplifiedAndReducedRules.addAll(newRule.simplifyAndReduce(true));
        }

        if (simplifiedAndReducedRules.stream().anyMatch(SigmaRule::isAlwaysFailed)) {
            throw new SigmaRulesetException("Set of rules is not satisfiable, as rule(s) " + simplifiedAndReducedRules.stream().filter(SigmaRule::isAlwaysFailed).collect(Collectors.toSet()) + " is a contradiction.");
        }

        return removeRedundant(simplifiedAndReducedRules);

    }

    @Override
    public boolean isRedundantTo(SigmaRule first, SigmaRule other) {
        return other
                .getAtoms()
                .stream()
                .allMatch(a -> CPFImplicator.atomIsImpliedByCPF(first.getCPF(), a));
    }

    /**
     * NOMINAL CASE
     **/

    private <T extends Comparable<? super T>> Set<SigmaRule> getAllENRulesNominal(String generator, NominalContractor<?> generatorContractor, Set<SigmaRule> candidateContributors) {

        // Split set of candidate contributors into contributors with a variable atom for the generator and contributors without a variable atom for the generator
        Set<SigmaRule> variableCCs = candidateContributors.stream().filter(cc -> cc.getVariableAtoms().stream().anyMatch(a -> a.getAttributes().anyMatch(at -> at.equals(generator)))).collect(Collectors.toSet());
        Set<SigmaRule> setCCs = candidateContributors.stream().filter(cc -> !variableCCs.contains(cc)).collect(Collectors.toSet());

        // Map all generator values to the corresponding rules
        Set<NominalImpliedRuleMapping<T>> constantCCMapping = getNominalImpliedRuleMapping(generator, setCCs);

        return getAllENRulesGeneralNominal(generator, (NominalContractor<T>) generatorContractor, constantCCMapping, variableCCs);

    }

    private <T extends Comparable<? super T>> Set<NominalImpliedRuleMapping<T>> getNominalImpliedRuleMapping(String generator, Set<SigmaRule> candidateContributors) {

        Set<NominalImpliedRuleMapping<T>> ccMapping = new HashSet<>();

        for (SigmaRule candidateContributor : candidateContributors) {
            ccMapping.add(new NominalImpliedRuleMapping<>(generator, Collections.singleton(candidateContributor), Collections.singleton(candidateContributor)));
        }

        return ccMapping;

    }

    private <T extends Comparable<? super T>> Set<SigmaRule> getAllENRulesGeneralNominal(String generator, NominalContractor<T> generatorContractor, Set<NominalImpliedRuleMapping<T>> constantCCMapping, Set<SigmaRule> variableCCs) {

        Set<SigmaRule> newRules = new HashSet<>(implyVariable(generator, generatorContractor, variableCCs));

        List<NominalImpliedRuleMapping<T>> currentLevelNodes = new ArrayList<>(constantCCMapping);

        for (NominalImpliedRuleMapping<T> currentLevelNode : currentLevelNodes) {
            Set<SigmaRule> candidateContributors = new HashSet<>(variableCCs);
            candidateContributors.addAll(currentLevelNode.getImpliedRules());
            newRules.addAll(implyVariable(generator, generatorContractor, candidateContributors));
        }

        Set<Set<SigmaRule>> combinationsToIgnore = new HashSet<>();

        while (currentLevelNodes.size() > 1) {

            Set<NominalImpliedRuleMapping<T>> nextLevelNodes = new HashSet<>();

            for (int i = 0; i < currentLevelNodes.size(); i++) {

                NominalImpliedRuleMapping<T> currentLevelNode1 = currentLevelNodes.get(i);
                Set<SigmaRule> contributors1 = currentLevelNode1.getContributors();
                NominalRuleValues<T> values1 = currentLevelNode1.getCoveredGeneratorValuesFlattened();

                for (int j = i + 1; j < currentLevelNodes.size(); j++) {

                    NominalImpliedRuleMapping<T> currentLevelNode2 = currentLevelNodes.get(j);
                    Set<SigmaRule> contributors2 = currentLevelNode2.getContributors();
                    NominalRuleValues<T> values2 = currentLevelNode2.getCoveredGeneratorValuesFlattened();

                    // Only combine previous level nodes if both sets contain exactly 1 new candidate contributor
                    if (contributors1.stream().filter(c1 -> !contributors2.contains(c1)).count() != 1 || contributors2.stream().filter(c2 -> !contributors1.contains(c2)).count() != 1) {
                        continue;
                    }

                    Set<SigmaRule> impliedContributors = new HashSet<>(contributors1);
                    impliedContributors.addAll(contributors2);

                    if (combinationsToIgnore.stream().anyMatch(impliedContributors::containsAll)) {
                        continue;
                    }

                    // Skip combination if current level node contains no new generator value compared to previous level node or vice versa
                    if (!currentLevelNode1.addsNewValuesTo(currentLevelNode2) || !currentLevelNode2.addsNewValuesTo(currentLevelNode1)) {
                        combinationsToIgnore.add(impliedContributors);
                        continue;
                    }

                    Set<T> uncoveredValues;

                    if (values1.hasCoveredValues()) {
                        if (values2.hasCoveredValues()) {
                            continue;
                        } else {
                            uncoveredValues = values2.getUncoveredValues().stream().filter(v -> !values1.getCoveredValues().contains(v)).collect(Collectors.toSet());
                        }
                    } else {
                        if (values2.hasCoveredValues()) {
                            uncoveredValues = values1.getUncoveredValues().stream().filter(v -> !values2.getCoveredValues().contains(v)).collect(Collectors.toSet());
                        } else {
                            uncoveredValues = values1.getUncoveredValues().stream().filter(v -> values2.getUncoveredValues().contains(v)).collect(Collectors.toSet());
                        }
                    }

                    // Imply rule
                    Set<SigmaRule> impliedRules = implyConstantNominal(generator, generatorContractor, impliedContributors, uncoveredValues);

                    // If generator does not appear in implied rule, rule is essentially new, otherwise rule is implied or tautology (in case of exception)
                    if (impliedRules.stream().noneMatch(r -> r.getAtoms().stream().anyMatch(a -> a.getAttributes().allMatch(at -> at.equals(generator))))) {
                        newRules.addAll(impliedRules);
                        combinationsToIgnore.add(impliedContributors);
                    } else if (impliedRules.stream().anyMatch(ir -> impliedContributors.stream().noneMatch(ic -> isRedundantTo(ir, ic)))) {
                        nextLevelNodes.add(new NominalImpliedRuleMapping<>(generator, impliedContributors, impliedRules));
                    }

                }

            }

            currentLevelNodes = new ArrayList<>(nextLevelNodes);

            for (NominalImpliedRuleMapping<T> currentLevelNode : currentLevelNodes) {
                Set<SigmaRule> candidateContributors = new HashSet<>(variableCCs);
                candidateContributors.addAll(currentLevelNode.getImpliedRules());
                newRules.addAll(implyVariable(generator, generatorContractor, candidateContributors));
            }

        }

        return newRules;

    }

    /**
     * ORDINAL CASE
     **/

    private <T extends Comparable<? super T>> Set<SigmaRule> getAllENRulesOrdinal(String generator, OrdinalContractor<?> generatorContractor, Set<SigmaRule> candidateContributors) {

        // Split candidate contributors into variable-generator rules and constant-generator rules
        Set<SigmaRule> variableCCs = candidateContributors.stream().filter(cc -> cc.getVariableAtoms().stream().anyMatch(a -> a.getAttributes().anyMatch(at -> at.equals(generator)))).collect(Collectors.toSet());
        Set<SigmaRule> nonVariableCCs = candidateContributors.stream().filter(cc -> !variableCCs.contains(cc)).collect(Collectors.toSet());

        // Map all generator values to the corresponding rules with constants
        Set<OrdinalImpliedRuleMapping<T>> nonVariableCCMapping = getOrdinalImpliedRuleMapping(generator, nonVariableCCs);

        // Ordinal implication algorithm depends on the fact whether there are variable candidate contributors or not
        if (variableCCs.isEmpty()) {
            return getAllENRulesOnlyNonVariableOrdinal(generator, (OrdinalContractor<T>) generatorContractor, nonVariableCCMapping);
        } else {
            return getAllENRulesGeneralOrdinal(generator, (OrdinalContractor<T>) generatorContractor, nonVariableCCMapping, variableCCs);
        }

    }

    private <T extends Comparable<? super T>> Set<OrdinalImpliedRuleMapping<T>> getOrdinalImpliedRuleMapping(String generator, Set<SigmaRule> candidateContributors) {

        Set<OrdinalImpliedRuleMapping<T>> ccMapping = new HashSet<>();

        for (SigmaRule candidateContributor : candidateContributors) {
            ccMapping.add(new OrdinalImpliedRuleMapping<>(generator, Collections.singleton(candidateContributor), Collections.singleton(candidateContributor)));
        }

        return ccMapping;

    }

    private <T extends Comparable<? super T>> Set<SigmaRule> getAllENRulesOnlyNonVariableOrdinal(String generator, OrdinalContractor<T> generatorContractor, Set<OrdinalImpliedRuleMapping<T>> nonVariableCCMapping) {

        Predicate<Interval<T>> lBoundMatch = interval -> interval.getLeftBound() == null;
        Predicate<Interval<T>> rBoundMatch = interval -> interval.getRightBound() == null;

        Set<Set<Interval<T>>> coveredGeneratorValueSets = nonVariableCCMapping.stream().map(ImpliedRuleMapping::getCoveredGeneratorValues).flatMap(Set::stream).collect(Collectors.toSet());

        if (coveredGeneratorValueSets.stream().flatMap(Set::stream).allMatch(cvs -> lBoundMatch.test(cvs) || rBoundMatch.test(cvs))) {
            return getAllENRulesOrdinalDoubleLoop(generator, generatorContractor, nonVariableCCMapping, lBoundMatch, rBoundMatch);
        } else {
            return getAllENRulesOrdinalSortedIntervals(generator, generatorContractor, nonVariableCCMapping, lBoundMatch, rBoundMatch);
        }

    }

    private <T extends Comparable<? super T>> Set<SigmaRule> getAllENRulesOrdinalDoubleLoop(String generator, OrdinalContractor<T> generatorContractor, Set<OrdinalImpliedRuleMapping<T>> nonVariableCCMapping, Predicate<Interval<T>> lBoundMatch, Predicate<Interval<T>> rBoundMatch) {

        //New rules
        Set<SigmaRule> newRules = new HashSet<>();

        //Split contributors in two parts
        Set<OrdinalImpliedRuleMapping<T>> lInfiniteCCS = nonVariableCCMapping.stream().filter(m -> m.getCoveredGeneratorValues().stream().flatMap(Set::stream).allMatch(lBoundMatch)).collect(Collectors.toSet());
        Set<OrdinalImpliedRuleMapping<T>> rInfiniteCCS = nonVariableCCMapping.stream().filter(m -> m.getCoveredGeneratorValues().stream().flatMap(Set::stream).allMatch(rBoundMatch)).collect(Collectors.toSet());

        for (OrdinalImpliedRuleMapping<T> lInfiniteCC : lInfiniteCCS) {
            for (OrdinalImpliedRuleMapping<T> rInfiniteCC : rInfiniteCCS) {

                Set<SigmaRule> impliedContributors = new HashSet<>(lInfiniteCC.getContributors());
                impliedContributors.addAll(rInfiniteCC.getContributors());

                Set<Set<Interval<T>>> impliedIntervals = new HashSet<>(lInfiniteCC.getCoveredGeneratorValues());
                impliedIntervals.addAll(rInfiniteCC.getCoveredGeneratorValues());


                // Imply rule
                Set<SigmaRule> impliedRules = implyNonVariableOrdinal(generator, generatorContractor, impliedContributors, impliedIntervals);

                // If generator does not appear in implied rule, rule is essentially new, otherwise rule is implied or tautology (in case of exception)
                if (impliedRules.size() == 1 && impliedRules.stream().noneMatch(r -> r.getAtoms().stream().anyMatch(a -> a.getAttributes().allMatch(at -> at.equals(generator))))) {
                    newRules.addAll(impliedRules);
                }

            }
        }

        return newRules;

    }

    private <T extends Comparable<? super T>> Set<SigmaRule> getAllENRulesOrdinalSortedIntervals(String generator, OrdinalContractor<T> generatorContractor, Set<OrdinalImpliedRuleMapping<T>> nonVariableCCMapping, Predicate<Interval<T>> lBoundMatch, Predicate<Interval<T>> rBoundMatch) {

        //New rules
        Set<SigmaRule> newRules = new HashSet<>();

        Set<Interval<T>> intervals = nonVariableCCMapping.stream().map(OrdinalImpliedRuleMapping::getCoveredGeneratorValues).flatMap(Set::stream).flatMap(Set::stream).collect(Collectors.toSet());
        List<Interval<T>> sortedIntervals = intervals.stream().sorted(Interval.leftBoundComparator()).collect(Collectors.toList());

        //Sort rules according to left bound of intervals, nulls first
        List<Pair<Interval<T>, SigmaRule>> sortedCC = new ArrayList<>();

        for (Interval<T> interval : sortedIntervals) {

            for(OrdinalImpliedRuleMapping<T> nvcc: nonVariableCCMapping) {

                Set<Interval<T>> nvccIntervals = nvcc.getCoveredGeneratorValues().stream().flatMap(Set::stream).collect(Collectors.toSet());

                if (nvccIntervals.contains(interval)) {

                    SigmaRule contributor = nvcc.getImpliedRules().stream().findFirst().get();

                    Set<AbstractAtom<?, ?, ?>> restrictedAtoms = contributor.getAtoms().stream().filter(at -> at.getAttributes().noneMatch(a -> a.equals(generator))).collect(Collectors.toSet());

                    if (interval.getLeftBound() != null) {
                        restrictedAtoms.add(new ConstantOrdinalAtom<>(generatorContractor, generator, InequalityOperator.GEQ, interval.getLeftBound()));
                    }

                    if (interval.getRightBound() != null) {
                        restrictedAtoms.add(new ConstantOrdinalAtom<>(generatorContractor, generator, InequalityOperator.LEQ, interval.getRightBound()));
                    }

                    SigmaRule restrictedContributor = new SigmaRule(restrictedAtoms);

                    sortedCC.add(new Pair<>(interval, restrictedContributor));
                }

            }

        }

        //Initialize stack
        LinkedList<OrdinalSequence<T>> stack = sortedCC
                .stream()
                .filter(cc -> lBoundMatch.test(cc.getFirst()))
                .map(cc -> new OrdinalSequence<>(new OrdinalImpliedRuleMapping<>(generator, Collections.singleton(cc.getSecond()), Collections.singleton(cc.getSecond())), cc.getFirst())).collect(Collectors.toCollection(LinkedList::new));

        //Remove from sorted rules those that are initiated on the stack
        sortedCC.removeIf(cc -> lBoundMatch.test(cc.getFirst()));

        //For efficient update of search pointer, keep a map where the left bound begins
        Map<Pair<T, Boolean>, Integer> searchPointerMap = new HashMap<>();

        for (int i = 1; i < sortedCC.size(); i++) {
            T prevLeftBound = sortedCC.get(i - 1).getFirst().getLeftBound();
            T currLeftBound = sortedCC.get(i).getFirst().getLeftBound();

            Boolean prevLeftOpen = sortedCC.get(i - 1).getFirst().isLeftOpen();
            Boolean currLeftOpen = sortedCC.get(i).getFirst().isLeftOpen();

            if (prevLeftBound.equals(currLeftBound) && !prevLeftOpen.equals(currLeftOpen)) {
                searchPointerMap.put(new Pair<>(prevLeftBound, Boolean.FALSE), i);
            } else if (!prevLeftBound.equals(currLeftBound)) {
                searchPointerMap.put(new Pair<>(prevLeftBound, prevLeftOpen), i);
            }
        }

        //Continue until the stack is empty
        while (!stack.isEmpty()) {
            //System.out.println("Stack size: " + stack.size());
            OrdinalSequence<T> base = stack.pop();
            Interval<T> bInterval = base.getLastGeneratorInterval();

            Pair<T, Boolean> probe = new Pair<>(bInterval.getLeftBound(), bInterval.isLeftOpen());

            int startIndex = searchPointerMap.get(probe) == null ? 0 : searchPointerMap.get(probe);

            for (int i = startIndex; i < sortedCC.size(); i++) {

                SigmaRule candidate = sortedCC.get(i).getSecond();
                Interval<T> cInterval = sortedCC.get(i).getFirst();

                //If the intervals can't merge, break the loop
                if (bInterval.getRightBound() != null
                        && cInterval.getLeftBound() != null
                        && generatorContractor.next(bInterval.getRightBound()).compareTo(cInterval.getLeftBound()) < 0) {
                    break;
                }

                //If the last interval of the sequence does not overlap and does not meet the new interval, this rule will be redundant
                if (bInterval.con(cInterval))
                    continue;

                //If the second last interval is not null and overlaps with the candidate, we don't need the last interval
                if (base.getSecondLastGeneratorInterval() != null &&
                        (base.getSecondLastGeneratorInterval().getRightBound() == null || cInterval.getLeftBound() == null ||
                                base.getSecondLastGeneratorInterval().getRightBound() != null && cInterval.getLeftBound() != null && generatorContractor.next(base.getSecondLastGeneratorInterval().getRightBound()).compareTo(cInterval.getLeftBound()) >= 0)) {
                    continue;
                }

                //Calculate the implied rule
                Set<SigmaRule> impliedContributors = new HashSet<>();
                impliedContributors.add(new ArrayList<>(base.getImpliedRuleMapping().getImpliedRules()).get(0));
                impliedContributors.add(candidate);

                Set<Set<Interval<T>>> impliedIntervals = new HashSet<>();
                impliedIntervals.add(new ArrayList<>(base.getImpliedRuleMapping().getCoveredGeneratorValues()).get(0));
                impliedIntervals.add(Collections.singleton(cInterval));

                SigmaRule impliedRule = new ArrayList<>(implyNonVariableOrdinal(generator, generatorContractor, impliedContributors, impliedIntervals)).get(0);

                if (rBoundMatch.test(cInterval)) {
                    newRules.add(impliedRule);
                } else if (searchPointerMap.get(new Pair<>(cInterval.getLeftBound(), cInterval.isLeftOpen())) != null) {
                    OrdinalSequence<T> newSequence = new OrdinalSequence<>(new OrdinalImpliedRuleMapping<>(generator, impliedContributors, Collections.singleton(impliedRule)), cInterval, base.getLastGeneratorInterval());
                    stack.push(newSequence);
                }

            }
        }

        return newRules;
    }

    private <T extends Comparable<? super T>> Set<SigmaRule> getAllENRulesGeneralOrdinal(String generator, OrdinalContractor<T> generatorContractor, Set<OrdinalImpliedRuleMapping<T>> nonVariableCCMapping, Set<SigmaRule> variableCCs) {

        Set<SigmaRule> newRules = new HashSet<>(implyVariable(generator, generatorContractor, variableCCs));

        List<OrdinalImpliedRuleMapping<T>> currentLevelNodes = nonVariableCCMapping.stream().filter(m -> !m.getCoveredGeneratorValues().isEmpty()).collect(Collectors.toList());

        for (OrdinalImpliedRuleMapping<T> currentLevelNode : currentLevelNodes) {
            Set<SigmaRule> candidateContributors = new HashSet<>(variableCCs);
            candidateContributors.addAll(currentLevelNode.getImpliedRules());
            newRules.addAll(implyVariable(generator, generatorContractor, candidateContributors));
        }

        Set<Set<SigmaRule>> combinationsToIgnore = new HashSet<>();

        while (currentLevelNodes.size() > 1) {

            Set<OrdinalImpliedRuleMapping<T>> nextLevelNodes = new HashSet<>();

            for (int i = 0; i < currentLevelNodes.size(); i++) {

                OrdinalImpliedRuleMapping<T> currentLevelNode1 = currentLevelNodes.get(i);
                Set<SigmaRule> contributors1 = currentLevelNode1.getContributors();
                Set<Set<Interval<T>>> generatorValues1 = currentLevelNode1.getCoveredGeneratorValues();

                for (int j = i + 1; j < currentLevelNodes.size(); j++) {

                    OrdinalImpliedRuleMapping<T> currentLevelNode2 = currentLevelNodes.get(j);
                    Set<SigmaRule> contributors2 = currentLevelNode2.getContributors();
                    Set<Set<Interval<T>>> generatorValues2 = currentLevelNode2.getCoveredGeneratorValues();

                    // Only combine previous level nodes if both sets contain exactly 1 new candidate contributor
                    if (contributors1.stream().filter(c1 -> !contributors2.contains(c1)).count() != 1 || contributors2.stream().filter(c2 -> !contributors1.contains(c2)).count() != 1) {
                        continue;
                    }

                    Set<SigmaRule> impliedContributors = new HashSet<>(contributors1);
                    impliedContributors.addAll(contributors2);

                    Set<Set<Interval<T>>> impliedIntervals = new HashSet<>(generatorValues1);
                    impliedIntervals.addAll(generatorValues2);

                    if (combinationsToIgnore.stream().anyMatch(impliedContributors::containsAll)) {
                        continue;
                    }

                    // Skip combination if current level node contains no new generator value compared to previous level node or vice versa
                    if (!currentLevelNode1.addsNewValuesTo(currentLevelNode2) || !currentLevelNode2.addsNewValuesTo(currentLevelNode1)) {
                        combinationsToIgnore.add(impliedContributors);
                        continue;
                    }

                    // Imply rule
                    Set<SigmaRule> impliedRules = implyNonVariableOrdinal(generator, generatorContractor, impliedContributors, impliedIntervals);

                    // If generator does not appear in implied rule, rule is essentially new, otherwise rule is implied or tautology (in case of exception)
                    if (impliedRules.size() == 1 && impliedRules.stream().noneMatch(r -> r.getAtoms().stream().anyMatch(a -> a.getAttributes().allMatch(at -> at.equals(generator))))) {
                        newRules.addAll(impliedRules);
                        combinationsToIgnore.add(impliedContributors);
                    } else if (impliedRules.stream().anyMatch(ir -> impliedContributors.stream().noneMatch(ic -> isRedundantTo(ir, ic)))) {
                        nextLevelNodes.add(new OrdinalImpliedRuleMapping<>(generator, impliedContributors, impliedRules));
                    }

                }

            }

            currentLevelNodes = new ArrayList<>(nextLevelNodes);

            for (OrdinalImpliedRuleMapping<T> currentLevelNode : currentLevelNodes) {
                Set<SigmaRule> candidateContributors = new HashSet<>(variableCCs);
                candidateContributors.addAll(currentLevelNode.getImpliedRules());
                newRules.addAll(implyVariable(generator, generatorContractor, candidateContributors));
            }

        }

        return newRules;

    }

    /* VARIABLE CASE */

    private Set<SigmaRule> implyVariable(String generator, Contractor generatorContractor, Set<SigmaRule> candidateContributors) {

        Set<SigmaRule> newRules = new HashSet<>();

        Set<Set<AbstractAtom<?, ?, ?>>> inversedCCs = inverseAtomSets(candidateContributors.stream().map(SigmaRule::getAtoms).collect(Collectors.toSet()));
        Set<Set<AbstractAtom<?, ?, ?>>> impliedAtomSets = inversedCCs.stream().map(as -> implyVariableAtoms(generator, generatorContractor, as)).collect(Collectors.toSet());

        Set<Set<AbstractAtom<?, ?, ?>>> impliedGeneratorAtomSets = new HashSet<>();

        for (Set<AbstractAtom<?, ?, ?>> impliedAtomSet : impliedAtomSets) {

            impliedAtomSet.removeIf(ia -> ia.equals(AbstractAtom.ALWAYS_TRUE));

            if (impliedAtomSet.isEmpty()) {
                return newRules;
            }

            if (impliedAtomSet.stream().anyMatch(ia -> ia.equals(AbstractAtom.ALWAYS_FALSE))) {
                impliedGeneratorAtomSets.add(Collections.singleton(AbstractAtom.ALWAYS_FALSE));
            } else {
                impliedGeneratorAtomSets.add(impliedAtomSet);
            }
        }
        
        Set<Set<AbstractAtom<?, ?, ?>>> resultingAtomSets = inverseAtomSets(impliedGeneratorAtomSets);
        Set<SigmaRule> resultingRules = resultingAtomSets.stream().map(SigmaRule::new).map(r -> r.simplifyAndReduce(true)).flatMap(Set::stream).collect(Collectors.toSet());

        newRules = removeRedundant(resultingRules);

        return newRules;

    }

    private Set<Set<AbstractAtom<?, ?, ?>>> inverseAtomSets(Set<Set<AbstractAtom<?, ?, ?>>> atomSets) {

        List<Set<AbstractAtom<?, ?, ?>>> inversedAtoms = new ArrayList<>();

        for (Set<AbstractAtom<?, ?, ?>> atomSet : atomSets) {
            inversedAtoms.add(atomSet.stream().map(AbstractAtom::getInverse).collect(Collectors.toSet()));
        }

        return combineAtoms(inversedAtoms, 0);
    }

    private Set<Set<AbstractAtom<?, ?, ?>>> combineAtoms(List<Set<AbstractAtom<?, ?, ?>>> atomSets, int index) {

        Set<Set<AbstractAtom<?, ?, ?>>> result = new HashSet<>();

        if (index == atomSets.size()) {
            result.add(new HashSet<>());
        } else {
            for (AbstractAtom<?, ?, ?> atom : atomSets.get(index)) {
                for (Set<AbstractAtom<?, ?, ?>> nextSet : combineAtoms(atomSets, index + 1)) {
                    nextSet.add(atom);
                    result.add(nextSet);
                }
            }
        }

        return result;

    }

    private <T extends Comparable<? super T>> Set<AbstractAtom<?, ?, ?>> implyVariableAtoms(String generator, Contractor generatorContractor, Set<AbstractAtom<?, ?, ?>> atoms) {

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = new HashSet<>();

        Set<AbstractAtom<?, ?, ?>> ngAtoms = atoms.stream().filter(at -> at.getAttributes().noneMatch(a -> a.equals(generator))).collect(Collectors.toSet());

        if (!ngAtoms.isEmpty()) {
            CPF atomsCPF = new CPF(atoms.stream().filter(at -> at.getAttributes().noneMatch(a -> a.equals(generator))).collect(Collectors.toSet()));
            Set<CPF> srAtomsCPF = atomsCPF.simplifyAndReduce(true);

            if (srAtomsCPF.stream().allMatch(r -> r.getAtoms().stream().allMatch(at -> at.equals(AbstractAtom.ALWAYS_FALSE)))) {
                return new HashSet<>(Collections.singleton(AbstractAtom.ALWAYS_FALSE));
            }
        }


        List<AbstractAtom<?, ?, ?>> atomsList = new ArrayList<>(atoms);

        if (atomsList.size() == 1) {

            AbstractAtom<?, ?, ?> atom = atomsList.get(0);

            if (atom.getAttributes().noneMatch(at -> at.equals(generator))) {
                impliedAtoms.add(atom);
            }

        }

        for (int i = 0; i < atomsList.size(); i++) {
            for (int j = i + 1; j < atomsList.size(); j++) {

                AbstractAtom<?, ?, ?> atom1 = atomsList.get(i);
                AbstractAtom<?, ?, ?> atom2 = atomsList.get(j);

                boolean skipLoop = false;

                if (atom1.getAttributes().noneMatch(at -> at.equals(generator))) {
                    impliedAtoms.add(atom1);
                    skipLoop = true;
                }

                if (atom2.getAttributes().noneMatch(at -> at.equals(generator))) {
                    impliedAtoms.add(atom2);
                    skipLoop = true;
                }

                if (skipLoop) continue;

                if (atom1 instanceof VariableAtom && atom2 instanceof VariableAtom) {
                    if (generatorContractor instanceof OrdinalContractor) {
                        impliedAtoms.add(implyTwoVariableOrdinalAtoms(generator, (OrdinalContractor<T>) generatorContractor, (VariableOrdinalAtom<?>) atom1, (VariableOrdinalAtom<?>) atom2));
                    } else {
                        impliedAtoms.add(implyTwoVariableNominalAtoms(generator, (NominalContractor<T>) generatorContractor, (VariableNominalAtom<?>) atom1, (VariableNominalAtom<?>) atom2));
                    }
                } else if (atom1 instanceof VariableAtom && atom2 instanceof ConstantOrdinalAtom) {
                    impliedAtoms.add(implyVariableConstantOrdinalAtoms(generator, (OrdinalContractor<T>) generatorContractor, (VariableOrdinalAtom<?>) atom1, (ConstantOrdinalAtom<?>) atom2));
                } else if (atom1 instanceof VariableAtom && atom2 instanceof SetOrdinalAtom) {
                    impliedAtoms.add(implyVariableSetOrdinalAtoms(generator, (OrdinalContractor<T>) generatorContractor, (VariableOrdinalAtom<?>) atom1, (SetOrdinalAtom<?>) atom2));
                } else if (atom1 instanceof SetOrdinalAtom && atom2 instanceof VariableAtom) {
                    impliedAtoms.add(implyVariableSetOrdinalAtoms(generator, (OrdinalContractor<T>) generatorContractor, (VariableOrdinalAtom<?>) atom2, (SetOrdinalAtom<?>) atom1));
                } else if (atom1 instanceof VariableAtom && atom2 instanceof SetNominalAtom) {
                    impliedAtoms.add(implyVariableSetNominalAtoms(generator, (NominalContractor<T>) generatorContractor, (VariableNominalAtom<?>) atom1, (SetNominalAtom<?>) atom2));
                } else if (atom1 instanceof ConstantOrdinalAtom && atom2 instanceof VariableAtom) {
                    impliedAtoms.add(implyVariableConstantOrdinalAtoms(generator, (OrdinalContractor<T>) generatorContractor, (VariableOrdinalAtom<?>) atom2, (ConstantOrdinalAtom<?>) atom1));
                } else if (atom1 instanceof SetNominalAtom && atom2 instanceof VariableAtom) {
                    impliedAtoms.add(implyVariableSetNominalAtoms(generator, (NominalContractor<T>) generatorContractor, (VariableNominalAtom<?>) atom2, (SetNominalAtom<?>) atom1));
                } else if (atom1 instanceof ConstantOrdinalAtom && atom2 instanceof ConstantOrdinalAtom) {
                    impliedAtoms.add(implyTwoConstantOrdinalAtoms((OrdinalContractor<T>) generatorContractor, (ConstantOrdinalAtom<?>) atom1, (ConstantOrdinalAtom<?>) atom2));
                } else if (atom1 instanceof SetOrdinalAtom && atom2 instanceof SetOrdinalAtom) {
                    impliedAtoms.add(implyTwoSetAtoms((SetOrdinalAtom<?>) atom1, (SetOrdinalAtom<?>) atom2));
                } else if (atom1 instanceof SetOrdinalAtom && atom2 instanceof ConstantOrdinalAtom) {
                    impliedAtoms.add(implyConstantSetOrdinalAtoms((OrdinalContractor<T>) generatorContractor, (ConstantOrdinalAtom<?>) atom2, (SetOrdinalAtom<?>) atom1));
                } else if (atom1 instanceof ConstantOrdinalAtom && atom2 instanceof SetOrdinalAtom) {
                    impliedAtoms.add(implyConstantSetOrdinalAtoms((OrdinalContractor<T>) generatorContractor, (ConstantOrdinalAtom<?>) atom1, (SetOrdinalAtom<?>) atom2));
                } else if (atom1 instanceof SetNominalAtom && atom2 instanceof SetNominalAtom) {
                    impliedAtoms.add(implyTwoSetAtoms((SetNominalAtom<?>) atom1, (SetNominalAtom<?>) atom2));
                }
            }
        }

        return impliedAtoms;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyTwoVariableOrdinalAtoms(String generator, OrdinalContractor<T> generatorContractor, VariableOrdinalAtom<?> atom1, VariableOrdinalAtom<?> atom2) {

        boolean atom1GeneratorIsLeft = atom1.getLeftAttribute().equals(generator);
        boolean atom2GeneratorIsLeft = atom2.getLeftAttribute().equals(generator);

        ComparableOperator atom1Operator = atom1GeneratorIsLeft ? atom1.getOperator() : atom1.getOperator().getReversedOperator();
        String atom1Variable = atom1GeneratorIsLeft ? atom1.getRightAttribute() : atom1.getLeftAttribute();
        int atom1Constant = atom1GeneratorIsLeft ? atom1.getRightConstant() : atom1.getRightConstant() * -1;

        ComparableOperator atom2Operator = atom2GeneratorIsLeft ? atom2.getOperator() : atom2.getOperator().getReversedOperator();
        String atom2Variable = atom2GeneratorIsLeft ? atom2.getRightAttribute() : atom2.getLeftAttribute();
        int atom2Constant = atom2GeneratorIsLeft ? atom2.getRightConstant() : atom2.getRightConstant() * -1;

        VariableOrdinalAtom<?> impliedAtom = null;

        if (atom1Operator.equals(InequalityOperator.LT) && atom2Operator.equals(InequalityOperator.GT)) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GT, atom2Variable, atom2Constant - atom1Constant + 1);
        } else if ((atom1Operator.equals(InequalityOperator.LT) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.GEQ)))
                || (atom1Operator.equals(InequalityOperator.LEQ) && atom2Operator.equals(InequalityOperator.GT))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.GT))) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GT, atom2Variable, atom2Constant - atom1Constant);
        } else  if ((atom1Operator.equals(InequalityOperator.LEQ) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.GEQ)))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.GEQ))) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GEQ, atom2Variable, atom2Constant - atom1Constant);
        } else  if ((atom1Operator.equals(EqualityOperator.NEQ) && atom2Operator.equals(EqualityOperator.EQ))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.NEQ))) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, EqualityOperator.NEQ, atom2Variable, atom2Constant - atom1Constant);
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.EQ)) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, EqualityOperator.EQ, atom2Variable, atom2Constant - atom1Constant);
        } else  if ((atom1Operator.equals(InequalityOperator.GEQ) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.LEQ)))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.LEQ))) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LEQ, atom2Variable, atom2Constant - atom1Constant);
        } else if ((atom1Operator.equals(InequalityOperator.GT) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.LEQ)))
                || (atom1Operator.equals(InequalityOperator.GEQ) && atom2Operator.equals(InequalityOperator.LT))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.LT))) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LT, atom2Variable, atom2Constant - atom1Constant);
        } else if (atom1Operator.equals(InequalityOperator.GT) && atom2Operator.equals(InequalityOperator.LT)) {
            impliedAtom = new VariableOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LT, atom2Variable, atom2Constant - atom1Constant - 1);
        }

        if (impliedAtom != null) {
            if (impliedAtom.isAlwaysFalse()) {
                return AbstractAtom.ALWAYS_FALSE;
            } else if (impliedAtom.isAlwaysTrue()) {
                return AbstractAtom.ALWAYS_TRUE;
            }
            return impliedAtom;
        }

        return AbstractAtom.ALWAYS_TRUE;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyTwoVariableNominalAtoms(String generator, NominalContractor<T> generatorContractor, VariableNominalAtom<?> atom1, VariableNominalAtom<?> atom2) {

        boolean atom1GeneratorIsLeft = atom1.getLeftAttribute().equals(generator);
        boolean atom2GeneratorIsLeft = atom2.getLeftAttribute().equals(generator);

        ComparableOperator atom1Operator = atom1.getOperator();
        String atom1Variable = atom1GeneratorIsLeft ? atom1.getRightAttribute() : atom1.getLeftAttribute();

        ComparableOperator atom2Operator = atom2.getOperator();
        String atom2Variable = atom2GeneratorIsLeft ? atom2.getRightAttribute() : atom2.getLeftAttribute();

        VariableNominalAtom<?> impliedAtom = null;

        if ((atom1Operator.equals(EqualityOperator.NEQ) && atom2Operator.equals(EqualityOperator.EQ))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.NEQ))) {
            impliedAtom = new VariableNominalAtom<>(generatorContractor, atom1Variable, EqualityOperator.NEQ, atom2Variable);
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.EQ)) {
            impliedAtom = new VariableNominalAtom<>(generatorContractor, atom1Variable, EqualityOperator.EQ, atom2Variable);
        }

        if (impliedAtom != null) {
            if (impliedAtom.isAlwaysFalse()) {
                return AbstractAtom.ALWAYS_FALSE;
            } else if (impliedAtom.isAlwaysTrue()) {
                return AbstractAtom.ALWAYS_TRUE;
            }
            return impliedAtom;
        }

        return AbstractAtom.ALWAYS_TRUE;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyVariableConstantOrdinalAtoms(String generator, OrdinalContractor<T> generatorContractor, VariableOrdinalAtom<?> atom1, ConstantOrdinalAtom<?> atom2) {

        boolean atom1GeneratorIsLeft = atom1.getLeftAttribute().equals(generator);

        ComparableOperator atom1Operator = atom1GeneratorIsLeft ? atom1.getOperator() : atom1.getOperator().getReversedOperator();
        String atom1Variable = atom1GeneratorIsLeft ? atom1.getRightAttribute() : atom1.getLeftAttribute();
        int atom1Constant = atom1GeneratorIsLeft ? atom1.getRightConstant() : atom1.getRightConstant() * -1;

        ComparableOperator atom2Operator = atom2.getOperator();
        T atom2Constant = (T) atom2.getConstant();

        if (atom1Constant > 0) {
            while (atom1Constant != 0) {
                atom2Constant = generatorContractor.previous(atom2Constant);
                atom1Constant--;
            }
        } else if (atom1Constant < 0) {
            while (atom1Constant != 0) {
                atom2Constant = generatorContractor.next(atom2Constant);
                atom1Constant++;
            }
        }

        if (atom1Operator.equals(InequalityOperator.LT) && atom2Operator.equals(InequalityOperator.GT)) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GT, generatorContractor.next(atom2Constant));
        } else if ((atom1Operator.equals(InequalityOperator.LT) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.GEQ)))
                || (atom1Operator.equals(InequalityOperator.LEQ) && atom2Operator.equals(InequalityOperator.GT))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.GT))) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GT, atom2Constant);
        }  else if ((atom1Operator.equals(InequalityOperator.LEQ) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.GEQ)))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.GEQ))) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GEQ, atom2Constant);
        } else if ((atom1Operator.equals(EqualityOperator.NEQ) && atom2Operator.equals(EqualityOperator.EQ))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.NEQ))) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, EqualityOperator.NEQ, atom2Constant);
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.EQ)) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, EqualityOperator.EQ, atom2Constant);
        } else if ((atom1Operator.equals(InequalityOperator.GEQ) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.LEQ)))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.LEQ))) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LEQ, atom2Constant);
        } else if ((atom1Operator.equals(InequalityOperator.GT) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.LEQ)))
                || (atom1Operator.equals(InequalityOperator.GEQ) && atom2Operator.equals(InequalityOperator.LT))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.LT))) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LT, atom2Constant);
        } else if (atom1Operator.equals(InequalityOperator.GT) && atom2Operator.equals(InequalityOperator.LT)) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LT, generatorContractor.previous(atom2Constant));
        }

        return AbstractAtom.ALWAYS_TRUE;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyVariableSetOrdinalAtoms(String generator, OrdinalContractor<T> generatorContractor, VariableOrdinalAtom<?> atom1, SetOrdinalAtom<?> atom2) {

        boolean atom1GeneratorIsLeft = atom1.getLeftAttribute().equals(generator);

        ComparableOperator atom1Operator = atom1GeneratorIsLeft ? atom1.getOperator() : atom1.getOperator().getReversedOperator();
        String atom1Variable = atom1GeneratorIsLeft ? atom1.getRightAttribute() : atom1.getLeftAttribute();
        int atom1Constant = atom1GeneratorIsLeft ? atom1.getRightConstant() : atom1.getRightConstant() * -1;

        SetOperator atom2Operator = atom2.getOperator();
        Set<T> atom2Constants = (Set<T>) atom2.getConstants();

        if (atom1Constant > 0) {
            while (atom1Constant != 0) {
                atom2Constants = atom2Constants.stream().map(generatorContractor::previous).collect(Collectors.toSet());
                atom1Constant--;
            }
        } else if (atom1Constant < 0) {
            while (atom1Constant != 0) {
                atom2Constants = atom2Constants.stream().map(generatorContractor::next).collect(Collectors.toSet());
                atom1Constant++;
            }
        }

        if (atom1Operator.equals(InequalityOperator.LT) && atom2Operator.equals(SetOperator.IN)) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GT, Collections.min(atom2Constants));
        } else if (atom1Operator.equals(InequalityOperator.LEQ) && atom2Operator.equals(SetOperator.IN)) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.GEQ, Collections.min(atom2Constants));
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(SetOperator.IN)) {
            return new SetOrdinalAtom<>(generatorContractor, atom1Variable, SetOperator.IN, atom2Constants);
        } else if ((atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(SetOperator.NOTIN))
                || (atom1Operator.equals(EqualityOperator.NEQ) && atom2Operator.equals(SetOperator.IN) && atom2Constants.size() == 1)) {
            return new SetOrdinalAtom<>(generatorContractor, atom1Variable, SetOperator.NOTIN, atom2Constants);
        } else if (atom1Operator.equals(InequalityOperator.GEQ) && atom2Operator.equals(SetOperator.IN)) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LEQ, Collections.max(atom2Constants));
        } else if (atom1Operator.equals(InequalityOperator.GT) && atom2Operator.equals(SetOperator.IN)) {
            return new ConstantOrdinalAtom<>(generatorContractor, atom1Variable, InequalityOperator.LT, Collections.max(atom2Constants));
        }

        return AbstractAtom.ALWAYS_TRUE;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyVariableSetNominalAtoms(String generator, NominalContractor<T> generatorContractor, VariableNominalAtom<?> atom1, SetNominalAtom<?> atom2) {

        boolean atom1GeneratorIsLeft = atom1.getLeftAttribute().equals(generator);

        ComparableOperator atom1Operator = atom1.getOperator();
        String atom1Attribute = atom1GeneratorIsLeft ? atom1.getRightAttribute() : atom1.getLeftAttribute();

        SetOperator atom2Operator = atom2.getOperator();
        Set<T> atom2Constants = (Set<T>) atom2.getConstants();

        if ((atom1Operator.equals(EqualityOperator.NEQ) && atom2Operator.equals(SetOperator.IN) && atom2Constants.size() == 1)
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(SetOperator.NOTIN))) {
            return new SetNominalAtom<>(generatorContractor, atom1Attribute, SetOperator.NOTIN, atom2Constants);
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(SetOperator.IN)) {
            return new SetNominalAtom<>(generatorContractor, atom1Attribute, SetOperator.IN, atom2Constants);
        }

        return AbstractAtom.ALWAYS_TRUE;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyTwoConstantOrdinalAtoms(OrdinalContractor<T> generatorContractor, ConstantOrdinalAtom<?> atom1, ConstantOrdinalAtom<?> atom2) {

        ComparableOperator atom1Operator = atom1.getOperator();
        T atom1Constant = (T) atom1.getConstant();

        ComparableOperator atom2Operator = atom2.getOperator();
        T atom2Constant = (T) atom2.getConstant();

        boolean result = true;

        if (atom1Operator.equals(InequalityOperator.LT) && atom2Operator.equals(InequalityOperator.GT)) {
            result = InequalityOperator.GT.test(atom1Constant, generatorContractor.next(atom2Constant));
        } else if ((atom1Operator.equals(InequalityOperator.LT) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.GEQ)))
                || (atom1Operator.equals(InequalityOperator.LEQ) && atom2Operator.equals(InequalityOperator.GT))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.GT))) {
            result = InequalityOperator.GT.test(atom1Constant, atom2Constant);
        } else if ((atom1Operator.equals(InequalityOperator.LEQ) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.GEQ)))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.GEQ))) {
            result = InequalityOperator.GEQ.test(atom1Constant, atom2Constant);
        } else if ((atom1Operator.equals(EqualityOperator.NEQ) && atom2Operator.equals(EqualityOperator.EQ))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.NEQ))) {
            result = EqualityOperator.NEQ.test(atom1Constant, atom2Constant);
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(EqualityOperator.EQ)) {
            result = EqualityOperator.EQ.test(atom1Constant, atom2Constant);
        } else if ((atom1Operator.equals(InequalityOperator.GEQ) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.LEQ)))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.LEQ))) {
            result = InequalityOperator.LEQ.test(atom1Constant, atom2Constant);
        } else if ((atom1Operator.equals(InequalityOperator.GT) && (atom2Operator.equals(EqualityOperator.EQ) || atom2Operator.equals(InequalityOperator.LEQ)))
                || (atom1Operator.equals(InequalityOperator.GEQ) && atom2Operator.equals(InequalityOperator.LT))
                || (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(InequalityOperator.LT))) {
            result = InequalityOperator.LT.test(atom1Constant, atom2Constant);
        } else if (atom1Operator.equals(InequalityOperator.GT) && atom2Operator.equals(InequalityOperator.LT)) {
            result = InequalityOperator.LT.test(atom1Constant, generatorContractor.previous(atom2Constant));
        }

        if (result) {
            return AbstractAtom.ALWAYS_TRUE;
        }

        return AbstractAtom.ALWAYS_FALSE;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyTwoSetAtoms(SetAtom<?, ?> atom1, SetAtom<?, ?> atom2) {

        SetOperator atom1Operator = atom1.getOperator();
        Set<T> atom1Constants = (Set<T>) atom1.getConstants();

        SetOperator atom2Operator = atom2.getOperator();
        Set<T> atom2Constants = (Set<T>) atom2.getConstants();

        boolean result = true;

        if (atom1Operator.equals(SetOperator.NOTIN) && atom2Operator.equals(SetOperator.IN)) {
            result = atom2Constants.stream().anyMatch(c -> !atom1Constants.contains(c));
        } else if (atom1Operator.equals(SetOperator.IN) && atom2Operator.equals(SetOperator.NOTIN)) {
            result = atom1Constants.stream().anyMatch(c -> !atom2Constants.contains(c));
        } else if (atom1Operator.equals(SetOperator.IN) && atom2Operator.equals(SetOperator.IN)) {
            result = atom1Constants.stream().anyMatch(atom2Constants::contains);
        }

        if (result) {
            return AbstractAtom.ALWAYS_TRUE;
        }

        return AbstractAtom.ALWAYS_FALSE;

    }

    private <T extends Comparable<? super T>> AbstractAtom<?, ?, ?> implyConstantSetOrdinalAtoms(OrdinalContractor<T> generatorContractor, ConstantOrdinalAtom<?> atom1, SetOrdinalAtom<?> atom2) {

        ComparableOperator atom1Operator = atom1.getOperator();
        T atom1Constant = (T) atom1.getConstant();

        SetOperator atom2Operator = atom2.getOperator();
        Set<T> atom2Constants = (Set<T>) atom2.getConstants();

        boolean result = true;

        if (atom1Operator.equals(InequalityOperator.LT) && atom2Operator.equals(SetOperator.IN)) {
            result = InequalityOperator.GT.test(atom1Constant, Collections.min(atom2Constants));
        } else if (atom1Operator.equals(InequalityOperator.LEQ) && atom2Operator.equals(SetOperator.IN)) {
            result = InequalityOperator.GEQ.test(atom1Constant, Collections.min(atom2Constants));
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(SetOperator.IN)) {
            result = atom2Constants.contains(atom1Constant);
        } else if (atom1Operator.equals(EqualityOperator.EQ) && atom2Operator.equals(SetOperator.NOTIN)) {
            result = !atom2Constants.contains(atom1Constant);
        } else if (atom1Operator.equals(EqualityOperator.NEQ) && atom2Operator.equals(SetOperator.IN)) {
            result = !(atom2Constants.size() == 1 && atom2Constants.contains(atom1Constant));
        } else if (atom1Operator.equals(InequalityOperator.GEQ) && atom2Operator.equals(SetOperator.IN)) {
            result = InequalityOperator.LEQ.test(atom1Constant, Collections.max(atom2Constants));
        } else if (atom1Operator.equals(InequalityOperator.GT) && atom2Operator.equals(SetOperator.IN)) {
            result = InequalityOperator.LT.test(atom1Constant, Collections.max(atom2Constants));
        }

        if (result) {
            return AbstractAtom.ALWAYS_TRUE;
        }

        return AbstractAtom.ALWAYS_TRUE;

    }

    /**
     * UTILITY CLASSES
     **/

    private static class OrdinalSequence<T extends Comparable<? super T>> {

        private final Interval<T> lastGeneratorInterval;
        private final Interval<T> secondLastGeneratorInterval;
        private final OrdinalImpliedRuleMapping<T> impliedRuleMapping;

        public OrdinalSequence(OrdinalImpliedRuleMapping<T> impliedRuleMapping, Interval<T> lastGeneratorInterval, Interval<T> secondLastGeneratorInterval) {
            this.lastGeneratorInterval = lastGeneratorInterval;
            this.secondLastGeneratorInterval = secondLastGeneratorInterval;
            this.impliedRuleMapping = impliedRuleMapping;
        }

        public OrdinalSequence(OrdinalImpliedRuleMapping<T> impliedRuleMapping, Interval<T> lastGeneratorInterval) {
            this(impliedRuleMapping, lastGeneratorInterval, null);
        }

        public Interval<T> getLastGeneratorInterval() {
            return lastGeneratorInterval;
        }

        public OrdinalImpliedRuleMapping<T> getImpliedRuleMapping() {
            return impliedRuleMapping;
        }

        public Interval<T> getSecondLastGeneratorInterval() {
            return secondLastGeneratorInterval;
        }
    }

    private static abstract class ImpliedRuleMapping<V, M extends RuleValueMapping<V>> {

        private final String generator;
        private final Set<M> contributorsMapping;
        private final Set<M> impliedRulesMapping;

        public ImpliedRuleMapping(String generator, Set<SigmaRule> contributors, Set<SigmaRule> impliedRules) {
            this.generator = generator;
            this.contributorsMapping = getValueRuleMapping(contributors);
            this.impliedRulesMapping = getValueRuleMapping(impliedRules);
        }

        public String getGenerator() {
            return this.generator;
        }

        public Set<SigmaRule> getContributors() {
            return this.contributorsMapping.stream().map(M::getRule).collect(Collectors.toSet());
        }

        public Set<SigmaRule> getImpliedRules() {
            return this.impliedRulesMapping.stream().map(M::getRule).collect(Collectors.toSet());
        }

        public Set<V> getCoveredGeneratorValues() {
            return this.impliedRulesMapping.stream().map(RuleValueMapping::getCoveredAttributeValues).collect(Collectors.toSet());
        }

        protected abstract Set<M> getValueRuleMapping(Set<SigmaRule> rules);

        protected abstract boolean addsNewValuesTo(ImpliedRuleMapping<V, M> other);

    }

    private static class OrdinalImpliedRuleMapping<T extends Comparable<? super T>> extends ImpliedRuleMapping<Set<Interval<T>>, OrdinalRuleValueMapping<T>> {

        public OrdinalImpliedRuleMapping(String generator, Set<SigmaRule> contributors, Set<SigmaRule> impliedRules) {
            super(generator, contributors, impliedRules);
        }

        @Override
        protected Set<OrdinalRuleValueMapping<T>> getValueRuleMapping(Set<SigmaRule> rules) {
            Set<OrdinalRuleValueMapping<T>> ordinalValueRuleMappingSet = new HashSet<>();

            for (SigmaRule rule : rules) {
                ordinalValueRuleMappingSet.add(new OrdinalRuleValueMapping<>(getGenerator(), rule));
            }

            return ordinalValueRuleMappingSet;
        }

        @Override
        protected boolean addsNewValuesTo(ImpliedRuleMapping<Set<Interval<T>>, OrdinalRuleValueMapping<T>> other) {
            Set<Interval<T>> thisCoveredValues = this.getCoveredGeneratorValues().stream().flatMap(Set::stream).collect(Collectors.toSet());
            Set<Interval<T>> otherCoveredValues = other.getCoveredGeneratorValues().stream().flatMap(Set::stream).collect(Collectors.toSet());

            return thisCoveredValues.stream().anyMatch(v1 -> otherCoveredValues.stream().noneMatch(v2 -> v1.allenStarts(v2) || v1.allenDuring(v2) || v1.allenFinishes(v2) || v1.allenEquals(v2)));
        }

    }

    private static class NominalImpliedRuleMapping<T extends Comparable<? super T>> extends ImpliedRuleMapping<NominalRuleValues<T>, NominalRuleValueMapping<T>> {

        public NominalImpliedRuleMapping(String generator, Set<SigmaRule> contributors, Set<SigmaRule> impliedRules) {
            super(generator, contributors, impliedRules);
        }

        @Override
        protected Set<NominalRuleValueMapping<T>> getValueRuleMapping(Set<SigmaRule> rules) {
            Set<NominalRuleValueMapping<T>> nominalValueRuleMappingSet = new HashSet<>();

            for (SigmaRule rule : rules) {
                nominalValueRuleMappingSet.add(new NominalRuleValueMapping<>(getGenerator(), rule));
            }

            return nominalValueRuleMappingSet;
        }

        public NominalRuleValues<T> getCoveredGeneratorValuesFlattened() {
            Optional<NominalRuleValues<T>> coveredValues = this.getCoveredGeneratorValues().stream().filter(NominalRuleValues::hasCoveredValues).findAny();
            return coveredValues.orElseGet(() -> new NominalRuleValues<>(null, this.getCoveredGeneratorValues().stream().filter(NominalRuleValues::hasUncoveredValues).map(NominalRuleValues::getUncoveredValues).flatMap(Set::stream).collect(Collectors.toSet())));
        }

        @Override
        protected boolean addsNewValuesTo(ImpliedRuleMapping<NominalRuleValues<T>, NominalRuleValueMapping<T>> other) {

            NominalRuleValues<T> thisCoveredGeneratorValues = getCoveredGeneratorValuesFlattened();

            Optional<NominalRuleValues<T>> coveredValues = other.getCoveredGeneratorValues().stream().filter(NominalRuleValues::hasCoveredValues).findAny();
            NominalRuleValues<T> otherCoveredGeneratorValues = coveredValues.orElseGet(() -> new NominalRuleValues<>(null, other.getCoveredGeneratorValues().stream().filter(NominalRuleValues::hasUncoveredValues).map(NominalRuleValues::getUncoveredValues).flatMap(Set::stream).collect(Collectors.toSet())));

            if (thisCoveredGeneratorValues.hasCoveredValues()) {
                if (otherCoveredGeneratorValues.hasCoveredValues()) {
                    return !thisCoveredGeneratorValues.getCoveredValues().containsAll(otherCoveredGeneratorValues.getCoveredValues());
                    } else {
                    return otherCoveredGeneratorValues.getUncoveredValues().stream().anyMatch(uv -> thisCoveredGeneratorValues.getCoveredValues().contains(uv));
                }
            } else {
                if (otherCoveredGeneratorValues.hasCoveredValues()) {
                    return thisCoveredGeneratorValues.getUncoveredValues().stream().anyMatch(uv -> otherCoveredGeneratorValues.getCoveredValues().contains(uv));
                } else {
                    return !thisCoveredGeneratorValues.getUncoveredValues().containsAll(otherCoveredGeneratorValues.getUncoveredValues());
                }
            }

        }

    }

}
