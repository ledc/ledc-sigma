package be.ugent.ledc.sigma.sufficientsetgeneration.implication;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleException;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetException;
import be.ugent.ledc.sigma.repair.RepairOperations;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * A SimpleManager is a manager that manages implication steps where
 * the underlying reasoning is done in terms of sets of non-permitted values
 * @author abronsel
 */
public class SimpleManager implements ImplicationManager
{
    @Override
    public AbstractRuleImplicator getRuleImplicator()
    {
        return new SimpleImplicator();
    }

    @Override
    public void verify(SigmaRuleset ruleset) throws SigmaRulesetException
    {
        if(ruleset
            .getContractors()
            .values()
            .stream()
            .anyMatch(ctr -> !ctr.name().equals(SigmaContractorFactory.STRING.name())))
        {
            throw new SigmaRulesetException("Simple implication expects sigma rules with nominal atoms.");
        }
    }
    
    @Override
    /**
     * Transforms rules by formulating all atoms in terms of set atoms and then performing a
     * folding algorithm on that representation to compress the rules as much as possible.
     */
    public SigmaRuleset transformRuleset(SigmaRuleset ruleset)
    {
        Set<SigmaRule> transformedRules = new HashSet<>();
        
        Map<String, Set> permittedValueMap = buildPermittedValues(ruleset);
        
        Set<SigmaRule> nonDomainRules = ruleset
               .getRules()
               .stream()
               .filter(rule -> rule.getInvolvedAttributes().size() > 1)
               .collect(Collectors.toSet());
            
        for(SigmaRule rule: nonDomainRules)
        {
            Set<AbstractAtom<?,?,?>> atoms = new HashSet<>();
            
            String tautologyAttribute = null;
            
            for(String a: rule.getInvolvedAttributes())
            {
                SetAtom nextAtom = convert(
                    rule,
                    a,
                    permittedValueMap.get(a),
                    ruleset.getContractors().get(a));
                
                if(nextAtom.getConstants().isEmpty())
                    tautologyAttribute = a;
                
                atoms.add(nextAtom);
            }
            
            if(tautologyAttribute != null)
                Logger
                    .getLogger(SimpleManager.class.getName())
                    .log(Level.WARNING, "Removed sigma rule {0} from transformed set."
                        + "\nCause: this rule is a tautology."
                        + "\nRe-check constants in atoms for attribute ''{1}''",
                    new Object[]{rule.toString(), tautologyAttribute});
            else
                transformedRules.add(new SigmaRule(atoms));
        }
        
        
        Set<SigmaRule> foldedRules = new SimpleFolder(
                ruleset.getContractors(),
                permittedValueMap)
            .foldRules(transformedRules);
        
        //Add domain constraints
        for(String a: permittedValueMap.keySet())
        {
            if(ruleset.getContractors().get(a) instanceof NominalContractor)
            {
                transformedRules.add(
                    new SigmaRule(
                        new SetNominalAtom(
                            (NominalContractor) ruleset.getContractors().get(a),
                            a,
                            SetOperator.NOTIN,
                            permittedValueMap.get(a)
                        )
                    )
                );
            }
            else if(ruleset.getContractors().get(a) instanceof OrdinalContractor)
            {
                transformedRules.add(
                    new SigmaRule(
                        new SetOrdinalAtom(
                            (OrdinalContractor) ruleset.getContractors().get(a),
                            a,
                            SetOperator.NOTIN,
                            permittedValueMap.get(a)
                        )
                    )
                );
            }
            else
            {
                throw new SigmaRuleException("Unsupported type of contractor");
            }
        }
        
        return new SigmaRuleset(
            ruleset.getContractors(),
            foldedRules
        );
        
    }

    private SetAtom convert(SigmaRule rule, String a, Set<?> permittedValues, SigmaContractor contractor)
    {
        Set<AbstractAtom<?, ?, ?>> atoms = rule
            .getAtoms()
            .stream()
            .filter(atom -> atom
                .getAttributes()
                .anyMatch(aa -> aa.equals(a)))
            .collect(Collectors.toSet());
                
        Set<Object> restrictedValue = permittedValues
            .stream()
            .map(v -> new DataObject().set(a,v))
            .filter(o -> atoms
                    .stream()
                    .allMatch(atom -> atom.test(o)))
            .map(o -> o.get(a))
            .collect(Collectors.toSet());
        
        if(contractor instanceof NominalContractor)
        {
            return new SetNominalAtom(
                (NominalContractor) contractor,
                a,
                SetOperator.IN,
                restrictedValue);
        }
        else if(contractor instanceof OrdinalContractor)
        {
            return new SetOrdinalAtom(
                (OrdinalContractor) contractor,
                a,
                SetOperator.IN,
                restrictedValue);
        }
        
        throw new SigmaRuleException("Unknown contractor type");
        
    }
    
    private Map<String, Set> buildPermittedValues(SigmaRuleset ruleset)
    {
        Map<String, Set> permittedValues = new HashMap<>();
        
        for(String a: ruleset.getContractors().keySet())
        {
            Iterator valueIterator = RepairOperations
                .getPermittedValues(a, ruleset)
                .iterator();
            
            Set values = new HashSet();
            
            while(valueIterator.hasNext())
            {
                values.add(valueIterator.next());
            }
            
            permittedValues.put(a, values);
        }
        
        return permittedValues;
    }
    
}
