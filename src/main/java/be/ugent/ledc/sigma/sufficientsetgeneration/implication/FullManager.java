package be.ugent.ledc.sigma.sufficientsetgeneration.implication;

import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetException;
import java.util.stream.Collectors;

public class FullManager implements ImplicationManager
{
    @Override
    public void verify(SigmaRuleset ruleset) throws SigmaRulesetException{}

    @Override
    public AbstractRuleImplicator getRuleImplicator()
    {
        return new FullRuleImplicator();
    }

    @Override
    public SigmaRuleset transformRuleset(SigmaRuleset ruleset)
    {
        return new SigmaRuleset(
            ruleset.getContractors(),
            ruleset
                .stream()
                .flatMap(r -> r.simplifyAndReduce(true)
                    .stream())
                .collect(Collectors.toSet())
        );
    }
}
