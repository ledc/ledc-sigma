package be.ugent.ledc.sigma.sufficientsetgeneration;

import be.ugent.ledc.core.datastructures.rules.Rule;
import be.ugent.ledc.core.datastructures.rules.RuleSet;

import java.util.Set;

public interface SufficientSetGenerator<R extends Rule<?>, S extends RuleSet<?,R>> {

    Set<R> generateSufficientSet(S ruleset);

}
