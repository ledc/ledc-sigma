package be.ugent.ledc.sigma.sufficientsetgeneration.implication;


import be.ugent.ledc.core.datastructures.rules.Rule;
import java.util.Set;
import java.util.stream.Collectors;

public interface RuleImplicator<R extends Rule<?>> {

    default Set<R> getCandidateContributors(Set<R> rules, String generator) {
        return rules.stream().filter(r -> r.involves(generator)).collect(Collectors.toSet());
    }

}
