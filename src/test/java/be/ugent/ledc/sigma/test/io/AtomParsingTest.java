package be.ugent.ledc.sigma.test.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.AtomException;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.VariableOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.ComparableOperator;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.io.AtomParser;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class AtomParsingTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void setAtomNotInParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> expected = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.NOTIN,
            Stream.of("Yes", "No").collect(Collectors.toSet())
        );
        
        assertEquals(expected, AtomParser.parseAtom("a notin {'No', 'Yes'}", contractors));
        assertEquals(expected, AtomParser.parseAtom("anotin {'No', 'Yes'}", contractors));
    }
    
    @Test
    public void setAtomInParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> expected = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.IN,
            Stream.of("Yes", "No").collect(Collectors.toSet())
        );
        
        assertEquals(expected, AtomParser.parseAtom("a IN {'No', 'Yes'}", contractors));
        assertEquals(expected, AtomParser.parseAtom("a in{'No', 'Yes'}", contractors));
    }
    
    @Test
    public void constantAtomEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, EqualityOperator> expected = new ConstantNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.EQ,
            "Yes"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a == 'Yes'", contractors));
        assertEquals(expected, AtomParser.parseAtom("a=='Yes'", contractors));
    }
    
    @Test
    public void constantAtomNotEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, EqualityOperator> expected = new ConstantNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.NEQ,
            "Yes"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a != 'Yes'", contractors));
        assertEquals(expected, AtomParser.parseAtom("a!='Yes'", contractors));
    }
    
    @Test
    public void constantAtomLowerThanParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new ConstantOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LT,
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a < 3", contractors));
        assertEquals(expected, AtomParser.parseAtom("a<3", contractors));
    }
    
    @Test
    public void constantAtomLowerThanEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new ConstantOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LEQ,
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a<=3", contractors));
    }
    
    @Test
    public void constantAtomGreaterThanEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new ConstantOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GEQ,
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a >= 3", contractors));
        assertEquals(expected, AtomParser.parseAtom("a>=3", contractors));
    }
    
    @Test
    public void constantAtomGreaterThanParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new ConstantOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GT,
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a > 3", contractors));
        assertEquals(expected, AtomParser.parseAtom("a>3", contractors));
    }
    
    @Test
    public void variableAtomEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            EqualityOperator.EQ,
            "b"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a == b", contractors));
        assertEquals(expected, AtomParser.parseAtom("a==b", contractors));
    }
    
    @Test
    public void variableAtomEqualsParseWithRightShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            EqualityOperator.EQ,
            "b",
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a == S^3(b)", contractors));
        assertEquals(expected, AtomParser.parseAtom("a==S^3(b)", contractors));
    }
    
    @Test
    public void variableAtomEqualsParseWithLeftShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            EqualityOperator.EQ,
            "b",
            -5
        );
        
        assertEquals(expected, AtomParser.parseAtom("a == S^-5(b)", contractors));
        assertEquals(expected, AtomParser.parseAtom("a==S^-5(b)", contractors));
    }
    
    @Test
    public void variableAtomNotEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            EqualityOperator.NEQ,
            "b"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a != b", contractors));
        assertEquals(expected, AtomParser.parseAtom("a!=b", contractors));
    }
    
    @Test
    public void variableAtomNotEqualsParseWithRightShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            EqualityOperator.NEQ,
            "b",
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a != S^3(b)", contractors));
        assertEquals(expected, AtomParser.parseAtom("a!=S^3(b)", contractors));
    }
    
    @Test
    public void variableAtomNotEqualsParseWithLeftShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            EqualityOperator.NEQ,
            "b",
            -5
        );
        
        assertEquals(expected, AtomParser.parseAtom("a != S^-5(b)", contractors));
    }
    
    @Test
    public void variableAtomLowerThanParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LT,
            "b"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a < b", contractors));
        assertEquals(expected, AtomParser.parseAtom("a<b", contractors));
    }
    
    @Test
    public void variableAtomLowerThanParseWithRightShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LT,
            "b",
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a < S^3(b)", contractors));
        assertEquals(expected, AtomParser.parseAtom("a<S^3(b)", contractors));
    }
    
    @Test
    public void variableAtomLowerThanParseWithLeftShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LT,
            "b",
            -5
        );
        
        assertEquals(expected, AtomParser.parseAtom("a < S^-5(b)", contractors));
        assertEquals(expected, AtomParser.parseAtom("a < S^ -5 (b)", contractors));
    }
    
    @Test
    public void variableAtomLowerThanEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LEQ,
            "b"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a <= b", contractors));
    }
    
    @Test
    public void variableAtomLowerThanParseEqualsWithRightShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LEQ,
            "b",
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a <= S^ 3 ( b )", contractors));
        assertEquals(expected, AtomParser.parseAtom("a<=S^3(b)", contractors));
    }
    
    @Test
    public void variableAtomLowerThanEqualsParseWithLeftShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LEQ,
            "b",
            -5
        );
        
        assertEquals(expected, AtomParser.parseAtom("a <=  S^-5(b)", contractors));
    }
    
    @Test
    public void variableAtomGreaterThanEqualsParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GEQ,
            "b"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a >= b", contractors));
        assertEquals(expected, AtomParser.parseAtom("a>=b", contractors));
    }
    
    @Test
    public void variableAtomGreaterThanEqualsParseWithRightShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GEQ,
            "b",
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a >= S^3(b)", contractors));
    }
    
    @Test
    public void variableAtomGreaterThanEqualsParseWithLeftShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GEQ,
            "b",
            -5
        );
        
        assertEquals(expected, AtomParser.parseAtom("a >=  S^-5( b )", contractors));
        assertEquals(expected, AtomParser.parseAtom("a>=S^-5(b)", contractors));
    }
    
    @Test
    public void variableAtomGreaterThanParseTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GT,
            "b"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a > b", contractors));
    }
    
    @Test
    public void variableAtomGreaterThanParseWithRightShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GT,
            "b",
            3
        );
        
        assertEquals(expected, AtomParser.parseAtom("a >S^3( b)", contractors));
        assertEquals(expected, AtomParser.parseAtom("a>S^3(b)", contractors));
    }
    
    @Test
    public void variableAtomGreaterThanParseWithLeftShiftTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        AbstractAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> expected = new VariableOrdinalAtom<>
        (
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GT,
            "b",
            -5
        );
        
        assertEquals(expected, AtomParser.parseAtom("a >  S^-5(b)", contractors));
        assertEquals(expected, AtomParser.parseAtom("a> S^-5(b)", contractors));
    }
    
    @Test
    public void stringParsingWithQuotesTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, EqualityOperator> expected = new ConstantNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.EQ,
            "Hell's Angel"
        );
        
        assertEquals(expected, AtomParser.parseAtom("a == 'Hell's Angel'", contractors));
    }
    
    @Test
    public void stringSetParsingWithQuotesTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> expected = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.IN,
            Stream.of("Hell's Angels", "Devil's time").collect(Collectors.toSet())
        );
        
        assertEquals(expected, AtomParser.parseAtom("a IN {'Devil's time', 'Hell's Angels'}", contractors));
    }
    
    @Test
    public void stringSetParsingWithCommaTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> expected = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.IN,
            Stream.of("Hello, Friend", "It's me, your buddy").collect(Collectors.toSet())
        );
        
        assertEquals(expected, AtomParser.parseAtom("a IN {'Hello, Friend', 'It's me, your buddy'}", contractors));
    }
    
    @Test
    public void stringQuotingFailureTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("y", SigmaContractorFactory.STRING);

        exceptionRule.expect(AtomException.class);
        exceptionRule.expectMessage("String constants must be surrounded with single quotes. Found: 'd'}" );
        AtomParser.parseAtom("y == 'd'}", contractors);
    }
}
