package be.ugent.ledc.sigma.test.implication;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetInverter;
import be.ugent.ledc.sigma.io.CPFParser;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class SigmaRulesetInverterTest {

    public boolean isEquivalent(Set<CPF> result, Set<CPF> expectedResult) {
        return result.size() == expectedResult.size() &&
                result.stream().allMatch(resultCPF -> expectedResult.stream().anyMatch(expectedResultCPF ->
                        expectedResultCPF.getAtoms().size() == resultCPF.getAtoms().size()
                                && expectedResultCPF.isEquivalentTo(resultCPF)));
    }

    @Test
    public void invertRulesetNoVariableNominalTest1() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a == '0'", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a == '0'", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableNominalTest2() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {'0','1','2'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == '1' & b == 1", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a IN {'0','2'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'0','1','2'} & b != 1", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableNominalTest3() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {'1','2','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT d == '1' & a != '2'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT b == '2' & a != '1'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT b == '1' & a == '1'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT c == '1' & a == '3'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT c == '2' & a != '3'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT d != '1' & a == '2'", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a IN {'1','3'} & b NOTIN {'1','2'} & c NOTIN {'1','2'} & d != '1'", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '1' & b != '1' & c != '2' & d != '1'", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '2' & b != '2' & c != '2' & d == '1'", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '3' & b != '2' & c != '1' & d != '1'", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableNominalTest4() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {'1','2','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a IN {'1','2'} & b IN {'1','2'}", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a == '3'", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'1','2','3'} & b NOTIN {'1','2'}", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableNominalTest5() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a IN {'1','2'} & b == '1'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a IN {'1','2','3'} & b == '1'", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a NOTIN {'1','2','3'}", contractors));
        expectedResult.add(CPFParser.parseCPF("b != '1'", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableNominalTest6() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a NOTIN {'1','2'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a NOTIN {'1','2','3'}", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a IN {'1','2'}", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableNominalTest7() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {'1','2','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("b IN {'1','2','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("c IN {'1','2','3','4','5','6','7','8','9'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a IN {'1','2'} & b IN {'2','3'} & c IN {'3','4','9'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == '3' & b IN {'2','3'} & c IN {'3','4','8'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a IN {'1','3'} & b == '1' & c IN {'3','5','6'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == '2' & b == '1' & c IN {'3','5','7'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == '1' & b == '2' & c IN {'2','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == '1' & b IN {'1','3'} & c IN {'1','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == '1' & c IN {'1','2','4','5','6','7','8','9'}", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a == '2' & b == '1' & c IN {'1','2','4','6','8','9'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '3' & b == '1' & c IN {'1','2','4','7','8','9'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'2','3'} & b == '1' & c IN {'1','2','4','8','9'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '2' & b IN {'1','2','3'} & c IN {'1','2','6','8'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '2' & b IN {'2','3'} & c IN {'1','2','5','6','7','8'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '3' & b IN {'2','3'} & c IN {'1','2','5','6','7','9'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'2','3'} & b IN {'2','3'} & c IN {'1','2','5','6','7'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '3' & b IN {'1','2','3'} & c IN {'1','2','7','9'}", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'2','3'} & b IN {'1','2','3'} & c IN {'1','2'}", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetVariableNominalTest1() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {'0','1','2'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a != '0' & a == b", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a == '0'", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'0','1','2'} & a != b", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetVariableNominalTest2() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);
        contractors.put("e", SigmaContractorFactory.STRING);
        contractors.put("f", SigmaContractorFactory.STRING);
        contractors.put("g", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == b & f == g", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == c & a != d", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a != e", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a != b & a != c & a == e & b != e & c != e", contractors));
        expectedResult.add(CPFParser.parseCPF("a != b & a == d & b != d & a == e & b != e & d == e", contractors));
        expectedResult.add(CPFParser.parseCPF("f != g & a != c & a == e & c != e", contractors));
        expectedResult.add(CPFParser.parseCPF("f != g & a == d & a == e & d == e", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetVariableNominalTest3() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);
        contractors.put("e", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {'1','2','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == b & a != '2'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a != c", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a IN {'1','2','3'} & a != b & a == c & c IN {'1','2','3'} & b != c", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '2' & c == '2'", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetVariableNominalTest4() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);
        contractors.put("e", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {'1','2','3'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT b == '1' & a == '1'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT c != '1' & a == '2'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == d", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == e", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a IN {'1','2','3'} & b != '1' & c == '1' & a != d & a != e", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'1','3'} & b != '1' & a != d & a != e", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {'2','3'} & c == '1' & a != d & a != e", contractors));
        expectedResult.add(CPFParser.parseCPF("a == '3' & d != '3' & e != '3'", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetNoVariableOrdinalTest1() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a > 0", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a > 0", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableOrdinalTest2() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= 5 & b == 1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 7 & b == 2", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 6 & c <= 2", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= 3 & c <= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a > 3 & a < 6 & b != 1", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 6 & c > 0 & b != 1", contractors));
        expectedResult.add(CPFParser.parseCPF("a == 6 & c > 2", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 7 & b != 1 & c > 2 ", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 5 & b != 2 & c > 2", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & c > 2 & b NOTIN {1,2}", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableOrdinalTest3() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 2 & a <= 6 & b == 1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 7 & b == 2", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 6 & c <= 2", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 1 & a <= 3 & c <= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a == 3 & d == 1", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a == 1 & c > 0", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 6 & b != 2 & c > 2", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 3 & a < 6 & b != 1", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 6 & a != 3 & b != 1 & c > 0", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 6 & b != 1 & c > 0 & d != 1", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 7 & a != 3 & b != 1 & c > 2", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 7 & b != 1 & c > 2 & d != 1", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a != 3 & b NOTIN {1,2} & c > 2", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & b NOTIN {1,2} & c > 2 & d != 1", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetNoVariableOrdinalTest4() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 8", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= 1 & a <= 7 & a NOTIN {2,3,6} & b == 2", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a NOTIN {2,4,5,7} & c == 3", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a IN {2,3,4,6} & d == 4", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a == 2 & d != 4", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {2,3,6} & c != 3 & d != 4", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {5,7} & b != 2", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {2,4,5,7} & b != 2 & d != 4", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 8 & a NOTIN {2,3,4,6} & b != 2 & c != 3", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a < 8 & b != 2 & c != 3 & d != 4", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetVariableOrdinalTest1() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= b & a == 1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a > c & a == 2", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a > b & a <= c & b < c", contractors));
        expectedResult.add(CPFParser.parseCPF("a > b & a != 2", contractors));
        expectedResult.add(CPFParser.parseCPF("a <= c & a != 1", contractors));
        expectedResult.add(CPFParser.parseCPF("a NOTIN {1,2}", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetVariableOrdinalTest2() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a > S^1(b)", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= c & a > d", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a < e", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a <= S^1(b) & a > c & c <= b & a >= e & e <= S^1(b)", contractors));
        expectedResult.add(CPFParser.parseCPF("a <= S^1(b) & a <= d & a >= e & e <= S^1(b) & e <= d", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetVariableOrdinalTest3() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);
        contractors.put("f", SigmaContractorFactory.INTEGER);
        contractors.put("g", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a > S^1(b)", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a < S^1(c) & a > d", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a < e & a > f", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a < g", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a <= S^1(b) & a >= S^1(c) & b >= c & a >= e & b >= S^-1(e) & a >= g & b >= S^-1(g)", contractors));
        expectedResult.add(CPFParser.parseCPF("a <= S^1(b) & a >= S^1(c) & b >= c & a <= f & c <= S^-1(f) & a >= g & b >= S^-1(g) & f >= g", contractors));
        expectedResult.add(CPFParser.parseCPF("a <= S^1(b) & a <= d & a >= e & b >= S^-1(e) & d >= e & a >= g & b >= S^-1(g) & d >= g", contractors));
        expectedResult.add(CPFParser.parseCPF("a <= S^1(b) & a <= d & a <= f & a >= g & b >= S^-1(g) & d >= g & f >= g", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetVariableOrdinalTest4() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a > S^1(b)", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a < S^1(c) & a > d", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a < e", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a > 0 & a <= S^1(b) & b >= 0 & a > c & b >= c & a >= e & b >= S^-1(e)", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a <= S^1(b) & b >= 0 & a <= d & d > 0 & a >= e & b >= S^-1(e) & d >= e", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetVariableOrdinalTest5() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a IN {1,3,5,7}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a > 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a < 8", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a IN {1,3,4,5,6,7} & b == 2", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a < c", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT a >= d & a <= e", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a IN {1,3,5,7} & b != 2 & a >= c & c <= 7 & a < d & c < d & d > 1", contractors));
        expectedResult.add(CPFParser.parseCPF("a IN {1,3,5,7} & b != 2 & a >= c & c <= 7 & a > e & e < 7", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetVotingTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("bills", SigmaContractorFactory.INTEGER);
        contractors.put("p1", SigmaContractorFactory.INTEGER);
        contractors.put("blanks", SigmaContractorFactory.INTEGER);
        contractors.put("p1a", SigmaContractorFactory.INTEGER);
        contractors.put("p1b", SigmaContractorFactory.INTEGER);
        contractors.put("p1c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("bills >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("p1 >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("blanks >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("p1a >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("p1b >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("p1c >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT bills < blanks", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT bills < p1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT p1a > p1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT p1b > p1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT p1c > p1", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("bills >= 0 & p1 >= 0 & blanks >= 0 & p1a >= 0 & p1b >= 0 & p1c >= 0 & bills >= blanks & bills >= p1 & p1a <= p1 & p1b <= p1 & p1c <= p1 & p1a <= bills & p1b <= bills & p1c <= bills", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetEudraCTTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("controlled", SigmaContractorFactory.STRING);
        contractors.put("randomised", SigmaContractorFactory.STRING);
        contractors.put("crossover", SigmaContractorFactory.STRING);
        contractors.put("single_blind", SigmaContractorFactory.STRING);
        contractors.put("active_comparator", SigmaContractorFactory.STRING);
        contractors.put("parallel_group", SigmaContractorFactory.STRING);
        contractors.put("arms", SigmaContractorFactory.STRING);
        contractors.put("open", SigmaContractorFactory.STRING);
        contractors.put("double_blind", SigmaContractorFactory.STRING);
        contractors.put("placebo", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("controlled IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("randomised IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("crossover IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("single_blind IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("active_comparator IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("parallel_group IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("arms IN {'0','1','2+'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("open IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("double_blind IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("placebo IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT open == 'Yes' & single_blind == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT open == 'Yes' & double_blind == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT single_blind == 'Yes' & double_blind == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT open == 'No' & single_blind == 'No' & double_blind == 'No'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT controlled == 'No' & placebo == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT controlled == 'No' & active_comparator == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT parallel_group == 'Yes' & crossover == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT arms IN {'0','1'} & placebo == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT arms IN {'0','1'} & active_comparator == 'Yes'", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("controlled == 'Yes' & randomised IN {'No','Yes'} & crossover IN {'No','Yes'} & single_blind == 'No' & active_comparator IN {'No','Yes'} & parallel_group == 'No' & arms == '2+' & open == 'No' & double_blind == 'Yes' & placebo IN {'No','Yes'}", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled == 'Yes' & randomised IN {'No','Yes'} & crossover == 'No' & single_blind == 'No' & active_comparator IN {'No','Yes'} & parallel_group IN {'No','Yes'} & arms == '2+' & open == 'No' & double_blind == 'Yes' & placebo IN {'No','Yes'}", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled IN {'No','Yes'} & randomised IN {'No','Yes'} & crossover IN {'No','Yes'} & single_blind == 'No' & active_comparator == 'No' & parallel_group == 'No' & arms IN {'0','1','2+'} & open == 'No' & double_blind == 'Yes' & placebo == 'No'", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled IN {'No','Yes'} & randomised IN {'No','Yes'} & crossover == 'No' & single_blind == 'No' & active_comparator == 'No' & parallel_group IN {'No','Yes'} & arms IN {'0','1','2+'} & open == 'No' & double_blind == 'Yes' & placebo == 'No'", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled == 'Yes' & randomised IN {'No','Yes'} & crossover IN {'No','Yes'} & single_blind == 'Yes' & active_comparator IN {'No','Yes'} & parallel_group == 'No' & arms == '2+' & open == 'No' & double_blind == 'No' & placebo IN {'No','Yes'}", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled == 'Yes' & randomised IN {'No','Yes'} & crossover == 'No' & single_blind == 'Yes' & active_comparator IN {'No','Yes'} & parallel_group IN {'No','Yes'} & arms == '2+' & open == 'No' & double_blind == 'No' & placebo IN {'No','Yes'}", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled IN {'No','Yes'} & randomised IN {'No','Yes'} & crossover IN {'No','Yes'} & single_blind == 'Yes' & active_comparator == 'No' & parallel_group == 'No' & arms IN {'0','1','2+'} & open == 'No' & double_blind == 'No' & placebo == 'No'", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled IN {'No','Yes'} & randomised IN {'No','Yes'} & crossover == 'No' & single_blind == 'Yes' & active_comparator == 'No' & parallel_group IN {'No','Yes'} & arms IN {'0','1','2+'} & open == 'No' & double_blind == 'No' & placebo == 'No'", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled == 'Yes' & randomised IN {'No','Yes'} & crossover IN {'No','Yes'} & single_blind == 'No' & active_comparator IN {'No','Yes'} & parallel_group == 'No' & arms == '2+' & open == 'Yes' & double_blind == 'No' & placebo IN {'No','Yes'}", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled == 'Yes' & randomised IN {'No','Yes'} & crossover == 'No' & single_blind == 'No' & active_comparator IN {'No','Yes'} & parallel_group IN {'No','Yes'} & arms == '2+' & open == 'Yes' & double_blind == 'No' & placebo IN {'No','Yes'}", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled IN {'No','Yes'} & randomised IN {'No','Yes'} & crossover IN {'No','Yes'} & single_blind == 'No' & active_comparator == 'No' & parallel_group == 'No' & arms IN {'0','1','2+'} & open == 'Yes' & double_blind == 'No' & placebo == 'No'", contractors));
        expectedResult.add(CPFParser.parseCPF("controlled IN {'No','Yes'} & randomised IN {'No','Yes'} & crossover == 'No' & single_blind == 'No' & active_comparator == 'No' & parallel_group IN {'No','Yes'} & arms IN {'0','1','2+'} & open == 'Yes' & double_blind == 'No' & placebo == 'No'", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetEudraCTSmallTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("open", SigmaContractorFactory.STRING);
        contractors.put("single_blind", SigmaContractorFactory.STRING);
        contractors.put("double_blind", SigmaContractorFactory.STRING);
        contractors.put("masking", SigmaContractorFactory.STRING);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("open IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("single_blind IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("double_blind IN {'No','Yes'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("masking IN {'0','1','2','>2'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT open == 'Yes' & single_blind == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT open == 'Yes' & double_blind == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT single_blind == 'Yes' & double_blind == 'Yes'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT single_blind == 'No' & masking == '1'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT double_blind == 'No' & masking == '2'", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT double_blind == 'Yes' & masking IN {'0','1'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT single_blind == 'Yes' & masking == '0'", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("open == 'No' & single_blind == 'No' & double_blind == 'Yes' & masking IN {'2','>2'}", contractors));
        expectedResult.add(CPFParser.parseCPF("open == 'No' & single_blind == 'No' & double_blind IN {'No','Yes'} & masking == '>2'", contractors));
        expectedResult.add(CPFParser.parseCPF("open == 'No' & single_blind == 'Yes' & double_blind == 'No' & masking IN {'1','>2'}", contractors));
        expectedResult.add(CPFParser.parseCPF("open == 'No' & single_blind IN {'No','Yes'} & double_blind == 'No' & masking == '>2'", contractors));
        expectedResult.add(CPFParser.parseCPF("open IN {'No','Yes'} & single_blind == 'No' & double_blind == 'No' & masking IN {'0','>2'}", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetStockTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("previous_close", SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);
        contractors.put("open_price", SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);
        contractors.put("last_trading_price", SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);
        contractors.put("change_percentage", SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);
        contractors.put("change_in_dollar", SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);
        contractors.put("today_low", SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);
        contractors.put("today_high", SigmaContractorFactory.BIGDECIMAL_TWO_DECIMALS);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("previous_close >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("open_price >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("last_trading_price >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("today_low >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("today_high >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT previous_close <= last_trading_price & change_percentage < 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT previous_close >= last_trading_price & change_percentage > 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT previous_close != last_trading_price & change_percentage == 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT open_price < today_low", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT open_price > today_high", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT last_trading_price < today_low", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT last_trading_price > today_high", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT change_percentage > 0 & change_in_dollar <= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT change_percentage < 0 & change_in_dollar >= 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT change_percentage == 0 & change_in_dollar != 0", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("previous_close > 0 & open_price >= 0 & last_trading_price >= 0 & today_low >= 0 & today_high >= 0 & previous_close > last_trading_price & change_percentage < 0 & open_price >= today_low & open_price <= today_high & today_low <= today_high & last_trading_price >= today_low & last_trading_price <= today_high & previous_close > today_low & change_in_dollar < 0", contractors));
        expectedResult.add(CPFParser.parseCPF("previous_close >= 0 & open_price >= 0 & last_trading_price > 0 & today_low >= 0 & today_high >= 0 & previous_close < last_trading_price & change_percentage > 0 & open_price >= today_low & open_price <= today_high & today_low <= today_high & last_trading_price >= today_low & last_trading_price <= today_high & previous_close < today_high & change_in_dollar > 0", contractors));
        expectedResult.add(CPFParser.parseCPF("previous_close >= 0 & open_price >= 0 & last_trading_price >= 0 & today_low >= 0 & today_high >= 0 & previous_close == last_trading_price & change_percentage == 0 & open_price >= today_low & open_price <= today_high & today_low <= today_high & last_trading_price >= today_low & last_trading_price <= today_high & previous_close >= today_low & previous_close <= today_high & change_in_dollar == 0", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

    @Test
    public void invertRulesetEducationTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("education", SigmaContractorFactory.STRING);
        contractors.put("education_num", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("education_num >= 1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("education_num <= 16", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("education IN {'Masters','Prof-school','12th','11th','10th','Preschool','1st-4th','Some-college','9th','5th-6th','Doctorate','Assoc-voc','Assoc-acdm','Bachelors','HS-grad','7th-8th'}", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Preschool'} & education_num != 1", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'1st-4th'} & education_num != 2", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'5th-6th'} & education_num != 3", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'7th-8th'} & education_num != 4", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'9th'} & education_num != 5", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'10th'} & education_num != 6", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'11th'} & education_num != 7", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'12th'} & education_num != 8", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'HS-grad'} & education_num != 9", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Some-college'} & education_num != 10", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Assoc-voc'} & education_num != 11", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Assoc-acdm'} & education_num != 12", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Bachelors'} & education_num != 13", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Masters'} & education_num != 14", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Prof-school'} & education_num != 15", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT education in {'Doctorate'} & education_num != 16", contractors));
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("education_num == 16 & education == 'Doctorate'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 15 & education == 'Prof-school'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 14 & education == 'Masters'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 13 & education == 'Bachelors'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 12 & education == 'Assoc-acdm'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 11 & education == 'Assoc-voc'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 10 & education == 'Some-college'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 9 & education == 'HS-grad'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 8 & education == '12th'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 7 & education == '11th'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 6 & education == '10th'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 5 & education == '9th'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 4 & education == '7th-8th'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 3 & education == '5th-6th'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 2 & education == '1st-4th'", contractors));
        expectedResult.add(CPFParser.parseCPF("education_num == 1 & education == 'Preschool'", contractors));

        assertEquals(expectedResult, SigmaRulesetInverter.invert(ruleset));

    }

    @Test
    public void invertRulesetDSIAbstractTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.BIGDECIMAL_ONE_DECIMAL);
        contractors.put("b", SigmaContractorFactory.DATETIME_HOURS);
        contractors.put("c", SigmaContractorFactory.BIGDECIMAL_ONE_DECIMAL);
        contractors.put("d", SigmaContractorFactory.DATETIME_HOURS);

        Set<SigmaRule> sigmaRules = new HashSet<>();
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a > 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("a <= 350", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("c > 0", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("c <= 350", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT d == b & c != a", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT d <= b & c > S^10(a)", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT d <= b & c < S^-10(a)", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT d < S^24(b) & c > S^50(a)", contractors));
        sigmaRules.add(SigmaRuleParser.parseSigmaRule("NOT d < S^24(b) & c < S^-50(a)", contractors));

        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        Set<CPF> expectedResult = new HashSet<>();
        expectedResult.add(CPFParser.parseCPF("a > 0 & a <= 350 & c > 0 & c <= 350 & d >= S^24(b)", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a <= 350 & c > 0 & c <= 350 & d > b & c <= S^50(a) & c >= S^-50(a)", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a <= 350 & c > 0 & c <= 350 & d != b & c <= S^10(a) & c >= S^-10(a)", contractors));
        expectedResult.add(CPFParser.parseCPF("a > 0 & a <= 350 & c > 0 & c <= 350 & c == a", contractors));

        assertTrue(isEquivalent(SigmaRulesetInverter.invert(ruleset), expectedResult));

    }

}
