package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;


public class SetNominalAtomTest {

    @Test
    public void setNominalAtomIsAlwaysFalseTest() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>());
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void setNominalAtomIsNotAlwaysFalseTest1() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>());
        assertFalse(atom.isAlwaysFalse());

    }

    @Test
    public void setNominalAtomIsNotAlwaysFalseTest2() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2")));
        assertFalse(atom.isAlwaysFalse());

    }


    @Test
    public void setNominalAtomIsAlwaysTrueTest() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>());
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void setNominalAtomIsNotAlwaysTrueTest1() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>());
        assertFalse(atom.isAlwaysTrue());

    }

    @Test
    public void setNominalAtomIsNotAlwaysTrueTest2() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2")));
        assertFalse(atom.isAlwaysTrue());

    }

    @Test
    public void setNominalAtomAlwaysFalseSimplifyTest() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>());

        // Create expected simplified atom
        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        simplifiedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setNominalAtomTautologySimplifyTest() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>());

        // Create expected simplified atom
        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        simplifiedAtoms.add(AbstractAtom.ALWAYS_TRUE);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setNominalAtomInSimplifyTest() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3")));

        // Create expected simplified atoms
        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        SetNominalAtom<String> simplifiedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3")));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setNominalAtomNotInSimplifyTest() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList("1", "2", "3")));

        // Create expected simplified atom
        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        SetNominalAtom<String> simplifiedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList("1", "2", "3")));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setNominalAtomInSimplifiedTest1() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3")));
        assertTrue(atom.isSimplified());

    }

    @Test
    public void setNominalAtomInSimplifiedTest2() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>());
        assertFalse(atom.isSimplified());

    }

    @Test
    public void setNominalAtomNotInIsSimplifiedTest1() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList("1", "2", "3")));
        assertTrue(atom.isSimplified());

    }

    @Test
    public void setNominalAtomNotInIsSimplifiedTest2() {

        // Create atom
        SetNominalAtom<String> atom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>());
        assertFalse(atom.isSimplified());

    }


}
