package be.ugent.ledc.sigma.test.cpfs;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.formulas.CPFException;
import be.ugent.ledc.sigma.datastructures.formulas.CPFOperationsDep;
import be.ugent.ledc.sigma.io.CPFParser;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CPFReductionTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void notSimplifiedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a != S^1(b) & c == '1' & d < 4", contractors);

        exceptionRule.expect(CPFException.class);
        exceptionRule.expectMessage("Reducing a CPF can only be done if all atoms in CPF " + cpf + " are simplified.");

        CPFOperationsDep.reduce(cpf);

    }

    @Test
    public void reduceVariableNominalNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        CPF cpf = CPFParser.parseCPF("a != b & b != c & a == c", contractors);
        CPF reducedCPF1 = CPFParser.parseCPF("a != b & a == c", contractors);
        CPF reducedCPF2 = CPFParser.parseCPF("b != c & a == c", contractors);

        Set<CPF> reducedCPFs = new HashSet<>(Arrays.asList(reducedCPF1, reducedCPF2));

        assertTrue(reducedCPFs.contains(CPFOperationsDep.reduce(cpf)));

    }

    @Test
    public void reduceVariableNominalContradictionOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        CPF cpf = CPFParser.parseCPF("a != b & b == c & a == c", contractors);

        Set<AbstractAtom<?, ?, ?>> reducedAtoms = new HashSet<>();
        reducedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF reducedCPF = new CPF(reducedAtoms);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceVariableOrdinalNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < b & b > c & a < c", contractors);

        CPF reducedCPF = CPFParser.parseCPF("b > c & a < c", contractors);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceVariableOrdinalTransitiveLowerThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^2(b) & b < S^-1(c) & a < c", contractors);

        CPF reducedCPF = CPFParser.parseCPF("a < S^2(b) & b < S^-1(c)", contractors);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceVariableOrdinalTransitiveGreaterThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a > S^2(b) & b > S^-1(c) & a > S^2(c)", contractors);

        CPF reducedCPF = CPFParser.parseCPF("a > S^2(b) & b > S^-1(c)", contractors);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceVariableOrdinalIntersectionLowerThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < b & a < S^1(b) & a < S^-1(b)", contractors);

        CPF reducedCPF = CPFParser.parseCPF("a < S^-1(b)", contractors);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceVariableOrdinalIntersectionGreaterThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a > b & a > S^1(b) & a > S^-1(b)", contractors);

        CPF reducedCPF = CPFParser.parseCPF("a > S^1(b)", contractors);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceVariableOrdinalContradictionTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a < b & a > b", contractors);
        CPF cpf2 = CPFParser.parseCPF("a < b & a > S^-1(b)", contractors);
        CPF cpf3 = CPFParser.parseCPF("a < S^-1(b) & a > S^-2(b)", contractors);

        Set<AbstractAtom<?, ?, ?>> reducedAtoms = new HashSet<>();
        reducedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF reducedCPF = new CPF(reducedAtoms);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf2));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf3));

    }

    @Test
    public void reduceNonVariableOrdinalTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a <= 4 & a >= 1", contractors);
        CPF cpf2 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 2", contractors);
        CPF cpf3 = CPFParser.parseCPF("a <= 4 & a <= 2", contractors);
        CPF cpf4 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 1 & a >= 0", contractors);
        CPF cpf5 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 2 & a >= 0", contractors);
        CPF cpf6 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 6 & a >= 0", contractors);
        CPF cpf7 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 2", contractors);
        CPF cpf8 = CPFParser.parseCPF("a <= 4", contractors);
        CPF cpf9 = CPFParser.parseCPF("a >= 1", contractors);
        CPF cpf10 = CPFParser.parseCPF("a <= 4 & a >= 1 & a >= 2", contractors);
        CPF cpf11 = CPFParser.parseCPF("a >= 4 & a >= 2", contractors);
        CPF cpf12 = CPFParser.parseCPF("a <= 1 & a >= 1", contractors);
        CPF cpf13 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a IN {1,4}", contractors);
        CPF cpf14 = CPFParser.parseCPF("a NOTIN {1,2,3,4} & a NOTIN {1,3,5} & a NOTIN {6}", contractors);
        CPF cpf15 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a IN {1,4} & a NOTIN {1,2} & a NOTIN {2,3}", contractors);
        CPF cpf16 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a >= 3 & a <= 5", contractors);
        CPF cpf17 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a >= 0 & a <= 5", contractors);
        CPF cpf18 = CPFParser.parseCPF("a IN {1,2,3,4,6} & a IN {1,3,4,6,8,9} & a >= 0", contractors);
        CPF cpf19 = CPFParser.parseCPF("a IN {1,3,5} & a IN {1} & a >= 0", contractors);
        CPF cpf20 = CPFParser.parseCPF("a NOTIN {1,3,5} & a <= 0", contractors);
        CPF cpf21 = CPFParser.parseCPF("a NOTIN {1,3,4,5,7} & a >= 1 & a <= 8", contractors);
        CPF cpf22 = CPFParser.parseCPF("a NOTIN {1,2,3,8} & a >= 1 & a <= 8", contractors);
        CPF cpf23 = CPFParser.parseCPF("a NOTIN {1,2,3,5,8} & a >= 1 & a <= 8", contractors);
        CPF cpf24 = CPFParser.parseCPF("a NOTIN {1,2,3,8} & a >= 1", contractors);
        CPF cpf25 = CPFParser.parseCPF("a NOTIN {1,2,3,8} & a <= 8", contractors);
        CPF cpf26 = CPFParser.parseCPF("a NOTIN {1,2,4} & a >= 1 & a <= 4", contractors);

        CPF reducedCPF1 = CPFParser.parseCPF("a <= 4 & a >= 1", contractors);
        CPF reducedCPF2 = CPFParser.parseCPF("a >= 1 & a <= 2", contractors);
        CPF reducedCPF3 = CPFParser.parseCPF("a <= 2", contractors);
        CPF reducedCPF4 = CPFParser.parseCPF("a IN {1}", contractors);
        CPF reducedCPF5 = CPFParser.parseCPF("a >= 1 & a <= 2", contractors);
        CPF reducedCPF6 = CPFParser.parseCPF("a <= 4 & a >= 1", contractors);
        CPF reducedCPF7 = CPFParser.parseCPF("a >= 1 & a <= 2", contractors);
        CPF reducedCPF8 = CPFParser.parseCPF("a <= 4", contractors);
        CPF reducedCPF9 = CPFParser.parseCPF("a >= 1", contractors);
        CPF reducedCPF10 = CPFParser.parseCPF("a <= 4 & a >= 2", contractors);
        CPF reducedCPF11 = CPFParser.parseCPF("a >= 4", contractors);
        CPF reducedCPF12 = CPFParser.parseCPF("a IN {1}", contractors);
        CPF reducedCPF13 = CPFParser.parseCPF("a IN {1,4}", contractors);
        CPF reducedCPF14 = CPFParser.parseCPF("a NOTIN {1,2,3,4,5,6}", contractors);
        CPF reducedCPF15 = CPFParser.parseCPF("a IN {4}", contractors);
        CPF reducedCPF16 = CPFParser.parseCPF("a IN {3,4}", contractors);
        CPF reducedCPF17 = CPFParser.parseCPF("a IN {1,3,4}", contractors);
        CPF reducedCPF18 = CPFParser.parseCPF("a IN {1,3,4,6}", contractors);
        CPF reducedCPF19 = CPFParser.parseCPF("a IN {1}", contractors);
        CPF reducedCPF20 = CPFParser.parseCPF("a <= 0", contractors);
        CPF reducedCPF21 = CPFParser.parseCPF("a NOTIN {3,4,5,7} & a >= 2 & a <= 8", contractors);
        CPF reducedCPF22 = CPFParser.parseCPF("a >= 4 & a <= 7", contractors);
        CPF reducedCPF23 = CPFParser.parseCPF("a NOTIN {5} & a >= 4 & a <= 7", contractors);
        CPF reducedCPF24 = CPFParser.parseCPF("a NOTIN {8} & a >= 4", contractors);
        CPF reducedCPF25 = CPFParser.parseCPF("a NOTIN {1,2,3} & a <= 7", contractors);
        CPF reducedCPF26 = CPFParser.parseCPF("a IN {3}", contractors);

        assertEquals(reducedCPF1, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF2, CPFOperationsDep.reduce(cpf2));
        assertEquals(reducedCPF3, CPFOperationsDep.reduce(cpf3));
        assertEquals(reducedCPF4, CPFOperationsDep.reduce(cpf4));
        assertEquals(reducedCPF5, CPFOperationsDep.reduce(cpf5));
        assertEquals(reducedCPF6, CPFOperationsDep.reduce(cpf6));
        assertEquals(reducedCPF7, CPFOperationsDep.reduce(cpf7));
        assertEquals(reducedCPF8, CPFOperationsDep.reduce(cpf8));
        assertEquals(reducedCPF9, CPFOperationsDep.reduce(cpf9));
        assertEquals(reducedCPF10, CPFOperationsDep.reduce(cpf10));
        assertEquals(reducedCPF11, CPFOperationsDep.reduce(cpf11));
        assertEquals(reducedCPF12, CPFOperationsDep.reduce(cpf12));
        assertEquals(reducedCPF13, CPFOperationsDep.reduce(cpf13));
        assertEquals(reducedCPF14, CPFOperationsDep.reduce(cpf14));
        assertEquals(reducedCPF15, CPFOperationsDep.reduce(cpf15));
        assertEquals(reducedCPF16, CPFOperationsDep.reduce(cpf16));
        assertEquals(reducedCPF17, CPFOperationsDep.reduce(cpf17));
        assertEquals(reducedCPF18, CPFOperationsDep.reduce(cpf18));
        assertEquals(reducedCPF19, CPFOperationsDep.reduce(cpf19));
        assertEquals(reducedCPF20, CPFOperationsDep.reduce(cpf20));
        assertEquals(reducedCPF21, CPFOperationsDep.reduce(cpf21));
        assertEquals(reducedCPF22, CPFOperationsDep.reduce(cpf22));
        assertEquals(reducedCPF23, CPFOperationsDep.reduce(cpf23));
        assertEquals(reducedCPF24, CPFOperationsDep.reduce(cpf24));
        assertEquals(reducedCPF25, CPFOperationsDep.reduce(cpf25));
        assertEquals(reducedCPF26, CPFOperationsDep.reduce(cpf26));

    }

    @Test
    public void reduceConstantOrdinalContradictionTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a <= 4 & a >= 5", contractors);
        CPF cpf2 = CPFParser.parseCPF("a >= 5 & a <= 4", contractors);
        CPF cpf3 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a IN {1,4} & a NOTIN {1,2} & a NOTIN {2,4}", contractors);
        CPF cpf4 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {4,5} & a IN {6}", contractors);
        CPF cpf5 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4,5} & a >= 5", contractors);
        CPF cpf6 = CPFParser.parseCPF("a NOTIN {1,2,3,4,5,6,7,8} & a >= 1 & a <= 8", contractors);

        Set<AbstractAtom<?, ?, ?>> reducedAtoms = new HashSet<>();
        reducedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF reducedCPF = new CPF(reducedAtoms);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf2));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf3));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf4));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf5));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf6));


    }

    @Test
    public void reduceSetNominalTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a IN {'1','2'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a NOTIN {'1','2'}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a IN {'1','2','3'} & a IN {'2','3','4'}", contractors);
        CPF cpf4 = CPFParser.parseCPF("a NOTIN {'1','2','3'} & a NOTIN {'2','3','4'}", contractors);
        CPF cpf5 = CPFParser.parseCPF("a IN {'1','2'} & a IN {'1','2','3'} & a NOTIN {'2','3','4'}", contractors);

        CPF reducedCPF1 = CPFParser.parseCPF("a IN {'1','2'}", contractors);
        CPF reducedCPF2 = CPFParser.parseCPF("a NOTIN {'1','2'}", contractors);
        CPF reducedCPF3 = CPFParser.parseCPF("a IN {'2','3'}", contractors);
        CPF reducedCPF4 = CPFParser.parseCPF("a NOTIN {'1','2','3','4'}", contractors);
        CPF reducedCPF5 = CPFParser.parseCPF("a IN {'1'}", contractors);

        assertEquals(reducedCPF1, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF2, CPFOperationsDep.reduce(cpf2));
        assertEquals(reducedCPF3, CPFOperationsDep.reduce(cpf3));
        assertEquals(reducedCPF4, CPFOperationsDep.reduce(cpf4));
        assertEquals(reducedCPF5, CPFOperationsDep.reduce(cpf5));

    }

    @Test
    public void reduceSetNominalContradictionTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a IN {'1'} & a NOTIN {'1'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a IN {'1'} & a IN {'2'}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a IN {'1','2'} & a IN {'2','3'} & a NOTIN {'2'}", contractors);

        Set<AbstractAtom<?, ?, ?>> reducedAtoms = new HashSet<>();
        reducedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF reducedCPF = new CPF(reducedAtoms);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf2));
        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf3));

    }

    @Test
    public void reduceConstantOrdinalWithVariableNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < b & b > c & a <= 2 & c <= 6", contractors);

        CPF reducedCPF = CPFParser.parseCPF("a < b & b > c & a <= 2 & c <= 6", contractors);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceConstantOrdinalWithVariableLowerThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a < S^2(b) & a >= 0 & b <= 5", contractors);
        CPF cpf2 = CPFParser.parseCPF("a < S^2(b) & a IN {0,1,4,5,6,8} & b IN {-10,-5,-1,0,1,5,6}", contractors);

        CPF reducedCPF1 = CPFParser.parseCPF("a < S^2(b) & a >= 0 & a <= 6 & b >= -1 & b <= 5", contractors);
        CPF reducedCPF2 = CPFParser.parseCPF("a < S^2(b) & a IN {0,1,4,5,6} & b IN {-1,0,1,5,6}", contractors);

        assertEquals(reducedCPF1, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF2, CPFOperationsDep.reduce(cpf2));

    }

    @Test
    public void reduceConstantOrdinalWithVariableGreaterThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a > S^2(b) & a <= 5 & b >= 0", contractors);
        CPF cpf2 = CPFParser.parseCPF("a > S^2(b) & a IN {0,1,4,5,6,8} & b IN {-10,-5,-1,0,1,5,6}", contractors);

        CPF reducedCPF1 = CPFParser.parseCPF("a > S^2(b) & a <= 5 & b >= 0 & a >= 3 & b <= 2", contractors);
        CPF reducedCPF2 = CPFParser.parseCPF("a > S^2(b) & a IN {0,1,4,5,6,8} & b IN {-10,-5,-1,0,1,5}", contractors);

        assertEquals(reducedCPF1, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF2, CPFOperationsDep.reduce(cpf2));

    }

    @Test
    public void reduceSetNominalWithVariableNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        CPF cpf = CPFParser.parseCPF("a != b & b != c & a NOTIN {'2','3','4'} & c NOTIN {'6'}", contractors);

        CPF reducedCPF = CPFParser.parseCPF("a != b & b != c & a NOTIN {'2','3','4'} & c NOTIN {'6'}", contractors);

        assertEquals(reducedCPF, CPFOperationsDep.reduce(cpf));

    }

    @Test
    public void reduceSetNominalWithVariableEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a == b & a IN {'2','3'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a == b & a NOTIN {'2','3'}", contractors);

        CPF reducedCPF1 = CPFParser.parseCPF("a == b & a IN {'2','3'} & b IN {'2','3'}", contractors);
        CPF reducedCPF2 = CPFParser.parseCPF("a == b & a NOTIN {'2','3'} & b NOTIN {'2','3'}", contractors);

        assertEquals(reducedCPF1, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF2, CPFOperationsDep.reduce(cpf2));

    }

    @Test
    public void reduceSetNominalWithVariableNotEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a != b & a IN {'2','3'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a != b & a IN {'2'}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a != b & a NOTIN {'2'}", contractors);

        CPF reducedCPF1 = CPFParser.parseCPF("a != b & a IN {'2','3'}", contractors);
        CPF reducedCPF2 = CPFParser.parseCPF("a IN {'2'} & b NOTIN {'2'}", contractors);
        CPF reducedCPF3 = CPFParser.parseCPF("a != b & a NOTIN {'2'}", contractors);

        assertEquals(reducedCPF1, CPFOperationsDep.reduce(cpf1));
        assertEquals(reducedCPF2, CPFOperationsDep.reduce(cpf2));
        assertEquals(reducedCPF3, CPFOperationsDep.reduce(cpf3));

    }

    @Test
    public void dummyAtomContradictionTest() {
        assertTrue(new CPF(AbstractAtom.ALWAYS_FALSE).isContradiction());
    }
    
    @Test
    public void dummyAtomContradictionTest2() throws ParseException {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        CPF cpf = CPFParser.parseCPF("a != b & a == '2'", contractors);
        cpf.getAtoms().add(AbstractAtom.ALWAYS_FALSE);
        
        assertTrue(cpf.isContradiction());
    }
    
    @Test
    public void dummyAtomTautologyTest() {
        assertTrue(new CPF(AbstractAtom.ALWAYS_TRUE).isTautology());
    }
}
