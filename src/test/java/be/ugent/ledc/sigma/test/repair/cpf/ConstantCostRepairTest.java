package be.ugent.ledc.sigma.test.repair.cpf;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.AtomParser;
import be.ugent.ledc.sigma.io.CPFParser;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;
import be.ugent.ledc.sigma.repair.experimental.CPFRandomRepairSelection;
import be.ugent.ledc.sigma.repair.experimental.ConstantCostRepairEngine;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class ConstantCostRepairTest {

    private static Map<String, SigmaContractor<?>> contractors1;
    private static SigmaRuleset ruleset1;

    private static Map<String, SigmaContractor<?>> contractors2;
    private static SigmaRuleset ruleset2;

    private static Map<String, SigmaContractor<?>> adultContractors;
    private static SigmaRuleset adultRuleset;

    @BeforeClass
    public static void init() throws ParseException {

        contractors1 = new HashMap<>();
        contractors1.put("a", SigmaContractorFactory.STRING);
        contractors1.put("b", SigmaContractorFactory.INTEGER);
        contractors1.put("c", SigmaContractorFactory.INTEGER);
        contractors1.put("d", SigmaContractorFactory.STRING);

        ruleset1 = new SigmaRuleset(contractors1, Set.of(
                SigmaRuleParser.parseSigmaRule("a IN {'v','x','y','z'}", contractors1),
                SigmaRuleParser.parseSigmaRule("b >= 0", contractors1),
                SigmaRuleParser.parseSigmaRule("b <= 10", contractors1),
                SigmaRuleParser.parseSigmaRule("c >= 0", contractors1),
                SigmaRuleParser.parseSigmaRule("c <= 15", contractors1),
                SigmaRuleParser.parseSigmaRule("d IN {'1','2','3','4'}", contractors1),
                SigmaRuleParser.parseSigmaRule("NOT d IN {'1','4'} & c == 5", contractors1),
                SigmaRuleParser.parseSigmaRule("NOT a == 'v' & b <= c", contractors1)
        ));

        contractors2 = new HashMap<>();
        contractors2.put("a", SigmaContractorFactory.INTEGER);
        contractors2.put("b", SigmaContractorFactory.INTEGER);
        contractors2.put("c", SigmaContractorFactory.INTEGER);
        contractors2.put("d", SigmaContractorFactory.INTEGER);
        contractors2.put("e", SigmaContractorFactory.INTEGER);
        contractors2.put("f", SigmaContractorFactory.INTEGER);
        contractors2.put("g", SigmaContractorFactory.INTEGER);
        contractors2.put("h", SigmaContractorFactory.INTEGER);

        ruleset2 = new SigmaRuleset(contractors2, Set.of(
                SigmaRuleParser.parseSigmaRule("NOT a != b", contractors2),
                SigmaRuleParser.parseSigmaRule("NOT c != d", contractors2),
                SigmaRuleParser.parseSigmaRule("NOT e != f", contractors2),
                SigmaRuleParser.parseSigmaRule("NOT g != h", contractors2)
        ));

        adultContractors = new HashMap<>();
        adultContractors.put("sex", SigmaContractorFactory.STRING);
        adultContractors.put("marital_status", SigmaContractorFactory.STRING);
        adultContractors.put("relationship", SigmaContractorFactory.STRING);
        adultContractors.put("age", SigmaContractorFactory.INTEGER);

        adultRuleset = new SigmaRuleset(adultContractors, Set.of(
                SigmaRuleParser.parseSigmaRule("relationship IN {'Own-child','Husband','Not-in-family','Unmarried','Wife','Other-relative'}", adultContractors),
                SigmaRuleParser.parseSigmaRule("sex IN {'Female','Male'}", adultContractors),
                SigmaRuleParser.parseSigmaRule("marital_status IN {'Divorced','Married-spouse-absent','Widowed','Separated','Married-civ-spouse','Married-AF-spouse','Never-married'}", adultContractors),
                SigmaRuleParser.parseSigmaRule("age >= 17", adultContractors),
                SigmaRuleParser.parseSigmaRule("age <= 90", adultContractors),
                SigmaRuleParser.parseSigmaRule("NOT relationship == 'Husband' & sex != 'Male'", adultContractors),
                SigmaRuleParser.parseSigmaRule("NOT relationship == 'Wife' & sex != 'Female'", adultContractors),
                SigmaRuleParser.parseSigmaRule("NOT age < 18 & marital_status != 'Never-married'", adultContractors),
                SigmaRuleParser.parseSigmaRule("NOT marital_status in {'Married-civ-spouse','Married-AF-spouse','Married-spouse-absent'} & relationship == 'Unmarried'", adultContractors),
                SigmaRuleParser.parseSigmaRule("NOT age < 18 &  relationship in {'Husband', 'Wife'}", adultContractors)
        ));

    }

    @Test
    public void multipleMCCETest() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setString("a", "v")
                .setInteger("b", 5)
                .setInteger("c", 5)
                .setString("d", "3");

        ConstantCostModel costModel = new ConstantCostModel(dataObject.getAttributes());
        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("a IN {'x','y','z'}", contractors1),
                CPFParser.parseCPF("b > 5 & b <= 10", contractors1),
                CPFParser.parseCPF("c >= 0 & c < 5", contractors1)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void multipleMCCERepairTest() throws RepairException {

        DataObject dataObject = new DataObject()
                .setString("a", "v")
                .setInteger("b", 5)
                .setInteger("c", 5)
                .setString("d", "3");

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        ConstantCostModel costModel = new ConstantCostModel(dataObject.getAttributes());
        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(ruleset1.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void singleMCCETest() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setString("a", "v")
                .setInteger("b", 5)
                .setInteger("c", 5)
                .setString("d", "1");

        ConstantCostModel costModel = new ConstantCostModel(dataObject.getAttributes());
        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("c >= 0 & c < 5", contractors1)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void singleMCCERepairTest() throws RepairException {

        DataObject dataObject = new DataObject()
                .setString("a", "v")
                .setInteger("b", 5)
                .setInteger("c", 5)
                .setString("d", "1");

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        ConstantCostModel costModel = new ConstantCostModel(dataObject.getAttributes());
        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(ruleset1.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void partitionMCCETest() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 0)
                .setInteger("b", 1)
                .setInteger("c", 0)
                .setInteger("d", 1)
                .setInteger("e", 0)
                .setInteger("f", 1)
                .setInteger("g", 0)
                .setInteger("h", 1);

        ConstantCostModel costModel = new ConstantCostModel(dataObject.getAttributes());
        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(ruleset2, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        AbstractAtom<?, ?, ?> atom1 = AtomParser.parseAtom("a == 1", contractors2);
        AbstractAtom<?, ?, ?> atom2 = AtomParser.parseAtom("b == 0", contractors2);
        AbstractAtom<?, ?, ?> atom3 = AtomParser.parseAtom("c == 1", contractors2);
        AbstractAtom<?, ?, ?> atom4 = AtomParser.parseAtom("d == 0", contractors2);
        AbstractAtom<?, ?, ?> atom5 = AtomParser.parseAtom("e == 1", contractors2);
        AbstractAtom<?, ?, ?> atom6 = AtomParser.parseAtom("f == 0", contractors2);
        AbstractAtom<?, ?, ?> atom7 = AtomParser.parseAtom("g == 1", contractors2);
        AbstractAtom<?, ?, ?> atom8 = AtomParser.parseAtom("h == 0", contractors2);

        Set<CPF> expected = Set.of(
                new CPF(Set.of(atom1, atom3, atom5, atom7)),
                new CPF(Set.of(atom1, atom3, atom5, atom8)),
                new CPF(Set.of(atom1, atom3, atom6, atom7)),
                new CPF(Set.of(atom1, atom3, atom6, atom8)),
                new CPF(Set.of(atom1, atom4, atom5, atom7)),
                new CPF(Set.of(atom1, atom4, atom5, atom8)),
                new CPF(Set.of(atom1, atom4, atom6, atom7)),
                new CPF(Set.of(atom1, atom4, atom6, atom8)),
                new CPF(Set.of(atom2, atom3, atom5, atom7)),
                new CPF(Set.of(atom2, atom3, atom5, atom8)),
                new CPF(Set.of(atom2, atom3, atom6, atom7)),
                new CPF(Set.of(atom2, atom3, atom6, atom8)),
                new CPF(Set.of(atom2, atom4, atom5, atom7)),
                new CPF(Set.of(atom2, atom4, atom5, atom8)),
                new CPF(Set.of(atom2, atom4, atom6, atom7)),
                new CPF(Set.of(atom2, atom4, atom6, atom8))
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void partitionMCCERepairTest() throws RepairException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 0)
                .setInteger("b", 1)
                .setInteger("c", 0)
                .setInteger("d", 1)
                .setInteger("e", 0)
                .setInteger("f", 1)
                .setInteger("g", 0)
                .setInteger("h", 1);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        ConstantCostModel costModel = new ConstantCostModel(dataObject.getAttributes());
        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(ruleset2, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(ruleset2.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void adultMCCETest1() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Male")
                .setString("marital_status", "Widowed")
                .setString("relationship", "Own-child")
                .setInteger("age", 17);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 2)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 1)
                .addConstantCostFunction("age", 2);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("marital_status == 'Never-married'", adultContractors)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void adultMCCERepairTest1() throws RepairException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Male")
                .setString("marital_status", "Widowed")
                .setString("relationship", "Own-child")
                .setInteger("age", 17);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 2)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 1)
                .addConstantCostFunction("age", 2);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(adultRuleset.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void adultMCCETest2() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Male")
                .setString("marital_status", null)
                .setString("relationship", "Own-child")
                .setInteger("age", 17);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 2)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 1)
                .addConstantCostFunction("age", 2);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("marital_status == 'Never-married'", adultContractors)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void adultMCCERepairTest2() throws RepairException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Male")
                .setString("marital_status", null)
                .setString("relationship", "Own-child")
                .setInteger("age", 17);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 2)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 1)
                .addConstantCostFunction("age", 2);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(adultRuleset.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void adultMCCETest3() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Female")
                .setString("marital_status", "Married-civ-spouse")
                .setString("relationship", "Husband")
                .setInteger("age", 34);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 1)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 2)
                .addConstantCostFunction("age", 1);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("sex == 'Male'", adultContractors)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void adultMCCERepairTest3() throws RepairException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Female")
                .setString("marital_status", "Married-civ-spouse")
                .setString("relationship", "Husband")
                .setInteger("age", 34);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 1)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 2)
                .addConstantCostFunction("age", 1);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(adultRuleset.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void adultMCCETest4() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Male")
                .setString("marital_status", "Married-civ-spouse")
                .setString("relationship", "Husband")
                .setInteger("age", 17);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 1)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 1)
                .addConstantCostFunction("age", 1);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("age >= 18 & age <= 90", adultContractors)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void adultMCCERepairTest4() throws RepairException {

        DataObject dataObject = new DataObject()
                .setString("sex", "Male")
                .setString("marital_status", "Married-civ-spouse")
                .setString("relationship", "Husband")
                .setInteger("age", 17);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        ConstantCostModel costModel = new ConstantCostModel()
                .addConstantCostFunction("sex", 1)
                .addConstantCostFunction("marital_status",1)
                .addConstantCostFunction("relationship", 1)
                .addConstantCostFunction("age", 1);

        ConstantCostRepairEngine repairEngine = new ConstantCostRepairEngine(adultRuleset, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(adultRuleset.isSatisfied(repair.getDataObjects().get(0)));

    }

}
