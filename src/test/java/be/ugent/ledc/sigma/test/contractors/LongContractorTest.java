package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class LongContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.LONG.hasNext(1000L));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.LONG.hasNext(Long.MAX_VALUE - 1));
        assertFalse(SigmaContractorFactory.LONG.hasNext(Long.MAX_VALUE));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.LONG.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(153L, SigmaContractorFactory.LONG.next(152L).longValue());
    }

    @Test
    public void nextOverflowTest() {
        Long current = Long.MAX_VALUE;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Long has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.LONG.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.LONG.hasPrevious(1000L));
    }
    
    @Test
    public void nameTest()
    {
        assertEquals(TypeContractorFactory.LONG.name(), SigmaContractorFactory.LONG.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.LONG.hasPrevious(Long.MIN_VALUE + 1));
        assertFalse(SigmaContractorFactory.LONG.hasPrevious(Long.MIN_VALUE));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.LONG.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(151L, SigmaContractorFactory.LONG.previous(152L).longValue());
    }

    @Test
    public void previousOverflowTest() {
        Long current = Long.MIN_VALUE;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Long has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.LONG.previous(current);
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        assertEquals(5, SigmaContractorFactory.LONG.cardinality(new Interval<>(2L, 8L)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        assertEquals(7, SigmaContractorFactory.LONG.cardinality(new Interval<>(2L, 8L, false, false)));
    }
    
    @Test
    public void cardinalityNullTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.LONG.cardinality(new Interval<>(2L,null)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.LONG.cardinality(new Interval<>(2L,Long.MAX_VALUE)));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.LONG.cardinality(new Interval<>(Long.MIN_VALUE, 2L, false, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.LONG.cardinality(new Interval<>(2L, 2L, true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(Long.valueOf(Long.MIN_VALUE), SigmaContractorFactory.LONG.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(Long.valueOf(Long.MAX_VALUE), SigmaContractorFactory.LONG.last());
    }
    
    @Test
    public void printConstantTest()
    {
        assertEquals("-1235", SigmaContractorFactory.LONG.printConstant(-1235L));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(Long.valueOf(-1235), SigmaContractorFactory.LONG.parseConstant("-1235"));
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(Long.valueOf(153L), SigmaContractorFactory.LONG.add(90L, 63));
    }
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(Long.valueOf(27L), SigmaContractorFactory.LONG.add(90L, -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.LONG.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        Long value = Long.MAX_VALUE - 5L;
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Long " + value + " with " + units + " units.");
        SigmaContractorFactory.LONG.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        Long value = Long.MIN_VALUE + 5L;
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Long " + value + " with " + units + " units.");
        SigmaContractorFactory.LONG.add(value, units);
    } 
}
