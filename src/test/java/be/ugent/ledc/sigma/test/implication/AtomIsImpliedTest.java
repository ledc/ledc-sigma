package be.ugent.ledc.sigma.test.implication;

import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.atoms.AtomImplicator;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


import java.util.Set;

import static org.junit.Assert.*;

public class AtomIsImpliedTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void impliesAlwaysFalseTest() {

        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        assertTrue(AtomImplicator.implies(AbstractAtom.ALWAYS_FALSE, AbstractAtom.ALWAYS_FALSE));
        assertTrue(AtomImplicator.implies(AbstractAtom.ALWAYS_FALSE, atom));
        assertTrue(AtomImplicator.implies(AbstractAtom.ALWAYS_FALSE, AbstractAtom.ALWAYS_TRUE));

    }

    @Test
    public void notImpliesAlwaysFalseTest() {

        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        assertFalse(AtomImplicator.implies(atom, AbstractAtom.ALWAYS_FALSE));
        assertFalse(AtomImplicator.implies(AbstractAtom.ALWAYS_TRUE, AbstractAtom.ALWAYS_FALSE));

    }

    @Test
    public void impliesAlwaysTrueTest() {

        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        assertTrue(AtomImplicator.implies(AbstractAtom.ALWAYS_TRUE, AbstractAtom.ALWAYS_TRUE));
        assertTrue(AtomImplicator.implies(atom, AbstractAtom.ALWAYS_TRUE));
        assertTrue(AtomImplicator.implies(AbstractAtom.ALWAYS_FALSE, AbstractAtom.ALWAYS_TRUE));

    }

    @Test
    public void notImpliesAlwaysTrueTest() {

        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        assertFalse(AtomImplicator.implies(AbstractAtom.ALWAYS_TRUE, AbstractAtom.ALWAYS_FALSE));
        assertFalse(AtomImplicator.implies(AbstractAtom.ALWAYS_TRUE, atom));

    }

    @Test
    public void notImpliesNoEqualAttributesTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom12 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "1");

        VariableNominalAtom<String> atom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "c");

        VariableNominalAtom<String> atom31 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom32 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "c", EqualityOperator.EQ, "d");

        assertFalse(AtomImplicator.implies(atom11, atom12));
        assertFalse(AtomImplicator.implies(atom21, atom22));
        assertFalse(AtomImplicator.implies(atom31, atom32));

    }

    @Test
    public void impliesNoMatchingContractorTest() {

        ConstantOrdinalAtom<Integer> atom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 10);
        ConstantNominalAtom<String> atom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        exceptionRule.expect(AtomException.class);
        exceptionRule.expectMessage("Contractor of " + atom1 + " should be equal to contractor of " + atom2);

        AtomImplicator.implies(atom1, atom2);

    }

    @Test
    public void impliesTwoConstantNominalAtomsTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom12 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> atom32 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        assertTrue(AtomImplicator.implies(atom11, atom12));
        assertTrue(AtomImplicator.implies(atom21, atom22));
        assertTrue(AtomImplicator.implies(atom31, atom32));

    }

    @Test
    public void notImpliesTwoConstantNominalAtomsTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom12 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "2");

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> atom32 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "2");

        ConstantNominalAtom<String> atom41 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> atom42 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        ConstantNominalAtom<String> atom51 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> atom52 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");

        assertFalse(AtomImplicator.implies(atom11, atom12));
        assertFalse(AtomImplicator.implies(atom21, atom22));
        assertFalse(AtomImplicator.implies(atom31, atom32));
        assertFalse(AtomImplicator.implies(atom41, atom42));
        assertFalse(AtomImplicator.implies(atom51, atom52));

    }

    @Test
    public void impliesConstantSetNominalAtomsTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1"));

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("2", "3", "4"));

        ConstantNominalAtom<String> atom41 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom42 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));

        assertTrue(AtomImplicator.implies(atom11, atom12));
        assertTrue(AtomImplicator.implies(atom21, atom22));
        assertTrue(AtomImplicator.implies(atom31, atom32));
        assertTrue(AtomImplicator.implies(atom41, atom42));

    }

    @Test
    public void notImpliesConstantSetNominalAtomsTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2", "3"));

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));

        ConstantNominalAtom<String> atom41 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom42 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));

        ConstantNominalAtom<String> atom51 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom52 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2"));

        ConstantNominalAtom<String> atom61 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom62 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));

        ConstantNominalAtom<String> atom71 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom72 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("2"));

        assertFalse(AtomImplicator.implies(atom11, atom12));
        assertFalse(AtomImplicator.implies(atom21, atom22));
        assertFalse(AtomImplicator.implies(atom31, atom32));
        assertFalse(AtomImplicator.implies(atom41, atom42));
        assertFalse(AtomImplicator.implies(atom51, atom52));
        assertFalse(AtomImplicator.implies(atom61, atom62));
        assertFalse(AtomImplicator.implies(atom71, atom72));

    }

    @Test
    public void impliesSetConstantNominalAtomsTest() {

        SetNominalAtom<String> atom11 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1"));
        ConstantNominalAtom<String> atom12 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        SetNominalAtom<String> atom21 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1"));
        ConstantNominalAtom<String> atom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");

        SetNominalAtom<String> atom31 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        ConstantNominalAtom<String> atom32 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "4");

        SetNominalAtom<String> atom41 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        ConstantNominalAtom<String> atom42 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        SetNominalAtom<String> atom51 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        ConstantNominalAtom<String> atom52 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        assertTrue(AtomImplicator.implies(atom11, atom12));
        assertTrue(AtomImplicator.implies(atom21, atom22));
        assertTrue(AtomImplicator.implies(atom31, atom32));
        assertTrue(AtomImplicator.implies(atom41, atom42));
        assertTrue(AtomImplicator.implies(atom51, atom52));

    }

    @Test
    public void notImpliesSetConstantNominalAtomsTest() {

        SetNominalAtom<String> atom11 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        ConstantNominalAtom<String> atom12 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        SetNominalAtom<String> atom21 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2", "3", "4"));
        ConstantNominalAtom<String> atom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        SetNominalAtom<String> atom31 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1"));
        ConstantNominalAtom<String> atom32 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        SetNominalAtom<String> atom41 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        ConstantNominalAtom<String> atom42 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");

        SetNominalAtom<String> atom51 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        ConstantNominalAtom<String> atom52 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "2");

        SetNominalAtom<String> atom61 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        ConstantNominalAtom<String> atom62 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "2");

        SetNominalAtom<String> atom71 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        ConstantNominalAtom<String> atom72 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");

        SetNominalAtom<String> atom81 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        ConstantNominalAtom<String> atom82 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "4");

        assertFalse(AtomImplicator.implies(atom11, atom12));
        assertFalse(AtomImplicator.implies(atom21, atom22));
        assertFalse(AtomImplicator.implies(atom31, atom32));
        assertFalse(AtomImplicator.implies(atom41, atom42));
        assertFalse(AtomImplicator.implies(atom51, atom52));
        assertFalse(AtomImplicator.implies(atom61, atom62));
        assertFalse(AtomImplicator.implies(atom71, atom72));
        assertFalse(AtomImplicator.implies(atom81, atom82));

    }

    @Test
    public void impliesTwoSetNominalAtomsTest() {

        SetNominalAtom<String> atom11 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2"));
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));

        SetNominalAtom<String> atom21 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2"));
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("3", "4"));

        SetNominalAtom<String> atom31 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2"));
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));

        assertTrue(AtomImplicator.implies(atom11,atom12));
        assertTrue(AtomImplicator.implies(atom21,atom22));
        assertTrue(AtomImplicator.implies(atom31,atom32));

    }

    @Test
    public void notImpliesTwoSetNominalAtomsTest() {

        SetNominalAtom<String> atom11 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2"));
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "3", "4"));

        SetNominalAtom<String> atom21 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2"));
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("2", "3"));

        SetNominalAtom<String> atom31 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2"));
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("3", "4", "5"));

        SetNominalAtom<String> atom41 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2"));
        SetNominalAtom<String> atom42 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "3"));

        assertFalse(AtomImplicator.implies(atom11,atom12));
        assertFalse(AtomImplicator.implies(atom21,atom22));
        assertFalse(AtomImplicator.implies(atom31,atom32));
        assertFalse(AtomImplicator.implies(atom41,atom42));

    }

    @Test
    public void impliesTwoVariableNominalAtomsTest() {

        VariableNominalAtom<String> atom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");

        VariableNominalAtom<String> atom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        assertTrue(AtomImplicator.implies(atom11,atom12));
        assertTrue(AtomImplicator.implies(atom21,atom22));

    }

    @Test
    public void notImpliesTwoVariableNominalAtomsTest() {

        VariableNominalAtom<String> atom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        VariableNominalAtom<String> atom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");

        assertFalse(AtomImplicator.implies(atom11,atom12));
        assertFalse(AtomImplicator.implies(atom21,atom22));

    }

    @Test
    public void impliesTwoConstantOrdinalAtomsTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 2);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom72 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 1);

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom82 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 1);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom92 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 1);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom102 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom112 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 1);

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom122 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);

        ConstantOrdinalAtom<Integer> atom131 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom132 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 2);

        ConstantOrdinalAtom<Integer> atom141 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom142 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom151 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom152 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);

        ConstantOrdinalAtom<Integer> atom161 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom162 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 1);

        ConstantOrdinalAtom<Integer> atom171 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom172 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);

        ConstantOrdinalAtom<Integer> atom181 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom182 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);

        ConstantOrdinalAtom<Integer> atom191 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);
        ConstantOrdinalAtom<Integer> atom192 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);

        assertTrue(AtomImplicator.implies(atom11,atom12));
        assertTrue(AtomImplicator.implies(atom21,atom22));
        assertTrue(AtomImplicator.implies(atom31,atom32));

        assertTrue(AtomImplicator.implies(atom41,atom42));
        assertTrue(AtomImplicator.implies(atom51,atom52));
        assertTrue(AtomImplicator.implies(atom61,atom62));

        assertTrue(AtomImplicator.implies(atom71,atom72));
        assertTrue(AtomImplicator.implies(atom81,atom82));
        assertTrue(AtomImplicator.implies(atom91,atom92));

        assertTrue(AtomImplicator.implies(atom101,atom102));
        assertTrue(AtomImplicator.implies(atom111,atom112));
        assertTrue(AtomImplicator.implies(atom121,atom122));

        assertTrue(AtomImplicator.implies(atom131,atom132));
        assertTrue(AtomImplicator.implies(atom141,atom142));
        assertTrue(AtomImplicator.implies(atom151,atom152));

        assertTrue(AtomImplicator.implies(atom161,atom162));
        assertTrue(AtomImplicator.implies(atom171,atom172));
        assertTrue(AtomImplicator.implies(atom181,atom182));

        assertTrue(AtomImplicator.implies(atom191,atom192));

    }

    @Test
    public void notImpliesTwoConstantOrdinalAtomsTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 2);

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> atom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom72 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 1);

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom82 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 2);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom92 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom102 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom112 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> atom122 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);

        ConstantOrdinalAtom<Integer> atom131 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom132 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom141 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom142 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);

        ConstantOrdinalAtom<Integer> atom151 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom152 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);

        ConstantOrdinalAtom<Integer> atom161 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom162 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom171 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom172 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);

        ConstantOrdinalAtom<Integer> atom181 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);
        ConstantOrdinalAtom<Integer> atom182 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);

        ConstantOrdinalAtom<Integer> atom191 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom192 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);

        ConstantOrdinalAtom<Integer> atom201 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom202 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom211 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom212 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);

        ConstantOrdinalAtom<Integer> atom221 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom222 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 3);

        ConstantOrdinalAtom<Integer> atom231 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom232 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);

        ConstantOrdinalAtom<Integer> atom241 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> atom242 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);

        ConstantOrdinalAtom<Integer> atom251 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom252 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 1);

        ConstantOrdinalAtom<Integer> atom261 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom262 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 2);

        ConstantOrdinalAtom<Integer> atom271 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom272 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom281 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom282 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);

        ConstantOrdinalAtom<Integer> atom291 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom292 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);

        ConstantOrdinalAtom<Integer> atom301 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> atom302 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);

        ConstantOrdinalAtom<Integer> atom311 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);
        ConstantOrdinalAtom<Integer> atom312 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);

        ConstantOrdinalAtom<Integer> atom321 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);
        ConstantOrdinalAtom<Integer> atom322 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 2);

        ConstantOrdinalAtom<Integer> atom331 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);
        ConstantOrdinalAtom<Integer> atom332 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 1);

        ConstantOrdinalAtom<Integer> atom341 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);
        ConstantOrdinalAtom<Integer> atom342 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);

        ConstantOrdinalAtom<Integer> atom351 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);
        ConstantOrdinalAtom<Integer> atom352 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);

        ConstantOrdinalAtom<Integer> atom361 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);
        ConstantOrdinalAtom<Integer> atom362 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);

        assertFalse(AtomImplicator.implies(atom11,atom12));
        assertFalse(AtomImplicator.implies(atom21,atom22));
        assertFalse(AtomImplicator.implies(atom31,atom32));
        assertFalse(AtomImplicator.implies(atom41,atom42));
        assertFalse(AtomImplicator.implies(atom51,atom52));
        assertFalse(AtomImplicator.implies(atom61,atom62));

        assertFalse(AtomImplicator.implies(atom71,atom72));
        assertFalse(AtomImplicator.implies(atom81,atom82));
        assertFalse(AtomImplicator.implies(atom91,atom92));
        assertFalse(AtomImplicator.implies(atom101,atom102));
        assertFalse(AtomImplicator.implies(atom111,atom112));
        assertFalse(AtomImplicator.implies(atom121,atom122));

        assertFalse(AtomImplicator.implies(atom131,atom132));
        assertFalse(AtomImplicator.implies(atom141,atom142));
        assertFalse(AtomImplicator.implies(atom151,atom152));
        assertFalse(AtomImplicator.implies(atom161,atom162));
        assertFalse(AtomImplicator.implies(atom171,atom172));
        assertFalse(AtomImplicator.implies(atom181,atom182));

        assertFalse(AtomImplicator.implies(atom191,atom192));
        assertFalse(AtomImplicator.implies(atom201,atom202));
        assertFalse(AtomImplicator.implies(atom211,atom212));
        assertFalse(AtomImplicator.implies(atom221,atom222));
        assertFalse(AtomImplicator.implies(atom231,atom232));
        assertFalse(AtomImplicator.implies(atom241,atom242));

        assertFalse(AtomImplicator.implies(atom251,atom252));
        assertFalse(AtomImplicator.implies(atom261,atom262));
        assertFalse(AtomImplicator.implies(atom271,atom272));
        assertFalse(AtomImplicator.implies(atom281,atom282));
        assertFalse(AtomImplicator.implies(atom291,atom292));
        assertFalse(AtomImplicator.implies(atom301,atom302));

        assertFalse(AtomImplicator.implies(atom311,atom312));
        assertFalse(AtomImplicator.implies(atom321,atom322));
        assertFalse(AtomImplicator.implies(atom331,atom332));
        assertFalse(AtomImplicator.implies(atom341,atom342));
        assertFalse(AtomImplicator.implies(atom351,atom352));
        assertFalse(AtomImplicator.implies(atom361,atom362));

    }

    @Test
    public void impliesConstantSetOrdinalAtomsTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(4, 6, 7));

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(3, 5, 6));

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 4));

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        SetOrdinalAtom<Integer> atom42 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 5));

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);
        SetOrdinalAtom<Integer> atom52 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 5));

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);
        SetOrdinalAtom<Integer> atom62 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 5));

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);
        SetOrdinalAtom<Integer> atom72 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(3));

        assertTrue(AtomImplicator.implies(atom11,atom12));
        assertTrue(AtomImplicator.implies(atom21,atom22));
        assertTrue(AtomImplicator.implies(atom31,atom32));
        assertTrue(AtomImplicator.implies(atom41,atom42));
        assertTrue(AtomImplicator.implies(atom51,atom52));
        assertTrue(AtomImplicator.implies(atom61,atom62));
        assertTrue(AtomImplicator.implies(atom71,atom72));

    }

    @Test
    public void notImpliesConstantSetOrdinalAtomsTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3));

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 4, 6));

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2));

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        SetOrdinalAtom<Integer> atom42 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 3, 5));

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        SetOrdinalAtom<Integer> atom52 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(5, 7));

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        SetOrdinalAtom<Integer> atom62 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 6));

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        SetOrdinalAtom<Integer> atom72 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(6, 8));

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        SetOrdinalAtom<Integer> atom82 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 5, 7));

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);
        SetOrdinalAtom<Integer> atom92 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 5));

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);
        SetOrdinalAtom<Integer> atom102 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3));

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);
        SetOrdinalAtom<Integer> atom112 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 4));

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);
        SetOrdinalAtom<Integer> atom122 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(4));

        ConstantOrdinalAtom<Integer> atom131 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);
        SetOrdinalAtom<Integer> atom132 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 4));

        assertFalse(AtomImplicator.implies(atom11,atom12));
        assertFalse(AtomImplicator.implies(atom21,atom22));

        assertFalse(AtomImplicator.implies(atom31,atom32));
        assertFalse(AtomImplicator.implies(atom41,atom42));

        assertFalse(AtomImplicator.implies(atom51,atom52));
        assertFalse(AtomImplicator.implies(atom61,atom62));

        assertFalse(AtomImplicator.implies(atom71,atom72));
        assertFalse(AtomImplicator.implies(atom81,atom82));

        assertFalse(AtomImplicator.implies(atom91,atom92));
        assertFalse(AtomImplicator.implies(atom101,atom102));

        assertFalse(AtomImplicator.implies(atom111,atom112));
        assertFalse(AtomImplicator.implies(atom121,atom122));
        assertFalse(AtomImplicator.implies(atom131,atom132));

    }

    @Test
    public void impliesSetConstantOrdinalAtomsTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 5));
        ConstantOrdinalAtom<Integer> atom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 5));
        ConstantOrdinalAtom<Integer> atom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 6);

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 4, 5, 7));
        ConstantOrdinalAtom<Integer> atom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);

        SetOrdinalAtom<Integer> atom41 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 4, 5, 7));
        ConstantOrdinalAtom<Integer> atom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 1);

        SetOrdinalAtom<Integer> atom51 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(6));
        ConstantOrdinalAtom<Integer> atom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);

        SetOrdinalAtom<Integer> atom61 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 5));
        ConstantOrdinalAtom<Integer> atom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 6);

        SetOrdinalAtom<Integer> atom71 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 5));
        ConstantOrdinalAtom<Integer> atom72 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 2);

        assertTrue(AtomImplicator.implies(atom11,atom12));
        assertTrue(AtomImplicator.implies(atom21,atom22));
        assertTrue(AtomImplicator.implies(atom31,atom32));
        assertTrue(AtomImplicator.implies(atom41,atom42));
        assertTrue(AtomImplicator.implies(atom51,atom52));
        assertTrue(AtomImplicator.implies(atom61,atom62));
        assertTrue(AtomImplicator.implies(atom71,atom72));

    }

    @Test
    public void notImpliesSetConstantOrdinalAtomsTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 5));
        ConstantOrdinalAtom<Integer> atom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 5));
        ConstantOrdinalAtom<Integer> atom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 4, 5, 7));
        ConstantOrdinalAtom<Integer> atom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 3);

        SetOrdinalAtom<Integer> atom41 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 4, 5, 7));
        ConstantOrdinalAtom<Integer> atom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);

        SetOrdinalAtom<Integer> atom51 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(6));
        ConstantOrdinalAtom<Integer> atom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 7);

        SetOrdinalAtom<Integer> atom61 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 4, 5, 7));
        ConstantOrdinalAtom<Integer> atom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 7);

        SetOrdinalAtom<Integer> atom71 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 5));
        ConstantOrdinalAtom<Integer> atom72 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);

        SetOrdinalAtom<Integer> atom81 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 4, 5));
        ConstantOrdinalAtom<Integer> atom82 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 1);

        SetOrdinalAtom<Integer> atom91 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 4, 5));
        ConstantOrdinalAtom<Integer> atom92 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 2);

        SetOrdinalAtom<Integer> atom101 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 4, 5));
        ConstantOrdinalAtom<Integer> atom102 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 6);

        SetOrdinalAtom<Integer> atom111 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 4, 5));
        ConstantOrdinalAtom<Integer> atom112 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);

        SetOrdinalAtom<Integer> atom121 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 4, 5));
        ConstantOrdinalAtom<Integer> atom122 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);

        SetOrdinalAtom<Integer> atom131 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 4, 5));
        ConstantOrdinalAtom<Integer> atom132 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);

        assertFalse(AtomImplicator.implies(atom11,atom12));
        assertFalse(AtomImplicator.implies(atom21,atom22));
        assertFalse(AtomImplicator.implies(atom31,atom32));
        assertFalse(AtomImplicator.implies(atom41,atom42));
        assertFalse(AtomImplicator.implies(atom51,atom52));
        assertFalse(AtomImplicator.implies(atom61,atom62));
        assertFalse(AtomImplicator.implies(atom71,atom72));

        assertFalse(AtomImplicator.implies(atom81,atom82));
        assertFalse(AtomImplicator.implies(atom91,atom92));
        assertFalse(AtomImplicator.implies(atom101,atom102));
        assertFalse(AtomImplicator.implies(atom111,atom112));
        assertFalse(AtomImplicator.implies(atom121,atom122));
        assertFalse(AtomImplicator.implies(atom131,atom132));

    }

    @Test
    public void impliesTwoSetOrdinalAtomsTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2));
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3));

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2));
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(3, 4));

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2));
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1));

        assertTrue(AtomImplicator.implies(atom11,atom12));
        assertTrue(AtomImplicator.implies(atom21,atom22));
        assertTrue(AtomImplicator.implies(atom31,atom32));

    }

    @Test
    public void notImpliesTwoSetordinalAtomsTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2));
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4));

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2));
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 3));

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2));
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(3, 4, 5));

        SetOrdinalAtom<Integer> atom41 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2));
        SetOrdinalAtom<Integer> atom42 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3));

        assertFalse(AtomImplicator.implies(atom11,atom12));
        assertFalse(AtomImplicator.implies(atom21,atom22));
        assertFalse(AtomImplicator.implies(atom31,atom32));
        assertFalse(AtomImplicator.implies(atom41,atom42));

    }

    @Test
    public void impliesTwoVariableOrdinalAtomsTest() {

        VariableOrdinalAtom<Integer> atom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a", -2);

        VariableOrdinalAtom<Integer> atom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -2);

        VariableOrdinalAtom<Integer> atom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -2);

        VariableOrdinalAtom<Integer> atom41 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a");

        VariableOrdinalAtom<Integer> atom51 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -1);

        VariableOrdinalAtom<Integer> atom61 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -1);

        VariableOrdinalAtom<Integer> atom71 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a");

        VariableOrdinalAtom<Integer> atom81 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a");

        VariableOrdinalAtom<Integer> atom91 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a");

        VariableOrdinalAtom<Integer> atom101 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a", -2);

        VariableOrdinalAtom<Integer> atom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -1);

        VariableOrdinalAtom<Integer> atom121 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -1);

        VariableOrdinalAtom<Integer> atom131 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a", -1);

        VariableOrdinalAtom<Integer> atom141 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom142 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -2);

        VariableOrdinalAtom<Integer> atom151 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom152 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a", -1);

        VariableOrdinalAtom<Integer> atom161 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom162 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a");

        VariableOrdinalAtom<Integer> atom171 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom172 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a", -1);

        VariableOrdinalAtom<Integer> atom181 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom182 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a");

        VariableOrdinalAtom<Integer> atom191 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom192 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -1);

        assertTrue(AtomImplicator.implies(atom11,atom12));
        assertTrue(AtomImplicator.implies(atom21,atom22));
        assertTrue(AtomImplicator.implies(atom31,atom32));

        assertTrue(AtomImplicator.implies(atom41,atom42));
        assertTrue(AtomImplicator.implies(atom51,atom52));
        assertTrue(AtomImplicator.implies(atom61,atom62));

        assertTrue(AtomImplicator.implies(atom71,atom72));
        assertTrue(AtomImplicator.implies(atom81,atom82));
        assertTrue(AtomImplicator.implies(atom91,atom92));

        assertTrue(AtomImplicator.implies(atom101,atom102));
        assertTrue(AtomImplicator.implies(atom111,atom112));
        assertTrue(AtomImplicator.implies(atom121,atom122));

        assertTrue(AtomImplicator.implies(atom131,atom132));
        assertTrue(AtomImplicator.implies(atom141,atom142));
        assertTrue(AtomImplicator.implies(atom151,atom152));
        assertTrue(AtomImplicator.implies(atom161,atom162));
        assertTrue(AtomImplicator.implies(atom171,atom172));
        assertTrue(AtomImplicator.implies(atom181,atom182));

        assertTrue(AtomImplicator.implies(atom191,atom192));

    }

    @Test
    public void notImpliesTwoVariableOrdinalAtomsTest() {

        VariableOrdinalAtom<Integer> atom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a");

        VariableOrdinalAtom<Integer> atom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -1);

        VariableOrdinalAtom<Integer> atom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a");

        VariableOrdinalAtom<Integer> atom41 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -1);

        VariableOrdinalAtom<Integer> atom51 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a", -1);

        VariableOrdinalAtom<Integer> atom61 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -1);

        VariableOrdinalAtom<Integer> atom71 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a", 1);

        VariableOrdinalAtom<Integer> atom81 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a");

        VariableOrdinalAtom<Integer> atom91 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a");

        VariableOrdinalAtom<Integer> atom101 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -1);

        VariableOrdinalAtom<Integer> atom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a");

        VariableOrdinalAtom<Integer> atom121 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a");

        VariableOrdinalAtom<Integer> atom131 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a");

        VariableOrdinalAtom<Integer> atom141 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom142 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a");

        VariableOrdinalAtom<Integer> atom151 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom152 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a", -2);

        VariableOrdinalAtom<Integer> atom161 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom162 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -1);

        VariableOrdinalAtom<Integer> atom171 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom172 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a");

        VariableOrdinalAtom<Integer> atom181 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom182 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -1);

        VariableOrdinalAtom<Integer> atom191 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom192 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a");

        VariableOrdinalAtom<Integer> atom201 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom202 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a");

        VariableOrdinalAtom<Integer> atom211 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom212 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a", -3);

        VariableOrdinalAtom<Integer> atom221 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom222 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -2);

        VariableOrdinalAtom<Integer> atom231 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom232 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a");

        VariableOrdinalAtom<Integer> atom241 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom242 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -2);

        VariableOrdinalAtom<Integer> atom251 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom252 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a");

        VariableOrdinalAtom<Integer> atom261 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom262 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -1);

        VariableOrdinalAtom<Integer> atom271 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom272 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a", -2);

        VariableOrdinalAtom<Integer> atom281 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom282 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -1);

        VariableOrdinalAtom<Integer> atom291 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom292 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a");

        VariableOrdinalAtom<Integer> atom301 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> atom302 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a", -1);

        VariableOrdinalAtom<Integer> atom311 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom312 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "a");

        VariableOrdinalAtom<Integer> atom321 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom322 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -1);

        VariableOrdinalAtom<Integer> atom331 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom332 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "a", -2);

        VariableOrdinalAtom<Integer> atom341 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom342 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -1);

        VariableOrdinalAtom<Integer> atom351 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom352 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a");

        VariableOrdinalAtom<Integer> atom361 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom362 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "a");


        assertFalse(AtomImplicator.implies(atom11,atom12));
        assertFalse(AtomImplicator.implies(atom21,atom22));
        assertFalse(AtomImplicator.implies(atom31,atom32));
        assertFalse(AtomImplicator.implies(atom41,atom42));
        assertFalse(AtomImplicator.implies(atom51,atom52));
        assertFalse(AtomImplicator.implies(atom61,atom62));

        assertFalse(AtomImplicator.implies(atom71,atom72));
        assertFalse(AtomImplicator.implies(atom81,atom82));
        assertFalse(AtomImplicator.implies(atom91,atom92));
        assertFalse(AtomImplicator.implies(atom101,atom102));
        assertFalse(AtomImplicator.implies(atom111,atom112));
        assertFalse(AtomImplicator.implies(atom121,atom122));

        assertFalse(AtomImplicator.implies(atom131,atom132));
        assertFalse(AtomImplicator.implies(atom141,atom142));
        assertFalse(AtomImplicator.implies(atom151,atom152));
        assertFalse(AtomImplicator.implies(atom161,atom162));
        assertFalse(AtomImplicator.implies(atom171,atom172));
        assertFalse(AtomImplicator.implies(atom181,atom182));

        assertFalse(AtomImplicator.implies(atom191,atom192));
        assertFalse(AtomImplicator.implies(atom201,atom202));
        assertFalse(AtomImplicator.implies(atom211,atom212));
        assertFalse(AtomImplicator.implies(atom221,atom222));
        assertFalse(AtomImplicator.implies(atom231,atom232));
        assertFalse(AtomImplicator.implies(atom241,atom242));

        assertFalse(AtomImplicator.implies(atom251,atom252));
        assertFalse(AtomImplicator.implies(atom261,atom262));
        assertFalse(AtomImplicator.implies(atom271,atom272));
        assertFalse(AtomImplicator.implies(atom281,atom282));
        assertFalse(AtomImplicator.implies(atom291,atom292));
        assertFalse(AtomImplicator.implies(atom301,atom302));

        assertFalse(AtomImplicator.implies(atom311,atom312));
        assertFalse(AtomImplicator.implies(atom321,atom322));
        assertFalse(AtomImplicator.implies(atom331,atom332));
        assertFalse(AtomImplicator.implies(atom341,atom342));
        assertFalse(AtomImplicator.implies(atom351,atom352));
        assertFalse(AtomImplicator.implies(atom361,atom362));

    }

}
