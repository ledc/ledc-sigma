package be.ugent.ledc.sigma.test.repair;
import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.RepairRuntimeException;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.core.cost.CostModel;
import be.ugent.ledc.core.cost.errormechanism.SwapError;
import be.ugent.ledc.core.cost.errormechanism.integer.IntegerErrors;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.core.cost.ErrorFunction;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.PreferenceCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.SimpleIterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;
import be.ugent.ledc.sigma.repair.cost.models.IterableCostModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import be.ugent.ledc.sigma.repair.cost.models.NonConstantCostModel;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class CostCalculationTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    public static CostModel<ConstantCostFunction> constantCostModel;
    public static CostModel<IterableCostFunction<?>> nonConstantCostModel;
    public static CostModel<IterableCostFunction> iterableCostModel;
    public static CostModel<IterableCostFunction> preferenceCostModel;
    
    @BeforeClass
    public static void setupCostModels()
    {
        constantCostModel = new ConstantCostModel()
        .addCostFunction("a", new ConstantCostFunction(2))
        .addCostFunction("b", new ConstantCostFunction(1))
        .addCostFunction("c", new ConstantCostFunction(5))
        .addCostFunction("d", new ConstantCostFunction(3));
        
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 4);
        
        Map<Integer, Map<Integer, Integer>> costMapForB = new HashMap<>();
        costMapForB.put(2, new HashMap<>());
        costMapForB.put(3, new HashMap<>());
        
        costMapForB.get(2).put(3, 1);
        costMapForB.get(2).put(4, 3);
        costMapForB.get(2).put(1, 2);
        
        costMapForB.get(3).put(2, 1);
        costMapForB.get(3).put(4, 2);
        costMapForB.get(3).put(1, 2);
        
        Map<Integer, Map<Integer, Integer>> costMapForC = new HashMap<>();
        costMapForC.put(3, new HashMap<>());
        
        costMapForC.get(3).put(1, 1);
        costMapForC.get(3).put(2, 3);
        costMapForC.get(3).put(4, 2);

        nonConstantCostModel = new NonConstantCostModel()
            .addCostFunction("a", new SimpleIterableCostFunction<>(costMapForA))
            .addCostFunction("b", new SimpleIterableCostFunction<>(costMapForB))
            .addCostFunction("c", new SimpleIterableCostFunction<>(costMapForC));
        
        iterableCostModel = new IterableCostModel()
            .addCostFunction("a", new SimpleIterableCostFunction<>(costMapForA))
            .addCostFunction("b", new SimpleIterableCostFunction<>(costMapForB))
            .addCostFunction("c", new SimpleIterableCostFunction<>(costMapForC));
        
        preferenceCostModel = new IterableCostModel()
            .addCostFunction("a", new PreferenceCostFunction<>(1,8,6));
    }
    
    @Test
    public void constantCostPositiveTest()
    {
        exceptionRule.expect(RepairRuntimeException.class);
        exceptionRule.expectMessage("Cost functions must be positive-definite: fixed cost must be strictly greater than 0.");
        new ConstantCostFunction<>(-1);
    }
    
    @Test
    public void constantCostPositiveDefiniteTest()
    {
        exceptionRule.expect(RepairRuntimeException.class);
        exceptionRule.expectMessage("Cost functions must be positive-definite: fixed cost must be strictly greater than 0.");
        new ConstantCostFunction<>(0);
    }
    
    @Test
    public void constantCostFunctionNullRepairTest() 
    {
        assertEquals(Integer.MAX_VALUE,new ConstantCostFunction<>().cost(1, null, new DataObject()));
    }
    
    @Test
    public void constantCostFunctionKeepValueTest()
    {
        assertEquals(0, new ConstantCostFunction<>().cost(1, 1, new DataObject()));
    }
    
    @Test
    public void constantCostFunctionRepairTest() 
    {
        int cost = 1 + new Random().nextInt(20); //1+ to avoid zero cost, which would cause an exception
        assertEquals(cost,new ConstantCostFunction<>(cost).cost(1, 3, new DataObject()));
    }
    
    @Test
    public void constantCostModelSumOfCostsTest()
    {
        DataObject original = new DataObject()
            .set("a", 1)
            .set("b", "x")
            .set("c", 2)
            .set("d", "z");
        
        DataObject repair = new DataObject()
            .set("a", 1)
            .set("b", "y")
            .set("c", 2)
            .set("d", "boo");
                
        //Sum of costs of attributes that changed (b and d) is 4
        assertEquals(4, constantCostModel.cost(original, repair));
    }
    
    @Test
    public void constantCostModelNullTest() 
    {
        DataObject original = new DataObject()
            .set("a", 1)
            .set("b", "x")
            .set("c", 2)
            .set("d", "z");
        
        DataObject repair = new DataObject()
            .set("a", 1)
            .set("b", "y")
            .set("c", 2)
            .set("d", null);
                
        //Sum of costs of attributes that changed (b and d) is 4
        assertEquals(Integer.MAX_VALUE, constantCostModel.cost(original, repair));
    }
    
    @Test
    public void simpleIterableCostFunctionNoChangeTest()
    {
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);
        
        SimpleIterableCostFunction<Integer> costFunction = new SimpleIterableCostFunction<>(costMapForA);
        
        assertEquals(0, costFunction.cost(1, 1, new DataObject()));
        
    }
    
    @Test
    public void simpleIterableCostFunctionChangeTest()
    {
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);
        
        SimpleIterableCostFunction<Integer> costFunction = new SimpleIterableCostFunction<>(costMapForA);
        
        assertEquals(1, costFunction.cost(1, 2, new DataObject()));
        assertEquals(5, costFunction.cost(1, 3, new DataObject()));
        
    }
    
    @Test
    public void simpleIterableCostFunctionNullTest()
    {
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);
        
        SimpleIterableCostFunction<Integer> costFunction = new SimpleIterableCostFunction<>(costMapForA);
        
        assertEquals(Integer.MAX_VALUE, costFunction.cost(1, null, new DataObject()));
        assertEquals(Integer.MAX_VALUE, costFunction.cost(2, null, new DataObject()));
    }
    
    @Test
    public void simpleIterableCostFunctionDefaultTest()
    {
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);
        
        SimpleIterableCostFunction<Integer> costFunction = new SimpleIterableCostFunction<>(costMapForA);
        
        assertEquals(6, costFunction.cost(1, 5, new DataObject()));
        assertEquals(6, costFunction.cost(2, 5, new DataObject()));
    }
    
    @Test
    public void simpleNonConstantCostModelZeroTest()
    {
        DataObject original = new DataObject()
                .set("a", 1)
                .set("b", 2)
                .set("c", 3);
        
        DataObject repair = new DataObject()
                .set("a", 1)
                .set("b", 2)
                .set("c", 3);
        
        assertEquals(0, nonConstantCostModel.cost(original, repair));
    }

    @Test
    public void simpleNonConstantCostModelSumTest()
    {
        DataObject original = new DataObject()
                .set("a", 1)
                .set("b", 2)
                .set("c", 3);

        DataObject repair = new DataObject()
                .set("a", 2)
                .set("b", 4)
                .set("c", 3);

        assertEquals(4, nonConstantCostModel.cost(original, repair));
    }
    
    @Test
    public void simpleIterableCostModelIteratorTest() throws RepairException, ParseException
    {
        DataObject original = new DataObject()
                .set("a", 1)
                .set("b", 2)
                .set("c", 3);

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT a notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT c notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & b in {2,3}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 2 & c == 3", contractors)
        )
        .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        //Build a low cost first iteratoe
        Iterator<DataObject> lowCostFirstIterator = ((IterableCostModel)iterableCostModel)
            .buildLocoIterator(
                original,
                Stream.of("a", "b").collect(Collectors.toSet()),
                new HashSet<>(),
                ruleset,
                false);
        
        List<DataObject> objectsInOrder = new ArrayList<>();
        
        while(lowCostFirstIterator.hasNext())
        {
            objectsInOrder.add(lowCostFirstIterator.next());
        }
        
        List<DataObject> expected = new ArrayList<>();
        expected.add(new DataObject().set("a", 2).set("b", 3));//cost=2
        expected.add(new DataObject().set("a", 2).set("b", 1));//cost=3
        expected.add(new DataObject().set("a", 2).set("b", 4));//cost=4
        expected.add(new DataObject().set("a", 4).set("b", 3));//cost=5
        expected.add(new DataObject().set("a", 3).set("b", 3));//cost=6
        expected.add(new DataObject().set("a", 4).set("b", 1));//cost=6
        expected.add(new DataObject().set("a", 4).set("b", 4));//cost=7
        expected.add(new DataObject().set("a", 3).set("b", 1));//cost=7
        expected.add(new DataObject().set("a", 3).set("b", 4));//cost=8
        
        
        assertEquals(expected.subList(0, 4), objectsInOrder.subList(0, 4)); 
        assertEquals(new HashSet<>(expected.subList(4, 6)), new HashSet<>(objectsInOrder.subList(4, 6))); 
        assertEquals(new HashSet<>(expected.subList(6, 8)), new HashSet<>(objectsInOrder.subList(6, 8))); 
        assertEquals(expected.get(8), objectsInOrder.get(8)); 
    }
    
    @Test
    public void simpleIterableCostModelIteratorConditionedTest() throws RepairException, ParseException
    {
        DataObject original = new DataObject()
                .set("a", 1)
                .set("b", 2)
                .set("c", 3);

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> sigmaRules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT a notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT c notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & b in {2,3}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 2 & c == 3", contractors)
        )
        .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, sigmaRules);

        //Build a low cost first iteratoe
        Iterator<DataObject> lowCostFirstIterator = ((IterableCostModel)iterableCostModel)
            .buildLocoIterator(
                original,
                Stream.of("a", "b").collect(Collectors.toSet()),
                Stream.of("c").collect(Collectors.toSet()),
                ruleset,
                false);
        
        List<DataObject> objectsInOrder = new ArrayList<>();
        
        while(lowCostFirstIterator.hasNext())
        {
            objectsInOrder.add(lowCostFirstIterator.next());
        }
        
        List<DataObject> expected = new ArrayList<>();
        expected.add(new DataObject().set("a", 4).set("b", 3));//cost=5
        expected.add(new DataObject().set("a", 3).set("b", 3));//cost=6
        expected.add(new DataObject().set("a", 4).set("b", 1));//cost=6
        expected.add(new DataObject().set("a", 4).set("b", 4));//cost=7
        expected.add(new DataObject().set("a", 3).set("b", 1));//cost=7
        expected.add(new DataObject().set("a", 3).set("b", 4));//cost=8
        
        
        assertEquals(expected.get(0), objectsInOrder.get(0)); 
        assertEquals(new HashSet<>(expected.subList(1, 3)), new HashSet<>(objectsInOrder.subList(1, 3))); 
        assertEquals(new HashSet<>(expected.subList(3, 5)), new HashSet<>(objectsInOrder.subList(3, 5))); 
        assertEquals(expected.get(5), objectsInOrder.get(5)); 
    }
   
    @Test
    public void simpleNonConstantCostModelNullTest()
    {
        DataObject original = new DataObject()
                .set("a", 1)
                .set("b", 2)
                .set("c", 3);
        
        DataObject repair = new DataObject()
                .set("a", 1)
                .set("b", null)
                .set("c", 3);
        
        assertEquals(Integer.MAX_VALUE, nonConstantCostModel.cost(original, repair));
    }
    
    @Test
    public void errorBasedCostFunctionSingleDigitTest()
    {        
        ErrorFunction<Integer> costFunction = new ErrorFunction<>(
            2,
            IntegerErrors.SINGLE_DIGIT
        );
        
        assertEquals(2, costFunction.cost(null, 1236, new DataObject())); 
        assertEquals(1, costFunction.cost(1336, 1236, new DataObject())); 
        assertEquals(2, costFunction.cost(1337, 1236, new DataObject())); 
    }
    
    @Test
    public void errorBasedCostFunctionSwapErrorTest()
    {
        ErrorFunction<Integer> costFunction = new ErrorFunction<>(
            3,
            new SwapError<>("b")
        );
        
        assertEquals(3, costFunction.cost(null, 1236, new DataObject())); 
        assertEquals(1, costFunction.cost(15, 20, new DataObject().set("a", 15).set("b", 20).set("c", 25))); 
        assertEquals(3, costFunction.cost(15, 25, new DataObject().set("a", 15).set("b", 20).set("c", 25))); 
        assertEquals(3, costFunction.cost(15, 25, new DataObject().set("a", 15).set("c", 25))); 
    }
    
    @Test
    public void preferenceBasedCostFunctionTest1()
    {
        DataObject original = new DataObject().set("a", 1);
        DataObject repair = new DataObject().set("a", 8);
        assertEquals(2, preferenceCostModel.cost(original, repair));
    }
    
    @Test
    public void preferenceBasedCostFunctionTest2()
    {
        DataObject original = new DataObject().set("a", 8);
        DataObject repair = new DataObject().set("a", 1);
        assertEquals(1, preferenceCostModel.cost(original, repair));
    }
    
    @Test
    public void preferenceBasedCostFunctionTest3()
    {
        DataObject original = new DataObject().set("a", 1);
        DataObject repair = new DataObject().set("a", 6);
        assertEquals(3, preferenceCostModel.cost(original, repair));
    }
    
    @Test
    public void preferenceBasedCostFunctionTest4()
    {
        DataObject original = new DataObject().set("a", 1);
        DataObject repair = new DataObject().set("a", 10);
        assertEquals(4, preferenceCostModel.cost(original, repair));
    }
    
    @Test
    public void preferenceBasedCostFunctionTest8()
    {
        DataObject original = new DataObject().set("a", 8);
        DataObject repair = new DataObject().set("a", 10);
        assertEquals(3, preferenceCostModel.cost(original, repair));
    }
}

