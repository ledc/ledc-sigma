package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.time.Duration;
import java.time.temporal.ChronoUnit;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;


public class DurationInSecondsContractorTest
{
  
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.DURATION_SECONDS.hasNext(Duration.ofHours(6720)));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.DURATION_SECONDS.hasNext(SigmaContractorFactory.DURATION_SECONDS.last().minusSeconds(1)));
        assertFalse(SigmaContractorFactory.DURATION_SECONDS.hasNext(SigmaContractorFactory.DURATION_SECONDS.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.DURATION_SECONDS.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(Duration.ofSeconds(91001), SigmaContractorFactory.DURATION_SECONDS.next(Duration.ofSeconds(91000)));
    }

    @Test
    public void nextOverflowTest() {
        Duration current = ChronoUnit.FOREVER.getDuration();
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Duration has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DURATION_SECONDS.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.DURATION_SECONDS.hasPrevious(Duration.ofHours(6720)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.DURATION_SECONDS.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(Duration.ofSeconds(90059), SigmaContractorFactory.DURATION_SECONDS.previous(Duration.ofSeconds(90060)));
    }

    @Test
    public void previousOverflowTest() {
        Duration current = Duration.ZERO;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Duration has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DURATION_SECONDS.previous(current);
    }
    
    @Test
    public void nameTest()
    {
        assertEquals("duration_in_seconds", SigmaContractorFactory.DURATION_SECONDS.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.DURATION_SECONDS.hasPrevious(SigmaContractorFactory.DURATION_SECONDS.first().plusSeconds(1)));
        assertFalse(SigmaContractorFactory.DURATION_SECONDS.hasPrevious(SigmaContractorFactory.DURATION_SECONDS.first()));
        assertFalse(SigmaContractorFactory.DURATION_SECONDS.hasPrevious(Duration.ofHours(0)));
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        Duration start = Duration.ofHours(6720);
        Duration end = Duration.ofHours(6721);
        assertEquals(3599, SigmaContractorFactory.DURATION_SECONDS.cardinality(new Interval<>(start,end)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        Duration start = Duration.ofHours(6720);
        Duration end = Duration.ofHours(6721);
        assertEquals(3601, SigmaContractorFactory.DURATION_SECONDS.cardinality(new Interval<>(start,end,false,false)));
    }
    
    @Test
    public void cardinalityNullTest()
    {
        Duration start = Duration.ofHours(6720);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DURATION_SECONDS.cardinality(new Interval<>(start,null)));
    }

    @Test
    public void cardinalityLeftNullTest()
    {
        Duration end = Duration.ofHours(1);
        assertEquals(3599, SigmaContractorFactory.DURATION_SECONDS.cardinality(new Interval<>(null, end)));
    }
    
    @Test
    public void cardinalityFullSpanTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DURATION_SECONDS.cardinality(new Interval<>(Duration.ZERO,SigmaContractorFactory.DURATION_SECONDS.last(),false,false)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        Duration start = Duration.ofHours(6720);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory
            .DURATION_SECONDS
            .cardinality(
                new Interval<>(
                    start,
                    SigmaContractorFactory.DURATION_SECONDS.last())
            ));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        Duration end = Duration.ofHours(6720);
        assertEquals(24192001, SigmaContractorFactory.DURATION_SECONDS.cardinality(new Interval<>(Duration.ZERO,end, false, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.DURATION_SECONDS.cardinality(new Interval<>(Duration.ofSeconds(9000), Duration.ofSeconds(9000), true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(Duration.ZERO, SigmaContractorFactory.DURATION_SECONDS.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(ChronoUnit.FOREVER.getDuration().withNanos(0), SigmaContractorFactory.DURATION_SECONDS.last());
    }
    
    @Test
    public void printConstantTest()
    {
        Duration d = Duration.ofSeconds(91000);
        assertEquals("PT25H16M40S", SigmaContractorFactory.DURATION_SECONDS.printConstant(d));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(
            Duration.ofSeconds(91000),
            SigmaContractorFactory
                .DURATION_SECONDS
                .parseConstant("P1DT1H16M40.3S")
        );
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(
            Duration.ofSeconds(91063),
            SigmaContractorFactory
                .DURATION_SECONDS
                .add(Duration.ofSeconds(91000), 63));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(
            Duration.ofSeconds(90937),
            SigmaContractorFactory
                .DURATION_SECONDS
                .add(Duration.ofSeconds(91000), -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.DURATION_SECONDS.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        Duration value = SigmaContractorFactory.DURATION_SECONDS.last().minusSeconds(5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Duration " + value + " with " + units + " seconds.");
        SigmaContractorFactory.DURATION_SECONDS.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        Duration value = Duration.ZERO.plusSeconds(5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Duration " + value + " with " + units + " seconds.");
        SigmaContractorFactory.DURATION_SECONDS.add(value, units);
    }
   
}
