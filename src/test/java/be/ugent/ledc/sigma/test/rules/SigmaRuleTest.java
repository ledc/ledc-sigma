package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.io.SigmaRuleParser;

import java.util.*;

import static org.junit.Assert.assertEquals;

import org.junit.Assert;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class SigmaRuleTest
{

    @Test
    public void getInvolvedAttributesTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a < S^1(b) & c == '1' & d == 3 & a notin {1,2}", contractors);

        Set<String> involvedAttributes = new HashSet<>(Arrays.asList("a", "b", "c", "d"));

        assertEquals(involvedAttributes, rule.getInvolvedAttributes());

    }

    @Test
    public void involvesAttributeTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a < S^1(b) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertTrue(rule.involves("a"));
    }

    @Test
    public void notInvolvesAttributeTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a < S^1(b) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertFalse(rule.involves("e"));
    }

    @Test
    public void isAlwaysSatisfiedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a > S^1(a) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertTrue(rule.isAlwaysSatisfied());

    }

    @Test
    public void isNotAlwaysSatisfiedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a < S^1(a) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertFalse(rule.isAlwaysSatisfied());

    }

    @Test
    public void isAlwaysFailedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a < S^1(a)", contractors);

        assertTrue(rule.isAlwaysFailed());

    }

    @Test
    public void isNotAlwaysFailedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a > S^1(a) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertFalse(rule.isAlwaysFailed());

    }

    @Test
    public void failureConstantAtomsTest() throws ParseException
    {
        DataObject o = new DataObject()
            .setString("a", "x")
            .setInteger("b", 5);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a == 'x' & b >= 0 & b <= 10", contractors);

        Assert.assertTrue(rule.isFailed(o));
    }
    
    @Test
    public void failureTestConstantAtomsNull() throws ParseException
    {
        DataObject o = new DataObject()
            .setString("a", "x")
            .setInteger("b", null);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a == 'x' & b >= 0 & b <= 10", contractors);

        Assert.assertFalse(rule.isFailed(o));
    }
    
    @Test
    public void failureSetAtomsTest() throws ParseException
    {
        DataObject o = new DataObject()
            .setString("a", "y")
            .setInteger("b", 5);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        
        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a in {'x', 'y'} & b notin {10,11,15,19}", contractors);

        Assert.assertTrue(rule.isFailed(o));
    }
    
    @Test
    public void failureVariableAtomsTest() throws ParseException
    {
        DataObject o = new DataObject()
            .setInteger("a", 3)
            .setInteger("b", 5)
            .setInteger("c", 8);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        
        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a <= b & b < S^5(c)", contractors);

        Assert.assertTrue(rule.isFailed(o));
    }
    
    @Test
    public void fixVariableAtomsTest() throws ParseException
    {
        DataObject o = new DataObject()
            .setInteger("b", 5)
            .setInteger("d", 8);

        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        
        //The rule
        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a <= b & b < S^5(c) & d == S^3(b)", contractors);
        
        //The expected fix
        SigmaRule expectedFix = SigmaRuleParser.parseSigmaRule("NOT a <= 5 & c > 0", contractors);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_TRUE);
    
        SigmaRule fixed = rule.fix(o);
        
        assertEquals(expectedFix, fixed);
    }
    
    @Test
    public void fixConstantAtomsTest() throws ParseException
    {
        DataObject o = new DataObject()
            .setInteger("b", 5);

        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT b <= 5 & c == 5", contractors);
        SigmaRule expectedFix = SigmaRuleParser.parseSigmaRule("NOT c == 5", contractors);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_TRUE);
        
        SigmaRule fixed = rule.fix(o);
        
        assertEquals(expectedFix, fixed);
    }
    
    @Test
    public void fixSetAtomsTest() throws ParseException
    {
        DataObject o = new DataObject()
            .setInteger("b", 5)
            .setInteger("c", 10);

        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a in {1,2,3} & b in {5,7,9} & c notin {11,10}", contractors);
        SigmaRule expectedFix = SigmaRuleParser.parseSigmaRule("a notin {1,2,3}", contractors);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_TRUE);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_FALSE);
        
        SigmaRule fixed = rule.fix(o);
        
        assertEquals(expectedFix, fixed);
    }

    @Test
    public void getConstantOrdinalAtomsTest()  throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a != S^1(b) & c == '1' & d <= 3", contractors);

        Set<ConstantOrdinalAtom<?>> constantOrdinalAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> atom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", InequalityOperator.LEQ, 3);
        constantOrdinalAtoms.add(atom1);

        assertEquals(constantOrdinalAtoms, rule.getConstantOrdinalAtoms());

    }

    @Test
    public void getConstantAtomsTest() throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a != S^1(b) & c == '1' & d <= 3", contractors);

        Set<ConstantAtom<?, ?, ?>> constantAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> atom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", InequalityOperator.LEQ, 3);
        ConstantNominalAtom<String> atom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "c", EqualityOperator.EQ, "1");
        constantAtoms.add(atom1);
        constantAtoms.add(atom2);

        assertEquals(constantAtoms, rule.getConstantAtoms());

    }

    @Test
    public void getVariableAtomsTest() throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a != S^1(b) & c == '1' & d <= 3", contractors);

        Set<VariableAtom<?, ?, ?>> variableAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> atom1 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        variableAtoms.add(atom1);

        assertEquals(variableAtoms, rule.getVariableAtoms());

    }

    @Test
    public void getSetAtomsTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a in {'1', '2'} & b in {3} & c == '1' & d <= 3", contractors);

        Set<SetAtom<?, ?>> setAtoms = new HashSet<>();
        SetNominalAtom<String> atom1 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2")));
        SetOrdinalAtom<Integer> atom2 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(List.of(3)));
        setAtoms.add(atom1);
        setAtoms.add(atom2);

        assertEquals(setAtoms, rule.getSetAtoms());

    }

    @Test
    public void getSetNominalAtomsTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a in {'1', '2'} & b in {3} & c == '1' & d <= 3", contractors);

        Set<SetNominalAtom<?>> setNominalAtoms = new HashSet<>();
        SetNominalAtom<String> atom1 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2")));
        setNominalAtoms.add(atom1);

        assertEquals(setNominalAtoms, rule.getSetNominalAtoms());

    }

    @Test
    public void reduceVariableOrdinalIsAlwaysSatisfiedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("NOT a < b & a > b", contractors);
        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("NOT a < b & a > S^-1(b)", contractors);
        SigmaRule rule3 = SigmaRuleParser.parseSigmaRule("NOT a < S^-1(b) & a > S^-2(b)", contractors);

        assertTrue(rule1.isAlwaysSatisfied());
        assertTrue(rule2.isAlwaysSatisfied());
        assertTrue(rule3.isAlwaysSatisfied());

    }

    @Test
    public void reduceConstantOrdinalIsAlwaysSatisfiedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("NOT a <= 4 & a >= 5", contractors);
        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("NOT a >= 5 & a <= 4", contractors);

        assertTrue(rule1.isAlwaysSatisfied());
        assertTrue(rule2.isAlwaysSatisfied());


    }

    @Test
    public void reduceConstantNominalIsAlwaysSatisfiedTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);

        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("NOT a == '1' & a != '1'", contractors);
        assertTrue(rule1.isAlwaysSatisfied());

        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("NOT a == '1' & a == '2'", contractors);
        assertTrue(rule2.isAlwaysSatisfied());

    }

    @Test
    public void dummyAtomIsAlwaysSatisfiedTest()
    {
        assertTrue(new SigmaRule(AbstractAtom.ALWAYS_FALSE).isAlwaysSatisfied());
    }

    @Test
    public void dummyAtomIsAlwaysSatisfiedTest2() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        SigmaRule rule = SigmaRuleParser.parseSigmaRule("NOT a != b & a == '2'", contractors);
        rule.getAtoms().add(AbstractAtom.ALWAYS_FALSE);

        assertTrue(rule.isAlwaysSatisfied());
    }

    @Test
    public void dummyAtomIsAlwaysFailedTest()
    {
        assertTrue(new SigmaRule(AbstractAtom.ALWAYS_TRUE).isAlwaysFailed());
    }

}
