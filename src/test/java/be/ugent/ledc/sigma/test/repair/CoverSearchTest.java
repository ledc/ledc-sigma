package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.NullBehavior;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;
import be.ugent.ledc.sigma.repair.covers.ConditionalSetCoverSearch;
import be.ugent.ledc.sigma.repair.covers.MinimalCoverSearch;
import be.ugent.ledc.sigma.repair.covers.SubsetMinimalCoverSearch;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import org.junit.Test;
import org.junit.BeforeClass;

public class CoverSearchTest
{
    private static SigmaRuleset ruleset1;
    private static SigmaRuleset ruleset2;
    
    @BeforeClass
    public static void init() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors1 = new HashMap<>();
        contractors1.put("a", SigmaContractorFactory.STRING);
        contractors1.put("b", SigmaContractorFactory.INTEGER);
        contractors1.put("c", SigmaContractorFactory.INTEGER);
        contractors1.put("d", SigmaContractorFactory.STRING);
        
        Set<SigmaRule> rules1 = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT a notin {'v', 'x', 'y', 'z'}", contractors1),
            SigmaRuleParser.parseSigmaRule("NOT b < 0", contractors1),
            SigmaRuleParser.parseSigmaRule("NOT b > 10", contractors1),
            SigmaRuleParser.parseSigmaRule("NOT c < 0", contractors1),
            SigmaRuleParser.parseSigmaRule("NOT c > 15", contractors1),
            SigmaRuleParser.parseSigmaRule("NOT d notin {'1', '2', '3', '4'}", contractors1),
            SigmaRuleParser.parseSigmaRule("NOT d in {'1', '4'} & c==5", contractors1),
            SigmaRuleParser.parseSigmaRule("NOT a=='v' & b <= c", contractors1)
        )
        .collect(Collectors.toSet());
        
        ruleset1 = new SigmaRuleset(contractors1, rules1);

        Map<String, SigmaContractor<?>> contractors2 = new HashMap<>();
        contractors2.put("a", SigmaContractorFactory.INTEGER);
        contractors2.put("b", SigmaContractorFactory.INTEGER);
        contractors2.put("c", SigmaContractorFactory.INTEGER);
        contractors2.put("d", SigmaContractorFactory.INTEGER);
        contractors2.put("e", SigmaContractorFactory.INTEGER);
        contractors2.put("f", SigmaContractorFactory.INTEGER);
        contractors2.put("g", SigmaContractorFactory.INTEGER);
        contractors2.put("h", SigmaContractorFactory.INTEGER);
        contractors2.put("i", SigmaContractorFactory.INTEGER);
        contractors2.put("j", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules2 = Stream.of(
                        SigmaRuleParser.parseSigmaRule("NOT a != b", contractors2),
                        SigmaRuleParser.parseSigmaRule("NOT c != d", contractors2),
                        SigmaRuleParser.parseSigmaRule("NOT e != f", contractors2),
                        SigmaRuleParser.parseSigmaRule("NOT g != h", contractors2),
                        SigmaRuleParser.parseSigmaRule("NOT i != j", contractors2)
                )
                .collect(Collectors.toSet());

        ruleset2 = new SigmaRuleset(contractors2, rules2);
    }

    @Test
    public void multipleMinimalCoversTest()
    {
        DataObject o = new DataObject()
            .set("a", "v")
            .set("b", 5)
            .set("c", 5)
            .set("d", "3");
        
        ConstantCostModel costModel = new ConstantCostModel(o.getAttributes());
        
        SufficientSigmaRuleset sufficientSet = SufficientSigmaRuleset.create(ruleset1, new FCFGenerator());
        
        Set<Set<String>> minimalCovers = new MinimalCoverSearch(sufficientSet, costModel).findMinimalCovers(o, NullBehavior.NO_REPAIR);
        
        Set<Set<String>> expected = Stream.of(
            Stream.of("a").collect(Collectors.toSet()),
            Stream.of("b").collect(Collectors.toSet()),
            Stream.of("c").collect(Collectors.toSet())
        )
        .collect(Collectors.toSet());
        
        assertEquals(expected, minimalCovers);
    }
    
    @Test
    public void singleMinimalCoversTest()
    {
        DataObject o = new DataObject()
            .set("a", "v")
            .set("b", 5)
            .set("c", 5)
            .set("d", "1");
        
        ConstantCostModel costModel = new ConstantCostModel(o.getAttributes());
        
        SufficientSigmaRuleset sufficientSet = SufficientSigmaRuleset.create(ruleset1, new FCFGenerator());
        
        Set<Set<String>> minimalCovers = new MinimalCoverSearch(sufficientSet, costModel).findMinimalCovers(o, NullBehavior.NO_REPAIR);
        
        Set<Set<String>> expected = Stream.of(
            Stream.of("c").collect(Collectors.toSet())
        )
        .collect(Collectors.toSet());
        
        assertEquals(expected, minimalCovers);
    }
    
    @Test
    public void subsetMinimalCoverTest()
    {
        DataObject o = new DataObject()
            .set("a", "v")
            .set("b", 5)
            .set("c", 5)
            .set("d", "1");
        
        SufficientSigmaRuleset sufficientSet = SufficientSigmaRuleset.create(ruleset1, new FCFGenerator());
        
        Set<Set<String>> minimalCovers = new SubsetMinimalCoverSearch(sufficientSet).findMinimalCovers(o, NullBehavior.NO_REPAIR);
        
        Set<Set<String>> expected = Stream.of(
            Stream.of("c").collect(Collectors.toSet()),
            Stream.of("d", "a").collect(Collectors.toSet()),
            Stream.of("d", "b").collect(Collectors.toSet())
        )
        .collect(Collectors.toSet());
        
        assertEquals(expected, minimalCovers);
    }

    @Test
    public void conditionalSubsetMinimalCoverTest()
    {
        DataObject o = new DataObject()
            .set("a", "v")
            .set("b", 5)
            .set("c", 5)
            .set("d", "1");
        
        SufficientSigmaRuleset sufficientSet = SufficientSigmaRuleset.create(ruleset1, new FCFGenerator());
        
        Set<Set<String>> minimalCovers = new ConditionalSetCoverSearch(
            sufficientSet,
            Stream.of("a").collect(Collectors.toSet())
        )
        .findMinimalCovers(o, NullBehavior.NO_REPAIR);
        
        Set<Set<String>> expected = Stream.of(
            Stream.of("c").collect(Collectors.toSet()),
            Stream.of("d", "b").collect(Collectors.toSet())
        )
        .collect(Collectors.toSet());
        
        assertEquals(expected, minimalCovers);
    }

    @Test
    public void partitionMinimalCoverTest()
    {
        DataObject o = new DataObject()
                .set("a", 0)
                .set("b", 1)
                .set("c", 0)
                .set("d", 1)
                .set("e", 0)
                .set("f", 1)
                .set("g", 0)
                .set("h", 1)
                .set("i", 0)
                .set("j", 1);


        SufficientSigmaRuleset sufficientSet = SufficientSigmaRuleset.create(ruleset2, new FCFGenerator());

        Set<Set<String>> minimalCovers = new SubsetMinimalCoverSearch(sufficientSet).findMinimalCovers(o, NullBehavior.NO_REPAIR);

        Set<Set<String>> expected = Stream.of(
                        Stream.of("a", "c", "e", "g", "i").collect(Collectors.toSet()),
                        Stream.of("a", "c", "e", "g", "j").collect(Collectors.toSet()),
                        Stream.of("a", "c", "e", "h", "i").collect(Collectors.toSet()),
                        Stream.of("a", "c", "e", "h", "j").collect(Collectors.toSet()),
                        Stream.of("a", "c", "f", "g", "i").collect(Collectors.toSet()),
                        Stream.of("a", "c", "f", "g", "j").collect(Collectors.toSet()),
                        Stream.of("a", "c", "f", "h", "i").collect(Collectors.toSet()),
                        Stream.of("a", "c", "f", "h", "j").collect(Collectors.toSet()),
                        Stream.of("a", "d", "e", "g", "i").collect(Collectors.toSet()),
                        Stream.of("a", "d", "e", "g", "j").collect(Collectors.toSet()),
                        Stream.of("a", "d", "e", "h", "i").collect(Collectors.toSet()),
                        Stream.of("a", "d", "e", "h", "j").collect(Collectors.toSet()),
                        Stream.of("a", "d", "f", "g", "i").collect(Collectors.toSet()),
                        Stream.of("a", "d", "f", "g", "j").collect(Collectors.toSet()),
                        Stream.of("a", "d", "f", "h", "i").collect(Collectors.toSet()),
                        Stream.of("a", "d", "f", "h", "j").collect(Collectors.toSet()),
                        Stream.of("b", "c", "e", "g", "i").collect(Collectors.toSet()),
                        Stream.of("b", "c", "e", "g", "j").collect(Collectors.toSet()),
                        Stream.of("b", "c", "e", "h", "i").collect(Collectors.toSet()),
                        Stream.of("b", "c", "e", "h", "j").collect(Collectors.toSet()),
                        Stream.of("b", "c", "f", "g", "i").collect(Collectors.toSet()),
                        Stream.of("b", "c", "f", "g", "j").collect(Collectors.toSet()),
                        Stream.of("b", "c", "f", "h", "i").collect(Collectors.toSet()),
                        Stream.of("b", "c", "f", "h", "j").collect(Collectors.toSet()),
                        Stream.of("b", "d", "e", "g", "i").collect(Collectors.toSet()),
                        Stream.of("b", "d", "e", "g", "j").collect(Collectors.toSet()),
                        Stream.of("b", "d", "e", "h", "i").collect(Collectors.toSet()),
                        Stream.of("b", "d", "e", "h", "j").collect(Collectors.toSet()),
                        Stream.of("b", "d", "f", "g", "i").collect(Collectors.toSet()),
                        Stream.of("b", "d", "f", "g", "j").collect(Collectors.toSet()),
                        Stream.of("b", "d", "f", "h", "i").collect(Collectors.toSet()),
                        Stream.of("b", "d", "f", "h", "j").collect(Collectors.toSet())
                )
                .collect(Collectors.toSet());

        assertEquals(expected, minimalCovers);
    }
}
