package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class BooleanContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.BOOLEAN.hasNext(false));
        assertFalse(SigmaContractorFactory.BOOLEAN.hasNext(true));
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.BOOLEAN.hasPrevious(true));
        assertFalse(SigmaContractorFactory.BOOLEAN.hasPrevious(false));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.BOOLEAN.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(true, SigmaContractorFactory.BOOLEAN.next(false));
    }

    @Test
    public void nextOverflowTest() {
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Boolean has no next value for value " + true + " (overflow detected).");
        SigmaContractorFactory.BOOLEAN.next(true);
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.BOOLEAN.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(false, SigmaContractorFactory.BOOLEAN.previous(true));
    }

    @Test
    public void previousOverflowTest() {
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Boolean has no previous value for value " + false + " (overflow detected).");
        SigmaContractorFactory.BOOLEAN.previous(false);
    }
    
    @Test
    public void nameTest()
    {
        assertEquals(TypeContractorFactory.BOOLEAN.name(), SigmaContractorFactory.BOOLEAN.name());
    }
    
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        assertEquals(0, SigmaContractorFactory.BOOLEAN.cardinality(new Interval<>(false,true)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        assertEquals(2, SigmaContractorFactory.BOOLEAN.cardinality(new Interval<>(false,true, false, false)));
    }
    
    @Test
    public void cardinalityNullOpenTest()
    {
        assertEquals(1, SigmaContractorFactory.BOOLEAN.cardinality(new Interval<>(false,null)));
    }
    
    @Test
    public void cardinalityNullClosedTest()
    {
        assertEquals(2, SigmaContractorFactory.BOOLEAN.cardinality(new Interval<>(false,null, false, true)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(false, SigmaContractorFactory.BOOLEAN.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(true, SigmaContractorFactory.BOOLEAN.last());
    }
    
    @Test
    public void printConstantTest()
    {
        assertEquals("true", SigmaContractorFactory.BOOLEAN.printConstant(true));
        assertEquals("false", SigmaContractorFactory.BOOLEAN.printConstant(false));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(Boolean.TRUE, SigmaContractorFactory.BOOLEAN.parseConstant("true"));
        assertEquals(Boolean.FALSE, SigmaContractorFactory.BOOLEAN.parseConstant("false"));
    }

    @Test
    public void additionZeroTest()
    {
        assertEquals(false, SigmaContractorFactory.BOOLEAN.add(false, 0));
    }

    @Test
    public void additionTest()
    {
        assertEquals(true, SigmaContractorFactory.BOOLEAN.add(false, 1));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(false, SigmaContractorFactory.BOOLEAN.add(true, -1));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.BOOLEAN.add(null, 1);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Boolean " + true + " with " + 2 + " units.");
        SigmaContractorFactory.BOOLEAN.add(true, 2);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Boolean " + true + " with " + (-2) + " units.");
        SigmaContractorFactory.BOOLEAN.add(true, -2);
    } 
}
