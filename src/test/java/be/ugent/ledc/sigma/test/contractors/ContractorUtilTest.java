package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.dataset.contractors.ContractorException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorUtil;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;



public class ContractorUtilTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void getTypedContractorByNameMatchTest() {
        String name = "string";
        assertEquals(SigmaContractorFactory.STRING, SigmaContractorUtil.getTypedContractorByName(name));
    }

    @Test
    public void getTypedContractorByNameNoMatchTest() {

        String name = "test";

        exceptionRule.expect(ContractorException.class);
        exceptionRule.expectMessage("Given name " + name + " does not represent a known SigmaContractor object. Valid names are: \n"
                + Stream.concat(
                SigmaContractorUtil.getNominalContractors(),
                SigmaContractorUtil.getOrdinalContractors()
        ).map(SigmaContractor::name).collect(Collectors.joining("\n")));

        SigmaContractorUtil.getTypedContractorByName(name);
    }

}
