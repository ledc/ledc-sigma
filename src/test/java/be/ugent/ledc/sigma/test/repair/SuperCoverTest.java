package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.fd.PartialKey;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.ParkerRepair;
import be.ugent.ledc.core.cost.ConstantCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunctionWrapper;
import be.ugent.ledc.sigma.repair.cost.functions.SimpleIterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.models.ParkerModel;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFWrapper;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class SuperCoverTest
{
    @Test
    public void lowCostIterationRepairTest() throws RepairException, ParseException
    {
        Dataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
            .setInteger("a", 1)
            .setInteger("b", 2)
            .setInteger("c", 3)
        );
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream
            .of(
                SigmaRuleParser.parseSigmaRule("NOT a < 1", contractors),
                SigmaRuleParser.parseSigmaRule("NOT a > 4", contractors),
                SigmaRuleParser.parseSigmaRule("NOT b < 1", contractors),
                SigmaRuleParser.parseSigmaRule("NOT b > 4", contractors),
                SigmaRuleParser.parseSigmaRule("NOT c < 1", contractors),
                SigmaRuleParser.parseSigmaRule("NOT c > 4", contractors),
                SigmaRuleParser.parseSigmaRule("NOT a == 1 & b == 2", contractors),
                SigmaRuleParser.parseSigmaRule("NOT a == 2 & b == 2", contractors),
                SigmaRuleParser.parseSigmaRule("NOT a == 3 & c == 3", contractors)
            )
            .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);
        
        SufficientSigmaRuleset sufficientSet = new FCFWrapper().build(ruleset);
        
        Map<String, CostFunction<?>> costFunctions = new HashMap<>();
        
        Map<Integer, Map<Integer, Integer>> costMappingForA = new HashMap<>();
        
        //Conversion from value 1 to ?
        Map<Integer,Integer> submap = new HashMap<>();
        submap.put(2, 1);
        submap.put(3, 2);
        submap.put(4, 5);
        
        costMappingForA.put(1, submap);
        
        costFunctions.put(
            "a",
            new SimpleIterableCostFunction<>(costMappingForA)
        );
        
        costFunctions.put("b", new IterableCostFunctionWrapper<>(new ConstantCostFunction<Integer>(5)));
        costFunctions.put("c", new IterableCostFunctionWrapper<>(new ConstantCostFunction<Integer>(1)));
        
        PartialKey partialkey = new PartialKey
        (
            new HashSet<>(),
            new HashSet<>()
        );
        
        ParkerModel<?> costModel = new ParkerModel<>(
            partialkey,
            costFunctions,
            sufficientSet
        );
        
        ParkerRepair<?, ?> repairEngine = new ParkerRepair<>(
            costModel,
            new RandomRepairSelection());
        
        Dataset repair = repairEngine.repair(data);

        assertEquals(3, (int) repair.getDataObjects().get(0).getInteger("a"));
        assertTrue(SetOperations
            .set(1, 2, 4)
            .contains(repair
                .getDataObjects()
                .get(0)
                .getInteger("c")));
    }
}
