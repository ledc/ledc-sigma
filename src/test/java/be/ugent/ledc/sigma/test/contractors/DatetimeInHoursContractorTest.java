package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class DatetimeInHoursContractorTest
{

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_HOURS.hasNext(LocalDateTime.of(2020, Month.OCTOBER, 30, 12, 35, 48)));
    }

    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_HOURS.hasNext(SigmaContractorFactory.DATETIME_HOURS.last().minusHours(1)));
        assertFalse(SigmaContractorFactory.DATETIME_HOURS.hasNext(SigmaContractorFactory.DATETIME_HOURS.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.DATETIME_HOURS.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(LocalDateTime.of(2021, Month.JULY, 8, 14, 0, 0), SigmaContractorFactory.DATETIME_HOURS.next(LocalDateTime.of(2021, Month.JULY, 8, 13, 21, 16)));
    }

    @Test
    public void nextOverflowTest() {
        LocalDateTime current = LocalDateTime.MAX;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalDateTime has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DATETIME_HOURS.next(current);
    }

    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_HOURS.hasPrevious(LocalDateTime.of(2020, Month.OCTOBER, 30, 12, 35, 48)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.DATETIME_HOURS.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(LocalDateTime.of(2021, Month.JULY, 8, 12, 0, 0), SigmaContractorFactory.DATETIME_HOURS.previous(LocalDateTime.of(2021, Month.JULY, 8, 13, 21, 16)));
    }

    @Test
    public void previousOverflowTest() {
        LocalDateTime current = LocalDateTime.MIN;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalDateTime has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DATETIME_HOURS.previous(current);
    }

    @Test
    public void nameTest()
    {
        assertEquals("datetime_in_hours", SigmaContractorFactory.DATETIME_HOURS.name());
    }

    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_HOURS.hasPrevious(SigmaContractorFactory.DATETIME_HOURS.first().plusHours(1)));
        assertFalse(SigmaContractorFactory.DATETIME_HOURS.hasPrevious(SigmaContractorFactory.DATETIME_HOURS.first()));
    }

    @Test
    public void cardinalityOpenIntervalTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        LocalDateTime end = LocalDateTime.of(2020, Month.OCTOBER, 31, 12, 30, 20);
        assertEquals(24, SigmaContractorFactory.DATETIME_HOURS.cardinality(new Interval<>(start, end)));
    }

    @Test
    public void cardinalityClosedIntervalTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        LocalDateTime end = LocalDateTime.of(2020, Month.OCTOBER, 31, 12, 30, 20);

        assertEquals(26, SigmaContractorFactory.DATETIME_HOURS.cardinality(new Interval<>(start, end, false, false)));
    }

    @Test
    public void cardinalityNullTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_HOURS.cardinality(new Interval<>(start, null)));
    }

    @Test
    public void cardinalityFullSpanTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_HOURS.cardinality(new Interval<>(LocalDateTime.MIN, LocalDateTime.MAX, false, false)));
    }

    @Test
    public void cardinalityRightMaxTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_HOURS.cardinality(new Interval<>(start, LocalDateTime.MAX)));
    }

    @Test
    public void cardinalityLeftMaxTest()
    {
        LocalDateTime end = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_HOURS.cardinality(new Interval<>(LocalDateTime.MIN, end, true, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.DATETIME_HOURS.cardinality(new Interval<>( LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40),  LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 37, 12), true, false)));
    }

    @Test
    public void firstTest()
    {
        assertEquals(LocalDateTime
            .MIN
            .withMinute(0)
            .withSecond(0)
            .withNano(0),
            
            SigmaContractorFactory.DATETIME_HOURS.first());
    }

    @Test
    public void lastTest()
    {
        assertEquals(LocalDateTime
            .MAX
            .withMinute(0)
            .withSecond(0)
            .withNano(0),
            SigmaContractorFactory.DATETIME_HOURS.last());
    }

    @Test
    public void printConstantTest()
    {
        LocalDateTime d = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);

        assertEquals("2020-10-30T11:00", SigmaContractorFactory.DATETIME_HOURS.printConstant(d));
    }

    @Test
    public void parseConstantTest()
    {
        assertEquals(
                LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 0, 0),
                SigmaContractorFactory.DATETIME_HOURS
                        .parseConstant("2020-10-30T11:30:40")
        );
    }

    @Test
    public void additionTest()
    {
        assertEquals(
                LocalDateTime.of(2020, Month.NOVEMBER, 2, 2, 0, 0),
                SigmaContractorFactory.DATETIME_HOURS
                        .add(LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40), 63));
    }

    @Test
    public void additionNegativeTest()
    {
        assertEquals(
                LocalDateTime.of(2020, Month.OCTOBER, 27, 20, 0, 0),
                SigmaContractorFactory.DATETIME_HOURS
                        .add(LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40), -63));
    }

    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.DATETIME_HOURS.add(null, 63);
    }

    @Test
    public void additionOverflowRightTest()
    {
        LocalDateTime value = LocalDateTime.MAX.minusHours(5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalDateTime " + value + " with " + units + " hours.");
        SigmaContractorFactory.DATETIME_HOURS.add(value, units);
    }

    @Test
    public void additionOverflowLeftTest()
    {
        LocalDateTime value = LocalDateTime.MIN.plusHours(5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalDateTime " + value + " with " + units + " hours.");
        SigmaContractorFactory.DATETIME_HOURS.add(value, units);
    }
    
    @Test
    public void differenceTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 27, 20, 10, 10);
        LocalDateTime end   = LocalDateTime.of(2020, Month.OCTOBER, 27, 23, 20, 13);
        
        assertEquals(3, (long)SigmaContractorFactory.DATETIME_HOURS.differenceInUnits(end, start));
        assertEquals(190, (long)SigmaContractorFactory.DATETIME_MINUTES.differenceInUnits(end, start));
        assertNull(SigmaContractorFactory.DATETIME_MINUTES.differenceInUnits(end, null));
    }
}
