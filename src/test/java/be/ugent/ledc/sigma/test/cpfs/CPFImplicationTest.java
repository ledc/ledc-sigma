package be.ugent.ledc.sigma.test.cpfs;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.formulas.CPFImplicator;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.io.CPFParser;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class CPFImplicationTest {

    @Test
    public void implyVariableNominalNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a != b & a == c", contractors);
        CPF cpf2 = CPFParser.parseCPF("b != c & a == c", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();
        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableNominalAtom<String> impliedAtom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> impliedAtom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "a");
        VariableNominalAtom<String> impliedAtom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "c");
        VariableNominalAtom<String> impliedAtom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "c", EqualityOperator.EQ, "a");
        VariableNominalAtom<String> impliedAtom31 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "c");
        VariableNominalAtom<String> impliedAtom32 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "c", EqualityOperator.NEQ, "b");

        assertTrue(impliedAtoms1.size() == 3
                && (impliedAtoms1.contains(impliedAtom11) || impliedAtoms1.contains(impliedAtom12))
                && (impliedAtoms1.contains(impliedAtom21) || impliedAtoms1.contains(impliedAtom22))
                && (impliedAtoms1.contains(impliedAtom31) || impliedAtoms1.contains(impliedAtom32))
        );

        assertTrue(impliedAtoms2.size() == 3
                && (impliedAtoms2.contains(impliedAtom11) || impliedAtoms2.contains(impliedAtom12))
                && (impliedAtoms2.contains(impliedAtom21) || impliedAtoms2.contains(impliedAtom22))
                && (impliedAtoms2.contains(impliedAtom31) || impliedAtoms2.contains(impliedAtom32))
        );

    }

    @Test
    public void implyVariableNominalContradictionOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        CPF cpf = CPFParser.parseCPF("a != b & b == c & a == c", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = new HashSet<>();
        impliedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF impliedCPF = new CPF(impliedAtoms);

        assertEquals(impliedCPF, CPFImplicator.imply(cpf));

    }

    @Test
    public void implyVariableOrdinalNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("b > c & a < c", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = CPFImplicator.imply(cpf).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c");
        VariableOrdinalAtom<Integer> impliedAtom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.LT, "b");
        VariableOrdinalAtom<Integer> impliedAtom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c");
        VariableOrdinalAtom<Integer> impliedAtom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.GT, "a");
        VariableOrdinalAtom<Integer> impliedAtom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -1);
        VariableOrdinalAtom<Integer> impliedAtom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", 1);

        assertTrue(impliedAtoms.size() == 3
                && (impliedAtoms.contains(impliedAtom11) || impliedAtoms.contains(impliedAtom12))
                && (impliedAtoms.contains(impliedAtom21) || impliedAtoms.contains(impliedAtom22))
                && (impliedAtoms.contains(impliedAtom31) || impliedAtoms.contains(impliedAtom32))
        );

    }

    @Test
    public void implyVariableOrdinalTransitiveLowerThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a < S^2(b) & b < S^-1(c)", contractors);
        CPF cpf2 = CPFParser.parseCPF("a < S^2(b) & b == S^-1(c)", contractors);
        CPF cpf3 = CPFParser.parseCPF("b == S^-1(c) & a < S^1(c)", contractors);
        CPF cpf4 = CPFParser.parseCPF("a == b & b == S^-3(c)", contractors);
        CPF cpf5 = CPFParser.parseCPF("a == b & a == S^-3(c)", contractors);
        CPF cpf6 = CPFParser.parseCPF("b == S^-3(c) & a == S^-3(c)", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -2);
        VariableOrdinalAtom<Integer> impliedAtom121 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "c", -1);
        VariableOrdinalAtom<Integer> impliedAtom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom131 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c");
        VariableOrdinalAtom<Integer> impliedAtom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.GT, "a");

        assertTrue(impliedAtoms1.size() == 3
                && (impliedAtoms1.contains(impliedAtom111) || impliedAtoms1.contains(impliedAtom112))
                && (impliedAtoms1.contains(impliedAtom121) || impliedAtoms1.contains(impliedAtom122))
                && (impliedAtoms1.contains(impliedAtom131) || impliedAtoms1.contains(impliedAtom132))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom211 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom212 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -2);
        VariableOrdinalAtom<Integer> impliedAtom221 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", -1);
        VariableOrdinalAtom<Integer> impliedAtom222 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom231 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom232 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.GT, "a", -1);

        assertTrue(impliedAtoms2.size() == 3
                && (impliedAtoms2.contains(impliedAtom211) || impliedAtoms2.contains(impliedAtom212))
                && (impliedAtoms2.contains(impliedAtom221) || impliedAtoms2.contains(impliedAtom222))
                && (impliedAtoms2.contains(impliedAtom231) || impliedAtoms2.contains(impliedAtom232))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms3 = CPFImplicator.imply(cpf3).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom311 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", -1);
        VariableOrdinalAtom<Integer> impliedAtom312 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom321 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom322 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.GT, "a", -1);
        VariableOrdinalAtom<Integer> impliedAtom331 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom332 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -2);

        assertTrue(impliedAtoms3.size() == 3
                && (impliedAtoms3.contains(impliedAtom311) || impliedAtoms3.contains(impliedAtom312))
                && (impliedAtoms3.contains(impliedAtom321) || impliedAtoms3.contains(impliedAtom322))
                && (impliedAtoms3.contains(impliedAtom331) || impliedAtoms3.contains(impliedAtom332))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms4 = CPFImplicator.imply(cpf4).getAtoms();
        Set<AbstractAtom<?, ?, ?>> impliedAtoms5 = CPFImplicator.imply(cpf5).getAtoms();
        Set<AbstractAtom<?, ?, ?>> impliedAtoms6 = CPFImplicator.imply(cpf6).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom411 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b");
        VariableOrdinalAtom<Integer> impliedAtom412 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a");
        VariableOrdinalAtom<Integer> impliedAtom421 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", -3);
        VariableOrdinalAtom<Integer> impliedAtom422 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", EqualityOperator.EQ, "b", 3);
        VariableOrdinalAtom<Integer> impliedAtom431 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "c", -3);
        VariableOrdinalAtom<Integer> impliedAtom432 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", EqualityOperator.EQ, "a", 3);

        assertTrue(impliedAtoms4.size() == 3
                && (impliedAtoms4.contains(impliedAtom411) || impliedAtoms4.contains(impliedAtom412))
                && (impliedAtoms4.contains(impliedAtom421) || impliedAtoms4.contains(impliedAtom422))
                && (impliedAtoms4.contains(impliedAtom431) || impliedAtoms4.contains(impliedAtom432))
        );

        assertTrue(impliedAtoms5.size() == 3
                && (impliedAtoms5.contains(impliedAtom411) || impliedAtoms5.contains(impliedAtom412))
                && (impliedAtoms5.contains(impliedAtom421) || impliedAtoms5.contains(impliedAtom422))
                && (impliedAtoms5.contains(impliedAtom431) || impliedAtoms5.contains(impliedAtom432))
        );

        assertTrue(impliedAtoms6.size() == 3
                && (impliedAtoms6.contains(impliedAtom411) || impliedAtoms6.contains(impliedAtom412))
                && (impliedAtoms6.contains(impliedAtom421) || impliedAtoms6.contains(impliedAtom422))
                && (impliedAtoms6.contains(impliedAtom431) || impliedAtoms6.contains(impliedAtom432))
        );

    }

    @Test
    public void implyVariableOrdinalTransitiveGreaterThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a > S^2(b) & b > S^-1(c)", contractors);
        CPF cpf2 = CPFParser.parseCPF("a > S^2(b) & b == S^-1(c)", contractors);
        CPF cpf3 = CPFParser.parseCPF("b == S^-1(c) & a > S^1(c)", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -2);
        VariableOrdinalAtom<Integer> impliedAtom121 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c", -1);
        VariableOrdinalAtom<Integer> impliedAtom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom131 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "c", 2);
        VariableOrdinalAtom<Integer> impliedAtom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.LT, "a", -2);

        assertTrue(impliedAtoms1.size() == 3
                && (impliedAtoms1.contains(impliedAtom111) || impliedAtoms1.contains(impliedAtom112))
                && (impliedAtoms1.contains(impliedAtom121) || impliedAtoms1.contains(impliedAtom122))
                && (impliedAtoms1.contains(impliedAtom131) || impliedAtoms1.contains(impliedAtom132))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom211 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom212 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -2);
        VariableOrdinalAtom<Integer> impliedAtom221 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", -1);
        VariableOrdinalAtom<Integer> impliedAtom222 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom231 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom232 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.LT, "a", -1);

        assertTrue(impliedAtoms2.size() == 3
                && (impliedAtoms2.contains(impliedAtom211) || impliedAtoms2.contains(impliedAtom212))
                && (impliedAtoms2.contains(impliedAtom221) || impliedAtoms2.contains(impliedAtom222))
                && (impliedAtoms2.contains(impliedAtom231) || impliedAtoms2.contains(impliedAtom232))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms3 = CPFImplicator.imply(cpf3).getAtoms();

        assertTrue(impliedAtoms3.size() == 3
                && (impliedAtoms3.contains(impliedAtom211) || impliedAtoms3.contains(impliedAtom212))
                && (impliedAtoms3.contains(impliedAtom221) || impliedAtoms3.contains(impliedAtom222))
                && (impliedAtoms3.contains(impliedAtom231) || impliedAtoms3.contains(impliedAtom232))
        );

    }

    @Test
    public void implyVariableOrdinalIntersectionLowerThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a < b & a < S^1(b) & a < S^-1(b)", contractors);
        CPF cpf2 = CPFParser.parseCPF("a < S^-1(b) & a == S^-3(b)", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -1);
        VariableOrdinalAtom<Integer> impliedAtom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", 1);

        assertTrue(impliedAtoms1.size() == 1 && (impliedAtoms1.contains(impliedAtom11) || impliedAtoms1.contains(impliedAtom12)));

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", -3);
        VariableOrdinalAtom<Integer> impliedAtom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a", 3);

        assertTrue(impliedAtoms2.size() == 1 && (impliedAtoms2.contains(impliedAtom21) || impliedAtoms2.contains(impliedAtom22)));

    }

    @Test
    public void implyVariableOrdinalIntersectionGreaterThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a > b & a > S^1(b) & a > S^-1(b)", contractors);
        CPF cpf2 = CPFParser.parseCPF("a > S^1(b) & a == S^2(b)", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -1);

        assertTrue(impliedAtoms1.size() == 1 && (impliedAtoms1.contains(impliedAtom11) || impliedAtoms1.contains(impliedAtom12)));

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a", -2);

        assertTrue(impliedAtoms2.size() == 1 && (impliedAtoms2.contains(impliedAtom21) || impliedAtoms2.contains(impliedAtom22)));

    }

    @Test
    public void implyVariableOrdinalContradictionTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a < b & a > b", contractors);
        CPF cpf2 = CPFParser.parseCPF("a < b & a > S^-1(b)", contractors);
        CPF cpf3 = CPFParser.parseCPF("a < S^-1(b) & a > S^-2(b)", contractors);
        CPF cpf4 = CPFParser.parseCPF("a == S^1(b) & b == S^-2(c) & a == c", contractors);
        CPF cpf5 = CPFParser.parseCPF("a < S^-1(b) & a == S^-1(b)", contractors);
        CPF cpf6 = CPFParser.parseCPF("a > S^1(b) & a == S^1(b)", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = new HashSet<>();
        impliedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF impliedCPF = new CPF(impliedAtoms);

        assertEquals(impliedCPF, CPFImplicator.imply(cpf1));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf2));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf3));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf4));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf5));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf6));

    }

    @Test
    public void implyNonVariableOrdinalTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a <= 4 & a >= 1", contractors);
        CPF cpf2 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 2", contractors);
        CPF cpf3 = CPFParser.parseCPF("a <= 4 & a <= 2", contractors);
        CPF cpf4 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 1 & a >= 0", contractors);
        CPF cpf5 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 2 & a >= 0", contractors);
        CPF cpf6 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 6 & a >= 0", contractors);
        CPF cpf7 = CPFParser.parseCPF("a <= 4 & a >= 1 & a <= 2", contractors);
        CPF cpf8 = CPFParser.parseCPF("a <= 4", contractors);
        CPF cpf9 = CPFParser.parseCPF("a >= 1", contractors);
        CPF cpf10 = CPFParser.parseCPF("a <= 4 & a >= 1 & a >= 2", contractors);
        CPF cpf11 = CPFParser.parseCPF("a >= 4 & a >= 2", contractors);
        CPF cpf12 = CPFParser.parseCPF("a <= 1 & a >= 1", contractors);
        CPF cpf13 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a IN {1,4}", contractors);
        CPF cpf14 = CPFParser.parseCPF("a NOTIN {1,2,3,4} & a NOTIN {1,3,5} & a NOTIN {6}", contractors);
        CPF cpf15 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a IN {1,4} & a NOTIN {1,2} & a NOTIN {2,3}", contractors);
        CPF cpf16 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a >= 3 & a <= 5", contractors);
        CPF cpf17 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a >= 0 & a <= 5", contractors);
        CPF cpf18 = CPFParser.parseCPF("a IN {1,2,3,4,6} & a IN {1,3,4,6,8,9} & a >= 0", contractors);
        CPF cpf19 = CPFParser.parseCPF("a IN {1,3,5} & a == 1 & a >= 0", contractors);
        CPF cpf20 = CPFParser.parseCPF("a NOTIN {1,3,5} & a <= 0", contractors);
        CPF cpf21 = CPFParser.parseCPF("a NOTIN {1,3,4,5,7} & a >= 1 & a <= 8", contractors);
        CPF cpf22 = CPFParser.parseCPF("a NOTIN {1,2,3,8} & a >= 1 & a <= 8", contractors);
        CPF cpf23 = CPFParser.parseCPF("a NOTIN {1,2,3,5,8} & a >= 1 & a <= 8", contractors);
        CPF cpf24 = CPFParser.parseCPF("a NOTIN {1,2,3,8} & a >= 1", contractors);
        CPF cpf25 = CPFParser.parseCPF("a NOTIN {1,2,3,8} & a <= 8", contractors);
        CPF cpf26 = CPFParser.parseCPF("a NOTIN {1,2,4} & a >= 1 & a <= 4", contractors);

        CPF impliedCPF1 = CPFParser.parseCPF("a <= 4 & a >= 1", contractors);
        CPF impliedCPF2 = CPFParser.parseCPF("a >= 1 & a <= 2", contractors);
        CPF impliedCPF3 = CPFParser.parseCPF("a <= 2", contractors);
        CPF impliedCPF4 = CPFParser.parseCPF("a == 1", contractors);
        CPF impliedCPF5 = CPFParser.parseCPF("a >= 1 & a <= 2", contractors);
        CPF impliedCPF6 = CPFParser.parseCPF("a <= 4 & a >= 1", contractors);
        CPF impliedCPF7 = CPFParser.parseCPF("a >= 1 & a <= 2", contractors);
        CPF impliedCPF8 = CPFParser.parseCPF("a <= 4", contractors);
        CPF impliedCPF9 = CPFParser.parseCPF("a >= 1", contractors);
        CPF impliedCPF10 = CPFParser.parseCPF("a <= 4 & a >= 2", contractors);
        CPF impliedCPF11 = CPFParser.parseCPF("a >= 4", contractors);
        CPF impliedCPF12 = CPFParser.parseCPF("a == 1", contractors);
        CPF impliedCPF13 = CPFParser.parseCPF("a IN {1,4}", contractors);
        CPF impliedCPF14 = CPFParser.parseCPF("a NOTIN {1,2,3,4,5,6}", contractors);
        CPF impliedCPF15 = CPFParser.parseCPF("a == 4", contractors);
        CPF impliedCPF16 = CPFParser.parseCPF("a IN {3,4}", contractors);
        CPF impliedCPF17 = CPFParser.parseCPF("a IN {1,3,4}", contractors);
        CPF impliedCPF18 = CPFParser.parseCPF("a IN {1,3,4,6}", contractors);
        CPF impliedCPF19 = CPFParser.parseCPF("a == 1", contractors);
        CPF impliedCPF20 = CPFParser.parseCPF("a <= 0", contractors);
        CPF impliedCPF21 = CPFParser.parseCPF("a NOTIN {3,4,5,7} & a >= 2 & a <= 8", contractors);
        CPF impliedCPF22 = CPFParser.parseCPF("a >= 4 & a <= 7", contractors);
        CPF impliedCPF23 = CPFParser.parseCPF("a != 5 & a >= 4 & a <= 7", contractors);
        CPF impliedCPF24 = CPFParser.parseCPF("a != 8 & a >= 4", contractors);
        CPF impliedCPF25 = CPFParser.parseCPF("a NOTIN {1,2,3} & a <= 7", contractors);
        CPF impliedCPF26 = CPFParser.parseCPF("a == 3", contractors);

        assertEquals(impliedCPF1, CPFImplicator.imply(cpf1));
        assertEquals(impliedCPF2, CPFImplicator.imply(cpf2));
        assertEquals(impliedCPF3, CPFImplicator.imply(cpf3));
        assertEquals(impliedCPF4, CPFImplicator.imply(cpf4));
        assertEquals(impliedCPF5, CPFImplicator.imply(cpf5));
        assertEquals(impliedCPF6, CPFImplicator.imply(cpf6));
        assertEquals(impliedCPF7, CPFImplicator.imply(cpf7));
        assertEquals(impliedCPF8, CPFImplicator.imply(cpf8));
        assertEquals(impliedCPF9, CPFImplicator.imply(cpf9));
        assertEquals(impliedCPF10, CPFImplicator.imply(cpf10));
        assertEquals(impliedCPF11, CPFImplicator.imply(cpf11));
        assertEquals(impliedCPF12, CPFImplicator.imply(cpf12));
        assertEquals(impliedCPF13, CPFImplicator.imply(cpf13));
        assertEquals(impliedCPF14, CPFImplicator.imply(cpf14));
        assertEquals(impliedCPF15, CPFImplicator.imply(cpf15));
        assertEquals(impliedCPF16, CPFImplicator.imply(cpf16));
        assertEquals(impliedCPF17, CPFImplicator.imply(cpf17));
        assertEquals(impliedCPF18, CPFImplicator.imply(cpf18));
        assertEquals(impliedCPF19, CPFImplicator.imply(cpf19));
        assertEquals(impliedCPF20, CPFImplicator.imply(cpf20));
        assertEquals(impliedCPF21, CPFImplicator.imply(cpf21));
        assertEquals(impliedCPF22, CPFImplicator.imply(cpf22));
        assertEquals(impliedCPF23, CPFImplicator.imply(cpf23));
        assertEquals(impliedCPF24, CPFImplicator.imply(cpf24));
        assertEquals(impliedCPF25, CPFImplicator.imply(cpf25));
        assertEquals(impliedCPF26, CPFImplicator.imply(cpf26));

    }

    @Test
    public void implyConstantOrdinalContradictionTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a <= 4 & a >= 5", contractors);
        CPF cpf2 = CPFParser.parseCPF("a >= 5 & a <= 4", contractors);
        CPF cpf3 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4} & a IN {1,4} & a NOTIN {1,2} & a NOTIN {2,4}", contractors);
        CPF cpf4 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {4,5} & a == 6", contractors);
        CPF cpf5 = CPFParser.parseCPF("a IN {1,2,3,4} & a IN {1,3,4,5} & a >= 5", contractors);
        CPF cpf6 = CPFParser.parseCPF("a NOTIN {1,2,3,4,5,6,7,8} & a >= 1 & a <= 8", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = new HashSet<>();
        impliedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF impliedCPF = new CPF(impliedAtoms);

        assertEquals(impliedCPF, CPFImplicator.imply(cpf1));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf2));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf3));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf4));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf5));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf6));


    }

    @Test
    public void implySetNominalTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a IN {'1','2'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a NOTIN {'1','2'}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a IN {'1','2','3'} & a IN {'2','3','4'}", contractors);
        CPF cpf4 = CPFParser.parseCPF("a NOTIN {'1','2','3'} & a NOTIN {'2','3','4'}", contractors);
        CPF cpf5 = CPFParser.parseCPF("a IN {'1','2'} & a IN {'1','2','3'} & a NOTIN {'2','3','4'}", contractors);

        CPF impliedCPF1 = CPFParser.parseCPF("a IN {'1','2'}", contractors);
        CPF impliedCPF2 = CPFParser.parseCPF("a NOTIN {'1','2'}", contractors);
        CPF impliedCPF3 = CPFParser.parseCPF("a IN {'2','3'}", contractors);
        CPF impliedCPF4 = CPFParser.parseCPF("a NOTIN {'1','2','3','4'}", contractors);
        CPF impliedCPF5 = CPFParser.parseCPF("a == '1'", contractors);

        assertEquals(impliedCPF1, CPFImplicator.imply(cpf1));
        assertEquals(impliedCPF2, CPFImplicator.imply(cpf2));
        assertEquals(impliedCPF3, CPFImplicator.imply(cpf3));
        assertEquals(impliedCPF4, CPFImplicator.imply(cpf4));
        assertEquals(impliedCPF5, CPFImplicator.imply(cpf5));

    }

    @Test
    public void implySetNominalContradictionTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a == '1' & a != '1'", contractors);
        CPF cpf2 = CPFParser.parseCPF("a == '1' & a == '2'", contractors);
        CPF cpf3 = CPFParser.parseCPF("a IN {'1','2'} & a IN {'2','3'} & a != '2'", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = new HashSet<>();
        impliedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        CPF impliedCPF = new CPF(impliedAtoms);

        assertEquals(impliedCPF, CPFImplicator.imply(cpf1));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf2));
        assertEquals(impliedCPF, CPFImplicator.imply(cpf3));

    }

    @Test
    public void implyConstantOrdinalWithVariableNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < b & b > c & a <= 2 & c <= 6", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = CPFImplicator.imply(cpf).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b");
        VariableOrdinalAtom<Integer> impliedAtom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a");
        VariableOrdinalAtom<Integer> impliedAtom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c");
        VariableOrdinalAtom<Integer> impliedAtom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.LT, "b");
        ConstantOrdinalAtom<Integer> impliedAtom3 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 2);
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", InequalityOperator.LEQ, 6);

        assertTrue(impliedAtoms.size() == 4
                && (impliedAtoms.contains(impliedAtom11) || impliedAtoms.contains(impliedAtom12))
                && (impliedAtoms.contains(impliedAtom21) || impliedAtoms.contains(impliedAtom22))
                && (impliedAtoms.contains(impliedAtom3)) && (impliedAtoms.contains(impliedAtom4))
        );

    }

    @Test
    public void implyConstantOrdinalWithVariableLowerThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a < S^2(b) & a >= 0 & b <= 5", contractors);
        CPF cpf2 = CPFParser.parseCPF("a < S^2(b) & a IN {0,1,4,5,6,8} & b IN {-10,-5,-1,0,1,5,6}", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -2);
        ConstantOrdinalAtom<Integer> impliedAtom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 0);
        ConstantOrdinalAtom<Integer> impliedAtom13 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 7);
        ConstantOrdinalAtom<Integer> impliedAtom14 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, -2);
        ConstantOrdinalAtom<Integer> impliedAtom15 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, 5);

        assertTrue(impliedAtoms1.size() == 5
                && (impliedAtoms1.contains(impliedAtom111) || impliedAtoms1.contains(impliedAtom112))
                && (impliedAtoms1.contains(impliedAtom12)) && (impliedAtoms1.contains(impliedAtom13)) && (impliedAtoms1.contains(impliedAtom14)) && (impliedAtoms1.contains(impliedAtom15))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom211 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom212 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "a", -2);
        SetOrdinalAtom<Integer> impliedAtom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(0,1,4,5,6)));
        SetOrdinalAtom<Integer> impliedAtom23 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(-1,0,1,5,6)));

        assertTrue(impliedAtoms2.size() == 3
                && (impliedAtoms2.contains(impliedAtom211) || impliedAtoms2.contains(impliedAtom212))
                && (impliedAtoms2.contains(impliedAtom22)) && (impliedAtoms2.contains(impliedAtom23))
        );

    }

    @Test
    public void implyConstantOrdinalWithVariableGreaterThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a > S^2(b) & a <= 5 & b >= 0", contractors);
        CPF cpf2 = CPFParser.parseCPF("a > S^2(b) & a IN {0,1,4,5,6,8} & b IN {-10,-5,-1,0,1,5,6}", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -2);
        ConstantOrdinalAtom<Integer> impliedAtom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom13 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);
        ConstantOrdinalAtom<Integer> impliedAtom14 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, 0);
        ConstantOrdinalAtom<Integer> impliedAtom15 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, 3);

        assertTrue(impliedAtoms1.size() == 5
                && (impliedAtoms1.contains(impliedAtom111) || impliedAtoms1.contains(impliedAtom112))
                && (impliedAtoms1.contains(impliedAtom12)) && (impliedAtoms1.contains(impliedAtom13)) && (impliedAtoms1.contains(impliedAtom14)) && (impliedAtoms1.contains(impliedAtom15))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom211 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom212 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "a", -2);
        SetOrdinalAtom<Integer> impliedAtom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(0,1,4,5,6,8)));
        SetOrdinalAtom<Integer> impliedAtom23 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(-10,-5,-1,0,1,5)));

        assertTrue(impliedAtoms2.size() == 3
                && (impliedAtoms2.contains(impliedAtom211) || impliedAtoms2.contains(impliedAtom212))
                && (impliedAtoms2.contains(impliedAtom22)) && (impliedAtoms2.contains(impliedAtom23))
        );

    }

    @Test
    public void implyConstantOrdinalWithVariableEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a == S^2(b) & a IN {0,1,2,3,5,8,9} & b >= 0 & b <= 10", contractors);
        CPF cpf2 = CPFParser.parseCPF("a == S^2(b) & a >= 0 & a <= 3 & a != 1 & b IN {0,5}", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableOrdinalAtom<Integer> impliedAtom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "a", -2);
        SetOrdinalAtom<Integer> impliedAtom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(2,3,5,8,9)));
        SetOrdinalAtom<Integer> impliedAtom13 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(0,1,3,6,7)));

        assertTrue(impliedAtoms1.size() == 3
                && (impliedAtoms1.contains(impliedAtom111) || impliedAtoms1.contains(impliedAtom112))
                && (impliedAtoms1.contains(impliedAtom12)) && (impliedAtoms1.contains(impliedAtom13))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        ConstantOrdinalAtom<Integer> impliedAtom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 2);
        ConstantOrdinalAtom<Integer> impliedAtom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, 0);

        assertTrue(impliedAtoms2.size() == 2 && (impliedAtoms2.contains(impliedAtom21)) && (impliedAtoms2.contains(impliedAtom22)));

    }

    @Test
    public void implySetNominalWithVariableNoTransitiveOperatorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        CPF cpf = CPFParser.parseCPF("a != b & b != c & a NOTIN {'2','3','4'} & c != '6'", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms = CPFImplicator.imply(cpf).getAtoms();

        VariableNominalAtom<String> impliedAtom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> impliedAtom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "a");
        VariableNominalAtom<String> impliedAtom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "c");
        VariableNominalAtom<String> impliedAtom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "c", EqualityOperator.NEQ, "b");
        SetNominalAtom<String> impliedAtom3 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList("2", "3", "4")));
        ConstantNominalAtom<String> impliedAtom4 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "c", EqualityOperator.NEQ, "6");

        assertTrue(impliedAtoms.size() == 4
                && (impliedAtoms.contains(impliedAtom11) || impliedAtoms.contains(impliedAtom12))
                && (impliedAtoms.contains(impliedAtom21) || impliedAtoms.contains(impliedAtom22))
                && (impliedAtoms.contains(impliedAtom3)) && (impliedAtoms.contains(impliedAtom4))
        );

    }

    @Test
    public void implySetNominalWithVariableEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a == b & a IN {'2','3'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a == b & a NOTIN {'2','3'}", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableNominalAtom<String> impliedAtom111 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> impliedAtom112 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "a");
        SetNominalAtom<String> impliedAtom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("2", "3")));
        SetNominalAtom<String> impliedAtom13 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "b", SetOperator.IN, new HashSet<>(Arrays.asList("2", "3")));

        assertTrue(impliedAtoms1.size() == 3
                && (impliedAtoms1.contains(impliedAtom111) || impliedAtoms1.contains(impliedAtom112))
                && (impliedAtoms1.contains(impliedAtom12)) && (impliedAtoms1.contains(impliedAtom13))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        VariableNominalAtom<String> impliedAtom211 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> impliedAtom212 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "a");
        SetNominalAtom<String> impliedAtom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList("2", "3")));
        SetNominalAtom<String> impliedAtom23 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "b", SetOperator.NOTIN, new HashSet<>(Arrays.asList("2", "3")));

        assertTrue(impliedAtoms2.size() == 3
                && (impliedAtoms2.contains(impliedAtom211) || impliedAtoms2.contains(impliedAtom212))
                && (impliedAtoms2.contains(impliedAtom22)) && (impliedAtoms2.contains(impliedAtom23))
        );

    }

    @Test
    public void implySetNominalWithVariableNotEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a != b & a IN {'2','3'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a != b & a == '2'", contractors);
        CPF cpf3 = CPFParser.parseCPF("a != b & a != '2'", contractors);

        Set<AbstractAtom<?, ?, ?>> impliedAtoms1 = CPFImplicator.imply(cpf1).getAtoms();

        VariableNominalAtom<String> impliedAtom111 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> impliedAtom112 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "a");
        SetNominalAtom<String> impliedAtom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("2", "3")));

        assertTrue(impliedAtoms1.size() == 2
                && (impliedAtoms1.contains(impliedAtom111) || impliedAtoms1.contains(impliedAtom112)) && (impliedAtoms1.contains(impliedAtom12))
        );

        Set<AbstractAtom<?, ?, ?>> impliedAtoms2 = CPFImplicator.imply(cpf2).getAtoms();

        ConstantNominalAtom<String> impliedAtom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "2");
        ConstantNominalAtom<String> impliedAtom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "2");

        assertTrue(impliedAtoms2.size() == 2 && (impliedAtoms2.contains(impliedAtom21)) && (impliedAtoms2.contains(impliedAtom22)));

        Set<AbstractAtom<?, ?, ?>> impliedAtoms3 = CPFImplicator.imply(cpf3).getAtoms();

        VariableNominalAtom<String> impliedAtom311 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> impliedAtom312 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "a");
        ConstantNominalAtom<String> impliedAtom32 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");

        assertTrue(impliedAtoms3.size() == 2
                && (impliedAtoms3.contains(impliedAtom311) || impliedAtoms3.contains(impliedAtom312)) && (impliedAtoms3.contains(impliedAtom32))
        );

    }

    @Test
    public void dummyAtomContradictionTest() {
        assertTrue(new CPF(AbstractAtom.ALWAYS_FALSE).isContradiction());
    }

    @Test
    public void dummyAtomContradictionTest2() throws ParseException {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        CPF cpf = CPFParser.parseCPF("a != b & a == '2'", contractors);
        cpf.getAtoms().add(AbstractAtom.ALWAYS_FALSE);

        assertTrue(cpf.isContradiction());
    }

    @Test
    public void dummyAtomTautologyTest() {
        assertTrue(new CPF(AbstractAtom.ALWAYS_TRUE).isTautology());
    }
}
