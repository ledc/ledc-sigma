package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.math.BigDecimal;
import java.math.RoundingMode;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class BigDecimalZeroDecimalsContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.hasNext(BigDecimal.valueOf(1000L)));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.hasNext(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.last().subtract(BigDecimal.ONE)));
        assertFalse(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.hasNext(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(BigDecimal.valueOf(153), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.next(BigDecimal.valueOf(152)));
    }

    @Test
    public void nextOverflowTest() {
        BigDecimal current = BigDecimal.valueOf(Double.MAX_VALUE);
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("BigDecimal has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.hasPrevious(BigDecimal.valueOf(1000L)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(BigDecimal.valueOf(151), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.previous(BigDecimal.valueOf(152)));
    }

    @Test
    public void previousOverflowTest() {
        BigDecimal current = BigDecimal.valueOf(-Double.MAX_VALUE);
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("BigDecimal has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.previous(current);
    }
    
    @Test
    public void getTest()
    {
        assertEquals(new BigDecimal("10"),  SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.get(new BigDecimal("10.536")));
    }
    
    @Test
    public void nameTest()
    {
        assertEquals("decimal_scale_0", SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.hasPrevious(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.first().add(BigDecimal.ONE)));
        assertFalse(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.hasPrevious(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.first()));
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        assertEquals(5, SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.cardinality(new Interval<>(BigDecimal.valueOf(2), BigDecimal.valueOf(8))));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        assertEquals(7, SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.cardinality(new Interval<>(BigDecimal.valueOf(2), BigDecimal.valueOf(8), false, false)));
    }
    
    @Test
    public void cardinalityNullTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.cardinality(new Interval<>(BigDecimal.valueOf(2),null)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.cardinality(new Interval<>(BigDecimal.valueOf(2), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.last())));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.cardinality(new Interval<>(SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.first(), BigDecimal.valueOf(2), false, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.cardinality(new Interval<>(BigDecimal.valueOf(2), BigDecimal.valueOf(2), true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(BigDecimal.valueOf(-Double.MAX_VALUE).setScale(0, RoundingMode.DOWN), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(BigDecimal.valueOf(Double.MAX_VALUE).setScale(0, RoundingMode.DOWN), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.last());
    }
    
    @Test
    public void printConstantTest()
    {
        assertEquals("-1235", SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.printConstant(BigDecimal.valueOf(-1235.5)));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(BigDecimal.valueOf(-1235), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.parseConstant("-1235.5"));
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(BigDecimal.valueOf(153), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.add(BigDecimal.valueOf(90), 63));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(BigDecimal.valueOf(27), SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.add(BigDecimal.valueOf(90), -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        BigDecimal value = BigDecimal.valueOf(Double.MAX_VALUE - 5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of BigDecimal " + value + " with " + units + " units.");
        SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        BigDecimal value = BigDecimal.valueOf(-Double.MAX_VALUE + 5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of BigDecimal " + value + " with " + units + " units.");
        SigmaContractorFactory.BIGDECIMAL_ZERO_DECIMALS.add(value, units);
    } 
}
