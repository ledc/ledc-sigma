package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;

import java.time.LocalDate;
import java.time.Month;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class DateInDaysContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.DATE_DAYS.hasNext(LocalDate.of(2020, Month.OCTOBER, 30)));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.DATE_DAYS.hasNext(SigmaContractorFactory.DATE_DAYS.last().minusDays(1)));
        assertFalse(SigmaContractorFactory.DATE_DAYS.hasNext(SigmaContractorFactory.DATE_DAYS.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.DATE_DAYS.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(LocalDate.of(2021, Month.JULY, 9), SigmaContractorFactory.DATE_DAYS.next(LocalDate.of(2021, Month.JULY, 8)));
    }

    @Test
    public void nextOverflowTest() {
        LocalDate current = LocalDate.MAX;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalDate has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DATE_DAYS.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.DATE_DAYS.hasPrevious(LocalDate.of(2020, Month.OCTOBER, 30)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.DATE_DAYS.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(LocalDate.of(2021, Month.JULY, 7), SigmaContractorFactory.DATE_DAYS.previous(LocalDate.of(2021, Month.JULY, 8)));
    }

    @Test
    public void previousOverflowTest() {
        LocalDate current = LocalDate.MIN;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalDate has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DATE_DAYS.previous(current);
    }
    
    @Test
    public void nameTest()
    {
        assertEquals("date_in_days", SigmaContractorFactory.DATE_DAYS.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.DATE_DAYS.hasPrevious(SigmaContractorFactory.DATE_DAYS.first().plusDays(1)));
        assertFalse(SigmaContractorFactory.DATE_DAYS.hasPrevious(SigmaContractorFactory.DATE_DAYS.first()));
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        LocalDate start = LocalDate.of(2020, Month.JANUARY, 20);
        LocalDate end   = LocalDate.of(2020, Month.APRIL, 1);
        assertEquals(71, SigmaContractorFactory.DATE_DAYS.cardinality(new Interval<>(start,end)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        LocalDate start = LocalDate.of(2020, Month.JANUARY, 20);
        LocalDate end   = LocalDate.of(2020, Month.APRIL, 1);
        assertEquals(73, SigmaContractorFactory.DATE_DAYS.cardinality(new Interval<>(start,end,false,false)));
    }
    
    @Test
    public void cardinalityNullTest()
    {
        LocalDate start = LocalDate.of(2020, Month.JANUARY, 20);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATE_DAYS.cardinality(new Interval<>(start,null)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        LocalDate start = LocalDate.of(2020, Month.JANUARY, 20);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATE_DAYS.cardinality(new Interval<>(start,LocalDate.MAX)));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        LocalDate end   = LocalDate.of(2020, Month.APRIL, 1);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATE_DAYS.cardinality(new Interval<>(LocalDate.MIN,end, true, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.DATE_DAYS.cardinality(new Interval<>(LocalDate.of(2020, Month.JANUARY, 20), LocalDate.of(2020, Month.JANUARY, 20), true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(LocalDate.MIN, SigmaContractorFactory.DATE_DAYS.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(LocalDate.MAX, SigmaContractorFactory.DATE_DAYS.last());
    }
    
    @Test
    public void printConstantTest()
    {
        LocalDate d = LocalDate.of(2020, Month.JANUARY, 20);
        
        assertEquals("2020-01-20", SigmaContractorFactory.DATE_DAYS.printConstant(d));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(LocalDate.of(2020, Month.JANUARY, 20), SigmaContractorFactory.DATE_DAYS.parseConstant("2020-01-20"));
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(LocalDate.of(2020, Month.MARCH, 23), SigmaContractorFactory.DATE_DAYS.add(LocalDate.of(2020, Month.JANUARY, 20), 63));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(LocalDate.of(2019, Month.NOVEMBER, 18), SigmaContractorFactory.DATE_DAYS.add(LocalDate.of(2020, Month.JANUARY, 20), -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.DATE_DAYS.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        LocalDate value = LocalDate.MAX.minusDays(5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalDate " + value + " with " + units + " days.");
        SigmaContractorFactory.DATE_DAYS.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        LocalDate value = LocalDate.MIN.plusDays(5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalDate " + value + " with " + units + " days.");
        SigmaContractorFactory.DATE_DAYS.add(value, units);
    } 
}
