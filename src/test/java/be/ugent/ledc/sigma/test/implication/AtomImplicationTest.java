package be.ugent.ledc.sigma.test.implication;

import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.atoms.AtomImplicator;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.Set;

import static org.junit.Assert.assertEquals;

public class AtomImplicationTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void implyWithAlwaysTrueTest() {

        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> impliedAtom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");

        assertEquals(Set.of(AbstractAtom.ALWAYS_TRUE), AtomImplicator.imply(AbstractAtom.ALWAYS_TRUE, AbstractAtom.ALWAYS_TRUE));
        assertEquals(Set.of(impliedAtom), AtomImplicator.imply(AbstractAtom.ALWAYS_TRUE, atom));
        assertEquals(Set.of(impliedAtom), AtomImplicator.imply(atom, AbstractAtom.ALWAYS_TRUE));

    }

    @Test
    public void implyWithAlwaysFalseTest() {

        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 10);

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(AbstractAtom.ALWAYS_FALSE, atom));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom, AbstractAtom.ALWAYS_FALSE));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(AbstractAtom.ALWAYS_TRUE, AbstractAtom.ALWAYS_FALSE));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(AbstractAtom.ALWAYS_FALSE, AbstractAtom.ALWAYS_TRUE));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(AbstractAtom.ALWAYS_FALSE, AbstractAtom.ALWAYS_FALSE));

    }

    @Test
    public void implyNoCommonAttributesTest() {

        ConstantOrdinalAtom<Integer> atom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 10);
        VariableOrdinalAtom<Integer> atom2 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c");

        assertEquals(Set.of(atom1, atom2), AtomImplicator.imply(atom1, atom2));

    }

    @Test
    public void implyNoMatchingContractorTest() {

        ConstantOrdinalAtom<Integer> atom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 10);
        ConstantNominalAtom<String> atom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        exceptionRule.expect(AtomException.class);
        exceptionRule.expectMessage("Contractor of " + atom1 + " should be equal to contractor of " + atom2);

        AtomImplicator.imply(atom1, atom2);

    }

    @Test
    public void implyTwoConstantNominalAtomsTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom12 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> impliedAtom1 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");
        ConstantNominalAtom<String> impliedAtom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> atom32 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "2");
        SetNominalAtom<String> impliedAtom3 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2"));

        ConstantNominalAtom<String> atom41 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> atom42 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> impliedAtom4 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));

    }

    @Test
    public void implyTwoConstantNominalAtomsAlwaysFalseTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom12 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "2");

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> atom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom22, atom21));

    }

    @Test
    public void implyConstantSetNominalAtomsTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        ConstantNominalAtom<String> impliedAtom1 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("2", "3", "4"));
        ConstantNominalAtom<String> impliedAtom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        SetNominalAtom<String> impliedAtom3 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2", "3"));

        ConstantNominalAtom<String> atom41 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom42 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2"));
        ConstantNominalAtom<String> impliedAtom4 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "2");

        ConstantNominalAtom<String> atom51 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom52 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("2", "3"));
        SetNominalAtom<String> impliedAtom5 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));

        ConstantNominalAtom<String> atom61 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom62 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        ConstantNominalAtom<String> impliedAtom6 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom62, atom61));

    }

    @Test
    public void implyConstantSetNominalAtomsAlwaysFalseTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2", "3", "4"));

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1"));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom32, atom31));

    }

    @Test
    public void implyTwoSetNominalAtomsTest() {

        SetNominalAtom<String> atom11 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2", "3", "4"));
        SetNominalAtom<String> impliedAtom1 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2", "3"));

        SetNominalAtom<String> atom21 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("3", "4", "5"));
        ConstantNominalAtom<String> impliedAtom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "3");

        SetNominalAtom<String> atom31 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("3", "4", "5"));
        SetNominalAtom<String> impliedAtom3 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2"));

        SetNominalAtom<String> atom41 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom42 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("2", "3", "4"));
        ConstantNominalAtom<String> impliedAtom4 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");

        SetNominalAtom<String> atom51 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom52 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("3", "4", "5"));
        SetNominalAtom<String> impliedAtom5 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("4", "5"));

        SetNominalAtom<String> atom61 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom62 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("2", "3", "4"));
        ConstantNominalAtom<String> impliedAtom6 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "4");

        SetNominalAtom<String> atom71 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom72 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("3", "4", "5"));
        SetNominalAtom<String> impliedAtom7 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3", "4", "5"));

        SetNominalAtom<String> atom81 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        SetNominalAtom<String> atom82 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        ConstantNominalAtom<String> impliedAtom8 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(impliedAtom8), AtomImplicator.imply(atom81, atom82));

    }

    @Test
    public void implyTwoSetNominalAtomsAlwaysFalseTest() {

        SetNominalAtom<String> atom11 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom12 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("4", "5", "6"));

        SetNominalAtom<String> atom21 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        SetNominalAtom<String> atom22 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3", "4"));

        SetNominalAtom<String> atom31 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3", "4", "5"));
        SetNominalAtom<String> atom32 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3", "4", "5"));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom31, atom32));

    }

    @Test
    public void implyConstantVariableNominalAtomsTest() {

        ConstantNominalAtom<String> atom11 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        VariableNominalAtom<String> atom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        ConstantNominalAtom<String> impliedAtom1 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "1");

        ConstantNominalAtom<String> atom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        ConstantNominalAtom<String> impliedAtom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "1");

        ConstantNominalAtom<String> atom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        VariableNominalAtom<String> atom32 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        ConstantNominalAtom<String> impliedAtom3 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "1");

        ConstantNominalAtom<String> atom41 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        VariableNominalAtom<String> atom42 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        assertEquals(Set.of(atom11, impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom41, atom42));

        assertEquals(Set.of(atom11, impliedAtom1), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(atom21, impliedAtom2), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom42, atom41));

    }

    @Test
    public void implySetVariableNominalAtomsTest() {

        SetNominalAtom<String> atom11 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        VariableNominalAtom<String> atom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        SetNominalAtom<String> impliedAtom1 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "b", SetOperator.IN, Set.of("1", "2", "3"));

        SetNominalAtom<String> atom21 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1"));
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        ConstantNominalAtom<String> impliedAtom21 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> impliedAtom22 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "1");

        SetNominalAtom<String> atom31 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1"));
        VariableNominalAtom<String> atom32 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        ConstantNominalAtom<String> impliedAtom31 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "1");
        ConstantNominalAtom<String> impliedAtom32 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "1");

        SetNominalAtom<String> atom41 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, Set.of("1", "2", "3"));
        VariableNominalAtom<String> atom42 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        SetNominalAtom<String> atom51 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        VariableNominalAtom<String> atom52 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        SetNominalAtom<String> impliedAtom5 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "b", SetOperator.NOTIN, Set.of("1", "2", "3"));

        SetNominalAtom<String> atom61 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        VariableNominalAtom<String> atom62 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        ConstantNominalAtom<String> impliedAtom61 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "1");
        ConstantNominalAtom<String> impliedAtom62 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "1");

        SetNominalAtom<String> atom71 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1", "2", "3"));
        VariableNominalAtom<String> atom72 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        SetNominalAtom<String> atom81 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, Set.of("1"));
        VariableNominalAtom<String> atom82 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom21, impliedAtom22), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom31, impliedAtom32), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52, impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom61, atom62, impliedAtom62), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(atom81, atom82), AtomImplicator.imply(atom81, atom82));

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(impliedAtom21, impliedAtom22), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(impliedAtom31, impliedAtom32), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(impliedAtom61, atom62, impliedAtom62), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(atom81, atom82), AtomImplicator.imply(atom82, atom81));

    }

    @Test
    public void implyTwoVariableNominalAtomsTest() {

        VariableNominalAtom<String> atom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "c");
        VariableNominalAtom<String> impliedAtom1 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "c");

        VariableNominalAtom<String> atom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "c");
        VariableNominalAtom<String> impliedAtom2 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "c");

        VariableNominalAtom<String> atom31 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> atom32 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "c");
        VariableNominalAtom<String> impliedAtom3 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "c");

        VariableNominalAtom<String> atom41 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> atom42 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.NEQ, "c");

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22, impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom41, atom42));

    }

    @Test
    public void implyTwoVariableNominalAtomsEqualAttributesTest() {

        VariableNominalAtom<String> atom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> impliedAtom1 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");

        VariableNominalAtom<String> atom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> impliedAtom2 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));

    }

    @Test
    public void implyTwoVariableNominalAtomsAlwaysEqualAttributesFalseTest() {

        VariableNominalAtom<String> atom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> atom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        VariableNominalAtom<String> atom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");
        VariableNominalAtom<String> atom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));

    }

    @Test
    public void implyTwoConstantOrdinalAtomsTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);
        ConstantOrdinalAtom<Integer> impliedAtom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 3);

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> impliedAtom2 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> impliedAtom3 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);
        ConstantOrdinalAtom<Integer> impliedAtom5 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom6 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom72 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom7 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom82 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> impliedAtom8 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom92 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom9 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom102 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 3);
        ConstantOrdinalAtom<Integer> impliedAtom10 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom112 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ,4);

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom122 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);

        ConstantOrdinalAtom<Integer> atom131 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom132 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 3);
        ConstantOrdinalAtom<Integer> impliedAtom13 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);

        ConstantOrdinalAtom<Integer> atom141 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom142 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 7);
        ConstantOrdinalAtom<Integer> impliedAtom14 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 7);

        ConstantOrdinalAtom<Integer> atom151 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom152 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        ConstantOrdinalAtom<Integer> impliedAtom15 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);

        ConstantOrdinalAtom<Integer> atom161 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom162 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom16 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom171 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom172 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom17 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 6);

        ConstantOrdinalAtom<Integer> atom181 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        ConstantOrdinalAtom<Integer> atom182 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 3);
        ConstantOrdinalAtom<Integer> impliedAtom18 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);

        ConstantOrdinalAtom<Integer> atom191 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        ConstantOrdinalAtom<Integer> atom192 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom19 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ,6);

        ConstantOrdinalAtom<Integer> atom201 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        ConstantOrdinalAtom<Integer> atom202 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom20 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 6);

        ConstantOrdinalAtom<Integer> atom211 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> atom212 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom221 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> atom222 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom231 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);
        ConstantOrdinalAtom<Integer> atom232 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 6);
        SetOrdinalAtom<Integer> impliedAtom23 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(5, 6));

        ConstantOrdinalAtom<Integer> atom241 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);
        ConstantOrdinalAtom<Integer> atom242 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom24 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom71, atom72));

        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(impliedAtom8), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(impliedAtom9), AtomImplicator.imply(atom91, atom92));
        assertEquals(Set.of(impliedAtom10), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(impliedAtom11), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(impliedAtom12), AtomImplicator.imply(atom121, atom122));

        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(impliedAtom9), AtomImplicator.imply(atom92, atom91));
        assertEquals(Set.of(impliedAtom13), AtomImplicator.imply(atom131, atom132));
        assertEquals(Set.of(impliedAtom14), AtomImplicator.imply(atom141, atom142));
        assertEquals(Set.of(impliedAtom15), AtomImplicator.imply(atom151, atom152));
        assertEquals(Set.of(impliedAtom16), AtomImplicator.imply(atom161, atom162));
        assertEquals(Set.of(impliedAtom17), AtomImplicator.imply(atom171, atom172));

        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(impliedAtom10), AtomImplicator.imply(atom102, atom101));
        assertEquals(Set.of(impliedAtom14), AtomImplicator.imply(atom142, atom141));
        assertEquals(Set.of(impliedAtom15), AtomImplicator.imply(atom152, atom151));
        assertEquals(Set.of(impliedAtom18), AtomImplicator.imply(atom181, atom182));
        assertEquals(Set.of(impliedAtom19), AtomImplicator.imply(atom191, atom192));
        assertEquals(Set.of(impliedAtom20), AtomImplicator.imply(atom201, atom202));

        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(impliedAtom11), AtomImplicator.imply(atom112, atom111));
        assertEquals(Set.of(impliedAtom16), AtomImplicator.imply(atom162, atom161));
        assertEquals(Set.of(impliedAtom19), AtomImplicator.imply(atom192, atom191));
        assertEquals(Set.of(impliedAtom21), AtomImplicator.imply(atom211, atom212));
        assertEquals(Set.of(impliedAtom22), AtomImplicator.imply(atom221, atom222));

        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(impliedAtom12), AtomImplicator.imply(atom122, atom121));
        assertEquals(Set.of(impliedAtom17), AtomImplicator.imply(atom172, atom171));
        assertEquals(Set.of(impliedAtom20), AtomImplicator.imply(atom202, atom201));
        assertEquals(Set.of(impliedAtom22), AtomImplicator.imply(atom222, atom221));
        assertEquals(Set.of(impliedAtom23), AtomImplicator.imply(atom231, atom232));
        assertEquals(Set.of(impliedAtom24), AtomImplicator.imply(atom241, atom242));

    }

    @Test
    public void implyTwoConstantOrdinalAtomsAlwaysFalseTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 6);

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom72 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        ConstantOrdinalAtom<Integer> atom82 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> atom92 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> atom102 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom71, atom72));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom81, atom82));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom91, atom92));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom101, atom102));

    }

    @Test
    public void implyTwoConstantOrdinalAtomsNoImpliedAtomTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 3);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> atom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 6);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 2);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom72 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        ConstantOrdinalAtom<Integer> atom82 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 5);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom92 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 6);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> atom102 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        ConstantOrdinalAtom<Integer> atom112 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 7);

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 7);
        ConstantOrdinalAtom<Integer> atom122 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 7);

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(atom41), AtomImplicator.imply(atom41, atom42));

        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(atom81), AtomImplicator.imply(atom81, atom82));

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(atom91, atom92), AtomImplicator.imply(atom91, atom92));
        assertEquals(Set.of(atom101), AtomImplicator.imply(atom101, atom102));

        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(atom111, atom112), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(atom121), AtomImplicator.imply(atom121, atom122));

        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(atom41), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(atom81), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(atom91, atom92), AtomImplicator.imply(atom92, atom91));
        assertEquals(Set.of(atom101), AtomImplicator.imply(atom102, atom101));
        assertEquals(Set.of(atom111, atom112), AtomImplicator.imply(atom112, atom111));
        assertEquals(Set.of(atom121), AtomImplicator.imply(atom122, atom121));

    }

    @Test
    public void implyConstantSetOrdinalAtomsTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom1 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4));

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 2);
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom2 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 1);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom3 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4));

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 2);
        SetOrdinalAtom<Integer> atom42 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 1);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        SetOrdinalAtom<Integer> atom52 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom5 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(6, 8));

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 7);
        SetOrdinalAtom<Integer> atom62 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom6 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 8);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        SetOrdinalAtom<Integer> atom72 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom7 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(6, 8));

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 7);
        SetOrdinalAtom<Integer> atom82 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom8 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 8);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        SetOrdinalAtom<Integer> atom92 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom9 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        SetOrdinalAtom<Integer> atom102 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom10 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 6, 8));

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        SetOrdinalAtom<Integer> atom112 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(3, 4));
        ConstantOrdinalAtom<Integer> impliedAtom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        SetOrdinalAtom<Integer> atom122 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4));

        ConstantOrdinalAtom<Integer> atom131 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 2);
        SetOrdinalAtom<Integer> atom132 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom13 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 1);

        ConstantOrdinalAtom<Integer> atom141 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 0);
        SetOrdinalAtom<Integer> atom142 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom151 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 5);
        SetOrdinalAtom<Integer> atom152 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom151 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 3);
        ConstantOrdinalAtom<Integer> impliedAtom152 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 1);

        ConstantOrdinalAtom<Integer> atom161 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 2);
        SetOrdinalAtom<Integer> atom162 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom16 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 1);

        ConstantOrdinalAtom<Integer> atom171 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 1);
        SetOrdinalAtom<Integer> atom172 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom181 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        SetOrdinalAtom<Integer> atom182 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom18 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(6, 8));

        ConstantOrdinalAtom<Integer> atom191 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 7);
        SetOrdinalAtom<Integer> atom192 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom19 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 8);

        ConstantOrdinalAtom<Integer> atom201 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 9);
        SetOrdinalAtom<Integer> atom202 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom211 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 5);
        SetOrdinalAtom<Integer> atom212 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom211 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 6);
        ConstantOrdinalAtom<Integer> impliedAtom212 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 8);

        ConstantOrdinalAtom<Integer> atom221 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 7);
        SetOrdinalAtom<Integer> atom222 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 8);

        ConstantOrdinalAtom<Integer> atom231 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 8);
        SetOrdinalAtom<Integer> atom232 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom241 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        SetOrdinalAtom<Integer> atom242 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        ConstantOrdinalAtom<Integer> impliedAtom24 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);

        ConstantOrdinalAtom<Integer> atom251 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        SetOrdinalAtom<Integer> atom252 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 6, 8));
        SetOrdinalAtom<Integer> impliedAtom25 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom261 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        SetOrdinalAtom<Integer> atom262 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(4));
        ConstantOrdinalAtom<Integer> impliedAtom26 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(impliedAtom8), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(impliedAtom9), AtomImplicator.imply(atom91, atom92));
        assertEquals(Set.of(impliedAtom10), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(impliedAtom11), AtomImplicator.imply(atom111, atom112));

        assertEquals(Set.of(atom121, impliedAtom12), AtomImplicator.imply(atom121, atom122));
        assertEquals(Set.of(atom131, impliedAtom13), AtomImplicator.imply(atom131, atom132));
        assertEquals(Set.of(atom141), AtomImplicator.imply(atom141, atom142));
        assertEquals(Set.of(impliedAtom151, impliedAtom152), AtomImplicator.imply(atom151, atom152));
        assertEquals(Set.of(impliedAtom16), AtomImplicator.imply(atom161, atom162));
        assertEquals(Set.of(atom171), AtomImplicator.imply(atom171, atom172));
        assertEquals(Set.of(atom181, impliedAtom18), AtomImplicator.imply(atom181, atom182));
        assertEquals(Set.of(atom191, impliedAtom19), AtomImplicator.imply(atom191, atom192));
        assertEquals(Set.of(atom201), AtomImplicator.imply(atom201, atom202));
        assertEquals(Set.of(impliedAtom211, impliedAtom212), AtomImplicator.imply(atom211, atom212));
        assertEquals(Set.of(impliedAtom22), AtomImplicator.imply(atom221, atom222));
        assertEquals(Set.of(atom231), AtomImplicator.imply(atom231, atom232));
        assertEquals(Set.of(impliedAtom24), AtomImplicator.imply(atom241, atom242));
        assertEquals(Set.of(impliedAtom25), AtomImplicator.imply(atom251, atom252));
        assertEquals(Set.of(impliedAtom26), AtomImplicator.imply(atom261, atom262));

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(impliedAtom8), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(impliedAtom9), AtomImplicator.imply(atom92, atom91));
        assertEquals(Set.of(impliedAtom10), AtomImplicator.imply(atom102, atom101));
        assertEquals(Set.of(impliedAtom11), AtomImplicator.imply(atom112, atom111));

        assertEquals(Set.of(atom121, impliedAtom12), AtomImplicator.imply(atom122, atom121));
        assertEquals(Set.of(atom131, impliedAtom13), AtomImplicator.imply(atom132, atom131));
        assertEquals(Set.of(atom141), AtomImplicator.imply(atom142, atom141));
        assertEquals(Set.of(impliedAtom151, impliedAtom152), AtomImplicator.imply(atom152, atom151));
        assertEquals(Set.of(impliedAtom16), AtomImplicator.imply(atom162, atom161));
        assertEquals(Set.of(atom171), AtomImplicator.imply(atom172, atom171));
        assertEquals(Set.of(atom181, impliedAtom18), AtomImplicator.imply(atom182, atom181));
        assertEquals(Set.of(atom191, impliedAtom19), AtomImplicator.imply(atom192, atom191));
        assertEquals(Set.of(atom201), AtomImplicator.imply(atom202, atom201));
        assertEquals(Set.of(impliedAtom211, impliedAtom212), AtomImplicator.imply(atom212, atom211));
        assertEquals(Set.of(impliedAtom22), AtomImplicator.imply(atom222, atom221));
        assertEquals(Set.of(atom231), AtomImplicator.imply(atom232, atom231));
        assertEquals(Set.of(impliedAtom24), AtomImplicator.imply(atom242, atom241));
        assertEquals(Set.of(impliedAtom25), AtomImplicator.imply(atom252, atom251));
        assertEquals(Set.of(impliedAtom26), AtomImplicator.imply(atom262, atom261));

    }

    @Test
    public void implyConstantSetOrdinalAtomsAlwaysFalseTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 0);
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 1);
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 9);
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 8);
        SetOrdinalAtom<Integer> atom42 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        SetOrdinalAtom<Integer> atom52 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 3);
        SetOrdinalAtom<Integer> atom62 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(3));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom62, atom61));

    }

    @Test
    public void implyTwoSetOrdinalAtomsTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 3, 4));
        SetOrdinalAtom<Integer> impliedAtom1 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 3));

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(3, 4, 5));
        ConstantOrdinalAtom<Integer> impliedAtom2 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 3);

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(3, 4, 5));
        SetOrdinalAtom<Integer> impliedAtom3 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2));

        SetOrdinalAtom<Integer> atom41 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom42 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(2, 3, 4));
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 1);

        SetOrdinalAtom<Integer> atom51 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom52 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(3, 4, 5));
        SetOrdinalAtom<Integer> impliedAtom5 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(4, 5));

        SetOrdinalAtom<Integer> atom61 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom62 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(2, 3, 4));
        ConstantOrdinalAtom<Integer> impliedAtom6 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);

        SetOrdinalAtom<Integer> atom71 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom72 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(3, 4, 5));
        SetOrdinalAtom<Integer> impliedAtom7 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 3, 4, 5));

        SetOrdinalAtom<Integer> atom81 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1));
        SetOrdinalAtom<Integer> atom82 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1));
        ConstantOrdinalAtom<Integer> impliedAtom8 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 1);

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(impliedAtom8), AtomImplicator.imply(atom81, atom82));

    }

    @Test
    public void implyTwoSetOrdinalAtomsAlwaysFalseTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(4, 5, 6));

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3));
        SetOrdinalAtom<Integer> atom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 3, 4));

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 2, 3, 4, 5));
        SetOrdinalAtom<Integer> atom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 2, 3, 4, 5));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom31, atom32));

    }

    @Test
    public void implyConstantVariableOrdinalAtomsTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, 3);

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom2 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom3 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, 3);

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom5 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, 2);

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom6 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, 3);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom7 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom8 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 3);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom9 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom10 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 3);

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 4);

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 3);

        ConstantOrdinalAtom<Integer> atom131 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        VariableOrdinalAtom<Integer> atom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom13 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, 3);

        ConstantOrdinalAtom<Integer> atom141 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        VariableOrdinalAtom<Integer> atom142 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom14 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 3);

        ConstantOrdinalAtom<Integer> atom151 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        VariableOrdinalAtom<Integer> atom152 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom15 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, 3);

        ConstantOrdinalAtom<Integer> atom161 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        VariableOrdinalAtom<Integer> atom162 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom16 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 3);

        ConstantOrdinalAtom<Integer> atom171 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        VariableOrdinalAtom<Integer> atom172 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom17 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, 3);

        ConstantOrdinalAtom<Integer> atom181 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        VariableOrdinalAtom<Integer> atom182 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom18 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, 3);

        ConstantOrdinalAtom<Integer> atom191 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        VariableOrdinalAtom<Integer> atom192 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom19 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, 3);

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22, impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(atom41, atom42, impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52, impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(atom61, atom62, impliedAtom6), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(atom71, atom72, impliedAtom7), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(atom81, atom82, impliedAtom8), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(atom91, atom92, impliedAtom9), AtomImplicator.imply(atom91, atom92));

        assertEquals(Set.of(atom101, atom102, impliedAtom10), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(atom111, atom112, impliedAtom11), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(atom121, atom122, impliedAtom12), AtomImplicator.imply(atom121, atom122));

        assertEquals(Set.of(atom131, impliedAtom13), AtomImplicator.imply(atom131, atom132));
        assertEquals(Set.of(atom141, impliedAtom14), AtomImplicator.imply(atom141, atom142));
        assertEquals(Set.of(atom151, impliedAtom15), AtomImplicator.imply(atom151, atom152));
        assertEquals(Set.of(atom161, impliedAtom16), AtomImplicator.imply(atom161, atom162));
        assertEquals(Set.of(atom171, impliedAtom17), AtomImplicator.imply(atom171, atom172));
        assertEquals(Set.of(atom181, impliedAtom18), AtomImplicator.imply(atom181, atom182));

        assertEquals(Set.of(atom191, atom192, impliedAtom19), AtomImplicator.imply(atom191, atom192));

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(atom21, atom22, impliedAtom2), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom32, atom31));

        assertEquals(Set.of(atom41, atom42, impliedAtom4), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(atom51, atom52, impliedAtom5), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(atom61, atom62, impliedAtom6), AtomImplicator.imply(atom62, atom61));

        assertEquals(Set.of(atom71, atom72, impliedAtom7), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(atom81, atom82, impliedAtom8), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(atom91, atom92, impliedAtom9), AtomImplicator.imply(atom92, atom91));

        assertEquals(Set.of(atom101, atom102, impliedAtom10), AtomImplicator.imply(atom102, atom101));
        assertEquals(Set.of(atom111, atom112, impliedAtom11), AtomImplicator.imply(atom112, atom111));
        assertEquals(Set.of(atom121, atom122, impliedAtom12), AtomImplicator.imply(atom122, atom121));

        assertEquals(Set.of(atom131, impliedAtom13), AtomImplicator.imply(atom132, atom131));
        assertEquals(Set.of(atom141, impliedAtom14), AtomImplicator.imply(atom142, atom141));
        assertEquals(Set.of(atom151, impliedAtom15), AtomImplicator.imply(atom152, atom151));
        assertEquals(Set.of(atom161, impliedAtom16), AtomImplicator.imply(atom162, atom161));
        assertEquals(Set.of(atom171, impliedAtom17), AtomImplicator.imply(atom172, atom171));
        assertEquals(Set.of(atom181, impliedAtom18), AtomImplicator.imply(atom182, atom181));

        assertEquals(Set.of(atom191, atom192, impliedAtom19), AtomImplicator.imply(atom192, atom191));
    }

    @Test
    public void implyConstantVariableOrdinalAtomsAlwaysTrueTest() {

        ConstantOrdinalAtom<Integer> atom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        ConstantOrdinalAtom<Integer> atom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        ConstantOrdinalAtom<Integer> atom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, 4);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom71 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);

        ConstantOrdinalAtom<Integer> atom91 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);

        ConstantOrdinalAtom<Integer> atom121 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, 4);
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom131 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        VariableOrdinalAtom<Integer> atom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom141 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        VariableOrdinalAtom<Integer> atom142 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        ConstantOrdinalAtom<Integer> atom151 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        VariableOrdinalAtom<Integer> atom152 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);

        ConstantOrdinalAtom<Integer> atom161 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        VariableOrdinalAtom<Integer> atom162 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);

        ConstantOrdinalAtom<Integer> atom171 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, 4);
        VariableOrdinalAtom<Integer> atom172 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(atom81, atom82), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(atom91, atom92), AtomImplicator.imply(atom91, atom92));

        assertEquals(Set.of(atom101, atom102), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(atom111, atom112), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(atom121, atom122), AtomImplicator.imply(atom121, atom122));

        assertEquals(Set.of(atom131, atom132), AtomImplicator.imply(atom131, atom132));
        assertEquals(Set.of(atom141, atom142), AtomImplicator.imply(atom141, atom142));
        assertEquals(Set.of(atom151, atom152), AtomImplicator.imply(atom151, atom152));
        assertEquals(Set.of(atom161, atom162), AtomImplicator.imply(atom161, atom162));
        assertEquals(Set.of(atom171, atom172), AtomImplicator.imply(atom171, atom172));

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom32, atom31));

        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom62, atom61));

        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(atom81, atom82), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(atom91, atom92), AtomImplicator.imply(atom92, atom91));

        assertEquals(Set.of(atom101, atom102), AtomImplicator.imply(atom102, atom101));
        assertEquals(Set.of(atom111, atom112), AtomImplicator.imply(atom112, atom111));
        assertEquals(Set.of(atom121, atom122), AtomImplicator.imply(atom122, atom121));

        assertEquals(Set.of(atom131, atom132), AtomImplicator.imply(atom132, atom131));
        assertEquals(Set.of(atom141, atom142), AtomImplicator.imply(atom142, atom141));
        assertEquals(Set.of(atom151, atom152), AtomImplicator.imply(atom152, atom151));
        assertEquals(Set.of(atom161, atom162), AtomImplicator.imply(atom162, atom161));
        assertEquals(Set.of(atom171, atom172), AtomImplicator.imply(atom172, atom171));

    }

    @Test
    public void implySetVariableOrdinalAtomsTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, 0);

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1));
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 1);
        ConstantOrdinalAtom<Integer> impliedAtom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, 0);

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom3 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 0);

        SetOrdinalAtom<Integer> atom41 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1));
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 1);
        ConstantOrdinalAtom<Integer> impliedAtom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, 0);

        SetOrdinalAtom<Integer> atom51 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom5 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, 7);

        SetOrdinalAtom<Integer> atom61 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(8));
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 8);
        ConstantOrdinalAtom<Integer> impliedAtom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, 7);

        SetOrdinalAtom<Integer> atom71 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom7 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, 7);

        SetOrdinalAtom<Integer> atom81 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(8));
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom81 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 8);
        ConstantOrdinalAtom<Integer> impliedAtom82 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, 7);

        SetOrdinalAtom<Integer> atom91 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        SetOrdinalAtom<Integer> impliedAtom9 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, Set.of(0, 2, 3, 5, 7));

        SetOrdinalAtom<Integer> atom101 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1));
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom101 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 1);
        ConstantOrdinalAtom<Integer> impliedAtom102 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, 0);

        SetOrdinalAtom<Integer> atom111 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1));
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        ConstantOrdinalAtom<Integer> impliedAtom111 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 1);
        ConstantOrdinalAtom<Integer> impliedAtom112 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, 0);

        SetOrdinalAtom<Integer> atom121 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        SetOrdinalAtom<Integer> impliedAtom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.NOTIN, Set.of(0, 2, 3, 5, 7));

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom21, impliedAtom22), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom41, impliedAtom42), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52, impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom61, impliedAtom62), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(atom71, atom72, impliedAtom7), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(impliedAtom81, impliedAtom82), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(atom91, atom92, impliedAtom9), AtomImplicator.imply(atom91, atom92));
        assertEquals(Set.of(impliedAtom101, impliedAtom102), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(impliedAtom111, impliedAtom112), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(atom121, atom122, impliedAtom12), AtomImplicator.imply(atom121, atom122));

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(impliedAtom21, impliedAtom22), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(impliedAtom41, impliedAtom42), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(atom51, atom52, impliedAtom5), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(impliedAtom61, impliedAtom62), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(atom71, atom72, impliedAtom7), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(impliedAtom81, impliedAtom82), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(atom91, atom92, impliedAtom9), AtomImplicator.imply(atom92, atom91));
        assertEquals(Set.of(impliedAtom101, impliedAtom102), AtomImplicator.imply(atom102, atom101));
        assertEquals(Set.of(impliedAtom111, impliedAtom112), AtomImplicator.imply(atom112, atom111));
        assertEquals(Set.of(atom121, atom122, impliedAtom12), AtomImplicator.imply(atom122, atom121));

    }

    @Test
    public void implySetVariableOrdinalAtomsAlwaysTrueTest() {

        SetOrdinalAtom<Integer> atom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        SetOrdinalAtom<Integer> atom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);

        SetOrdinalAtom<Integer> atom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        SetOrdinalAtom<Integer> atom41 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);

        SetOrdinalAtom<Integer> atom51 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);

        SetOrdinalAtom<Integer> atom61 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, Set.of(1, 3, 4, 6, 8));
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom62, atom61));

    }

    @Test
    public void implyTwoVariableOrdinalAtomsTest() {

        VariableOrdinalAtom<Integer> atom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom1 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "c", -1);

        VariableOrdinalAtom<Integer> atom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom2 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", -1);

        VariableOrdinalAtom<Integer> atom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom3 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "c", -1);

        VariableOrdinalAtom<Integer> atom41 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -2);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom4 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", -1);

        VariableOrdinalAtom<Integer> atom51 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -2);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom5 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", -2);

        VariableOrdinalAtom<Integer> atom61 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -2);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom6 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", -1);

        VariableOrdinalAtom<Integer> atom71 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom7 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "c", -1);

        VariableOrdinalAtom<Integer> atom81 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom8 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "c", -1);

        VariableOrdinalAtom<Integer> atom91 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom9 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "c", -1);

        VariableOrdinalAtom<Integer> atom101 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", -2);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom10 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "c", -1);

        VariableOrdinalAtom<Integer> atom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", -2);
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "c", 0);

        VariableOrdinalAtom<Integer> atom121 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", -2);
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "c", -1);

        VariableOrdinalAtom<Integer> atom131 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", -2);
        VariableOrdinalAtom<Integer> atom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom13 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "c", -1);

        VariableOrdinalAtom<Integer> atom141 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", -2);
        VariableOrdinalAtom<Integer> atom142 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom14 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", -1);

        VariableOrdinalAtom<Integer> atom151 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", -2);
        VariableOrdinalAtom<Integer> atom152 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom15 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "c", -1);

        VariableOrdinalAtom<Integer> atom161 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", -2);
        VariableOrdinalAtom<Integer> atom162 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom16 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "c", -1);

        VariableOrdinalAtom<Integer> atom171 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", -2);
        VariableOrdinalAtom<Integer> atom172 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom17 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "c", -1);

        VariableOrdinalAtom<Integer> atom181 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", -2);
        VariableOrdinalAtom<Integer> atom182 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom18 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "c", -1);

        VariableOrdinalAtom<Integer> atom191 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom192 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.EQ, "c", 1);
        VariableOrdinalAtom<Integer> impliedAtom19 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "c", -1);

        assertEquals(Set.of(atom11, atom12, impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22, impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32, impliedAtom3), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(atom41, atom42, impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52, impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(atom61, atom62, impliedAtom6), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(atom71, atom72, impliedAtom7), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(atom81, atom82, impliedAtom8), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(atom91, atom92, impliedAtom9), AtomImplicator.imply(atom91, atom92));

        assertEquals(Set.of(atom101, atom102, impliedAtom10), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(atom111, atom112, impliedAtom11), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(atom121, atom122, impliedAtom12), AtomImplicator.imply(atom121, atom122));

        assertEquals(Set.of(atom131, atom132, impliedAtom13), AtomImplicator.imply(atom131, atom132));
        assertEquals(Set.of(atom141, atom142, impliedAtom14), AtomImplicator.imply(atom141, atom142));
        assertEquals(Set.of(atom151, atom152, impliedAtom15), AtomImplicator.imply(atom151, atom152));
        assertEquals(Set.of(atom161, atom162, impliedAtom16), AtomImplicator.imply(atom161, atom162));
        assertEquals(Set.of(atom171, atom172, impliedAtom17), AtomImplicator.imply(atom171, atom172));
        assertEquals(Set.of(atom181, atom182, impliedAtom18), AtomImplicator.imply(atom181, atom182));

        assertEquals(Set.of(atom191, atom192, impliedAtom19), AtomImplicator.imply(atom191, atom192));

    }

    @Test
    public void implyTwoVariableOrdinalAtomsAlwaysTrueTest() {

        VariableOrdinalAtom<Integer> atom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c", 1);

        VariableOrdinalAtom<Integer> atom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom41 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -2);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom51 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -2);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c", 1);

        VariableOrdinalAtom<Integer> atom61 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", -2);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom71 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom81 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "c", 1);

        VariableOrdinalAtom<Integer> atom91 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom101 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", -2);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", -2);
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "c", 1);

        VariableOrdinalAtom<Integer> atom121 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", -2);
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom131 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom141 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom142 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.LT, "c", 1);

        VariableOrdinalAtom<Integer> atom151 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom152 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GEQ, "c", 1);

        VariableOrdinalAtom<Integer> atom161 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom162 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", InequalityOperator.GT, "c", 1);

        VariableOrdinalAtom<Integer> atom171 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", -2);
        VariableOrdinalAtom<Integer> atom172 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", EqualityOperator.NEQ, "c", 1);

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(atom81, atom82), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(atom91, atom92), AtomImplicator.imply(atom91, atom92));

        assertEquals(Set.of(atom101, atom102), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(atom111, atom112), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(atom121, atom122), AtomImplicator.imply(atom121, atom122));

        assertEquals(Set.of(atom131, atom132), AtomImplicator.imply(atom131, atom132));
        assertEquals(Set.of(atom141, atom142), AtomImplicator.imply(atom141, atom142));
        assertEquals(Set.of(atom151, atom152), AtomImplicator.imply(atom151, atom152));
        assertEquals(Set.of(atom161, atom162), AtomImplicator.imply(atom161, atom162));
        assertEquals(Set.of(atom171, atom172), AtomImplicator.imply(atom171, atom172));

    }

    @Test
    public void implyTwoVariableOrdinalAtomsEqualAttributesTest() {

        VariableOrdinalAtom<Integer> atom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom1 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);

        VariableOrdinalAtom<Integer> atom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom2 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);

        VariableOrdinalAtom<Integer> atom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 3);
        VariableOrdinalAtom<Integer> impliedAtom3 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);

        VariableOrdinalAtom<Integer> atom41 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom4 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);

        VariableOrdinalAtom<Integer> atom51 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom5 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);

        VariableOrdinalAtom<Integer> atom61 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom6 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);

        VariableOrdinalAtom<Integer> atom71 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom7 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);

        VariableOrdinalAtom<Integer> atom81 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 3);
        VariableOrdinalAtom<Integer> impliedAtom8 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);

        VariableOrdinalAtom<Integer> atom91 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom9 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);

        VariableOrdinalAtom<Integer> atom101 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom10 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        VariableOrdinalAtom<Integer> atom111 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);
        VariableOrdinalAtom<Integer> atom112 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        VariableOrdinalAtom<Integer> atom121 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom122 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);

        VariableOrdinalAtom<Integer> atom131 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom132 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 0);
        VariableOrdinalAtom<Integer> impliedAtom13 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);

        VariableOrdinalAtom<Integer> atom141 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom142 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom14 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);

        VariableOrdinalAtom<Integer> atom151 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom152 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom15 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);

        VariableOrdinalAtom<Integer> atom161 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom162 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom16 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        VariableOrdinalAtom<Integer> atom171 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom172 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom17 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);

        VariableOrdinalAtom<Integer> atom181 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom182 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom18 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);

        VariableOrdinalAtom<Integer> atom191 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 3);
        VariableOrdinalAtom<Integer> atom192 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom19 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 3);

        VariableOrdinalAtom<Integer> atom201 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom202 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom20 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);

        VariableOrdinalAtom<Integer> atom211 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 0);
        VariableOrdinalAtom<Integer> atom212 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);

        VariableOrdinalAtom<Integer> atom221 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 3);
        VariableOrdinalAtom<Integer> atom222 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 3);

        VariableOrdinalAtom<Integer> atom231 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom232 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom23 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 3);

        VariableOrdinalAtom<Integer> atom241 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> atom242 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom24 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);

        VariableOrdinalAtom<Integer> atom251 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom252 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom25 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);

        VariableOrdinalAtom<Integer> atom261 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 0);
        VariableOrdinalAtom<Integer> atom262 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);
        VariableOrdinalAtom<Integer> impliedAtom26 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);

        VariableOrdinalAtom<Integer> atom271 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> atom272 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom27 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);

        VariableOrdinalAtom<Integer> atom281 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> atom282 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 3);
        VariableOrdinalAtom<Integer> impliedAtom28 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 3);

        VariableOrdinalAtom<Integer> atom291 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);
        VariableOrdinalAtom<Integer> atom292 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom29 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);

        VariableOrdinalAtom<Integer> atom301 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);
        VariableOrdinalAtom<Integer> atom302 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 3);
        VariableOrdinalAtom<Integer> impliedAtom30 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);

        assertEquals(Set.of(impliedAtom1), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(impliedAtom2), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom31, atom32));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom61, atom62));
        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom71, atom72));
        assertEquals(Set.of(impliedAtom8), AtomImplicator.imply(atom81, atom82));
        assertEquals(Set.of(impliedAtom9), AtomImplicator.imply(atom91, atom92));

        assertEquals(Set.of(impliedAtom3), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(impliedAtom4), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(impliedAtom10), AtomImplicator.imply(atom101, atom102));
        assertEquals(Set.of(impliedAtom11), AtomImplicator.imply(atom111, atom112));
        assertEquals(Set.of(impliedAtom12), AtomImplicator.imply(atom121, atom122));
        assertEquals(Set.of(impliedAtom13), AtomImplicator.imply(atom131, atom132));
        assertEquals(Set.of(impliedAtom14), AtomImplicator.imply(atom141, atom142));
        assertEquals(Set.of(impliedAtom15), AtomImplicator.imply(atom151, atom152));
        assertEquals(Set.of(impliedAtom16), AtomImplicator.imply(atom161, atom162));

        assertEquals(Set.of(impliedAtom5), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(impliedAtom12), AtomImplicator.imply(atom122, atom121));
        assertEquals(Set.of(impliedAtom17), AtomImplicator.imply(atom171, atom172));
        assertEquals(Set.of(impliedAtom18), AtomImplicator.imply(atom181, atom182));
        assertEquals(Set.of(impliedAtom19), AtomImplicator.imply(atom191, atom192));
        assertEquals(Set.of(impliedAtom20), AtomImplicator.imply(atom201, atom202));
        assertEquals(Set.of(impliedAtom21), AtomImplicator.imply(atom211, atom212));
        assertEquals(Set.of(impliedAtom22), AtomImplicator.imply(atom221, atom222));
        assertEquals(Set.of(impliedAtom23), AtomImplicator.imply(atom231, atom232));

        assertEquals(Set.of(impliedAtom6), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(impliedAtom13), AtomImplicator.imply(atom132, atom131));
        assertEquals(Set.of(impliedAtom19), AtomImplicator.imply(atom192, atom191));
        assertEquals(Set.of(impliedAtom20), AtomImplicator.imply(atom202, atom201));
        assertEquals(Set.of(impliedAtom24), AtomImplicator.imply(atom241, atom242));
        assertEquals(Set.of(impliedAtom25), AtomImplicator.imply(atom251, atom252));
        assertEquals(Set.of(impliedAtom26), AtomImplicator.imply(atom261, atom262));
        assertEquals(Set.of(impliedAtom27), AtomImplicator.imply(atom271, atom272));
        assertEquals(Set.of(impliedAtom28), AtomImplicator.imply(atom281, atom282));

        assertEquals(Set.of(impliedAtom7), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(impliedAtom14), AtomImplicator.imply(atom142, atom141));
        assertEquals(Set.of(impliedAtom21), AtomImplicator.imply(atom212, atom211));
        assertEquals(Set.of(impliedAtom26), AtomImplicator.imply(atom262, atom261));
        assertEquals(Set.of(impliedAtom29), AtomImplicator.imply(atom291, atom292));
        assertEquals(Set.of(impliedAtom30), AtomImplicator.imply(atom301, atom302));

        assertEquals(Set.of(impliedAtom8), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(impliedAtom9), AtomImplicator.imply(atom92, atom91));
        assertEquals(Set.of(impliedAtom15), AtomImplicator.imply(atom152, atom151));
        assertEquals(Set.of(impliedAtom16), AtomImplicator.imply(atom162, atom161));
        assertEquals(Set.of(impliedAtom22), AtomImplicator.imply(atom222, atom221));
        assertEquals(Set.of(impliedAtom23), AtomImplicator.imply(atom232, atom231));
        assertEquals(Set.of(impliedAtom27), AtomImplicator.imply(atom272, atom271));
        assertEquals(Set.of(impliedAtom28), AtomImplicator.imply(atom282, atom281));
        assertEquals(Set.of(impliedAtom30), AtomImplicator.imply(atom302, atom301));

    }

    @Test
    public void implyTwoVariableOrdinalAtomsEqualAttributesAlwaysFalseTest() {

        VariableOrdinalAtom<Integer> atom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 1);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);

        VariableOrdinalAtom<Integer> atom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);

        VariableOrdinalAtom<Integer> atom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 3);

        VariableOrdinalAtom<Integer> atom41 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);

        VariableOrdinalAtom<Integer> atom51 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);

        VariableOrdinalAtom<Integer> atom61 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);

        VariableOrdinalAtom<Integer> atom71 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 1);

        VariableOrdinalAtom<Integer> atom81 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 2);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);

        VariableOrdinalAtom<Integer> atom91 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);
        VariableOrdinalAtom<Integer> atom92 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 3);

        VariableOrdinalAtom<Integer> atom101 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, "b", 2);
        VariableOrdinalAtom<Integer> atom102 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 2);

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom71, atom72));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom81, atom82));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom82, atom81));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom91, atom92));
        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom101, atom102));

        assertEquals(Set.of(AbstractAtom.ALWAYS_FALSE), AtomImplicator.imply(atom102, atom101));
    }

    @Test
    public void implyTwoVariableOrdinalAtomsEqualAttributesAlwaysTrueTest() {

        VariableOrdinalAtom<Integer> atom11 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom12 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 1);

        VariableOrdinalAtom<Integer> atom21 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom22 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 0);

        VariableOrdinalAtom<Integer> atom31 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom32 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);

        VariableOrdinalAtom<Integer> atom41 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom42 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 0);

        VariableOrdinalAtom<Integer> atom51 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 3);
        VariableOrdinalAtom<Integer> atom52 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 0);

        VariableOrdinalAtom<Integer> atom61 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> atom62 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 0);

        VariableOrdinalAtom<Integer> atom71 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, "b", 2);
        VariableOrdinalAtom<Integer> atom72 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 3);

        VariableOrdinalAtom<Integer> atom81 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);
        VariableOrdinalAtom<Integer> atom82 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 3);

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom11, atom12));
        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom21, atom22));
        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom31, atom32));

        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom41, atom42));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom51, atom52));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom61, atom62));

        assertEquals(Set.of(atom11, atom12), AtomImplicator.imply(atom12, atom11));
        assertEquals(Set.of(atom41, atom42), AtomImplicator.imply(atom42, atom41));
        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom71, atom72));

        assertEquals(Set.of(atom21, atom22), AtomImplicator.imply(atom22, atom21));
        assertEquals(Set.of(atom51, atom52), AtomImplicator.imply(atom52, atom51));
        assertEquals(Set.of(atom81, atom82), AtomImplicator.imply(atom81, atom82));

        assertEquals(Set.of(atom31, atom32), AtomImplicator.imply(atom32, atom31));
        assertEquals(Set.of(atom61, atom62), AtomImplicator.imply(atom62, atom61));
        assertEquals(Set.of(atom71, atom72), AtomImplicator.imply(atom72, atom71));
        assertEquals(Set.of(atom81, atom82), AtomImplicator.imply(atom82, atom81));

    }

}
