package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class IntegerContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.INTEGER.hasNext(1000));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.INTEGER.hasNext(Integer.MAX_VALUE - 1));
        assertFalse(SigmaContractorFactory.INTEGER.hasNext(Integer.MAX_VALUE));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.INTEGER.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(153, SigmaContractorFactory.INTEGER.next(152).intValue());
    }

    @Test
    public void nextOverflowTest() {
        Integer current = Integer.MAX_VALUE;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Integer has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.INTEGER.next(current);
    }

    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.INTEGER.hasPrevious(1000));
    }
    
    @Test
    public void nameTest()
    {
        assertEquals(TypeContractorFactory.INTEGER.name(), SigmaContractorFactory.INTEGER.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.INTEGER.hasPrevious(Integer.MIN_VALUE + 1));
        assertFalse(SigmaContractorFactory.INTEGER.hasPrevious(Integer.MIN_VALUE));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.INTEGER.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(151, SigmaContractorFactory.INTEGER.previous(152).intValue());
    }

    @Test
    public void previousOverflowTest() {
        Integer current = Integer.MIN_VALUE;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Integer has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.INTEGER.previous(current);
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        assertEquals(5, SigmaContractorFactory.INTEGER.cardinality(new Interval<>(2,8)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        assertEquals(7, SigmaContractorFactory.INTEGER.cardinality(new Interval<>(2,8, false, false)));
    }
    
    @Test
    public void cardinalityNullTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.INTEGER.cardinality(new Interval<>(2,null)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.INTEGER.cardinality(new Interval<>(2,Integer.MAX_VALUE)));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.INTEGER.cardinality(new Interval<>(Integer.MIN_VALUE,2, false, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.INTEGER.cardinality(new Interval<>(2, 2, true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(Integer.valueOf(Integer.MIN_VALUE), SigmaContractorFactory.INTEGER.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(Integer.valueOf(Integer.MAX_VALUE), SigmaContractorFactory.INTEGER.last());
    }
    
    @Test
    public void printConstantTest()
    {
        assertEquals("-1235", SigmaContractorFactory.INTEGER.printConstant(-1235));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(Integer.valueOf(-1235), SigmaContractorFactory.INTEGER.parseConstant("-1235"));
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(153, SigmaContractorFactory.INTEGER.add(90, 63).intValue());
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(Integer.valueOf(27), SigmaContractorFactory.INTEGER.add(90, -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.INTEGER.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        Integer value = Integer.MAX_VALUE - 5;
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Integer " + value + " with " + units + " units.");
        SigmaContractorFactory.INTEGER.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        Integer value = Integer.MIN_VALUE + 5;
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Integer " + value + " with " + units + " units.");
        SigmaContractorFactory.INTEGER.add(value, units);
    } 
}
