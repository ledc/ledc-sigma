package be.ugent.ledc.sigma.test.values;

import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.values.attributevalues.EnumerationMapping;
import be.ugent.ledc.sigma.datastructures.values.NominalValueIterator;
import be.ugent.ledc.sigma.datastructures.values.attributevalues.TransformerException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.*;

public class EnumerationMappingTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void hasMappingForAttributeTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        assertTrue(eMapping.hasMappingForAttribute("a"));

    }

    @Test
    public void notHasMappingForAttributeTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        assertFalse(eMapping.hasMappingForAttribute("e"));

    }

    @Test
    public void getMappingForAttributeTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        NominalValueIterator<String> expected = new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3")));

        assertEquals(expected, eMapping.getMappingForAttribute("a"));

    }

    @Test
    public void getMappingForAttributeNullTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        assertNull(eMapping.getMappingForAttribute("e"));

    }

    @Test
    public void transformToCPFNoMatchingContractorTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2, 3))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);

        String attribute = "d";

        exceptionRule.expect(TransformerException.class);
        exceptionRule.expectMessage("No contractor in set of contractors " + contractors + " exists for " +
                "attribute " + attribute + ". Therefore, it is not possible to construct a CPF that involves " +
                "this attribute.");


        EnumerationMapping.Transformer.transformToCPF(eMapping, contractors);

    }

    @Test
    public void transformToCPFNominalTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);

        Set<AbstractAtom<?, ?, ?>> atoms = new HashSet<>();
        atoms.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));
        atoms.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "b", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atoms.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "c", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atoms.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "d", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));

        CPF expected = new CPF(atoms);

        assertEquals(expected, EnumerationMapping.Transformer.transformToCPF(eMapping, contractors));

    }

    @Test
    public void transformToCPFOrdinalTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2, 3))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2, 3))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        Set<AbstractAtom<?, ?, ?>> atoms = new HashSet<>();
        atoms.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3))));
        atoms.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atoms.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atoms.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3))));

        CPF expected = new CPF(atoms);

        assertEquals(expected, EnumerationMapping.Transformer.transformToCPF(eMapping, contractors));

    }

    @Test
    public void transformToCPFTest() {

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2, 3))));

        EnumerationMapping eMapping = new EnumerationMapping(mapping);

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        Set<AbstractAtom<?, ?, ?>> atoms = new HashSet<>();
        atoms.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));
        atoms.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atoms.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "c", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atoms.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3))));

        CPF expected = new CPF(atoms);

        assertEquals(expected, EnumerationMapping.Transformer.transformToCPF(eMapping, contractors));

    }

    @Test
    public void transformFromAtomsetsNotInTest() {

        Set<SetAtom<?, ?>> atomset = new HashSet<>();
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "c", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", SetOperator.NOTIN, new HashSet<>(Arrays.asList(1, 2, 3))));

        exceptionRule.expect(TransformerException.class);
        exceptionRule.expectMessage("Given set of SetAtom objects " + atomset + " contains a SetAtom object featuring a NOTIN operator.");

        EnumerationMapping.Transformer.transformFromAtomset(atomset);

    }

    @Test
    public void transformFromAtomsetsSameAttributeTest() {

        Set<SetAtom<?, ?>> atomset = new HashSet<>();
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3))));

        String attribute = "a";

        exceptionRule.expect(TransformerException.class);
        exceptionRule.expectMessage("Given set of SetAtom objects contains multiple SetAtom objects with attribute " + attribute + ".");

        EnumerationMapping.Transformer.transformFromAtomset(atomset);

    }

    @Test
    public void transformFromAtomsetNominalTest() {

        Set<SetAtom<?, ?>> atomset = new HashSet<>();
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "b", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "c", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "d", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));

        EnumerationMapping expected = new EnumerationMapping(mapping);

        assertEquals(expected, EnumerationMapping.Transformer.transformFromAtomset(atomset));

    }

    @Test
    public void transformFromAtomsetOrdinalTest() {

        Set<SetAtom<?, ?>> atomset = new HashSet<>();
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "c", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3))));

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2, 3))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2, 3))));

        EnumerationMapping expected = new EnumerationMapping(mapping);

        assertEquals(expected, EnumerationMapping.Transformer.transformFromAtomset(atomset));

    }

    @Test
    public void transformFromAtomsetTest() {

        Set<SetAtom<?, ?>> atomset = new HashSet<>();
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2", "3"))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2))));
        atomset.add(new SetNominalAtom<>(SigmaContractorFactory.STRING, "c", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2"))));
        atomset.add(new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3))));

        Map<String, NominalValueIterator<?>> mapping = new HashMap<>();

        mapping.put("a", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2", "3"))));
        mapping.put("b", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2))));
        mapping.put("c", new NominalValueIterator<>(new HashSet<>(Arrays.asList("1", "2"))));
        mapping.put("d", new NominalValueIterator<>(new HashSet<>(Arrays.asList(1, 2, 3))));

        EnumerationMapping expected = new EnumerationMapping(mapping);

        assertEquals(expected, EnumerationMapping.Transformer.transformFromAtomset(atomset));

    }

}
