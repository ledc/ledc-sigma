package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class VariableOrdinalAtomTest {

    @Test
    public void variableOrdinalAtomLowerThanIsAlwaysFalseTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "A");
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void variableOrdinalAtomGreaterThanIsAlwaysFalseTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, "A");
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void variableOrdinalAtomLowerThanOrEqualIsAlwaysFalseTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, "A", -1);
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void variableOrdinalAtomGreaterThanOrEqualIsAlwaysFalseTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, "A", 1);
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void variableOrdinalAtomNotEqualIsAlwaysFalseTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.NEQ, "A");
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void variableOrdinalAtomIsNotAlwaysFalseDifferentAttributeTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, "B");
        assertFalse(atom.isAlwaysFalse());

    }

    @Test
    public void variableOrdinalAtomLowerThanIsAlwaysTrueTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "A", 1);
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void variableOrdinalAtomGreaterThanIsAlwaysTrueTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, "A", -1);
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void variableOrdinalAtomLowerThanOrEqualIsAlwaysTrueTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, "A");
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void variableOrdinalAtomGreaterThanOrEqualIsAlwaysTrueTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, "A");
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void variableOrdinalAtomEqualIsAlwaysTrueTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.EQ, "A");
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void variableOrdinalAtomIsNotAlwaysTrueDifferentAttributeTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, "B");
        assertFalse(atom.isAlwaysTrue());

    }

    @Test
    public void variableOrdinalAtomGreaterThanSimplifyTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, "B", 2);

        // Create expected simplified atom
        Set<VariableOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> simplifiedAtom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, "B", 2);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableOrdinalAtomLowerThanSimplifyTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B", 2);

        // Create expected simplified atom
        Set<VariableOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> simplifiedAtom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B", 2);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableOrdinalAtomGreaterThanOrEqualSimplifyTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, "B", 2);

        // Create expected simplified atom
        Set<VariableOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> simplifiedAtom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, "B", 1);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableOrdinalAtomLowerThanOrEqualSimplifyTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, "B", 2);

        // Create expected simplified atom
        Set<VariableOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> simplifiedAtom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B", 3);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableOrdinalAtomEqualSimplifyTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.EQ, "B", 2);

        // Create expected simplified atom
        Set<VariableOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> simplifiedAtom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.EQ, "B", 2);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableOrdinalAtomNotEqualSimplifyTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.NEQ, "B", 2);

        // Create expected simplified atom
        Set<VariableOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> simplifiedAtom1 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B", 2);
        VariableOrdinalAtom<Integer> simplifiedAtom2 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, "B", 2);
        simplifiedAtoms.add(simplifiedAtom1);
        simplifiedAtoms.add(simplifiedAtom2);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableOrdinalAtomGreaterThanIsSimplifiedTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, "B", 2);
        assertTrue(atom.isSimplified());

    }

    @Test
    public void variableOrdinalAtomLowerThanIsSimplifiedTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B", 2);
        assertTrue(atom.isSimplified());

    }

    @Test
    public void variableOrdinalAtomGreaterThanOrEqualIsSimplifiedTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, "B", 2);
        assertFalse(atom.isSimplified());

    }

    @Test
    public void variableOrdinalAtomLowerThanOrEqualIsSimplifiedTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, "B", 2);
        assertFalse(atom.isSimplified());

    }

    @Test
    public void variableOrdinalAtomEqualIsSimplifiedTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.EQ, "B", 2);
        assertTrue(atom.isSimplified());

    }

    @Test
    public void variableOrdinalAtomNotEqualIsSimplifiedTest() {

        // Create atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.NEQ, "B", 2);
        assertFalse(atom.isSimplified());

    }

    @Test
    public void variableOrdinalAtomFixNoAttributesTest() {

        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B");

        DataObject o = new DataObject(true)
                .setInteger("X", 0)
                .setInteger("Y", 0);

        assertEquals(atom, atom.fix(o));

    }

    @Test
    public void variableOrdinalAtomFixLeftAttributeTest() {

        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B");

        DataObject o = new DataObject(true)
                .setInteger("A", 0)
                .setInteger("Y", 0);

        assertEquals(new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "B", InequalityOperator.GT, 0), atom.fix(o));

    }

    @Test
    public void variableOrdinalAtomFixRightAttributeTest() {

        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B");

        DataObject o = new DataObject(true)
                .setInteger("B", 0)
                .setInteger("Y", 0);

        assertEquals(new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, 0), atom.fix(o));

    }

    @Test
    public void variableOrdinalAtomFixAlwaysTrueTest() {

        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B");

        DataObject o = new DataObject(true)
                .setInteger("A", 0)
                .setInteger("B", 1);

        assertEquals(AbstractAtom.ALWAYS_TRUE, atom.fix(o));

    }

    @Test
    public void variableOrdinalAtomFixAlwaysFalseTest() {

        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, "B");

        DataObject o = new DataObject(true)
                .setInteger("A", 1)
                .setInteger("B", 0);

        assertEquals(AbstractAtom.ALWAYS_FALSE, atom.fix(o));

    }


}
