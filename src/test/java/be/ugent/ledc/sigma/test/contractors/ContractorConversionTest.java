package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.dataset.Contract;
import be.ugent.ledc.core.dataset.ContractedDataset;
import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class ContractorConversionTest
{
    @Test
    public void sigmaContractConversionTest()
    {
        Contract contract = new Contract.ContractBuilder()
            .addContractor("a", TypeContractorFactory.INTEGER)
            .addContractor("b", TypeContractorFactory.LONG)
            .build();
        
        ContractedDataset dataset = new ContractedDataset(contract);

        dataset = dataset.asInteger("a", SigmaContractorFactory.INTEGER);
        dataset = dataset.asInteger("b", SigmaContractorFactory.INTEGER);

        assertEquals(SigmaContractorFactory.INTEGER, dataset
            .getContract()
            .getAttributeContract("a"));
    }
}
