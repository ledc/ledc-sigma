package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.time.LocalTime;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class TimeInSecondsContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.TIME_SECONDS.hasNext(LocalTime.of(12, 35, 48)));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.TIME_SECONDS.hasNext(SigmaContractorFactory.TIME_SECONDS.last().minusSeconds(1)));
        assertFalse(SigmaContractorFactory.TIME_SECONDS.hasNext(SigmaContractorFactory.TIME_SECONDS.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.TIME_SECONDS.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(LocalTime.of(13, 21, 17), SigmaContractorFactory.TIME_SECONDS.next(LocalTime.of(13, 21, 16)));
    }

    @Test
    public void nextOverflowTest() {
        LocalTime current = LocalTime.MAX;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalTime has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.TIME_SECONDS.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.TIME_SECONDS.hasPrevious(LocalTime.of(12, 35, 48)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.TIME_SECONDS.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(LocalTime.of(13, 21, 15), SigmaContractorFactory.TIME_SECONDS.previous(LocalTime.of(13, 21, 16)));
    }

    @Test
    public void previousOverflowTest() {
        LocalTime current = LocalTime.MIN;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalTime has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.TIME_SECONDS.previous(current);
    }
    
    @Test
    public void nameTest()
    {
        assertEquals("time_in_seconds", SigmaContractorFactory.TIME_SECONDS.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.TIME_SECONDS.hasPrevious(SigmaContractorFactory.TIME_SECONDS.first().plusSeconds(1)));
        assertFalse(SigmaContractorFactory.TIME_SECONDS.hasPrevious(SigmaContractorFactory.TIME_SECONDS.first()));
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        LocalTime end   = LocalTime.of(14, 45, 23);
        assertEquals(11374, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>(start,end)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        LocalTime end   = LocalTime.of(14, 45, 23);
        assertEquals(11376, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>(start,end,false,false)));
    }

    @Test
    public void cardinalityNullEndTest()
    {
        LocalTime end = LocalTime.of(11, 35, 48);
        assertEquals(41747, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>(null, end)));
    }
    
    @Test
    public void cardinalityNullStartTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        assertEquals(44650, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>(start,null)));
    }
    
    @Test
    public void cardinalityFullSpanTest()
    {
        assertEquals(86400, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>(LocalTime.MIN,LocalTime.MAX,false,false)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        assertEquals(44650, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>(start,LocalTime.MAX)));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        LocalTime end = LocalTime.of(11, 35, 48);
        assertEquals(41748, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>(LocalTime.MIN,end, true, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.TIME_SECONDS.cardinality(new Interval<>( LocalTime.of(11, 30, 40),  LocalTime.of(11, 30, 40), true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(LocalTime.MIN.withNano(0), SigmaContractorFactory.TIME_SECONDS.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(LocalTime.MAX.withNano(0), SigmaContractorFactory.TIME_SECONDS.last());
    }
    
    @Test
    public void printConstantTest()
    {
        LocalTime d = LocalTime.of(11, 35, 48);
        
        assertEquals("11:35:48", SigmaContractorFactory.TIME_SECONDS.printConstant(d));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(LocalTime.of(11, 35, 48), SigmaContractorFactory.TIME_SECONDS.parseConstant("11:35:48"));
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(LocalTime.of(11, 36, 51), SigmaContractorFactory.TIME_SECONDS.add(LocalTime.of(11, 35, 48), 63));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(LocalTime.of(11, 34, 45), SigmaContractorFactory.TIME_SECONDS.add(LocalTime.of(11, 35, 48), -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.TIME_SECONDS.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        LocalTime value = LocalTime.MAX.minusSeconds(5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalTime " + value + " with " + units + " seconds.");
        SigmaContractorFactory.TIME_SECONDS.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        LocalTime value = LocalTime.MIN.plusSeconds(5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalTime " + value + " with " + units + " seconds.");
        SigmaContractorFactory.TIME_SECONDS.add(value, units);
    } 
}
