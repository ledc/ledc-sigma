package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.time.LocalTime;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class TimeInMinutesContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.TIME_MINUTES.hasNext(LocalTime.of(12, 35, 48)));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.TIME_MINUTES.hasNext(SigmaContractorFactory.TIME_MINUTES.last().minusMinutes(1)));
        assertFalse(SigmaContractorFactory.TIME_MINUTES.hasNext(SigmaContractorFactory.TIME_MINUTES.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.TIME_MINUTES.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(LocalTime.of(13, 22, 0), SigmaContractorFactory.TIME_MINUTES.next(LocalTime.of(13, 21, 16)));
    }

    @Test
    public void nextOverflowTest() {
        LocalTime current = LocalTime.MAX;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalTime has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.TIME_MINUTES.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.TIME_MINUTES.hasPrevious(LocalTime.of(12, 35, 48)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.TIME_MINUTES.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(LocalTime.of(13, 20, 0), SigmaContractorFactory.TIME_MINUTES.previous(LocalTime.of(13, 21, 16)));
    }

    @Test
    public void previousOverflowTest() {
        LocalTime current = LocalTime.MIN;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalTime has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.TIME_MINUTES.previous(current);
    }

    @Test
    public void nameTest()
    {
        assertEquals("time_in_minutes", SigmaContractorFactory.TIME_MINUTES.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.TIME_MINUTES.hasPrevious(SigmaContractorFactory.TIME_MINUTES.first().plusMinutes(1)));
        assertFalse(SigmaContractorFactory.TIME_MINUTES.hasPrevious(SigmaContractorFactory.TIME_MINUTES.first()));
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        LocalTime end   = LocalTime.of(14, 45, 23);
        assertEquals(189, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>(start,end)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        LocalTime end   = LocalTime.of(14, 45, 23);
        assertEquals(191, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>(start,end,false,false)));
    }

    @Test
    public void cardinalityNullStartTest()
    {
        LocalTime end = LocalTime.of(11, 35, 48);
        assertEquals(694, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>(null, end)));
    }
    
    @Test
    public void cardinalityNullEndTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        assertEquals(743, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>(start,null)));
    }
    
    @Test
    public void cardinalityFullSpanTest()
    {
        assertEquals(1440, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>(LocalTime.MIN,LocalTime.MAX,false,false)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        LocalTime start = LocalTime.of(11, 35, 48);
        assertEquals(743, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>(start,LocalTime.MAX)));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        LocalTime end = LocalTime.of(11, 35, 48);
        assertEquals(695, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>(LocalTime.MIN,end, true, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.TIME_MINUTES.cardinality(new Interval<>( LocalTime.of(11, 30, 40),  LocalTime.of(11, 30, 40), true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(LocalTime.MIN.withNano(0).withSecond(0), SigmaContractorFactory.TIME_MINUTES.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(LocalTime.MAX.withNano(0).withSecond(0), SigmaContractorFactory.TIME_MINUTES.last());
    }
    
    @Test
    public void printConstantTest()
    {
        LocalTime d = LocalTime.of(11, 35, 48);
        
        assertEquals("11:35", SigmaContractorFactory.TIME_MINUTES.printConstant(d));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(LocalTime.of(11, 35, 0), SigmaContractorFactory.TIME_MINUTES.parseConstant("11:35:48"));
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(LocalTime.of(12, 38, 0), SigmaContractorFactory.TIME_MINUTES.add(LocalTime.of(11, 35, 48), 63));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(LocalTime.of(10, 32, 0), SigmaContractorFactory.TIME_MINUTES.add(LocalTime.of(11, 35, 48), -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.TIME_MINUTES.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        LocalTime value = LocalTime.MAX.minusMinutes(5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalTime " + value + " with " + units + " minutes.");
        SigmaContractorFactory.TIME_MINUTES.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        LocalTime value = LocalTime.MIN.plusMinutes(5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalTime " + value + " with " + units + " minutes.");
        SigmaContractorFactory.TIME_MINUTES.add(value, units);
    } 
}
