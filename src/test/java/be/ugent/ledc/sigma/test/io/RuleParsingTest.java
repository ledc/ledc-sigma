package be.ugent.ledc.sigma.test.io;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class RuleParsingTest
{
    @Test
    public void ruleParsingTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> atom1 = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.NOTIN,
            Stream.of("x&y", "x,y", "x'y").collect(Collectors.toSet())
        );
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> atom2 = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "b",
            SetOperator.NOTIN,
            Stream.of("x&y", "x,y", "x'y").collect(Collectors.toSet())
        );
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> atom3 = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "c",
            SetOperator.NOTIN,
            Stream.of("x&y", "x,y", "x'y").collect(Collectors.toSet())
        );
        
        AbstractAtom<String, NominalContractor<String>, EqualityOperator> atom4 = new ConstantNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.EQ,
            "x&y"
        );
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> atom5 = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "b",
            SetOperator.IN,
            Stream.of("x&y", "x'y").collect(Collectors.toSet())
        );
        
        AbstractAtom<String, NominalContractor<String>, SetOperator> atom6 = new SetNominalAtom<>
        (
            SigmaContractorFactory.STRING,
            "c",
            SetOperator.IN,
            Stream.of("x&y", "x,y").collect(Collectors.toSet())
        );
        
        Set<SigmaRule> expected = new HashSet<>();
        
        expected.add(new SigmaRule(atom1));
        expected.add(new SigmaRule(atom2));
        expected.add(new SigmaRule(atom3));
        expected.add(new SigmaRule(atom4,atom5,atom6));
        
        Set<SigmaRule> parsedRules = Stream
        .of(
            SigmaRuleParser.parseSigmaRule("a in {'x\\&y', 'x,y', 'x'y'}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b notin {'x\\&y', 'x,y', 'x'y'}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT c notin {'x\\&y', 'x,y', 'x'y'}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 'x\\&y' & b in {'x'y', 'x\\&y'} & c in {'x\\&y', 'x,y'}", contractors)
        )
        .collect(Collectors.toSet());
        
        assertEquals(expected, parsedRules);
    }
    
    @Test
    public void ordersParsingTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("category", SigmaContractorFactory.STRING);
        contractors.put("status", SigmaContractorFactory.STRING);
        contractors.put("discount", SigmaContractorFactory.BOOLEAN);
        contractors.put("discount_rate", SigmaContractorFactory.INTEGER);
        contractors.put("amount", SigmaContractorFactory.STRING);

        SigmaRuleset parsedRules = SigmaRuleParser.parseSigmaRuleSet(
            ListOperations.list(
                "category IN {'Electronics', 'Clothing', 'Furniture'}",
                "status IN {'Pending', 'Shipped', 'Delivered', 'Cancelled'}",
                "discount_rate IN {0, 5, 20, 50}",
                "amount IN {'1', '1-5', '5+'}",
                "NOT discount_rate == 0 & discount == true",
                "NOT discount_rate != 0 & discount == false",
                "NOT category == 'Clothing' & amount == '1' & discount_rate != 0",
                "NOT category == 'Furniture' & amount == '1'",
                "NOT category == 'Electronics' & amount IN {'1', '1-5'} & discount_rate != 0",
                "NOT amount != '5+' & status != 'Cancelled'"
            ),
            contractors);
        
        //category domain constraint
        assertTrue(
            parsedRules
            .getRules()
            .contains(
                new SigmaRule(
                    new SetNominalAtom<>
                    (
                        SigmaContractorFactory.STRING,
                        "category",
                        SetOperator.NOTIN,
                        SetOperations.set("Electronics", "Clothing", "Furniture")
                    )
                )
            )
        );
        
        //status domain constraint
        assertTrue(
            parsedRules
            .getRules()
            .contains(
                new SigmaRule(
                    new SetNominalAtom<>
                    (
                        SigmaContractorFactory.STRING,
                        "status",
                        SetOperator.NOTIN,
                        SetOperations.set("Pending", "Shipped", "Delivered", "Cancelled")
                    )
                )
            )
        );
        
        //discount rate domain constraint
        assertTrue(
            parsedRules
            .getRules()
            .contains(
                new SigmaRule(
                    new SetOrdinalAtom<>
                    (
                        SigmaContractorFactory.INTEGER,
                        "discount_rate",
                        SetOperator.NOTIN,
                        SetOperations.set(0,5,20,50)
                    )
                )
            )
        );
        
        //amount domain constraint
        assertTrue(
            parsedRules
            .getRules()
            .contains(
                new SigmaRule(
                    new SetNominalAtom<>
                    (
                        SigmaContractorFactory.STRING,
                        "amount",
                        SetOperator.NOTIN,
                        SetOperations.set("1", "1-5", "5+")
                    )
                )
            )
        );
        
        //NOT discount_rate == 0 & discount == true
        assertTrue(
            parsedRules
            .getRules()
            .contains(
                new SigmaRule(
                    new ConstantOrdinalAtom<>
                    (
                        SigmaContractorFactory.INTEGER,
                        "discount_rate",
                        EqualityOperator.EQ,
                        0
                    ),
                    new ConstantOrdinalAtom<>
                    (
                        SigmaContractorFactory.BOOLEAN,
                        "discount",
                        EqualityOperator.EQ,
                        true
                    )
                )
            )
        );
        
        //NOT discount_rate != 0 & discount == false
        assertTrue(
            parsedRules
            .getRules()
            .contains(
                new SigmaRule(
                    new ConstantOrdinalAtom<>
                    (
                        SigmaContractorFactory.INTEGER,
                        "discount_rate",
                        EqualityOperator.NEQ,
                        0
                    ),
                    new ConstantOrdinalAtom<>
                    (
                        SigmaContractorFactory.BOOLEAN,
                        "discount",
                        EqualityOperator.EQ,
                        false
                    )
                )
            )
        );
        
        //NOT category == 'Clothing' & amount == '1' & discount_rate != 0
        assertTrue(
            parsedRules
            .getRules()
            .contains(
                new SigmaRule(
                    new ConstantOrdinalAtom<>
                    (
                        SigmaContractorFactory.INTEGER,
                        "discount_rate",
                        EqualityOperator.NEQ,
                        0
                    ),
                    new ConstantNominalAtom<>
                    (
                        SigmaContractorFactory.STRING,
                        "amount",
                        EqualityOperator.EQ,
                        "1"
                    ),
                    new ConstantNominalAtom<>
                    (
                        SigmaContractorFactory.STRING,
                        "category",
                        EqualityOperator.EQ,
                        "Clothing"
                    )
                )
            )
        );
    }
}
