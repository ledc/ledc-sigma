package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.datastructures.values.NominalValueIterator;
import be.ugent.ledc.sigma.datastructures.values.OrdinalValueIterator;
import be.ugent.ledc.sigma.datastructures.values.attributevalues.ValueIterator;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.LoCoRepair;
import be.ugent.ledc.sigma.repair.ParkerRepair;
import be.ugent.ledc.sigma.repair.RepairOperations;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.SimpleIterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.models.IterableCostModel;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 * This test covers the entire pipeline for a simple case with an iterable cost model.
 * @author abronsel
 */
public class LoCoRepairTest
{
    @Test
    public void lowCostIterationRepairTest() throws RepairException, ParseException
    {
        //Create a simple dataset
        Dataset data = new SimpleDataset();
        
        //Add one object
        data.addDataObject(
            new DataObject()
                .set("a", 1)
                .set("b", 2)
                .set("c", 3)
        );
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT a notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT c notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & b in {2,3}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 2 & c == 3", contractors)
        )
        .collect(Collectors.toSet());
        
        SufficientSigmaRuleset sufficientRules = SufficientSigmaRuleset.create(
            new SigmaRuleset(contractors, rules),
            new FCFGenerator()
        );
        
        Map<String, IterableCostFunction> costFunctions = new HashMap<>();
        
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);
        
        Map<Integer, Map<Integer, Integer>> costMapForB = new HashMap<>();
        costMapForB.put(2, new HashMap<>());
        costMapForB.put(3, new HashMap<>());
        
        costMapForB.get(2).put(3, 1);
        costMapForB.get(2).put(4, 3);
        costMapForB.get(2).put(1, 3);
        
        costMapForB.get(3).put(2, 1);
        costMapForB.get(3).put(4, 1);
        costMapForB.get(3).put(1, 1);
        
        Map<Integer, Map<Integer, Integer>> costMapForC = new HashMap<>();
        costMapForC.put(3, new HashMap<>());
        
        costMapForC.get(3).put(1, 1);
        costMapForC.get(3).put(2, 3);
        costMapForC.get(3).put(4, 2);
        
        costFunctions.put("a", new SimpleIterableCostFunction<>(costMapForA));
        costFunctions.put("b", new SimpleIterableCostFunction<>(costMapForB));
        costFunctions.put("c", new SimpleIterableCostFunction<>(costMapForC));
        
        IterableCostModel costModel = new IterableCostModel(costFunctions);
        
        LoCoRepair engine = new LoCoRepair(
            sufficientRules,
            costModel,
            new RandomRepairSelection()
        );

        Dataset repair = engine.repair(data);
        
        assertEquals(1, repair.getDataObjects().size());
        
        DataObject expected = new DataObject()
            .set("a", 2)
            .set("b", 2)
            .set("c", 1);
        
        assertEquals(expected, repair.getDataObjects().get(0));
    }
    
    @Test
    public void noRepairTest() throws RepairException, ParseException
    {
        //Create a simple dataset
        Dataset data = new SimpleDataset();
        
        //Add one object
        data.addDataObject(
            new DataObject()
                .set("a", 4)
                .set("b", 4)
                .set("c", 4)
        );

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT a notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT c notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & b in {2,3}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 2 & c == 3", contractors)
        )
        .collect(Collectors.toSet());
        
        SufficientSigmaRuleset sufficientRules = SufficientSigmaRuleset.create(
            new SigmaRuleset(contractors, rules),
            new FCFGenerator()
        );
        
        Map<String, IterableCostFunction> costFunctions = new HashMap<>();
        
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);
        
        Map<Integer, Map<Integer, Integer>> costMapForB = new HashMap<>();
        costMapForB.put(2, new HashMap<>());
        costMapForB.put(3, new HashMap<>());
        
        costMapForB.get(2).put(3, 1);
        costMapForB.get(2).put(4, 3);
        costMapForB.get(2).put(1, 3);
        
        costMapForB.get(3).put(2, 1);
        costMapForB.get(3).put(4, 1);
        costMapForB.get(3).put(1, 1);
        
        Map<Integer, Map<Integer, Integer>> costMapForC = new HashMap<>();
        costMapForC.put(3, new HashMap<>());
        
        costMapForC.get(3).put(1, 1);
        costMapForC.get(3).put(2, 3);
        costMapForC.get(3).put(4, 2);
        
        costFunctions.put("a", new SimpleIterableCostFunction<>(costMapForA));
        costFunctions.put("b", new SimpleIterableCostFunction<>(costMapForB));
        costFunctions.put("c", new SimpleIterableCostFunction<>(costMapForC));
        
        IterableCostModel costModel = new IterableCostModel(costFunctions);
        
        LoCoRepair engine = new LoCoRepair(
            sufficientRules,
            costModel,
            new RandomRepairSelection()
        );

        Dataset repair = engine.repair(data);
        
        assertEquals(1, repair.getDataObjects().size());
        
        DataObject expected = new DataObject()
            .set("a", 4)
            .set("b", 4)
            .set("c", 4);
        
        assertEquals(expected, repair.getDataObjects().get(0));
    }
    
    @Test
    public void shrinkTest1() throws ParseException {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        
        SigmaRuleset rules = SigmaRuleParser.parseSigmaRuleSet(
                ListOperations.list(
                        "a in {'1','2','3','4','5','6'}",
                        "b in {'2','6','8'}",
                        "NOT a != b"
                ),
                contractors);
        
        Map<String, ValueIterator<?>> permittedValues = new HashMap<>();
        permittedValues.put("a", new NominalValueIterator<>(SetOperations.set("1", "6", "5")));
        
        Map<String, ValueIterator<?>> shrunkIterators = RepairOperations.shrink(permittedValues, rules);
        
        Set<String> expected = SetOperations.set("6");
        
        assertEquals(expected, shrunkIterators.get("a").convert().getValues());
        
    }
    
    @Test
    public void shrinkTest2() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        
        SigmaRuleset rules = SigmaRuleParser.parseSigmaRuleSet(
                ListOperations.list(
                    "b >= 50",
                    "b <= 60",
                    "NOT a < b"
                ),
                contractors);
        
        Map<String, ValueIterator<?>> permittedValues = new HashMap<>();
        permittedValues.put(
            "a",
            new OrdinalValueIterator<>(
                new ArrayList<>(List.of(Interval.closed(0,55))),
                SigmaContractorFactory.INTEGER
            ));
        
        Map<String, ValueIterator<?>> shrunkIterators = RepairOperations.shrink(permittedValues, rules);
        
        Set<Integer> expected = SetOperations.set(50,51,52,53,54,55);
        
        assertEquals(expected, shrunkIterators.get("a").convert().getValues());
        
    }
    
    @Test
    public void shrinkTest3() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        
        SigmaRuleset rules = SigmaRuleParser.parseSigmaRuleSet(
                ListOperations.list(
                    "b >= 50",
                    "b <= 60",
                    "NOT b > a"
                ),
                contractors);
        
        Map<String, ValueIterator<?>> permittedValues = new HashMap<>();
        permittedValues.put(
            "a",
            new OrdinalValueIterator<>(
                new ArrayList<>(List.of(Interval.closed(0,55))),
                SigmaContractorFactory.INTEGER
            ));
        
        Map<String, ValueIterator<?>> shrunkIterators = RepairOperations.shrink(permittedValues, rules);
        
        Set<Integer> expected = SetOperations.set(50,51,52,53,54,55);
        
        assertEquals(expected, shrunkIterators.get("a").convert().getValues());
        
    }
    
    @Test
    public void shrinkTest4() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        
        SigmaRuleset rules = SigmaRuleParser.parseSigmaRuleSet(
                ListOperations.list(
                    "b >= 50",
                    "b <= 60",
                    "NOT a < b & b >= c"
                ),
                contractors);
        
        Map<String, ValueIterator<?>> permittedValues = new HashMap<>();
        permittedValues.put(
            "a",
            new OrdinalValueIterator<>(
                    new ArrayList<>(List.of(Interval.closed(0,55))),
                SigmaContractorFactory.INTEGER
            ));
        
        Map<String, ValueIterator<?>> shrunkIterators = RepairOperations.shrink(permittedValues, rules);
        
        
        assertEquals(56, shrunkIterators.get("a").convert().getValues().size());
        
    }
    
    @Test
    public void shrinkTest5() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        
        SigmaRuleset rules = SigmaRuleParser.parseSigmaRuleSet(
            ListOperations.list(
                "b >= 50",
                "b <= 60",
                "NOT b > S^5(a)"
            ),
            contractors);
        
        Map<String, ValueIterator<?>> permittedValues = new HashMap<>();
        permittedValues.put(
            "a",
            new OrdinalValueIterator<>(
                    new ArrayList<>(List.of(Interval.closed(0,55))),
                SigmaContractorFactory.INTEGER
            ));
        
        Map<String, ValueIterator<?>> shrunkIterators = RepairOperations.shrink(permittedValues, rules);
        
        Set<Integer> expected = SetOperations.set(45,46,47,48,49,50,51,52,53,54,55);
        
        assertEquals(expected, shrunkIterators.get("a").convert().getValues());
        
    }
    
}
