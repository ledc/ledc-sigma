package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class VariableNominalAtomTest {

    @Test
    public void variableNominalAtomIsAlwaysFalseTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "A");
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void variableNominalAtomIsNotAlwaysFalseSameAttributeTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "A");
        assertFalse(atom.isAlwaysFalse());

    }

    @Test
    public void variableNominalAtomIsNotAlwaysFalseDifferentAttributeTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "B");
        assertFalse(atom.isAlwaysFalse());

    }


    @Test
    public void variableNominalAtomIsAlwaysTrueTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "A");
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void variableNominalAtomIsNotAlwaysTrueSameAttributeTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "A");
        assertFalse(atom.isAlwaysTrue());

    }

    @Test
    public void variableNominalAtomIsNotAlwaysTrueDifferentAttributeTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");
        assertFalse(atom.isAlwaysTrue());

    }

    @Test
    public void variableNominalAtomContradictionSimplifyTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "A");

        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        simplifiedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableNominalAtomTautologySimplifyTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "A");

        // Create expected simplified atom
        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        simplifiedAtoms.add(AbstractAtom.ALWAYS_TRUE);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableNominalAtomEqualSimplifyTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");

        // Create expected simplified atom
        Set<VariableNominalAtom<String>> simplifiedAtoms = new HashSet<>();
        VariableNominalAtom<String> simplifiedAtom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableNominalAtomNotEqualSimplifyTest() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "B");

        // Create expected simplified atom
        Set<VariableNominalAtom<String>> simplifiedAtoms = new HashSet<>();
        VariableNominalAtom<String> simplifiedAtom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "B");
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void variableNominalAtomEqualIsSimplifiedTest1() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");
        assertTrue(atom.isSimplified());

    }

    @Test
    public void variableNominalAtomEqualIsSimplifiedTest2() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "A");
        assertFalse(atom.isSimplified());

    }

    @Test
    public void variableNominalAtomEqualIsSimplifiedTest3() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "B", EqualityOperator.EQ, "A");
        assertFalse(atom.isSimplified());

    }

    @Test
    public void variableNominalAtomNotEqualIsSimplifiedTest1() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "B");
        assertTrue(atom.isSimplified());

    }

    @Test
    public void variableNominalAtomNotEqualIsSimplifiedTest2() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "A");
        assertFalse(atom.isSimplified());

    }

    @Test
    public void variableNominalAtomNotEqualIsSimplifiedTest3() {

        // Create atom
        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "B", EqualityOperator.NEQ, "A");
        assertFalse(atom.isSimplified());

    }

    @Test
    public void variableNominalAtomFixNoAttributesTest() {

        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");

        DataObject o = new DataObject(true)
                .setString("X", "0")
                .setString("Y", "0");

        assertEquals(atom, atom.fix(o));

    }

    @Test
    public void variableNominalAtomFixLeftAttributesTest() {

        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");

        DataObject o = new DataObject(true)
                .setString("A", "0")
                .setString("Y", "0");

        assertEquals(new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "B", EqualityOperator.EQ, "0"), atom.fix(o));

    }

    @Test
    public void variableNominalAtomFixRightAttributesTest() {

        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");

        DataObject o = new DataObject(true)
                .setString("B", "0")
                .setString("Y", "0");

        assertEquals(new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "0"), atom.fix(o));

    }

    @Test
    public void variableNominalAtomFixAlwaysTrueTest() {

        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");

        DataObject o = new DataObject(true)
                .setString("A", "0")
                .setString("B", "0");

        assertEquals(AbstractAtom.ALWAYS_TRUE, atom.fix(o));

    }

    @Test
    public void variableNominalAtomFixAlwaysFalseTest() {

        VariableNominalAtom<String> atom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "B");

        DataObject o = new DataObject(true)
                .setString("A", "0")
                .setString("B", "1");

        assertEquals(AbstractAtom.ALWAYS_FALSE, atom.fix(o));

    }


}
