package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.datastructures.Multiset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.atoms.AbstractAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.values.NominalValueIterator;
import be.ugent.ledc.sigma.datastructures.values.OrdinalValueIterator;
import be.ugent.ledc.sigma.datastructures.values.attributevalues.ValueIterator;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.bounding.EnumeratedBounding;
import be.ugent.ledc.sigma.repair.bounding.RangedBounding;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;
import org.junit.Assert;
import org.junit.Test;

public class BoundingTest
{
    @Test
    public void enumerateBoundingCardinalityTest()
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", "x"), 2);
        m.add(new DataObject().set("a", "y"), 3);
        m.add(new DataObject().set("a", "z"), 9);

        Set<String> hints = new HashSet<>();

        NominalValueIterator<String> valueIterator = new EnumeratedBounding<String>()
            .getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.STRING,
                hints);
        
        Assert.assertEquals(3, valueIterator.cardinality());
    }
    
    @Test
    public void enumerateBoundingIteratorTest()
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", "x"), 2);
        m.add(new DataObject().set("a", "y"), 3);
        m.add(new DataObject().set("a", "z"), 9);
        m.add(new DataObject().set("a", null), 2);
        
        ValueIterator<String> valueIterator = new EnumeratedBounding<String>()
            .getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.STRING,
                new HashSet<>()
            );
        
        Set<String> received = StreamSupport
                .stream(valueIterator.spliterator(),false)
                .collect(Collectors.toSet());
        
        Set<String> expected = Stream.of("x", "y", "z").collect(Collectors.toSet());
        
        Assert.assertEquals(expected, received);
    }
    
    @Test
    public void enumerateDomainRuleTest() throws ParseException
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", "x"), 2);
        m.add(new DataObject().set("a", "y"), 3);
        m.add(new DataObject().set("a", "z"), 9);
        m.add(new DataObject().set("a", null), 2);

        EnumeratedBounding<String> bounding = new EnumeratedBounding<>();

        Set<SigmaRule> domainRules = bounding.getDomainRules(
            "a",
            m,
            SigmaContractorFactory.STRING,
            bounding.getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.STRING,
                new HashSet<>()
            ));
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        
        Set<SigmaRule> rules = Stream.of(
                SigmaRuleParser.parseSigmaRule("a in {'x', 'y', 'z'}", contractors)
        )
        .collect(Collectors.toSet());
        
        Assert.assertEquals(rules, domainRules);
    }

    @Test
    public void rangedBoundingCardinalityTest()
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", 1), 2);
        m.add(new DataObject().set("a", 3), 3);
        m.add(new DataObject().set("a", 10), 9);
        
        ValueIterator<Integer> valueIterator = new RangedBounding<Integer>()
            .getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.INTEGER,
                new HashSet<>());
        
        Assert.assertEquals(10, valueIterator.cardinality());
    }
    
    @Test
    public void rangedBoundingIteratorTest()
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", 1), 2);
        m.add(new DataObject().set("a", 3), 3);
        m.add(new DataObject().set("a", 10), 9);
        
        ValueIterator<Integer> valueIterator = new RangedBounding<Integer>()
            .getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.INTEGER,
                new HashSet<>()
            );
        
        Set<Integer> received = StreamSupport
                .stream(valueIterator.spliterator(),false)
                .collect(Collectors.toSet());
        
        Set<Integer> expected = IntStream.rangeClosed(1, 10).boxed().collect(Collectors.toSet());
        
        Assert.assertEquals(expected, received);
    }
    
    @Test
    public void rangedBoundingJumpTest()
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", 0), 2);
        m.add(new DataObject().set("a", 20), 3);
        
        ValueIterator<Integer> valueIterator = new RangedBounding<Integer>(0, 5)
            .getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.INTEGER,
                new HashSet<>()
            );
        
        Set<Integer> received = StreamSupport
                .stream(valueIterator.spliterator(),false)
                .collect(Collectors.toSet());
        
        Set<Integer> expected = IntStream.of(0,4,8,12,16,20).boxed().collect(Collectors.toSet());
        
        Assert.assertEquals(expected, received);
    }
    
    @Test
    public void rangedBoundingJumpHintTest()
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", 0), 2);
        m.add(new DataObject().set("a", 20), 3);
        
        ValueIterator<Integer> valueIterator = new RangedBounding<Integer>(0, 5)
            .getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.INTEGER,
                SetOperations.set(-20)
            );
        
        Set<Integer> received = StreamSupport
                .stream(valueIterator.spliterator(),false)
                .collect(Collectors.toSet());
        
        Set<Integer> expected = IntStream.of(0,4,8,12,16,20,-20).boxed().collect(Collectors.toSet());
        
        Assert.assertEquals(expected, received);
    }
    
    @Test
    public void rangedDomainRuleTest() throws ParseException
    {
        //Init a multiset
        Multiset<DataObject> m = new Multiset<>();
        m.add(new DataObject().set("a", 1), 2);
        m.add(new DataObject().set("a", 3), 3);
        m.add(new DataObject().set("a", 10), 9);
        m.add(new DataObject().set("a", null), 2);

        RangedBounding<Integer> bounding = new RangedBounding<>();

        Set<SigmaRule> domainRules = bounding.getDomainRules(
            "a",
            m,
            SigmaContractorFactory.INTEGER,
            bounding.getPermittedValues(
                "a",
                m,
                SigmaContractorFactory.INTEGER,
                new HashSet<>()));
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        
        Set<SigmaRule> rules = Stream.of(
                SigmaRuleParser.parseSigmaRule("a >= 1", contractors),
                SigmaRuleParser.parseSigmaRule("a <= 10", contractors)
        )
        .collect(Collectors.toSet());
        
        Assert.assertEquals(rules, domainRules);
    }

    @Test
    public void rangedAtomsetsNullTest() {

        RangedBounding<Integer> bounding = new RangedBounding<>();

        String attribute = "a";
        DataObject dataObject = new DataObject();
        OrdinalContractor<Integer> contractor = SigmaContractorFactory.INTEGER;
        OrdinalValueIterator<Integer> currentPermittedValuesIterator = bounding.getPermittedValues(attribute, dataObject, contractor, new HashSet<>());

        Set<Set<AbstractAtom<?, ?, ?>>> domainAtomsets = bounding.getDomainAtomsets(attribute, dataObject, contractor, currentPermittedValuesIterator);

        Set<Set<AbstractAtom<?, ?, ?>>> expected = new HashSet<>();
        expected.add(new HashSet<>(Collections.singleton(AbstractAtom.ALWAYS_TRUE)));

        Assert.assertEquals(expected, domainAtomsets);

    }

    @Test
    public void rangedAtomsetsBelowThresholdOneIntervalTest() {

        RangedBounding<Integer> bounding = new RangedBounding<>(200);

        String attribute = "a";
        DataObject dataObject = new DataObject().setInteger("a", 10);
        OrdinalContractor<Integer> contractor = SigmaContractorFactory.INTEGER;
        OrdinalValueIterator<Integer> currentPermittedValuesIterator = bounding.getPermittedValues(attribute, dataObject, contractor, new HashSet<>());

        Set<Set<AbstractAtom<?, ?, ?>>> domainAtomsets = bounding.getDomainAtomsets(attribute, dataObject, contractor, currentPermittedValuesIterator);

        Set<Set<AbstractAtom<?, ?, ?>>> expected = new HashSet<>();
        expected.add(Set.of(
            new ConstantOrdinalAtom<>(contractor, attribute, InequalityOperator.GEQ, -190),
            new ConstantOrdinalAtom<>(contractor, attribute, InequalityOperator.LEQ, 210)
        ));

        Assert.assertEquals(expected, domainAtomsets);

    }

    @Test
    public void rangedAtomsetsAboveThresholdTest() {

        RangedBounding<Integer> bounding = new RangedBounding<>(500);

        String attribute = "a";
        DataObject dataObject = new DataObject().setInteger("a", 10);
        OrdinalContractor<Integer> contractor = SigmaContractorFactory.INTEGER;
        OrdinalValueIterator<Integer> currentPermittedValuesIterator = bounding.getPermittedValues(attribute, dataObject, contractor, new HashSet<>());

        Set<Set<AbstractAtom<?, ?, ?>>> domainAtomsets = bounding.getDomainAtomsets(attribute, dataObject, contractor, currentPermittedValuesIterator);

        Set<Set<AbstractAtom<?, ?, ?>>> expected = new HashSet<>();

        Set<Integer> expectedValues = new HashSet<>();

        for (int i = -490; i < 510; i += 2) {
            expectedValues.add(i);
        }

        expected.add(Set.of(
                new SetOrdinalAtom<>(contractor, attribute, SetOperator.IN, expectedValues)
        ));

        Assert.assertEquals(expected, domainAtomsets);

    }

}
