package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.time.LocalDateTime;
import java.time.Month;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class DatetimeInSecondsContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_SECONDS.hasNext(LocalDateTime.of(2020, Month.OCTOBER, 30, 12, 35, 48)));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_SECONDS.hasNext(SigmaContractorFactory.DATETIME_SECONDS.last().minusSeconds(1)));
        assertFalse(SigmaContractorFactory.DATETIME_SECONDS.hasNext(SigmaContractorFactory.DATETIME_SECONDS.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.DATETIME_SECONDS.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(LocalDateTime.of(2021, Month.JULY, 8, 13, 21, 17), SigmaContractorFactory.DATETIME_SECONDS.next(LocalDateTime.of(2021, Month.JULY, 8, 13, 21, 16)));
    }

    @Test
    public void nextOverflowTest() {
        LocalDateTime current = LocalDateTime.MAX;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalDateTime has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DATETIME_SECONDS.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_SECONDS.hasPrevious(LocalDateTime.of(2020, Month.OCTOBER, 30, 12, 35, 48)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.DATETIME_SECONDS.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(LocalDateTime.of(2021, Month.JULY, 8, 13, 21, 15), SigmaContractorFactory.DATETIME_SECONDS.previous(LocalDateTime.of(2021, Month.JULY, 8, 13, 21, 16)));
    }

    @Test
    public void previousOverflowTest() {
        LocalDateTime current = LocalDateTime.MIN;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("LocalDateTime has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.DATETIME_SECONDS.previous(current);
    }
    
    @Test
    public void nameTest()
    {
        assertEquals("datetime_in_seconds", SigmaContractorFactory.DATETIME_SECONDS.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.DATETIME_SECONDS.hasPrevious(SigmaContractorFactory.DATETIME_SECONDS.first().plusSeconds(1)));
        assertFalse(SigmaContractorFactory.DATETIME_SECONDS.hasPrevious(SigmaContractorFactory.DATETIME_SECONDS.first()));
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        LocalDateTime end   = LocalDateTime.of(2020, Month.OCTOBER, 31, 12, 30, 20);
        assertEquals(89979, SigmaContractorFactory.DATETIME_SECONDS.cardinality(new Interval<>(start,end)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        LocalDateTime end   = LocalDateTime.of(2020, Month.OCTOBER, 31, 12, 30, 20);
        assertEquals(89981, SigmaContractorFactory.DATETIME_SECONDS.cardinality(new Interval<>(start,end,false,false)));
    }
    
    @Test
    public void cardinalityNullTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_SECONDS.cardinality(new Interval<>(start,null)));
    }
    
    @Test
    public void cardinalityFullSpanTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_SECONDS.cardinality(new Interval<>(LocalDateTime.MIN,LocalDateTime.MAX,false,false)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        LocalDateTime start = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_SECONDS.cardinality(new Interval<>(start,LocalDateTime.MAX)));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        LocalDateTime end = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.DATETIME_SECONDS.cardinality(new Interval<>(LocalDateTime.MIN,end, true, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.DATETIME_SECONDS.cardinality(new Interval<>( LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40),  LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40), true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(LocalDateTime.MIN.withNano(0), SigmaContractorFactory.DATETIME_SECONDS.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(LocalDateTime.MAX.withNano(0), SigmaContractorFactory.DATETIME_SECONDS.last());
    }
    
    @Test
    public void printConstantTest()
    {
        LocalDateTime d = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40);
        
        assertEquals("2020-10-30T11:30:40", SigmaContractorFactory.DATETIME_SECONDS.printConstant(d));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(
            LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40),
            SigmaContractorFactory
                .DATETIME_SECONDS
                .parseConstant("2020-10-30T11:30:40")
        );
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(
            LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 31, 43),
            SigmaContractorFactory
                .DATETIME_SECONDS
                .add(LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40), 63));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(
            LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 29, 37),
            SigmaContractorFactory
                .DATETIME_SECONDS
                .add(LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 30, 40), -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.DATETIME_SECONDS.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        LocalDateTime value = LocalDateTime.MAX.minusSeconds(5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalDateTime " + value + " with " + units + " seconds.");
        SigmaContractorFactory.DATETIME_SECONDS.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        LocalDateTime value = LocalDateTime.MIN.plusSeconds(5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of LocalDateTime " + value + " with " + units + " seconds.");
        SigmaContractorFactory.DATETIME_SECONDS.add(value, units);
    }
    
    @Test
    public void beforeTest()
    {
        LocalDateTime v1 = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 29, 37);
        LocalDateTime v2 = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 29, 39);
        
        assertTrue(SigmaContractorFactory.DATETIME_SECONDS.isBefore(v1, v2));
        assertFalse(SigmaContractorFactory.DATETIME_MINUTES.isBefore(v1, v2));
        assertTrue(SigmaContractorFactory.DATETIME_MINUTES.isBeforeOrEqual(v1, v2));
    }
    
    @Test
    public void afterTest()
    {
        LocalDateTime v1 = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 29, 37);
        LocalDateTime v2 = LocalDateTime.of(2020, Month.OCTOBER, 30, 11, 29, 39);
        
        assertTrue(SigmaContractorFactory.DATETIME_SECONDS.isAfter(v2, v1));
        assertFalse(SigmaContractorFactory.DATETIME_MINUTES.isBefore(v1, v2));
    }
}
