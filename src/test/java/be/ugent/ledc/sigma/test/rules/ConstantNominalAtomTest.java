package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.sigma.datastructures.atoms.ConstantNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetNominalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class ConstantNominalAtomTest {

    @Test
    public void constantNominalAtomEqualSimplifyTest() {

        // Create atom
        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "1");

        // Create expected simplified atom
        Set<SetNominalAtom<String>> simplifiedAtoms = new HashSet<>();
        SetNominalAtom<String> simplifiedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.IN, new HashSet<>(List.of("1")));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantNominalAtomNotEqualSimplifyTest() {

        // Create atom
        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "1");

        // Create expected simplified atom
        Set<SetNominalAtom<String>> simplifiedAtoms = new HashSet<>();
        SetNominalAtom<String> simplifiedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "A", SetOperator.NOTIN, new HashSet<>(List.of("1")));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantNominalAtomEqualIsSimplifiedTest() {

        // Create atom
        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.EQ, "1");
        assertFalse(atom.isSimplified());

    }

    @Test
    public void constantNominalAtomNotEqualIsSimplifiedTest() {

        // Create atom
        ConstantNominalAtom<String> atom = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "A", EqualityOperator.NEQ, "1");
        assertFalse(atom.isSimplified());

    }


}
