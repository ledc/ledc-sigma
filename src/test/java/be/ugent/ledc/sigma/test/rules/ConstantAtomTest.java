package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.core.DataException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantNominalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class ConstantAtomTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void constantAtomEqualsTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", "2");
        
        //Create an atom
        ConstantAtom<String, NominalContractor<String>, EqualityOperator> atom = new ConstantNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.EQ,
            "2"
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void constantAtomEqualsTestNull()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", null);
        
        //Create an atom
        ConstantAtom<String, NominalContractor<String>, EqualityOperator> atom = new ConstantNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.EQ,
            "2"
        );

        assertFalse(atom.test(o));
    }
    
    @Test
    public void constantAtomNotEqualsTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", "3");
        
        //Create an atom
        ConstantAtom<String, NominalContractor<String>, EqualityOperator> atom = new ConstantNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.NEQ,
            "2"
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void constantAtomNotEqualsTestNull()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", null);
        
        //Create an atom
        ConstantAtom<String, NominalContractor<String>, EqualityOperator> atom = new ConstantNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.NEQ,
            "2"
        );

        assertFalse(atom.test(o));
    }
    
    @Test
    public void constantAtomLowerThanTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 3);
        
        //Create an atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LT,
            5
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void constantAtomLowerThanEqualsTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 5);
        
        //Create an atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LEQ,
            5
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void constantAtomGreaterThanTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 6);
        
        //Create an atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GT,
            5
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void constantAtomGreaterThanEqualsTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 7);
        
        //Create an atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GEQ,
            5
        );

        assertTrue(atom.test(o));
    }

    @Test
    public void constantAtomFailingContractTest() {
        //Create an object
        DataObject o = new DataObject()
                .setBoolean("a", true);

        //Create an atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(
                SigmaContractorFactory.INTEGER,
                "a",
                InequalityOperator.GEQ,
                5
        );

        exceptionRule.expect(DataException.class);
        exceptionRule.expectMessage("Passed attribute value " + o.get("a") + " does not fulfill contract specified by " + atom.getContractor());

        atom.test(o);

    }
    
    
}
