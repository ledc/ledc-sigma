package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetOperations;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class OperationsTest
{

    @Test
    public void partitionTest1() throws ParseException {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("alnatura_nuts", SigmaContractorFactory.INTEGER);
        contractors.put("alnatura_almondnuts", SigmaContractorFactory.INTEGER);
        contractors.put("off_nuts", SigmaContractorFactory.INTEGER);
        contractors.put("off_almondnuts", SigmaContractorFactory.INTEGER);
        
        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT alnatura_nuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_almondnuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT off_nuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT off_almondnuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_nuts != off_nuts", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_almondnuts != off_almondnuts", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_nuts < alnatura_almondnuts", contractors)
        )
        .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);
        Set<SigmaRuleset> partition = SigmaRulesetOperations.partition(ruleset);
        
        assertEquals(1, partition.size());
        assertTrue(partition.contains(ruleset));
    }
    
    @Test
    public void partitionTest2() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("alnatura_nuts", SigmaContractorFactory.INTEGER);
        contractors.put("alnatura_almondnuts", SigmaContractorFactory.INTEGER);
        contractors.put("off_nuts", SigmaContractorFactory.INTEGER);
        contractors.put("off_almondnuts", SigmaContractorFactory.INTEGER);
        
        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT alnatura_nuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_almondnuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT off_nuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT off_almondnuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_nuts != off_nuts", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_almondnuts != off_almondnuts", contractors)
        )
        .collect(Collectors.toSet());
        
        //Partition 1
        Map<String, SigmaContractor<?>> contractors1 = new HashMap<>();
        
        contractors1.put("alnatura_nuts", SigmaContractorFactory.INTEGER);
        contractors1.put("off_nuts", SigmaContractorFactory.INTEGER);
        
        Set<SigmaRule> rules1 = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT alnatura_nuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT off_nuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_nuts != off_nuts", contractors)
        )
        .collect(Collectors.toSet());
        
        //Partition 2
        Map<String, SigmaContractor<?>> contractors2 = new HashMap<>();
        
        contractors2.put("alnatura_almondnuts", SigmaContractorFactory.INTEGER);
        contractors2.put("off_almondnuts", SigmaContractorFactory.INTEGER);
        
        Set<SigmaRule> rules2 = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT alnatura_almondnuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT off_almondnuts notin {0,1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT alnatura_almondnuts != off_almondnuts", contractors)
        )
        .collect(Collectors.toSet());

        //Partition
        Set<SigmaRuleset> partition = SigmaRulesetOperations.partition(new SigmaRuleset(contractors, rules));
        
        assertEquals(2, partition.size());
        assertTrue(partition.contains(new SigmaRuleset(contractors1, rules1)));
        assertTrue(partition.contains(new SigmaRuleset(contractors2, rules2)));
    }
}
