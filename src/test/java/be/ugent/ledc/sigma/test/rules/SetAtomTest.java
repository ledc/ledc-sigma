package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.core.DataException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;

import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class SetAtomTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void setAtomInTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", "8");
        
        //Create an atom
        SetAtom<String, NominalContractor<String>> atom = new SetNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.IN,
            Stream.of("2", "3", "8", "9").collect(Collectors.toSet())
        );

        assertTrue(atom.test(o));
        
    }
    
    @Test
    public void setAtomInTestNull()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", null);
        
        //Create an atom
        SetAtom<String, NominalContractor<String>> atom = new SetNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.IN,
            Stream.of("2", "3", "8", "9").collect(Collectors.toSet())
        );

        assertFalse(atom.test(o));
    }
    
    @Test
    public void setAtomNotInTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", "10");
        
        //Create an atom
        SetAtom<String, NominalContractor<String>> atom = new SetNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.NOTIN,
            Stream.of("2", "3", "8", "9").collect(Collectors.toSet())
        );

        assertTrue(atom.test(o));
        
    }
    
    @Test
    public void setAtomNotInTestNull()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", null);
        
        //Create an atom
        SetAtom<String, NominalContractor<String>> atom = new SetNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            SetOperator.IN,
            Stream.of("2", "3", "8", "9").collect(Collectors.toSet())
        );

        assertFalse(atom.test(o));
    }

    @Test
    public void setAtomFailingContractTest() {
        //Create an object
        DataObject o = new DataObject()
                .setBoolean("a", true);

        //Create an atom
        SetAtom<String, NominalContractor<String>> atom = new SetNominalAtom<>(
                SigmaContractorFactory.STRING,
                "a",
                SetOperator.IN,
                Stream.of("2", "3", "8", "9").collect(Collectors.toSet())
        );

        exceptionRule.expect(DataException.class);
        exceptionRule.expectMessage("Passed attribute value " + o.get("a") + " does not fulfill contract specified by " + atom.getContractor());

        atom.test(o);

    }

}
