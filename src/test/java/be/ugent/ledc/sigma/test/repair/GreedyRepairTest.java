package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.GreedySequentialRepair;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.SimpleIterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.models.IterableCostModel;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import org.junit.Test;

public class GreedyRepairTest {
    
    @Test
    public void greedyRepairTest1() throws RepairException, ParseException
    {
        DataObject o = new DataObject()
            .set("a", 1)
            .set("b", 2)
            .set("c", 3);
        
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("a in {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("b in {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("c in {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & b in {2,3}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 2 & c == 3", contractors)
        )
        .collect(Collectors.toSet());
        
        SufficientSigmaRuleset sufficientRules = SufficientSigmaRuleset.create(
            new SigmaRuleset(contractors, rules),
            new FCFGenerator()
        );
        
        Map<String, IterableCostFunction> costFunctions = new HashMap<>();
        
        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());
        
        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);
        
        Map<Integer, Map<Integer, Integer>> costMapForB = new HashMap<>();
        costMapForB.put(2, new HashMap<>());
        costMapForB.put(3, new HashMap<>());
        
        costMapForB.get(2).put(3, 1);
        costMapForB.get(2).put(4, 3);
        costMapForB.get(2).put(1, 3);
        
        costMapForB.get(3).put(2, 1);
        costMapForB.get(3).put(4, 1);
        costMapForB.get(3).put(1, 1);
        
        Map<Integer, Map<Integer, Integer>> costMapForC = new HashMap<>();
        costMapForC.put(3, new HashMap<>());
        
        costMapForC.get(3).put(1, 1);
        costMapForC.get(3).put(2, 3);
        costMapForC.get(3).put(4, 2);
        
        
        costFunctions.put("a", new SimpleIterableCostFunction<>(costMapForA));
        costFunctions.put("b", new SimpleIterableCostFunction<>(costMapForB));
        costFunctions.put("c", new SimpleIterableCostFunction<>(costMapForC));
        
        IterableCostModel costModel = new IterableCostModel(costFunctions);
        
        GreedySequentialRepair engine = new GreedySequentialRepair(
            sufficientRules,
            costModel
        );

        DataObject repair = engine.repair(o);

        Assert.assertEquals(1, (int) repair.getInteger("a"));
        Assert.assertTrue(repair.getInteger("b").equals(1) || repair.getInteger("b").equals(4));
        Assert.assertEquals(3, (int) repair.getInteger("c"));
    }
}
