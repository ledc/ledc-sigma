package be.ugent.ledc.sigma.test.implication;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.SimpleImplicator;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.SimpleManager;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class SimpleImplicatorTest {

    @Test
    public void isRedundantToTest1() throws ParseException
    {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("a in {'1','2','3'}", contractors);
        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("NOT a in {'1','2'} & b in {'1', '2'}", contractors);

        SimpleImplicator implicator = new SimpleImplicator();

        assertFalse(implicator.isRedundantTo(rule2, rule1));

    }

    @Test
    public void isRedundantToTest2()  throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("NOT a in {'1', '2'} & b in {'1', '2'}", contractors);
        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("NOT a in {'1', '2', '3'}", contractors);

        SimpleImplicator implicator = new SimpleImplicator();

        assertTrue(implicator.isRedundantTo(rule1, rule2));
        assertFalse(implicator.isRedundantTo(rule2, rule1));

    }

    @Test
    public void isRedundantToTest3() throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("NOT a in {'1', '2'} & b in {'1'}", contractors);
        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("NOT a in {'1', '2', '3'} & b in {'1'}", contractors);

        SimpleImplicator implicator = new SimpleImplicator();

        assertTrue(implicator.isRedundantTo(rule1, rule2));
        assertFalse(implicator.isRedundantTo(rule2, rule1));

    }

    @Test
    public void isRedundantToTest4() throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("NOT a notin {'1','2'}", contractors);
        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("NOT a notin {'1','2','3'}", contractors);

        SimpleImplicator implicator = new SimpleImplicator();

        assertTrue(implicator.isRedundantTo(rule2, rule1));
        assertFalse(implicator.isRedundantTo(rule1, rule2));

    }

    @Test
    public void boskovitzExampleTest() throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);


        SigmaRule rule1 = SigmaRuleParser.parseSigmaRule("a in {'1','2','3'}", contractors);
        SigmaRule rule2 = SigmaRuleParser.parseSigmaRule("b in {'1','2','3'}", contractors);
        SigmaRule rule3 = SigmaRuleParser.parseSigmaRule("c in {'1','2','3','4','5','6','7','8','9'}", contractors);
        SigmaRule rule4 = SigmaRuleParser.parseSigmaRule("NOT a in {'1','2'} & b in {'2','3'} & c in {'3','4','9'}", contractors);
        SigmaRule rule5 = SigmaRuleParser.parseSigmaRule("NOT a in {'3'} & b in {'2','3'} & c in {'3','4','8'}", contractors);
        SigmaRule rule6 = SigmaRuleParser.parseSigmaRule("NOT a in {'1','3'} & b in {'1'} & c in {'3','5','6'}", contractors);
        SigmaRule rule7 = SigmaRuleParser.parseSigmaRule("NOT a in {'2'} & b in {'1'} & c in {'3','5','7'}", contractors);
        SigmaRule rule8 = SigmaRuleParser.parseSigmaRule("NOT a in {'1'} & b in {'2'} & c in {'2','3'}", contractors);
        SigmaRule rule9 = SigmaRuleParser.parseSigmaRule("NOT a in {'1'} & b in {'1','3'} & c in {'1','3'}", contractors);
        SigmaRule rule10 = SigmaRuleParser.parseSigmaRule("NOT a in {'1'} & c in {'1','2','4','5','6','7','8','9'}", contractors);


        SigmaRuleset sigmaRuleset = new SigmaRuleset(contractors, new HashSet<>(Arrays.asList(rule1, rule2, rule3, rule4, rule5, rule6, rule7, rule8, rule9, rule10)));

        SigmaRule suffRule1 = SigmaRuleParser.parseSigmaRule("a in {'1','2','3'}", contractors);
        SigmaRule suffRule2 = SigmaRuleParser.parseSigmaRule("b in {'1','2','3'}", contractors);
        SigmaRule suffRule3 = SigmaRuleParser.parseSigmaRule("c in {'1','2','3','4','5','6','7','8','9'}", contractors);
        SigmaRule suffRule4 = SigmaRuleParser.parseSigmaRule("NOT a in {'1','2'} & b in {'2','3'} & c in {'3','4','9'}", contractors);
        SigmaRule suffRule5 = SigmaRuleParser.parseSigmaRule("NOT a in {'3'} & b in {'2','3'} & c in {'3','4','8'}", contractors);
        SigmaRule suffRule6 = SigmaRuleParser.parseSigmaRule("NOT a in {'1','3'} & b in {'1'} & c in {'3','5','6'}", contractors);
        SigmaRule suffRule7 = SigmaRuleParser.parseSigmaRule("NOT a in {'2'} & b in {'1'} & c in {'3','5','7'}", contractors);
        SigmaRule suffRule8 = SigmaRuleParser.parseSigmaRule("NOT b in {'2','3'} & c in {'3','4'}", contractors);
        SigmaRule suffRule9 = SigmaRuleParser.parseSigmaRule("NOT b in {'1'} & c in {'3','5'}", contractors);
        SigmaRule suffRule10 = SigmaRuleParser.parseSigmaRule("NOT c in {'3'}", contractors);
        SigmaRule suffRule11 = SigmaRuleParser.parseSigmaRule("NOT a in {'1'}", contractors);

        SigmaRuleset sufficientSigmaRulesetValid = new SigmaRuleset(contractors, new HashSet<>(Arrays.asList(suffRule1, suffRule2, suffRule3, suffRule4, suffRule5, suffRule6, suffRule7, suffRule8, suffRule9, suffRule10, suffRule11)));

        SimpleManager manager = new SimpleManager();

        FCFGenerator fcf = new FCFGenerator(manager);

        Set<SigmaRule> sufficientSet = fcf.generateSufficientSet(sigmaRuleset);

        assertEquals(sufficientSet, sufficientSigmaRulesetValid.getRules());

    }

}
