package be.ugent.ledc.sigma.test.implication;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.util.ListOperations;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRulesetException;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFWrapper;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class FCFTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void fcfWrapperTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);
        contractors.put("f", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT a notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT c notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT d notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT e notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT f notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & b in {1,2}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT c == 1 & b in {3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT d <= e", contractors)
        )
        .collect(Collectors.toSet());
        
        SufficientSigmaRuleset ssr = new FCFWrapper()
            .build(new SigmaRuleset(contractors, rules));
        
        Set<SigmaRule> expectedNewRules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT e notin {1,2,3,4}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a in {1} & c in {1}", contractors)
        )
        .collect(Collectors.toSet());
                
        assertTrue(ssr.getRules().containsAll(expectedNewRules));
        

    }
    
    @Test
    public void contradictionTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        
        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("a in {0,1}", contractors),
            SigmaRuleParser.parseSigmaRule("b in {0,1}", contractors),
            SigmaRuleParser.parseSigmaRule("c in {0,1}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & b == 1", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 0 & b == 0", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 1 & c == 0", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a == 0 & c == 1", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b == 0 & c == 1", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b == 1 & c == 0", contractors)
        )
        .collect(Collectors.toSet());
        
        exceptionRule.expect(SigmaRulesetException.class);
        new FCFWrapper().build(new SigmaRuleset(contractors, rules));
        
    }
    
    @Test
    public void boundedDomainsTest()  throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("a >= 1", contractors),
            SigmaRuleParser.parseSigmaRule("a <= 10", contractors),
            SigmaRuleParser.parseSigmaRule("b >= 5", contractors),
            SigmaRuleParser.parseSigmaRule("b <= 15", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a < b", contractors)
        )
        .collect(Collectors.toSet());
        
        SufficientSigmaRuleset ssr = new FCFWrapper()
            .build(new SigmaRuleset(contractors, rules));
        
        for(SigmaRule sr: ssr.getRules())
        {
            System.out.println(sr);
        }
        
        Set<SigmaRule> expectedNewRules = Stream.of(
            SigmaRuleParser.parseSigmaRule("NOT a <= 4", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a >= 11", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b <= 4", contractors),
            SigmaRuleParser.parseSigmaRule("NOT b >= 11", contractors),
            SigmaRuleParser.parseSigmaRule("NOT a < b", contractors)
        )
        .collect(Collectors.toSet());
                
        Assert.assertEquals(expectedNewRules, ssr.getRules());

    }
    
    @Test
    public void nominalContractNoDomainsTest() throws ParseException, FileNotFoundException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        SigmaRuleset rules = SigmaRuleParser.parseSigmaRuleSet(
            ListOperations.list(
                "b in {1,2,3,4}",
                "NOT a == 'x' & b in {2,3}",
                "NOT a != 'x' & b in {3,4}"
            ),
            contractors);
        
        SigmaRule newRule = SigmaRuleParser.parseSigmaRule("b notin {3}", contractors);
        
        SufficientSigmaRuleset ssr = SufficientSigmaRuleset.create(rules, new FCFGenerator());
        ssr.stream().forEach(System.out::println);
        assertTrue(ssr.getRules().contains(newRule));
        
    }
}
