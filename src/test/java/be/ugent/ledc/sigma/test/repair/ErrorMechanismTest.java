package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.cost.errormechanism.ErrorMechanism;
import be.ugent.ledc.core.cost.errormechanism.SwapError;
import be.ugent.ledc.core.cost.errormechanism.datetime.DateTimeFieldError;
import be.ugent.ledc.core.cost.errormechanism.datetime.DayMonthSwitch;
import be.ugent.ledc.core.cost.errormechanism.decimal.DecimalErrors;
import be.ugent.ledc.core.cost.errormechanism.decimal.DecimalLinearError;
import be.ugent.ledc.core.cost.errormechanism.integer.*;
import be.ugent.ledc.core.dataset.DataObject;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.Month;
import java.time.temporal.ChronoField;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.*;
import org.junit.Test;

public class ErrorMechanismTest
{
    @Test
    public void singleDigitExplain()
    {
        SingleDigitError errorMechanism = new SingleDigitError();
        
        int original = 123456;
        int explained = 123486;
        int notExplained = 132456;

        assertTrue(errorMechanism.explains(original, explained, new DataObject()));
        assertFalse(errorMechanism.explains(original, notExplained, new DataObject()));
    }
    
    @Test
    public void singleDigitExplanations()
    {
        SingleDigitError errorMechanism = new SingleDigitError();
        
        int original = 106;
        
        List<Integer> explanations = Stream.of(
            100, 101, 102, 103, 104, 105, 107, 108, 109,
            116, 126, 136, 146, 156, 166, 176, 186, 196,
            206, 306, 406, 506, 606, 706, 806, 906
        ).collect(Collectors.toList());

        //[100, 101, 102, 103, 104, 105, 107, 108, 109, 116, 126, 136, 146, 156, 166, 176, 186, 196, 206, 306, 406, 506, 606, 706, 806, 906]
        assertEquals(explanations, errorMechanism.explanations(original, new DataObject()));
        
    }
    
    @Test
    public void adjacentTranspositionExplain()
    {
        AdjacentTranspositionError errorMechanism = new AdjacentTranspositionError();
        
        int original = 123456;
        int explained = 123546;
        int notExplained = 123457;

        assertTrue(errorMechanism.explains(original, explained, new DataObject()));
        assertFalse(errorMechanism.explains(original, notExplained, new DataObject()));
    }
    
    @Test
    public void adjacentTranspositionExplanations()
    {
        AdjacentTranspositionError errorMechanism = new AdjacentTranspositionError();
        
        int original = 10226;
        
        List<Integer> explanations = Stream.of(
            12026, 10262
        ).collect(Collectors.toList());

        assertEquals(explanations, errorMechanism.explanations(original, new DataObject()));
    }
    
    @Test
    public void twinExplain()
    {
        TwinError errorMechanism = new TwinError();
        
        int original = 123356;
        int explained = 124456;
        int notExplained = 123466;

        assertTrue(errorMechanism.explains(original, explained, new DataObject()));
        assertFalse(errorMechanism.explains(original, notExplained, new DataObject()));
    }
    
    @Test
    public void twinExplanations()
    {
        TwinError errorMechanism = new TwinError();
        
        int original = 22577;
        
        List<Integer> expectedExplanations = Stream.of(
            22500, 22511, 22522, 22533, 22544, 22555, 22566, 22588, 22599, 
            11577, 33577, 44577, 55577, 66577, 77577, 88577, 99577
            
        ).collect(Collectors.toList());

        List<Integer> actualExplanations = errorMechanism.explanations(original, new DataObject());
        
        assertEquals(expectedExplanations, actualExplanations);
    }
    
    @Test
    public void phoneticExplain()
    {
        PhoneticError errorMechanism = new PhoneticError();
        
        int original = 203010;
        int explained = 123010;
        int notExplained = 203001;

        assertTrue(errorMechanism.explains(original, explained, new DataObject()));
        assertTrue(errorMechanism.explains(explained, original, new DataObject()));
        assertFalse(errorMechanism.explains(original, notExplained, new DataObject()));
    }
    @Test
    public void phoneticExplanations()
    {
        PhoneticError errorMechanism = new PhoneticError();
        
        int original = 201310;
        
        List<Integer> expectedExplanations = Stream.of(
            203010, 121310
        ).collect(Collectors.toList());

        List<Integer> actualExplanations = errorMechanism.explanations(original, new DataObject());
        
        assertEquals(expectedExplanations, actualExplanations);
    }
    
    @Test
    public void roundingExplain()
    {
        RoundingError errorMechanism = new RoundingError();
        
        int original = 20300;
        int explained = 20301;
        int notExplained = 20400;

        assertTrue(errorMechanism.explains(original, explained, new DataObject()));
        assertFalse(errorMechanism.explains(explained, original, new DataObject()));
        assertFalse(errorMechanism.explains(original, notExplained, new DataObject()));
    }
    @Test
    public void roundingExplanations()
    {
        RoundingError errorMechanism = new RoundingError();
        
        int original = 1030;
        
        List<Integer> expectedExplanations = Stream.of(
            1031, 1032, 1033, 1034, 1035, 1036, 1037, 1038, 1039
        ).collect(Collectors.toList());

        List<Integer> actualExplanations = errorMechanism.explanations(original, new DataObject());
        
        assertEquals(expectedExplanations, actualExplanations);
    }
    
    @Test
    public void integerLinearExplain()
    {
        LinearError errorMechanism = new LinearError(1,5,2);
        
        int original = 15;
        int explained = 38;
        int notExplained = 39;
        
        assertTrue(errorMechanism.explains(original, explained, new DataObject()));
        assertFalse(errorMechanism.explains(original, notExplained, new DataObject()));
    }
    @Test
    public void integerLinearExplanations()
    {
        LinearError errorMechanism = new LinearError(1,5,2);
        
        int original = 15;
        
        List<Integer> expectedExplanations = Stream.of(
            38
        ).collect(Collectors.toList());

        List<Integer> actualExplanations = errorMechanism.explanations(original, new DataObject());
        
        assertEquals(expectedExplanations, actualExplanations);
    }

    @Test
    public void decimalLinearExplain()
    {
        DecimalLinearError errorMechanism = new DecimalLinearError(new BigDecimal("2"),new BigDecimal("3.2"),new BigDecimal("5"),2);

        BigDecimal original = new BigDecimal("15");

        List<BigDecimal> expectedExplanations = Stream.of(
                new BigDecimal("11.60")
        ).collect(Collectors.toList());

        List<BigDecimal> actualExplanations = errorMechanism.explanations(original, new DataObject());

        assertEquals(expectedExplanations, actualExplanations);
    }
    
    @Test
    public void decimalSignErrorExplain()
    {
        ErrorMechanism<BigDecimal> em = DecimalErrors.SIGN_ERROR;

        BigDecimal original = new BigDecimal("15.15");

        List<BigDecimal> expectedExplanations = Stream.of(
                new BigDecimal("-15.15")
        ).collect(Collectors.toList());

        List<BigDecimal> actualExplanations = em.explanations(original, new DataObject());

        assertEquals(expectedExplanations, actualExplanations);
        
        assertTrue(em.explains(original, new BigDecimal("-15.15"), new DataObject()));
        assertFalse(em.explains(original, new BigDecimal("-15.150"), new DataObject()));
    }
    
    @Test
    public void decimalRoundingExplain()
    {
        ErrorMechanism<BigDecimal> em = DecimalErrors.ROUNDING_ERROR;

        BigDecimal original = new BigDecimal("15.0");

        List<BigDecimal> expectedExplanations = Stream.of(
                new BigDecimal("15.1"),
                new BigDecimal("15.2"),
                new BigDecimal("15.3"),
                new BigDecimal("15.4"),
                new BigDecimal("15.5"),
                new BigDecimal("15.6"),
                new BigDecimal("15.7"),
                new BigDecimal("15.8"),
                new BigDecimal("15.9")
        ).collect(Collectors.toList());

        List<BigDecimal> actualExplanations = em.explanations(original, new DataObject());

        assertEquals(expectedExplanations, actualExplanations);
    }

    @Test
    public void decimalLinearExplanations()
    {
        DecimalLinearError errorMechanism = new DecimalLinearError(new BigDecimal("2"),new BigDecimal("3.2"),new BigDecimal("5"),2);

        BigDecimal original = new BigDecimal("15");

        List<BigDecimal> expectedExplanations = Stream.of(
                new BigDecimal("11.60")
        ).collect(Collectors.toList());

        List<BigDecimal> actualExplanations = errorMechanism.explanations(original, new DataObject());

        assertEquals(expectedExplanations, actualExplanations);
    }

    @Test
    public void swapExplain()
    {
        SwapError<String> errorMechanism = new SwapError<>("b");
        
        //Assume we target repair of attribute a and b is an adjacent field
        DataObject o = new DataObject()
            .setString("a", "560")
            .setString("b", "1560")
            .setString("c", "160");
        
        assertTrue(errorMechanism.explains("560", "1560", o));
        assertFalse(errorMechanism.explains("560", "160", o));
    }
    
    @Test
    public void swapExplanations()
    {
        SwapError<String> errorMechanism = new SwapError<>("b", "c", "d");
        
        //Assume we target repair of attribute a and b is an adjacent field
        DataObject o = new DataObject()
            .setString("a", "560")
            .setString("b", "1560")
            .setString("c", "160")
            .setString("d", null)
            .setString("e", "don't pick me!");
        
        List<String> expected = Stream.of("1560", "160").toList();
        List<String> received = errorMechanism.explanations("560", o);

        assertTrue(expected.containsAll(received)
                && received.containsAll(expected));
        
    }
    
    @Test
    public void dayMonthSwitchExplain()
    {
        DayMonthSwitch errorMechanism = new DayMonthSwitch();
        
        //Assume we target repair of attribute a and b is an adjacent field
        LocalDateTime original = LocalDateTime.of(2020, Month.FEBRUARY, 12, 13, 30, 10);
        
        LocalDateTime explains = LocalDateTime.of(2020, Month.DECEMBER, 2, 13, 30, 10);

        assertTrue(errorMechanism.explains(original, explains, new DataObject()));
        
    }
    
    @Test
    public void dayMonthSwitchExplanations()
    {
        DayMonthSwitch errorMechanism = new DayMonthSwitch();
        
        //Assume we target repair of attribute a and b is an adjacent field
        LocalDateTime original = LocalDateTime.of(2020, Month.FEBRUARY, 12, 13, 30, 10);
        
        List<LocalDateTime> explanations = Stream.of(
            LocalDateTime.of(2020, Month.DECEMBER, 2, 13, 30, 10))
            .collect(Collectors.toList());

        
        assertEquals(explanations, errorMechanism.explanations(original, new DataObject()));
        
    }
    
    @Test
    public void dayMonthSwitchEmptyExplanations()
    {
        DayMonthSwitch errorMechanism = new DayMonthSwitch();
        
        //Assume we target repair of attribute a and b is an adjacent field
        LocalDateTime original = LocalDateTime.of(2020, Month.FEBRUARY, 20, 13, 30, 10);

        assertTrue(errorMechanism.explanations(original, new DataObject()).isEmpty());
    }
    
    @Test
    public void dateTimeFieldExplains()
    {
        DateTimeFieldError errorMechanism = new DateTimeFieldError(ChronoField.MINUTE_OF_HOUR);
        
        //Assume we target repair of attribute a and b is an adjacent field
        LocalDateTime original = LocalDateTime.of(2020, Month.FEBRUARY, 12, 13, 30, 10);
        
        LocalDateTime explains = LocalDateTime.of(2020, Month.FEBRUARY, 12, 13, 35, 10);
        LocalDateTime notExplains = LocalDateTime.of(2020, Month.FEBRUARY, 12, 13, 30, 11);

        assertTrue(errorMechanism.explains(original, explains, new DataObject()));
        assertFalse(errorMechanism.explains(original, notExplains, new DataObject()));
    }
    
    @Test
    public void dateTimeFieldExplanations()
    {
        DateTimeFieldError errorMechanism = new DateTimeFieldError(ChronoField.DAY_OF_MONTH);
        
        //Assume we target repair of attribute a and b is an adjacent field
        LocalDateTime original = LocalDateTime.of(2020, Month.FEBRUARY, 12, 13, 30, 10);
        
        List<LocalDateTime> explanations = Stream.of(
            LocalDateTime.of(2020, Month.FEBRUARY, 1, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 2, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 3, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 4, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 5, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 6, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 7, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 8, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 9, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 10, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 11, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 13, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 14, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 15, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 16, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 17, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 18, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 19, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 20, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 21, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 22, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 23, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 24, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 25, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 26, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 27, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 28, 13, 30, 10),
            LocalDateTime.of(2020, Month.FEBRUARY, 29, 13, 30, 10)
        )
        .toList();
        
        List<LocalDateTime> received = errorMechanism.explanations(original, new DataObject());

        assertTrue(received.containsAll(explanations));
        assertTrue(explanations.containsAll(received));
    }
    
}
