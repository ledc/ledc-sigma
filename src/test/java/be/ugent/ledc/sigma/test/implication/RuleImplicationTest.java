package be.ugent.ledc.sigma.test.implication;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.FullRuleImplicator;
import be.ugent.ledc.sigma.sufficientsetgeneration.implication.SimpleManager;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;

public class RuleImplicationTest {

    @Test
    public void getAllNecessaryRulesNominalOneContributorTest() throws ParseException{

        String generator = "a";
        SigmaContractor<String> generatorContractor = SigmaContractorFactory.STRING;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);

        SigmaRule candidateContributor = SigmaRuleParser.parseSigmaRule("a IN {'0'}", contractors);

        FullRuleImplicator implicator = new FullRuleImplicator();

        assertEquals(new HashSet<>(), implicator.getAllNecessaryRules(generator, generatorContractor, Collections.singleton(candidateContributor)));

    }

    @Test
    public void getAllNecessaryRulesNominalOnlyConstantsValueLacksTest() throws ParseException {

        String generator = "a";
        SigmaContractor<String> generatorContractor = SigmaContractorFactory.STRING;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("a IN {'0', '1', '2'}", contractors);
        SigmaRule candidateContributor = SigmaRuleParser.parseSigmaRule("NOT a IN {'1'} & b IN {1}", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor));

        FullRuleImplicator implicator = new FullRuleImplicator();

        assertEquals(new HashSet<>(), implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesNominalOnlyConstantsTest() throws ParseException {

        String generator = "a";
        SigmaContractor<String> generatorContractor = SigmaContractorFactory.STRING;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);

        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("a IN {'1', '2', '3'}", contractors);
        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT d IN {'1'} & a NOTIN {'2'}", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT b IN {'2'} & a NOTIN {'1'}", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT b IN {'1'} & a IN {'1'}", contractors);
        SigmaRule candidateContributor4 = SigmaRuleParser.parseSigmaRule("NOT c IN {'1'} & a IN {'3'}", contractors);
        SigmaRule candidateContributor5 = SigmaRuleParser.parseSigmaRule("NOT c IN {'2'} & a NOTIN {'3'}", contractors);
        SigmaRule candidateContributor6 = SigmaRuleParser.parseSigmaRule("NOT d NOTIN {'1'} & a IN {'2'}", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor1, candidateContributor2, candidateContributor3, candidateContributor4, candidateContributor5, candidateContributor6));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT d IN {'1'} & b IN {'2'}", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT d IN {'1'} & c IN {'2'}", contractors);
        SigmaRule newRule3 = SigmaRuleParser.parseSigmaRule("NOT b IN {'2'} & c IN {'2'}", contractors);
        SigmaRule newRule4 = SigmaRuleParser.parseSigmaRule("NOT d NOTIN {'1'} & b IN {'1'} & c IN {'1'}", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2, newRule3, newRule4));

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesNominalNoConstantsValueLacksTest() throws ParseException {

        String generator = "a";
        SigmaContractor<String> generatorContractor = SigmaContractorFactory.STRING;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);


        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("a IN {'0','1','2'}", contractors);
        SigmaRule candidateContributor = SigmaRuleParser.parseSigmaRule("NOT a NOTIN {'0'} & a == b", contractors);

        FullRuleImplicator implicator = new FullRuleImplicator();

        assertEquals(new HashSet<>(), implicator.getAllNecessaryRules(generator, generatorContractor, new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor))));

    }

    @Test
    public void getAllNecessaryRulesNominalGeneralNoConstantsOnlyVariablesTest() throws ParseException {

        String generator = "a";
        SigmaContractor<String> generatorContractor = SigmaContractorFactory.STRING;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);
        contractors.put("e", SigmaContractorFactory.STRING);
        contractors.put("f", SigmaContractorFactory.STRING);
        contractors.put("g", SigmaContractorFactory.STRING);

        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a == b & f == g", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a == c & a != d", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a != e", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(candidateContributor1, candidateContributor2, candidateContributor3));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b == e & f == g", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT c == e & b == d & f == g", contractors);
        SigmaRule newRule3 = SigmaRuleParser.parseSigmaRule("NOT c == e & d != e", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2, newRule3));
        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesNominalGeneralNoConstantsOnlyVariablesWithConstantsTest() throws ParseException {

        String generator = "a";
        SigmaContractor<String> generatorContractor = SigmaContractorFactory.STRING;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);
        contractors.put("e", SigmaContractorFactory.STRING);

        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("a IN {'1','2','3'}", contractors);
        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a == b & a NOTIN {'2'}", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a != c", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor1, candidateContributor2));

        FullRuleImplicator implicator = new FullRuleImplicator();


        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("c IN {'1','2','3'}", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT b NOTIN {'2'} & c NOTIN {'2'} & b == c", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2));

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));
    }

    @Test
    public void getAllNecessaryRulesNominalGeneralConstantsAndVariablesTest() throws ParseException {

        String generator = "a";
        SigmaContractor<String> generatorContractor = SigmaContractorFactory.STRING;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.STRING);
        contractors.put("e", SigmaContractorFactory.STRING);


        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("a IN {'1','2','3'}", contractors);
        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT b IN {'1'} & a IN {'1'}", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT c NOTIN {'1'} & a IN {'2'}", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a == d", contractors);
        SigmaRule candidateContributor4 = SigmaRuleParser.parseSigmaRule("NOT a == e", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor1, candidateContributor2, candidateContributor3, candidateContributor4));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b IN {'1'} & c NOTIN {'1'} & d IN {'3'}", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT b IN {'1'} & c NOTIN {'1'} & e IN {'3'}", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2));

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));
    }

    @Test
    public void getAllNecessaryRulesOrdinalOneContributorTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        SigmaRule candidateContributor = SigmaRuleParser.parseSigmaRule("a > 0", contractors);

        FullRuleImplicator implicator = new FullRuleImplicator();

        assertEquals(new HashSet<>(), implicator.getAllNecessaryRules(generator, generatorContractor, Collections.singleton(candidateContributor)));

    }

    @Test
    public void getAllNecessaryRulesOrdinalOnlyConstantsDoubleLoopTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);

        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("NOT a <= 5 & b IN {1} & d < e", contractors);
        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a >= 7 & b IN {2}", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a >= 6 & c <= 2", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a <= 3 & c <= 0", contractors);
        SigmaRule candidateContributor4 = SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor1, candidateContributor2, candidateContributor3, candidateContributor4));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b IN {1} & c <= 2 & d < e", contractors);

        Set<SigmaRule> newRules = Collections.singleton(newRule1);

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesOrdinalOnlyConstantsSortedIntervalsTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);

        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("NOT a >= 2 & a <= 6 & b IN {1} & d < e", contractors);
        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a >= 7 & b IN {2}", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a >= 6 & c <= 2", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a >= 1 & a <= 3 & c <= 0", contractors);
        SigmaRule candidateContributor4 = SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors);
        SigmaRule candidateContributor5 = SigmaRuleParser.parseSigmaRule("NOT a IN {3} & d IN {1}", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor1, candidateContributor2, candidateContributor3, candidateContributor4, candidateContributor5));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b IN {1} & c <= 0 & d < e", contractors);

        Set<SigmaRule> newRules = Collections.singleton(newRule1);

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesOrdinalNoConstantsValueLacksTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("NOT a < S^1(b) & a IN {1}", contractors);
        SigmaRule candidateContributor = SigmaRuleParser.parseSigmaRule("NOT a > c & a IN {2}", contractors);

        FullRuleImplicator implicator = new FullRuleImplicator();

        assertEquals(new HashSet<>(), implicator.getAllNecessaryRules(generator, generatorContractor, new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor))));

    }

    @Test
    public void getAllNecessaryRulesOrdinalGeneralConstantsTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a >= 8", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a >= 1 & a <= 7 & a NOTIN {2,3,6} & b IN {2}", contractors);
        SigmaRule candidateContributor4 = SigmaRuleParser.parseSigmaRule("NOT a NOTIN {2,4,5,7} & c IN {3}", contractors);
        SigmaRule candidateContributor5 = SigmaRuleParser.parseSigmaRule("NOT a IN {2,3,4,6} & d IN {4}", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(candidateContributor1, candidateContributor2, candidateContributor3, candidateContributor4, candidateContributor5));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule = SigmaRuleParser.parseSigmaRule("NOT b IN {2} & d IN {4}", contractors);

        Set<SigmaRule> newRules = Collections.singleton(newRule);

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesOrdinalGeneralNoConstantsOnlyVariablesSimpleTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);

        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a > S^1(b)", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a < S^1(c) & a > d", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a < e", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(candidateContributor1, candidateContributor2, candidateContributor3));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b < c & d < e", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT b < S^-1(e)", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2));

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesOrdinalGeneralNoConstantsOnlyVariablesTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);
        contractors.put("f", SigmaContractorFactory.INTEGER);
        contractors.put("g", SigmaContractorFactory.INTEGER);

        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a > S^1(b)", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a < S^1(c) & a > d", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a < e & a > f", contractors);
        SigmaRule candidateContributor4 = SigmaRuleParser.parseSigmaRule("NOT a < g", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(candidateContributor1, candidateContributor2, candidateContributor3, candidateContributor4));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b < S^-1(g)", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT b < c & d < e & f < g", contractors);
        SigmaRule newRule3 = SigmaRuleParser.parseSigmaRule("NOT b < S^-1(e) & c > S^-1(f) & d < g", contractors);
        SigmaRule newRule4 = SigmaRuleParser.parseSigmaRule("NOT b < c & d < g", contractors);
        SigmaRule newRule5 = SigmaRuleParser.parseSigmaRule("NOT b < S^-1(e) & f < g", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2, newRule3, newRule4, newRule5));

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesOrdinalGeneralNoConstantsOnlyVariablesAndDomainRuleTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);
        contractors.put("f", SigmaContractorFactory.INTEGER);
        contractors.put("g", SigmaContractorFactory.INTEGER);

        SigmaRule generatorDomainRule = SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors);
        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a > S^1(b)", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a < S^1(c) & a > d", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a < e", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(generatorDomainRule, candidateContributor1, candidateContributor2, candidateContributor3));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b <= -1", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT b < c & d <= 0", contractors);
        SigmaRule newRule3 = SigmaRuleParser.parseSigmaRule("NOT b < c & d < e", contractors);
        SigmaRule newRule4 = SigmaRuleParser.parseSigmaRule("NOT b < S^-1(e)", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2, newRule3, newRule4));

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void getAllNecessaryRulesOrdinalGeneralVariablesAndConstantsTest() throws ParseException {

        String generator = "a";
        SigmaContractor<Integer> generatorContractor = SigmaContractorFactory.INTEGER;

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        contractors.put("e", SigmaContractorFactory.INTEGER);

        SigmaRule candidateContributor1 = SigmaRuleParser.parseSigmaRule("NOT a NOTIN {1,3,5,7}", contractors);
        SigmaRule candidateContributor2 = SigmaRuleParser.parseSigmaRule("NOT a <= 0", contractors);
        SigmaRule candidateContributor3 = SigmaRuleParser.parseSigmaRule("NOT a >= 8", contractors);
        SigmaRule candidateContributor4 = SigmaRuleParser.parseSigmaRule("NOT a IN {1,3,4,5,6,7} & b IN {2}", contractors);
        SigmaRule candidateContributor5 = SigmaRuleParser.parseSigmaRule("NOT a < c", contractors);
        SigmaRule candidateContributor6 = SigmaRuleParser.parseSigmaRule("NOT a > S^-1(d) & a < S^1(e)", contractors);

        Set<SigmaRule> candidateContributors = new HashSet<>(Arrays.asList(candidateContributor1, candidateContributor2, candidateContributor3, candidateContributor4, candidateContributor5, candidateContributor6));

        FullRuleImplicator implicator = new FullRuleImplicator();

        SigmaRule newRule1 = SigmaRuleParser.parseSigmaRule("NOT b IN {2}", contractors);
        SigmaRule newRule2 = SigmaRuleParser.parseSigmaRule("NOT c >= 8", contractors);
        SigmaRule newRule3 = SigmaRuleParser.parseSigmaRule("NOT d <= 1 & e >= 7", contractors);
        SigmaRule newRule4 = SigmaRuleParser.parseSigmaRule("NOT c > S^-1(d) & e >= 7", contractors);

        Set<SigmaRule> newRules = new HashSet<>(Arrays.asList(newRule1, newRule2, newRule3, newRule4));

        assertEquals(newRules, implicator.getAllNecessaryRules(generator, generatorContractor, candidateContributors));

    }

    @Test
    public void votingTest() throws ParseException {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("bills", SigmaContractorFactory.INTEGER);
        contractors.put("p1", SigmaContractorFactory.INTEGER);
        contractors.put("blanks", SigmaContractorFactory.INTEGER);
        contractors.put("p1a", SigmaContractorFactory.INTEGER);
        contractors.put("p1b", SigmaContractorFactory.INTEGER);
        contractors.put("p1c", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream
                .of(
                        //Domain constraints
                        SigmaRuleParser.parseSigmaRule("NOT bills < 0", contractors),
                        SigmaRuleParser.parseSigmaRule("NOT p1 < 0", contractors),
                        SigmaRuleParser.parseSigmaRule("NOT blanks < 0", contractors),
                        SigmaRuleParser.parseSigmaRule("p1a >= 0", contractors),
                        SigmaRuleParser.parseSigmaRule("p1b >= 0", contractors),
                        SigmaRuleParser.parseSigmaRule("p1c >= 0", contractors),
                        //Interactions
                        SigmaRuleParser.parseSigmaRule("NOT bills < blanks", contractors),
                        SigmaRuleParser.parseSigmaRule("NOT bills < p1", contractors),
                        SigmaRuleParser.parseSigmaRule("NOT p1a > p1", contractors),
                        SigmaRuleParser.parseSigmaRule("NOT p1b > p1", contractors),
                        SigmaRuleParser.parseSigmaRule("NOT p1c > p1", contractors)
                )
                .collect(Collectors.toSet());

        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);

        SufficientSigmaRuleset sufficientSet = SufficientSigmaRuleset.create(ruleset, new FCFGenerator());

        SigmaRule implied1 = SigmaRuleParser.parseSigmaRule("NOT bills < p1a", contractors);
        SigmaRule implied2 = SigmaRuleParser.parseSigmaRule("NOT bills < p1b", contractors);
        SigmaRule implied3 = SigmaRuleParser.parseSigmaRule("NOT bills < p1c", contractors);

        Assert.assertTrue(sufficientSet.getRules().contains(implied1));
        Assert.assertTrue(sufficientSet.getRules().contains(implied2));
        Assert.assertTrue(sufficientSet.getRules().contains(implied3));
    }
    
    @Test
    public void tautologyTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("ctgov_purpose", SigmaContractorFactory.STRING);
        contractors.put("drks_purpose", SigmaContractorFactory.STRING);
        
        Set<SigmaRule> rules = Stream
            .of(
                //Domain constraints
                SigmaRuleParser.parseSigmaRule("ctgov_purpose in {'Basic Science','Diagnostic','Health Services Research', 'Other','Prevention','Screening','Supportive Care','Treatment'}", contractors),
                SigmaRuleParser.parseSigmaRule("drks_purpose in {'Basic research/physiological study','Diagnostic','Health care system', 'Other','Prevention','Prognosis','Screening','Supportive care','Treatment'}", contractors),

                //Interactions
                SigmaRuleParser.parseSigmaRule("NOT ctgov_purpose in {'Basic Science', 'Diagnostic', 'Health Services Research', 'Other', 'Prevention', 'Screening', 'Treatment'} & drks_purpose in {'Supportive Care'}", contractors)
            )
            .collect(Collectors.toSet());

        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);
        
        SigmaRuleset transformRuleset = new SimpleManager().transformRuleset(ruleset);

        assertEquals(2, transformRuleset.getRules().size());
        Assert.assertTrue(transformRuleset.getRules().stream().allMatch(rule -> rule.getInvolvedAttributes().size() == 1));
    }

}
