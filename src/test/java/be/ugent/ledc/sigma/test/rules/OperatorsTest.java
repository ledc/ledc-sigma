package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.sigma.datastructures.operators.*;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

public class OperatorsTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void lowerThanOrEqualOperatorGetInverseTest() {
        Assert.assertEquals(InequalityOperator.GT, InequalityOperator.LEQ.getInverseOperator());
    }

    @Test
    public void lowerThanOperatorGetInverseTest() {
        Assert.assertEquals(InequalityOperator.GEQ, InequalityOperator.LT.getInverseOperator());
    }

    @Test
    public void greaterThanOrEqualOperatorGetInverseTest() {
        Assert.assertEquals(InequalityOperator.LT, InequalityOperator.GEQ.getInverseOperator());
    }

    @Test
    public void greaterThanOperatorGetInverseTest() {
        Assert.assertEquals(InequalityOperator.LEQ, InequalityOperator.GT.getInverseOperator());
    }

    @Test
    public void equalOperatorGetInverseTest() {
        Assert.assertEquals(EqualityOperator.NEQ, EqualityOperator.EQ.getInverseOperator());
    }

    @Test
    public void notEqualOperatorGetInverseTest() {
        Assert.assertEquals(EqualityOperator.EQ, EqualityOperator.NEQ.getInverseOperator());
    }

    @Test
    public void inOperatorGetInverseTest() {
        Assert.assertEquals(SetOperator.NOTIN, SetOperator.IN.getInverseOperator());
    }

    @Test
    public void notInOperatorGetInverseTest() {
        Assert.assertEquals(SetOperator.IN, SetOperator.NOTIN.getInverseOperator());
    }

    @Test
    public void lowerThanOrEqualOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Collections.singletonList(InequalityOperator.LEQ)), InequalityOperator.LEQ.getImpliedOperators());
    }

    @Test
    public void lowerThanOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Arrays.asList(InequalityOperator.LT, InequalityOperator.LEQ, EqualityOperator.NEQ)), InequalityOperator.LT.getImpliedOperators());
    }

    @Test
    public void greaterThanOrEqualOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Collections.singletonList(InequalityOperator.GEQ)), InequalityOperator.GEQ.getImpliedOperators());
    }

    @Test
    public void greaterThanOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Arrays.asList(InequalityOperator.GT, InequalityOperator.GEQ, EqualityOperator.NEQ)), InequalityOperator.GT.getImpliedOperators());
    }

    @Test
    public void equalOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Arrays.asList(EqualityOperator.EQ, InequalityOperator.GEQ, InequalityOperator.LEQ)), EqualityOperator.EQ.getImpliedOperators());
    }

    @Test
    public void notEqualOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Collections.singletonList(EqualityOperator.NEQ)), EqualityOperator.NEQ.getImpliedOperators());
    }

    @Test
    public void inOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Collections.singleton(SetOperator.IN)), SetOperator.IN.getImpliedOperators());
    }

    @Test
    public void notInOperatorGetImpliedTest() {
        Assert.assertEquals(new HashSet<>(Collections.singleton(SetOperator.NOTIN)), SetOperator.NOTIN.getImpliedOperators());
    }

    @Test
    public void lowerThanOrEqualOperatorGetReverseTest() {
        Assert.assertEquals(InequalityOperator.GEQ, InequalityOperator.LEQ.getReversedOperator());
    }

    @Test
    public void lowerThanOperatorGetReverseTest() {
        Assert.assertEquals(InequalityOperator.GT, InequalityOperator.LT.getReversedOperator());
    }

    @Test
    public void greaterThanOrEqualOperatorGetReverseTest() {
        Assert.assertEquals(InequalityOperator.LEQ, InequalityOperator.GEQ.getReversedOperator());
    }

    @Test
    public void greaterThanOperatorGetReverseTest() {
        Assert.assertEquals(InequalityOperator.LT, InequalityOperator.GT.getReversedOperator());
    }

    @Test
    public void equalOperatorGetReverseTest() {
        Assert.assertEquals(EqualityOperator.EQ, EqualityOperator.EQ.getReversedOperator());
    }

    @Test
    public void notEqualOperatorGetReverseTest() {
        Assert.assertEquals(EqualityOperator.NEQ, EqualityOperator.NEQ.getReversedOperator());
    }

    @Test
    public void inOperatorGetReverseTest() {
        Assert.assertEquals(SetOperator.IN, SetOperator.IN.getReversedOperator());
    }

    @Test
    public void notInOperatorGetReverseTest() {
        Assert.assertEquals(SetOperator.NOTIN, SetOperator.NOTIN.getReversedOperator());
    }

    @Test
    public void lowerThanOrEqualOperatorEqualTrueTest() {
        Assert.assertTrue(InequalityOperator.LEQ.test(1, 1));
    }

    @Test
    public void lowerThanOrEqualOperatorLowerThanTrueTest() {
        Assert.assertTrue(InequalityOperator.LEQ.test(1, 3));
    }

    @Test
    public void lowerThanOrEqualOperatorFalseTest() {
        Assert.assertFalse(InequalityOperator.LEQ.test(3, 1));
    }

    @Test
    public void greaterThanOrEqualOperatorEqualTrueTest() {
        Assert.assertTrue(InequalityOperator.GEQ.test(1, 1));
    }

    @Test
    public void greaterThanOrEqualOperatorLowerThanTrueTest() {
        Assert.assertTrue(InequalityOperator.GEQ.test(3, 1));
    }

    @Test
    public void greaterThanOrEqualOperatorFalseTest() {
        Assert.assertFalse(InequalityOperator.GEQ.test(1, 3));
    }

    @Test
    public void lowerThanOperatorTrueTest() {
        Assert.assertTrue(InequalityOperator.LT.test(1, 3));
    }

    @Test
    public void lowerThanOperatorEqualFalseTest() {
        Assert.assertFalse(InequalityOperator.LT.test(1, 1));
    }

    @Test
    public void lowerThanOperatorEqualGreaterThanTest() {
        Assert.assertFalse(InequalityOperator.LT.test(3, 1));
    }

    @Test
    public void greaterThanOperatorTrueTest() {
        Assert.assertTrue(InequalityOperator.GT.test(3, 1));
    }

    @Test
    public void greaterThanOperatorEqualFalseTest() {
        Assert.assertFalse(InequalityOperator.GT.test(1, 1));
    }

    @Test
    public void greaterThanOperatorEqualGreaterThanTest() {
        Assert.assertFalse(InequalityOperator.GT.test(1, 3));
    }

    @Test
    public void equalOperatorTrueTest() {
        Assert.assertTrue(EqualityOperator.EQ.test(1, 1));
    }

    @Test
    public void equalOperatorFalseTest() {
        Assert.assertFalse(EqualityOperator.EQ.test(1, 3));
    }

    @Test
    public void notEqualOperatorTrueTest() {
        Assert.assertTrue(EqualityOperator.NEQ.test(1, 3));
    }

    @Test
    public void notEqualOperatorFalseTest() {
        Assert.assertFalse(EqualityOperator.NEQ.test(1, 1));
    }

    @Test
    public void inOperatorTrueTest() {
        Assert.assertTrue(SetOperator.IN.test(1, new HashSet<>(Arrays.asList(1, 2, 3, 4))));
    }

    @Test
    public void inOperatorFalseTest() {
        Assert.assertFalse(SetOperator.IN.test(0, new HashSet<>(Arrays.asList(1, 2, 3, 4))));
    }

    @Test
    public void notInOperatorTrueTest() {
        Assert.assertTrue(SetOperator.NOTIN.test(0, new HashSet<>(Arrays.asList(1, 2, 3, 4))));
    }

    @Test
    public void notInOperatorFalseTest() {
        Assert.assertFalse(SetOperator.NOTIN.test(1, new HashSet<>(Arrays.asList(1, 2, 3, 4))));
    }

    @Test
    public void lowerThanOrEqualOperatorGetBySymbolTest() {
        Assert.assertEquals(InequalityOperator.LEQ, OperatorUtil.getOperatorBySymbol("<="));
    }

    @Test
    public void lowerThanOperatorGetBySymbolTest() {
        Assert.assertEquals(InequalityOperator.LT, OperatorUtil.getOperatorBySymbol("<"));
    }

    @Test
    public void greaterhanOrEqualOperatorGetBySymbolTest() {
        Assert.assertEquals(InequalityOperator.GEQ, OperatorUtil.getOperatorBySymbol(">="));
    }

    @Test
    public void greaterThanOperatorGetBySymbolTest() {
        Assert.assertEquals(InequalityOperator.GT, OperatorUtil.getOperatorBySymbol(">"));
    }

    @Test
    public void equalOperatorGetBySymbolTest() {
        Assert.assertEquals(EqualityOperator.EQ, OperatorUtil.getOperatorBySymbol("=="));
    }

    @Test
    public void notEqualOperatorGetBySymbolTest() {
        Assert.assertEquals(EqualityOperator.NEQ, OperatorUtil.getOperatorBySymbol("!="));
    }

    @Test
    public void inOperatorGetBySymbolLowercaseTest() {
        Assert.assertEquals(SetOperator.IN, OperatorUtil.getOperatorBySymbol("in"));
    }

    @Test
    public void inOperatorGetBySymbolUppercaseTest() {
        Assert.assertEquals(SetOperator.IN, OperatorUtil.getOperatorBySymbol("IN"));
    }

    @Test
    public void notInOperatorGetBySymbolLowercaseTest() {
        Assert.assertEquals(SetOperator.NOTIN, OperatorUtil.getOperatorBySymbol("notin"));
    }

    @Test
    public void notInOperatorGetBySymbolUppercaseTest() {
        Assert.assertEquals(SetOperator.NOTIN, OperatorUtil.getOperatorBySymbol("NOTIN"));
    }

    @Test
    public void notExistingOperatorGetBySymbolTest() {

        String symbol = "//";

        exceptionRule.expect(OperatorException.class);
        exceptionRule.expectMessage("Given symbol " + symbol + " does not represent a InequalityOperator object." +
                "Possible symbols are '<=', '<', '>=', '>', '==', '!=', 'in' and 'notin'.");

        OperatorUtil.getOperatorBySymbol(symbol);

    }

    @Test
    public void lowerThanOperatorGetTransitiveTest() {

        List<ComparableOperator> operators1 = Arrays.asList(InequalityOperator.LT, EqualityOperator.EQ);
        List<ComparableOperator> operators2 = Arrays.asList(InequalityOperator.LT, InequalityOperator.LEQ);

        assertEquals(InequalityOperator.LT, ComparableOperator.getTransitiveOperatorVariable(operators1));
        assertEquals(InequalityOperator.LT, ComparableOperator.getTransitiveOperatorVariable(operators2));

    }

    @Test
    public void lowerThanOrEqualOperatorGetTransitiveTest() {

        List<ComparableOperator> operators = Arrays.asList(InequalityOperator.LEQ, EqualityOperator.EQ);

        assertEquals(InequalityOperator.LEQ, ComparableOperator.getTransitiveOperatorVariable(operators));

    }

    @Test
    public void greaterThanOperatorGetTransitiveTest() {

        List<ComparableOperator> operators1 = Arrays.asList(InequalityOperator.GT, EqualityOperator.EQ);
        List<ComparableOperator> operators2 = Arrays.asList(InequalityOperator.GT, InequalityOperator.GEQ);

        assertEquals(InequalityOperator.GT, ComparableOperator.getTransitiveOperatorVariable(operators1));
        assertEquals(InequalityOperator.GT, ComparableOperator.getTransitiveOperatorVariable(operators2));

    }

    @Test
    public void greaterThanOrEqualOperatorGetTransitiveTest() {

        List<ComparableOperator> operators = Arrays.asList(InequalityOperator.GEQ, EqualityOperator.EQ);

        assertEquals(InequalityOperator.GEQ, ComparableOperator.getTransitiveOperatorVariable(operators));

    }

    @Test
    public void equalOperatorGetTransitiveTest() {

        List<ComparableOperator> operators = Arrays.asList(EqualityOperator.EQ, EqualityOperator.EQ);

        assertEquals(EqualityOperator.EQ, ComparableOperator.getTransitiveOperatorVariable(operators));

    }

    @Test
    public void notEqualOperatorGetTransitiveTest() {

        List<ComparableOperator> operators = Arrays.asList(EqualityOperator.EQ, EqualityOperator.EQ, EqualityOperator.NEQ);

        assertEquals(EqualityOperator.NEQ, ComparableOperator.getTransitiveOperatorVariable(operators));

    }

    @Test
    public void nullGetTransitiveTest() {

        List<ComparableOperator> operators = Arrays.asList(InequalityOperator.LT, InequalityOperator.GT);

        assertNull(ComparableOperator.getTransitiveOperatorVariable(operators));

    }


}
