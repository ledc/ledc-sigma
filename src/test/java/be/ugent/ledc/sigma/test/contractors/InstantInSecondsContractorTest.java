package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.datastructures.Interval;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractException;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import java.time.Instant;
import static java.time.temporal.ChronoUnit.SECONDS;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

public class InstantInSecondsContractorTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();
    
    @Test
    public void hasNextTest()
    {
        assertTrue(SigmaContractorFactory.INSTANT_SECONDS.hasNext(Instant.ofEpochSecond(100000L)));
    }
    
    @Test
    public void hasNextMaxTest()
    {
        assertTrue(SigmaContractorFactory.INSTANT_SECONDS.hasNext(SigmaContractorFactory.INSTANT_SECONDS.last().minusSeconds(1)));
        assertFalse(SigmaContractorFactory.INSTANT_SECONDS.hasNext(SigmaContractorFactory.INSTANT_SECONDS.last()));
    }

    @Test
    public void nextNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no next value.");
        SigmaContractorFactory.INSTANT_SECONDS.next(null);
    }

    @Test
    public void nextTest() {
        assertEquals(Instant.ofEpochSecond(100001L), SigmaContractorFactory.INSTANT_SECONDS.next(Instant.ofEpochSecond(100000L)));
    }

    @Test
    public void nextOverflowTest() {
        Instant current = Instant.MAX;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Instant has no next value for value " + current + " (overflow detected).");
        SigmaContractorFactory.INSTANT_SECONDS.next(current);
    }
    
    @Test
    public void hasPreviousTest()
    {
        assertTrue(SigmaContractorFactory.INSTANT_SECONDS.hasPrevious(Instant.ofEpochSecond(100000L)));
    }

    @Test
    public void previousNullTest() {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Null has no previous value.");
        SigmaContractorFactory.INSTANT_SECONDS.previous(null);
    }

    @Test
    public void previousTest() {
        assertEquals(Instant.ofEpochSecond(99999L), SigmaContractorFactory.INSTANT_SECONDS.previous(Instant.ofEpochSecond(100000L)));
    }

    @Test
    public void previousOverflowTest() {
        Instant current = Instant.MIN;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Instant has no previous value for value " + current + " (overflow detected).");
        SigmaContractorFactory.INSTANT_SECONDS.previous(current);
    }
    
    @Test
    public void nameTest()
    {
        assertEquals("instant_in_seconds", SigmaContractorFactory.INSTANT_SECONDS.name());
    }
    
    @Test
    public void hasPreviousMinTest()
    {
        assertTrue(SigmaContractorFactory.INSTANT_SECONDS.hasPrevious(SigmaContractorFactory.INSTANT_SECONDS.first().plusSeconds(1)));
        assertFalse(SigmaContractorFactory.INSTANT_SECONDS.hasPrevious(SigmaContractorFactory.INSTANT_SECONDS.first()));
    }
    
    @Test
    public void cardinalityOpenIntervalTest()
    {
        Instant start = Instant.ofEpochSecond(100000L);
        Instant end   = Instant.ofEpochSecond(100010L);
        assertEquals(9, SigmaContractorFactory.INSTANT_SECONDS.cardinality(new Interval<>(start, end)));
    }
    
    @Test
    public void cardinalityClosedIntervalTest()
    {
        Instant start = Instant.ofEpochSecond(100000L);
        Instant end   = Instant.ofEpochSecond(100010L);
        assertEquals(11, SigmaContractorFactory.INSTANT_SECONDS.cardinality(new Interval<>(start, end, false, false)));
    }
    
    @Test
    public void cardinalityNullTest()
    {
        Instant start = Instant.ofEpochSecond(100000L);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.INSTANT_SECONDS.cardinality(new Interval<>(start,null)));
    }
    
    @Test
    public void cardinalityFullSpanTest()
    {
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.INSTANT_SECONDS.cardinality(new Interval<>(Instant.MIN,Instant.MAX,false,false)));
    }
    
    @Test
    public void cardinalityRightMaxTest()
    {
        Instant start = Instant.ofEpochSecond(100000L);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.INSTANT_SECONDS.cardinality(new Interval<>(start,Instant.MAX)));
    }
    
    @Test
    public void cardinalityLeftMaxTest()
    {
        Instant end = Instant.ofEpochSecond(100000L);
        assertEquals(Long.MAX_VALUE, SigmaContractorFactory.INSTANT_SECONDS.cardinality(new Interval<>(Instant.MIN,end, true, false)));
    }

    @Test
    public void cardinalityZeroTest() {
        assertEquals(0, SigmaContractorFactory.INSTANT_SECONDS.cardinality(new Interval<>(Instant.ofEpochSecond(1000), Instant.ofEpochSecond(1000), true, false)));
    }
    
    @Test
    public void firstTest()
    {
        assertEquals(Instant.MIN.truncatedTo(SECONDS), SigmaContractorFactory.INSTANT_SECONDS.first());
    }
    
    @Test
    public void lastTest()
    {
        assertEquals(Instant.MAX.truncatedTo(SECONDS), SigmaContractorFactory.INSTANT_SECONDS.last());
    }
    
    @Test
    public void printConstantTest()
    {
        Instant d = Instant.ofEpochSecond(100000L);
        
        assertEquals("1970-01-02T03:46:40Z", SigmaContractorFactory.INSTANT_SECONDS.printConstant(d));
    }
    
    @Test
    public void parseConstantTest()
    {
        assertEquals(
            Instant.ofEpochSecond(100000L),
            SigmaContractorFactory
                .INSTANT_SECONDS
                .parseConstant("1970-01-02T03:46:40Z")
        );
    }
    
    @Test
    public void additionTest()
    {
        assertEquals(
            Instant.ofEpochSecond(100063L),
            SigmaContractorFactory
                .INSTANT_SECONDS
                .add(Instant.ofEpochSecond(100000L), 63));
    }   
    
    @Test
    public void additionNegativeTest()
    {
        assertEquals(
            Instant.ofEpochSecond(99937L),
            SigmaContractorFactory
                .INSTANT_SECONDS
                .add(Instant.ofEpochSecond(100000L), -63));
    }
    
    @Test
    public void additionNullTest()
    {
        exceptionRule.expect(NullPointerException.class);
        exceptionRule.expectMessage("Cannot add units to null value.");
        SigmaContractorFactory.INSTANT_SECONDS.add(null, 63);
    }   
    
    @Test
    public void additionOverflowRightTest()
    {
        Instant value = Instant.MAX.minusSeconds(5);
        int units = 6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Instant " + value + " with " + units + " seconds.");
        SigmaContractorFactory.INSTANT_SECONDS.add(value, units);
    } 
    
    @Test
    public void additionOverflowLeftTest()
    {
        Instant value = Instant.MIN.plusSeconds(5);
        int units = -6;
        exceptionRule.expect(SigmaContractException.class);
        exceptionRule.expectMessage("Overflow detected in addition of Instant " + value + " with " + units + " seconds.");
        SigmaContractorFactory.INSTANT_SECONDS.add(value, units);
    }
}
