package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;
import static org.junit.Assert.assertTrue;

public class SetOrdinalAtomTest {

    @Test
    public void setOrdinalAtomIsAlwaysFalseTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>());
        assertTrue(atom.isAlwaysFalse());

    }

    @Test
    public void setOrdinalAtomIsNotAlwaysFalseTest1() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>());
        assertFalse(atom.isAlwaysFalse());

    }

    @Test
    public void setOrdinalAtomIsNotAlwaysFalseTest2() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2)));
        assertFalse(atom.isAlwaysFalse());

    }


    @Test
    public void setOrdinalAtomIsAlwaysTrueTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>());
        assertTrue(atom.isAlwaysTrue());

    }

    @Test
    public void setOrdinalAtomIsNoyAlwaysTrueTest1() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>());
        assertFalse(atom.isAlwaysTrue());

    }

    @Test
    public void setOrdinalAtomIsNotAlwaysTrueTest2() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2)));
        assertFalse(atom.isAlwaysTrue());

    }

    @Test
    public void setOrdinalAtomContradictionSimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>());

        // Create expected simplified atom
        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        simplifiedAtoms.add(AbstractAtom.ALWAYS_FALSE);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomTautologySimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>());

        // Create expected simplified atom
        Set<AbstractAtom<?, ?, ?>> simplifiedAtoms = new HashSet<>();
        simplifiedAtoms.add(AbstractAtom.ALWAYS_TRUE);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomInSimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3)));

        // Create expected simplified atoms
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3)));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomNotInOneConstantSimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Collections.singleton(0)));

        // Create expected simplified atoms
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Collections.singleton(0)));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomNotInTwoConstantsBigGapSimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 3)));

        // Create expected simplified atoms
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 3)));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomNotInTwoConstantsSmallGapSimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 2)));

        // Create expected simplified atoms
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 2)));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomNotInTwoConstantsNoGapSimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 1)));

        // Create expected simplified atoms
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 1)));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomNotInMultipleConstantsSimplifyTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 1, 4, 6, 10)));

        // Create expected simplified atoms
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0, 1, 4, 6, 10)));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void setOrdinalAtomInSimplifiedTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3)));
        assertTrue(atom.isSimplified());

    }

    @Test
    public void setOrdinalAtomInNotSimplifiedTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>());
        assertFalse(atom.isSimplified());

    }

    @Test
    public void setOrdinalAtomNotInIsSimplifiedTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, new HashSet<>(Arrays.asList(1, 2, 3)));
        assertTrue(atom.isSimplified());

    }

    @Test
    public void setOrdinalAtomNotInIsNotSimplifiedTest() {

        // Create atom
        SetOrdinalAtom<Integer> atom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, new HashSet<>());
        assertFalse(atom.isSimplified());

    }


}
