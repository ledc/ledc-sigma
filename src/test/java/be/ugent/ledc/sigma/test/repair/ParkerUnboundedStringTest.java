package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.core.util.SetOperations;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.fd.PartialKey;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.repair.ParkerRepair;
import be.ugent.ledc.sigma.repair.cost.models.ParkerModel;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

public class ParkerUnboundedStringTest
{
    @Test
    public void stringUnboundTest() throws RepairException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        //In the flight dataset, departure and arrival info is truncated to minutes
        contractors.put("gate", SigmaContractorFactory.STRING);
        
        SigmaRuleset rules = new SigmaRuleset(contractors, new HashSet<>());
        
        SufficientSigmaRuleset sufficientRules = SufficientSigmaRuleset.create(rules, new FCFGenerator());
        
        PartialKey pk = new PartialKey(SetOperations.set("key"), SetOperations.set("gate"));
        
        ParkerRepair<?, ?> parker = new ParkerRepair<>(
            new ParkerModel<>(pk, sufficientRules),
            new RandomRepairSelection()
        );
        
        SimpleDataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .setString("key", "flight1")
            .setString("gate", "A1"));
        dirty.addDataObject(new DataObject()
            .setString("key", "flight1")
            .setString("gate", "A1"));
        dirty.addDataObject(new DataObject()
            .setString("key", "flight1")
            .setString("gate", "A2"));// <- Causes a violation of the key
        
        Dataset repair = parker.repair(dirty);

        assertEquals(3, repair.getSize());
        assertTrue(repair.stream().allMatch(o -> o.getString("gate").equals("A1")));
                
    }
}
