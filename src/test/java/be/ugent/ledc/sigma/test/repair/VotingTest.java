package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.ErrorFunction;
import be.ugent.ledc.core.cost.errormechanism.SwapError;
import be.ugent.ledc.core.cost.errormechanism.integer.IntegerErrors;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.LoCoRepair;
import be.ugent.ledc.sigma.repair.RepairEngine;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunctionWrapper;
import be.ugent.ledc.sigma.repair.cost.models.IterableCostModel;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * In this test, a full example on counting votes is treated, where we assume 
 * different types of typical errors that might occur.
 * We assume bills coming in from voting bureaus.
 * The counting bureau counts the bills and then divides them in piles per party.
 * We assume two parties p1 and p2 each with three candidates (p1a, p1b...)
 * One pile is reserved for blanks and/or illegal voting bills.
 * For each candidate, we count individual votes and for each party, we count the total
 * amount of bills with at least one vote for that candidate
 * @author abronsel
 */
public class VotingTest
{
    public static SufficientSigmaRuleset voteRules;
    
    @BeforeClass
    public static void init() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("bills", SigmaContractorFactory.INTEGER);
        contractors.put("p1", SigmaContractorFactory.INTEGER);
        contractors.put("p2", SigmaContractorFactory.INTEGER);
        contractors.put("blanks", SigmaContractorFactory.INTEGER);
        contractors.put("p1a", SigmaContractorFactory.INTEGER);
        contractors.put("p1b", SigmaContractorFactory.INTEGER);
        contractors.put("p1c", SigmaContractorFactory.INTEGER);
        contractors.put("p2a", SigmaContractorFactory.INTEGER);
        contractors.put("p2b", SigmaContractorFactory.INTEGER);
        contractors.put("p2c", SigmaContractorFactory.INTEGER);
        
        Set<SigmaRule> rules = Stream
            .of(
                //Domain constraints
                SigmaRuleParser.parseSigmaRule("NOT bills < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT bills > 100", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p1 < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p2 < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT blanks < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p1a < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p1b < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p1c < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p2a < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p2b < 0", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p2c < 0", contractors),
                //Interactions
                SigmaRuleParser.parseSigmaRule("NOT bills < blanks", contractors),
                SigmaRuleParser.parseSigmaRule("NOT bills < p1", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p1a > p1", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p1b > p1", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p1c > p1", contractors),
                SigmaRuleParser.parseSigmaRule("NOT bills < p2", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p2a > p2", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p2b > p2", contractors),
                SigmaRuleParser.parseSigmaRule("NOT p2c > p2", contractors)
            )
            .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);
        
        voteRules = SufficientSigmaRuleset.create(ruleset, new FCFGenerator());
    }
    
    @Test
    public void singleDigitErrorRepair() throws RepairException
    {
        Dataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("bills", 100)
            .set("blanks", 10)
            .set("p1", 30)
            .set("p1a", 50)
            .set("p1b", 40)
            .set("p1c", 10)
            .set("p2", 60)
            .set("p2a", 21)
            .set("p2b", 15)
            .set("p2c", 9)
        );
        
        assertFalse(voteRules.isSatisfied(dirty.getDataObjects().get(0)));
        
        //Build a cost model
        IterableCostModel costModel = new IterableCostModel();
        costModel
            .addCostFunction("bills", new IterableCostFunctionWrapper(new ErrorFunction<>(new SwapError<>("p1", "p2"))))
            .addCostFunction("p1", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("p2", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("blanks", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("p1a", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("p1b", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("p1c", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("p2a", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("p2b", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)))
            .addCostFunction("p2c", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        
        //Build a LoCo repair engine
        RepairEngine<?> repairEngine = new LoCoRepair(voteRules, costModel, new RandomRepairSelection());

        Dataset repair = repairEngine.repair(dirty);
        
        //Expect single object in the repair
        assertEquals(1, repair.getSize());
        
        DataObject o = dirty.getDataObjects().get(0);
        DataObject r = repair.getDataObjects().get(0);
        
        //Expect all attributes to be the same, but not p1
        assertEquals(o.inverseProject("p1"), r.inverseProject("p1"));
        
        Set<Integer> permitted = Stream.of(50,60,70,80,90).collect(Collectors.toSet());
        
        assertTrue(permitted.contains(r.getInteger("p1")));
    }
}
