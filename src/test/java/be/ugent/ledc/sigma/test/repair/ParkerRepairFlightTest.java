package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.CostFunction;
import be.ugent.ledc.core.cost.errormechanism.datetime.TemporalOffsetError;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.fd.PartialKey;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.ParkerRepair;
import be.ugent.ledc.sigma.repair.RepairEngine;
import be.ugent.ledc.core.cost.ErrorFunction;
import be.ugent.ledc.sigma.repair.bounding.RangedBounding;
import be.ugent.ledc.sigma.repair.cost.models.ParkerModel;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * In this test, we apply the Parker engine on some particular cases of the standard
 * 'flight' dataset. The original dataset can be found
 * <a href="http://lunadong.com/fusionDataSets.htm">here</a>
 * In our tests, all timestamps are converted to the same time zone and
 * all different date/time formats have been standardized to a single format to make sure
 * data could be modeled via timestamps rather than strings.
 * Note: the number of sources has been reduced in these test cases for the sake of simplicity
 *
 * @author abronsel
 */
public class ParkerRepairFlightTest
{
    public static RepairEngine<?> repairEngine;
    
    @BeforeClass
    public static void init() throws RepairException, ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        //In the flight dataset, departure and arrival info is truncated to minutes
        contractors.put("scheduled_departure", SigmaContractorFactory.DATETIME_MINUTES);
        contractors.put("scheduled_arrival", SigmaContractorFactory.DATETIME_MINUTES);
        contractors.put("actual_departure", SigmaContractorFactory.DATETIME_MINUTES);
        contractors.put("actual_arrival", SigmaContractorFactory.DATETIME_MINUTES);

        Set<SigmaRule> rules = Stream
            .of(
                //Interactions
                SigmaRuleParser.parseSigmaRule("NOT scheduled_departure >= scheduled_arrival", contractors),
                SigmaRuleParser.parseSigmaRule("NOT actual_departure >= actual_arrival", contractors),
                SigmaRuleParser.parseSigmaRule("NOT actual_departure >= S^720(scheduled_departure)", contractors),
                SigmaRuleParser.parseSigmaRule("NOT actual_departure <= S^-720(scheduled_departure)", contractors)
            )
            .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);
        
        SufficientSigmaRuleset flightRules = SufficientSigmaRuleset.create(ruleset, new FCFGenerator());

        Map<String, CostFunction<?>> costFunctions = new HashMap<>();

        //Cost for non-explainable values is fixed to 3
        //Original null values are imputed with fixed cost 1
        ErrorFunction<LocalDateTime> costFuction = new ErrorFunction<>(
            3,
            new TemporalOffsetError(ChronoUnit.DAYS, 1)
        );

        costFunctions.put("scheduled_departure", costFuction);
        costFunctions.put("scheduled_arrival", costFuction);
        costFunctions.put("actual_departure", costFuction);
        costFunctions.put("actual_arrival", costFuction);

        PartialKey partialkey = new PartialKey
        (
            Stream.of("date_collected", "flight_number").collect(Collectors.toSet()),
            Stream.of(
                "scheduled_departure",
                "actual_departure",
                "scheduled_arrival",
                "actual_arrival"
            ).collect(Collectors.toSet())
        );
        
        ParkerModel<?> costModel = new ParkerModel<>(
            partialkey,
            costFunctions,
            flightRules,
            new RangedBounding(1440),
            false,
            true
        );
        
        repairEngine = new ParkerRepair<>(
            costModel,
            new RandomRepairSelection());
    }

    /**
     * Test case for flight AA-3823-LAX-DEN collected at 2011-12-31
     */
    @Test
    public void flightCase1() throws RepairException
    {
        SimpleDataset dirty = new SimpleDataset();
        
        DataObject key = new DataObject()
            .set("flight_number", "AA-3823-LAX-DEN")
            .setDate("date_collected", LocalDate.of(2011, 12, 31));
        
        //Add object 1
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 1, 5, 0))
                .setDateTime("actual_departure", LocalDateTime.of(2012, 1, 1, 5, 0))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2011, 12, 31, 7, 15))
                .setDateTime("actual_arrival", LocalDateTime.of(2011, 12, 31, 7, 15))
            ));
        
        //Add object 2
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 1, 5, 0))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 1, 7, 15))
            ));
        
        //Add object 3
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_arrival", LocalDateTime.of(2011, 12, 31, 7, 15))
            ));
        
        //Add object 4
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 1, 5, 0))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 1, 6, 46))
            ));
        
        //Add object 5
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 1, 5, 0))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2011, 12, 31, 7, 15))
            ));
        
        
        Dataset repair = repairEngine.repair(dirty);
        
        //We expect five objects in the repair
        assertEquals(5, repair.getSize());
        
        //We expect all objects to be the same
        assertEquals(1L, repair.getDataObjects().stream().distinct().count());
        
        DataObject expected = key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 1, 5, 0))
                .setDateTime("actual_departure", LocalDateTime.of(2012, 1, 1, 5, 0))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 1, 7, 15))
                .setDateTime("actual_arrival", LocalDateTime.of(2012, 1, 1, 7, 15))
            );
        
        //The single dataobject should be the one we expected and has minimal cost
        assertEquals(expected, repair.getDataObjects().get(0));
    }
    

    /**
     * Test case for flight AA-3715-ORD-PHL collected at 2012-01-02
     */
    @Test
    public void flightCase2() throws RepairException
    {
        SimpleDataset dirty = new SimpleDataset();
        
        DataObject key = new DataObject()
            .set("flight_number", "AA-3715-ORD-PHL")
            .setDate("date_collected", LocalDate.of(2012, 1, 2));
        
        //Add object 1
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 3, 4, 30))
                .setDateTime("actual_departure", LocalDateTime.of(2012, 1, 3, 4, 46))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 2, 6, 3))
                .setDateTime("actual_arrival", LocalDateTime.of(2012, 1, 2, 6, 19))
            ));

        //Add object 2
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 3, 4, 30))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 2, 6, 15))
            ));
        
        //Add object 3
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("actual_departure", LocalDateTime.of(2012, 1, 3, 4, 46))
                .setDateTime("actual_arrival", LocalDateTime.of(2012, 1, 3, 6, 19))
            ));
        
        //Add object 4
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("actual_departure", LocalDateTime.of(2012, 1, 3, 4, 46))
            ));
        
        //Add object 5
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 3, 4, 30))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 3, 6, 15))
            ));
        
        //Add object 6
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 3, 4, 30))
                .setDateTime("actual_departure", LocalDateTime.of(2012, 1, 3, 4, 46))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 2, 6, 15))
                .setDateTime("actual_arrival", LocalDateTime.of(2012, 1, 2, 6, 19))
            ));
        
        Dataset repair = repairEngine.repair(dirty);
        
        //We expect five objects in the repair
        assertEquals(6, repair.getSize());
        
        //We expect all objects to be the same
        assertEquals(1L, repair.getDataObjects().stream().distinct().count());
        
        DataObject expected = key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2012, 1, 3, 4, 30))
                .setDateTime("actual_departure", LocalDateTime.of(2012, 1, 3, 4, 46))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2012, 1, 3, 6, 15))
                .setDateTime("actual_arrival", LocalDateTime.of(2012, 1, 3, 6, 19))
            );
        
        //The single dataobject should be the one we expected and has minimal cost
        assertEquals(expected, repair.getDataObjects().get(0));
        
        int totalCost = 0;
        
        //Compute cost
        for(int i=0; i<dirty.getSize(); i++)
        {
            int rowCost = repairEngine
                .getCostModel()
                .cost(
                    dirty.getDataObjects().get(i),
                    repair.getDataObjects().get(i));
            
            totalCost += rowCost;
        }
        
        assertEquals(34, totalCost);
    }
    

    /**
     * Test case for flight AA-1184-DFW-IAH collected at 2011-12-16
     */
    @Test
    public void flightCase3NullValues() throws RepairException
    {
        SimpleDataset dirty = new SimpleDataset();
        
        DataObject key = new DataObject()
            .set("flight_number", "AA-1184-DFW-IAH")
            .setDate("date_collected", LocalDate.of(2011, 12, 16));
        
        //Add object 1
        dirty
        .addDataObject(key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2011, 12, 17, 0, 15))
                .setDateTime("scheduled_arrival", LocalDateTime.of(2011, 12, 17, 1, 20))
            ));
        
        Dataset repair = repairEngine.repair(dirty);
        
        //We expect five objects in the repair
        assertEquals(1, repair.getSize());
        
        DataObject expected = key
            .concat(new DataObject()
                .setDateTime("scheduled_departure", LocalDateTime.of(2011, 12, 17, 0, 15))
                .setDateTime("actual_departure", null)
                .setDateTime("scheduled_arrival", LocalDateTime.of(2011, 12, 17, 1, 20))
                .setDateTime("actual_arrival", null)
            );
        
        //The single dataobject should be the one we expected and has minimal cost
        assertEquals(expected, repair.getDataObjects().get(0));
        
        int totalCost = 0;
        
        //Compute cost
        for(int i=0; i<dirty.getSize(); i++)
        {
            int rowCost = repairEngine
                .getCostModel()
                .cost(
                    dirty.getDataObjects().get(i),
                    repair.getDataObjects().get(i));
            
            totalCost += rowCost;
        }
        
        assertEquals(6, totalCost);
    }
}
