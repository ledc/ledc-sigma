package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.sigma.datastructures.atoms.ConstantOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.atoms.SetOrdinalAtom;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import org.junit.Test;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ConstantOrdinalAtomTest {

    @Test
    public void constantOrdinalAtomGreaterThanSimplifyTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, 3);

        // Create expected simplified atom
        Set<ConstantOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> simplifiedAtom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, 4);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantOrdinalAtomLowerThanSimplifyTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, 3);

        // Create expected simplified atom
        Set<ConstantOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> simplifiedAtom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, 2);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantOrdinalAtomNotEqualSimplifyTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.NEQ, 3);

        // Create simplified atom
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.NOTIN, Collections.singleton(3));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantOrdinalAtomEqualSimplifyTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.EQ, 3);

        // Create simplified atom
        Set<SetOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        SetOrdinalAtom<Integer> simplifiedAtom = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", SetOperator.IN, Collections.singleton(3));
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantOrdinalAtomLowerThanOrEqualSimplifyTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, 3);

        // Create simplified atom
        Set<ConstantOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> simplifiedAtom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, 3);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantOrdinalAtomGreaterThanOrEqualSimplifyTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, 3);

        // Create simplified atom
        Set<ConstantOrdinalAtom<Integer>> simplifiedAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> simplifiedAtom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, 3);
        simplifiedAtoms.add(simplifiedAtom);

        assertEquals(simplifiedAtoms, atom.simplify());

    }

    @Test
    public void constantOrdinalAtomGreaterThanIsSimplifiedTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GT, 3);
        assertFalse(atom.isSimplified());

    }

    @Test
    public void constantOrdinalAtomLowerThanIsSimplifiedTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LT, 3);
        assertFalse(atom.isSimplified());

    }

    @Test
    public void constantOrdinalAtomEqualIsSimplifiedTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.EQ, 3);
        assertFalse(atom.isSimplified());

    }

    @Test
    public void constantOrdinalAtomNotEqualIsSimplifiedTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", EqualityOperator.NEQ, 3);
        assertFalse(atom.isSimplified());

    }

    @Test
    public void constantOrdinalAtomLowerThanOrEqualIsSimplifiedTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.LEQ, 3);
        assertTrue(atom.isSimplified());

    }

    @Test
    public void constantOrdinalAtomGreaterThanOrEqualIsSimplifiedTest() {

        // Create atom
        ConstantOrdinalAtom<Integer> atom = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "A", InequalityOperator.GEQ, 3);
        assertTrue(atom.isSimplified());

    }


}
