package be.ugent.ledc.sigma.test.cpfs;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.io.CPFParser;
import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class CPFTest
{

    @Test
    public void getAttributesTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^1(b) & c == '1' & d == 3 & a notin {1,2}", contractors);

        Set<String> attributes = new HashSet<>(Arrays.asList("a", "b", "c", "d"));

        assertEquals(attributes, cpf.getAttributes());
    }

    @Test
    public void hasAttributeTest()  throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^1(b) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertTrue(cpf.hasAttribute("a"));
    }

    @Test
    public void notHasAttributeTest()  throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^1(b) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertFalse(cpf.hasAttribute("e"));
    }

    @Test
    public void isContradictionTest()  throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a > S^1(a) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertTrue(cpf.isContradiction());

    }

    @Test
    public void isNoContradictionTest()  throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^1(a) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertFalse(cpf.isContradiction());

    }

    @Test
    public void isTautologyTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^1(a)", contractors);

        assertTrue(cpf.isTautology());

    }

    @Test
    public void isNoTautologyTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a > S^1(a) & c == '1' & d == 3 & a notin {1,2}", contractors);

        assertFalse(cpf.isTautology());

    }

    @Test
    public void simplifyTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a != S^1(b) & c IN {'1','2'} & d < 4", contractors);

        CPF simplifiedCPF1 = CPFParser.parseCPF("a < S^1(b) & c IN {'1','2'} & d <= 3", contractors);
        CPF simplifiedCPF2 = CPFParser.parseCPF("a > S^1(b) & c IN {'1','2'} & d <= 3", contractors);

        Set<CPF> simplifiedCPFs = new HashSet<>(Arrays.asList(simplifiedCPF1, simplifiedCPF2));

        assertEquals(simplifiedCPFs, cpf.simplify());

    }

    @Test
    public void satisfactionConstantAtomsTest() throws ParseException {
        DataObject o = new DataObject()
            .setString("a", "x")
            .setInteger("b", 5);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a == 'x' & b >= 0 & b <= 10", contractors);

        Assert.assertTrue(cpf.isSatisfied(o));
    }
    
    @Test
    public void satisfactionTestConstantAtomsNull() throws ParseException {

        DataObject o = new DataObject()
            .setString("a", "x")
            .setInteger("b", null);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a == 'x' & b >= 0 & b <= 10", contractors);

        Assert.assertFalse(cpf.isSatisfied(o));
    }
    
    @Test
    public void satisfactionSetAtomsTest() throws ParseException {

        DataObject o = new DataObject()
            .setString("a", "y")
            .setInteger("b", 5);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a in {'x', 'y'} & b notin {10,11,15,19}", contractors);

        Assert.assertTrue(cpf.isSatisfied(o));
    }
    
    @Test
    public void satisfactionVariableAtomsTest() throws ParseException{

        DataObject o = new DataObject()
            .setInteger("a", 3)
            .setInteger("b", 5)
            .setInteger("c", 8);
        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a <= b & b < S^5(c)", contractors);

        Assert.assertTrue(cpf.isSatisfied(o));
    }
    
    @Test
    public void fixVariableAtomsTest() throws ParseException 
    {
        DataObject o = new DataObject()
            .setInteger("b", 5)
            .setInteger("d", 8);

        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);
        contractors.put("d", SigmaContractorFactory.INTEGER);
        
        //The rule
        CPF cpf = CPFParser.parseCPF("a <= b & b < S^5(c) & d == S^3(b)", contractors);
        
        //The expected fix
        CPF expectedFix = CPFParser.parseCPF("a <= 5 & c > 0", contractors);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_TRUE);
        
        CPF fixed = cpf.fix(o);
        
        assertEquals(expectedFix, fixed);
    }
    
    @Test
    public void fixConstantAtomsTest() throws ParseException 
    {
        DataObject o = new DataObject()
            .setInteger("b", 5);

        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("b <= 5 & c == 5", contractors);
        CPF expectedFix = CPFParser.parseCPF("c == 5", contractors);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_TRUE);

        CPF fixed = cpf.fix(o);
        
        assertEquals(expectedFix, fixed);
    }
    
    @Test
    public void fixSetAtomsTest() throws ParseException 
    {
        DataObject o = new DataObject()
            .setInteger("b", 5)
            .setInteger("c", 10);

        
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a in {1,2,3} & b in {5,7,9} & c notin {11,10}", contractors);
        CPF expectedFix = CPFParser.parseCPF("a in {1,2,3}", contractors);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_TRUE);
        expectedFix.getAtoms().add(AbstractAtom.ALWAYS_FALSE);
        
        CPF fixed = cpf.fix(o);
        
        assertEquals(expectedFix, fixed);
    }

    @Test
    public void getConstantOrdinalAtomsTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a != S^1(b) & c == '1' & d <= 3", contractors);

        Set<ConstantOrdinalAtom<?>> constantOrdinalAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> atom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", InequalityOperator.LEQ, 3);
        constantOrdinalAtoms.add(atom1);

        assertEquals(constantOrdinalAtoms, cpf.getConstantOrdinalAtoms());

    }

    @Test
    public void getConstantAtomsTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a != S^1(b) & c == '1' & d <= 3", contractors);

        Set<ConstantAtom<?, ?, ?>> constantAtoms = new HashSet<>();
        ConstantOrdinalAtom<Integer> atom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "d", InequalityOperator.LEQ, 3);
        ConstantNominalAtom<String> atom2 = new ConstantNominalAtom<>(SigmaContractorFactory.STRING, "c", EqualityOperator.EQ, "1");
        constantAtoms.add(atom1);
        constantAtoms.add(atom2);

        assertEquals(constantAtoms, cpf.getConstantAtoms());

    }

    @Test
    public void getVariableAtomsTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a != S^1(b) & c == '1' & d <= 3", contractors);

        Set<VariableAtom<?, ?, ?>> variableAtoms = new HashSet<>();
        VariableOrdinalAtom<Integer> atom1 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.NEQ, "b", 1);
        variableAtoms.add(atom1);

        assertEquals(variableAtoms, cpf.getVariableAtoms());

    }

    @Test
    public void getSetAtomsTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a in {'1', '2'} & b in {3} & c == '1' & d <= 3", contractors);

        Set<SetAtom<?, ?>> setAtoms = new HashSet<>();
        SetNominalAtom<String> atom1 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2")));
        SetOrdinalAtom<Integer> atom2 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "b", SetOperator.IN, new HashSet<>(List.of(3)));
        setAtoms.add(atom1);
        setAtoms.add(atom2);

        assertEquals(setAtoms, cpf.getSetAtoms());

    }

    @Test
    public void getSetNominalAtomsTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a in {'1', '2'} & b in {3} & c == '1' & d <= 3", contractors);

        Set<SetAtom<?, ?>> setNominalAtoms = new HashSet<>();
        SetNominalAtom<String> atom1 = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(Arrays.asList("1", "2")));
        setNominalAtoms.add(atom1);

        assertEquals(setNominalAtoms, cpf.getSetNominalAtoms());

    }

}
