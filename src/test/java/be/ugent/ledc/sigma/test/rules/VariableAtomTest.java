package be.ugent.ledc.sigma.test.rules;

import be.ugent.ledc.core.DataException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.NominalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.OrdinalContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.operators.ComparableOperator;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

public class VariableAtomTest
{
    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void variableAtomEqualsTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", "2")
            .setString("b", "2");
        
        //Create an atom
        VariableAtom<String, NominalContractor<String>, EqualityOperator> atom = new VariableNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.EQ,
            "b"
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void variableAtomEqualsTestNull()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", "2")
            .setString("b", null);
        
        //Create an atom
        VariableAtom<String, NominalContractor<String>, EqualityOperator> atom = new VariableNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.EQ,
            "b"
        );

        assertFalse(atom.test(o));
    }
    
    @Test
    public void variableAtomNotEqualsTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setString("a", "2")
            .setString("b", "3");
        
        //Create an atom
        VariableAtom<String, NominalContractor<String>, EqualityOperator> atom = new VariableNominalAtom<>(
            SigmaContractorFactory.STRING,
            "a",
            EqualityOperator.NEQ,
            "b"
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void variableAtomLowerThanTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 2)
            .setInteger("b", 3);
        
        //Create an atom
        VariableAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> atom = new VariableOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LT,
            "b"
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void variableAtomLowerThanWithOffsetTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 2)
            .setInteger("b", 5);
        
        //Create an atom
        VariableAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> atom = new VariableOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LT,
            "b",
            -3
        );

        assertFalse(atom.test(o));
    }
    
    @Test
    public void variableAtomLowerThanEqualWithOffsetTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 2)
            .setInteger("b", 5);
        
        //Create an atom
        VariableAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> atom = new VariableOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.LEQ,
            "b",
            -3
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void variableAtomGreaterThanTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 4)
            .setInteger("b", 3);
        
        //Create an atom
        VariableAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> atom = new VariableOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GT,
            "b"
        );

        assertTrue(atom.test(o));
    }
    
    @Test
    public void variableAtomGreaterThanOffsetTest()
    {
        //Create an object
        DataObject o = new DataObject()
            .setInteger("a", 2)
            .setInteger("b", 3);
        
        //Create an atom
        VariableAtom<Integer, OrdinalContractor<Integer>, ComparableOperator> atom = new VariableOrdinalAtom<>(
            SigmaContractorFactory.INTEGER,
            "a",
            InequalityOperator.GT,
            "b",
            -2
        );

        assertTrue(atom.test(o));
    }

    @Test
    public void variableAtomFailingContractTest() {
        //Create an object
        DataObject o = new DataObject()
                .setBoolean("a", true)
                .setInteger("b", 1);

        //Create an atom
        VariableOrdinalAtom<Integer> atom = new VariableOrdinalAtom<>(
                SigmaContractorFactory.INTEGER,
                "a",
                InequalityOperator.LT,
                "b"
        );

        exceptionRule.expect(DataException.class);
        exceptionRule.expectMessage("Passed attribute value " + o.get("a") + " or " + o.get("b") + " does not fulfill contract specified by " + atom.getContractor());

        atom.test(o);

    }
}
