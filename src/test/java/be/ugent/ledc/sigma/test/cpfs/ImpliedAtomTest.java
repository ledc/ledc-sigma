package be.ugent.ledc.sigma.test.cpfs;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.sigma.datastructures.atoms.*;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.formulas.CPFException;
import be.ugent.ledc.sigma.datastructures.formulas.CPFImplicator;
import be.ugent.ledc.sigma.datastructures.operators.EqualityOperator;
import be.ugent.ledc.sigma.datastructures.operators.InequalityOperator;
import be.ugent.ledc.sigma.datastructures.operators.SetOperator;
import be.ugent.ledc.sigma.io.CPFParser;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.*;

import static org.junit.Assert.*;

public class ImpliedAtomTest {

    @Rule
    public ExpectedException exceptionRule = ExpectedException.none();

    @Test
    public void noMatchingContractorTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^1(b) & c == '1' & d <= 3", contractors);

        SetNominalAtom<String> impliedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(List.of("1")));

        exceptionRule.expect(CPFException.class);
        exceptionRule.expectMessage("Contractor of " + impliedAtom + " should be equal to contractor of " + cpf + " for same attributes.");

        CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom);

    }

    @Test
    public void noEqualAttributesTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < S^1(b) & c == '1' & d <= 3", contractors);

        SetNominalAtom<String> impliedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "e", SetOperator.IN, new HashSet<>(List.of("1")));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom));

    }

    @Test
    public void atomIsImpliedByContradictionTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.STRING);
        contractors.put("d", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a < b & a > b & c == '1' & d <= 3", contractors);

        AbstractAtom<?, ?, ?> impliedAtom1 = AbstractAtom.ALWAYS_TRUE;
        VariableOrdinalAtom<Integer> impliedAtom2 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b");

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom1));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom2));

    }

    @Test
    public void atomIsImpliedByTautologyTest() throws ParseException  {

        CPF cpf = new CPF(Collections.singleton(AbstractAtom.ALWAYS_TRUE));

        AbstractAtom<?, ?, ?> impliedAtom1 = AbstractAtom.ALWAYS_TRUE;
        VariableOrdinalAtom<Integer> impliedAtom2 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b");

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom1));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom2));

    }

    @Test
    public void variableIsImpliedByVariableEmptySetTest() throws ParseException  {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        CPF cpf = CPFParser.parseCPF("a IN {'1','2'} & b IN {'2','3'}", contractors);
        VariableNominalAtom<String> impliedAtom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom));

    }

    @Test
    public void variableIsImpliedByVariableNominalTest() throws ParseException  {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);
        contractors.put("c", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a != b & b == c & a != c", contractors);
        CPF cpf2 = CPFParser.parseCPF("a == b & b == c & a == c", contractors);

        VariableNominalAtom<String> impliedAtom11 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "c");
        VariableNominalAtom<String> impliedAtom12 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        VariableNominalAtom<String> impliedAtom21 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "c");
        VariableNominalAtom<String> impliedAtom22 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");
        VariableNominalAtom<String> impliedAtom23 = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "b", EqualityOperator.EQ, "c");

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom11));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom12));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom21));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom22));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom23));

    }

    @Test
    public void variableIsImpliedByVariableOrdinalTest() throws ParseException{

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);
        contractors.put("c", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a < S^1(b) & b < S^2(c) & a < S^3(c)", contractors);
        CPF cpf2 = CPFParser.parseCPF("a == S^1(b)", contractors);

        VariableOrdinalAtom<Integer> impliedAtom1 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "c", 3);
        VariableOrdinalAtom<Integer> impliedAtom2 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 2);
        VariableOrdinalAtom<Integer> impliedAtom3 = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b");

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom1));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom2));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom3));

    }

    @Test
    public void variableIsImpliedByNonVariableOrdinalLowerThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a IN {0,1}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a < S^1(b) & a IN {0,1} & b >= 1 & b <= 10 & b NOTIN {3,5}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a < S^1(b) & a <= 4 & a >= 0 & b >= 4 & b <= 7", contractors);

        VariableOrdinalAtom<Integer> impliedAtom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LT, "b", 1);

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom));

    }

    @Test
    public void variableIsImpliedByNonVariableGreaterThanTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a IN {0,1}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a > S^1(b) & a >= 2 & b == 0", contractors);
        CPF cpf3 = CPFParser.parseCPF("a > S^1(b) & a <= 7 & a >= 6 & b <= 4 & b >= 0", contractors);

        VariableOrdinalAtom<Integer> impliedAtom = new VariableOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GT, "b", 1);

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom));

    }

    @Test
    public void variableIsImpliedBySetNominalEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        CPF cpf = CPFParser.parseCPF("a == b & a == '2' & b == '2'", contractors);

        VariableNominalAtom<String> impliedAtom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.EQ, "b");

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom));

    }

    @Test
    public void variableIsImpliedByConstantNominalNotEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        CPF cpf1 = CPFParser.parseCPF("a != b & a IN {'2', '3'} & b NOTIN {'1', '2', '3'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a != b & b IN {'2', '3'} & a NOTIN {'1', '2', '3'}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a != b & a IN {'2','3','4'} & b == '1'", contractors);

        VariableNominalAtom<String> impliedAtom = new VariableNominalAtom<>(SigmaContractorFactory.STRING, "a", EqualityOperator.NEQ, "b");

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom));

    }

    @Test
    public void constantOrdinalIsImpliedByNonVariableOrdinalTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a <= 5 & a >= 0", contractors);
        CPF cpf2 = CPFParser.parseCPF("a >= 0", contractors);
        CPF cpf3 = CPFParser.parseCPF("a <= 5", contractors);
        CPF cpf4 = CPFParser.parseCPF("a IN {1,2,4,7}", contractors);
        CPF cpf5 = CPFParser.parseCPF("a NOTIN {1,2,4,7}", contractors);
        CPF cpf6 = CPFParser.parseCPF("a >= 0 & a <= 10 & a NOTIN {1,2,4,7}", contractors);
        CPF cpf7 = CPFParser.parseCPF("a >= 5 & a <= 0", contractors);

        ConstantOrdinalAtom<Integer> impliedAtom11 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom12 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 0);
        ConstantOrdinalAtom<Integer> impliedAtom13 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);

        ConstantOrdinalAtom<Integer> impliedAtom21 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom22 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, -1);
        ConstantOrdinalAtom<Integer> impliedAtom23 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);

        ConstantOrdinalAtom<Integer> impliedAtom31 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom32 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, -1);
        ConstantOrdinalAtom<Integer> impliedAtom33 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 2);

        ConstantOrdinalAtom<Integer> impliedAtom41 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom42 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 0);
        ConstantOrdinalAtom<Integer> impliedAtom43 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 2);

        ConstantOrdinalAtom<Integer> impliedAtom51 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom52 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 0);
        ConstantOrdinalAtom<Integer> impliedAtom53 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 7);

        ConstantOrdinalAtom<Integer> impliedAtom61 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 7);
        ConstantOrdinalAtom<Integer> impliedAtom62 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 0);
        ConstantOrdinalAtom<Integer> impliedAtom63 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 15);

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom11));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom12));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom13));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom21));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom22));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom23));

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom31));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom32));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom33));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf4, impliedAtom41));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf4, impliedAtom42));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf4, impliedAtom43));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf5, impliedAtom51));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf5, impliedAtom52));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf5, impliedAtom53));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf6, impliedAtom61));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf6, impliedAtom62));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf6, impliedAtom63));

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom11));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom12));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom13));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom21));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom22));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom23));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom31));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom32));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom33));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom41));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom42));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom43));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom51));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom52));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom53));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom61));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom62));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf7, impliedAtom63));

    }

    @Test
    public void constantOrdinalIsImpliedByNonVariableOrdinalLowerThanOrEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a <= 5 & a >= 0", contractors);
        ConstantOrdinalAtom<Integer> impliedAtom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom2 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom3 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom5 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom6 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom7 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom8 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom9 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom1));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom2));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom3));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom4));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom5));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom6));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom7));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom8));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom9));

    }

    @Test
    public void constantOrdinalIsImpliedByConstantOrdinalEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a == 5", contractors);
        ConstantOrdinalAtom<Integer> impliedAtom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom2 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom3 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom5 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom6 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom7 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom8 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom9 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom1));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom2));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom3));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom4));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom5));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom6));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom7));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom8));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom9));

    }

    @Test
    public void constantOrdinalIsImpliedByConstantOrdinalGreaterThanOrEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a >= 5 & a <= 10", contractors);
        ConstantOrdinalAtom<Integer> impliedAtom1 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom2 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom3 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.LEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom4 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom5 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom6 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", InequalityOperator.GEQ, 6);
        ConstantOrdinalAtom<Integer> impliedAtom7 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 4);
        ConstantOrdinalAtom<Integer> impliedAtom8 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 5);
        ConstantOrdinalAtom<Integer> impliedAtom9 = new ConstantOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", EqualityOperator.EQ, 6);

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom1));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom2));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom3));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom4));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom5));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom6));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom7));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom8));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf, impliedAtom9));

    }

    @Test
    public void setOrdinalIsImpliedByNonVariableOrdinalTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.INTEGER);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf1 = CPFParser.parseCPF("a <= 5 & a >= 0", contractors);
        CPF cpf2 = CPFParser.parseCPF("a >= 0", contractors);
        CPF cpf3 = CPFParser.parseCPF("a <= 5", contractors);

        SetOrdinalAtom<Integer> impliedAtom11 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(-1,0,3,4,5,6,7)));
        SetOrdinalAtom<Integer> impliedAtom12 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(0,1,2,3,4,5,6,7)));
        SetOrdinalAtom<Integer> impliedAtom13 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList(0,1,2,3,4,5,6,7)));
        SetOrdinalAtom<Integer> impliedAtom14 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList(-1,6,7)));

        SetOrdinalAtom<Integer> impliedAtom21 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(-2,-1,0,3,4,5,6,7)));
        SetOrdinalAtom<Integer> impliedAtom22 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(2,3,4,5,6,7)));
        SetOrdinalAtom<Integer> impliedAtom23 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList(-5,-4,-1)));
        SetOrdinalAtom<Integer> impliedAtom24 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList(-1,6,7)));

        SetOrdinalAtom<Integer> impliedAtom31 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(3,4,5,8,9,12)));
        SetOrdinalAtom<Integer> impliedAtom32 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.IN, new HashSet<>(Arrays.asList(-1,3,4)));
        SetOrdinalAtom<Integer> impliedAtom33 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList(6,9,10)));
        SetOrdinalAtom<Integer> impliedAtom34 = new SetOrdinalAtom<>(SigmaContractorFactory.INTEGER, "a", SetOperator.NOTIN, new HashSet<>(Arrays.asList(-1,6,7)));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom11));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom12));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom13));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom14));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom21));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom22));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom23));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom24));

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom31));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom32));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom33));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom34));

    }

    @Test
    public void setNominalIsImpliedBySetNominalEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        SetNominalAtom<String> impliedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.IN, new HashSet<>(List.of("0", "1")));

        CPF cpf1 = CPFParser.parseCPF("a IN {'0'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a NOTIN {'0'}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a NOTIN {'1'}", contractors);

        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom));

    }

    @Test
    public void setNominalIsImpliedBySetNominalNotEqualTest() throws ParseException {

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.STRING);

        SetNominalAtom<String> impliedAtom = new SetNominalAtom<>(SigmaContractorFactory.STRING, "a", SetOperator.NOTIN, new HashSet<>(List.of("0", "1")));

        CPF cpf1 = CPFParser.parseCPF("a IN {'0', '1'}", contractors);
        CPF cpf2 = CPFParser.parseCPF("a NOTIN {'0', '1'}", contractors);
        CPF cpf3 = CPFParser.parseCPF("a NOTIN {'1'}", contractors);

        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf1, impliedAtom));
        assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf2, impliedAtom));
        assertFalse(CPFImplicator.atomIsImpliedByCPF(cpf3, impliedAtom));

    }

    @Test
    public void atomImplicationTest() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();

        contractors.put("a", SigmaContractorFactory.STRING);
        contractors.put("b", SigmaContractorFactory.INTEGER);

        CPF cpf = CPFParser.parseCPF("a IN {'x'} & b >= 0 & b <= 10", contractors);

        AbstractAtom<?, ?, ?> atom = ConstantAtom.parseConstantAtom("b", InequalityOperator.GEQ, "-2", SigmaContractorFactory.INTEGER);

        Assert.assertTrue(CPFImplicator.atomIsImpliedByCPF(cpf, atom));
    }

}
