package be.ugent.ledc.sigma.test.repair.cpf;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.cost.ErrorFunction;
import be.ugent.ledc.core.cost.errormechanism.SwapError;
import be.ugent.ledc.core.cost.errormechanism.integer.IntegerErrors;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.formulas.CPF;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.io.CPFParser;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.bounding.RangedBounding;
import be.ugent.ledc.sigma.repair.cost.functions.DistanceCostFunction;
import be.ugent.ledc.sigma.repair.cost.functions.IterableCostFunctionWrapper;
import be.ugent.ledc.sigma.repair.cost.functions.SimpleIterableCostFunction;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;
import be.ugent.ledc.sigma.repair.cost.models.NonConstantCostModel;
import be.ugent.ledc.sigma.repair.experimental.CPFRandomRepairSelection;
import be.ugent.ledc.sigma.repair.experimental.ConstantCostRepairEngine;
import be.ugent.ledc.sigma.repair.experimental.NonConstantCostRepairEngine;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class NonConstantCostRepairTest {

    private static Map<String, SigmaContractor<?>> contractors12;
    private static Map<String, SigmaContractor<?>> contractors3;

    private static SigmaRuleset ruleset1;
    private static SigmaRuleset ruleset2;
    private static SigmaRuleset ruleset3;

    @BeforeClass
    public static void init() throws ParseException {

        contractors12 = new HashMap<>();
        contractors12.put("a", SigmaContractorFactory.INTEGER);
        contractors12.put("b", SigmaContractorFactory.INTEGER);
        contractors12.put("c", SigmaContractorFactory.INTEGER);

        ruleset1 = new SigmaRuleset(contractors12, Set.of(
                SigmaRuleParser.parseSigmaRule("a IN {1,2,3,4}", contractors12),
                SigmaRuleParser.parseSigmaRule("b IN {1,2,3,4}", contractors12),
                SigmaRuleParser.parseSigmaRule("c IN {1,2,3,4}", contractors12),
                SigmaRuleParser.parseSigmaRule("NOT a == 1 & b in {2,3}", contractors12),
                SigmaRuleParser.parseSigmaRule("NOT a == 2 & c == 3", contractors12)
        ));

        ruleset2 = new SigmaRuleset(contractors12, Set.of(
                SigmaRuleParser.parseSigmaRule("NOT a IN {1,2} & b == 2", contractors12),
                SigmaRuleParser.parseSigmaRule("NOT a == 3 & c == 3", contractors12),
                SigmaRuleParser.parseSigmaRule("NOT a < c", contractors12)
        ));

        contractors3 = new HashMap<>();
        contractors3.put("bills", SigmaContractorFactory.INTEGER);
        contractors3.put("p1", SigmaContractorFactory.INTEGER);
        contractors3.put("p2", SigmaContractorFactory.INTEGER);
        contractors3.put("blanks", SigmaContractorFactory.INTEGER);
        contractors3.put("p1a", SigmaContractorFactory.INTEGER);
        contractors3.put("p1b", SigmaContractorFactory.INTEGER);
        contractors3.put("p1c", SigmaContractorFactory.INTEGER);
        contractors3.put("p2a", SigmaContractorFactory.INTEGER);
        contractors3.put("p2b", SigmaContractorFactory.INTEGER);
        contractors3.put("p2c", SigmaContractorFactory.INTEGER);

        ruleset3 = new SigmaRuleset(contractors3, Set.of(
                SigmaRuleParser.parseSigmaRule("bills >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("bills <= 100", contractors3),
                SigmaRuleParser.parseSigmaRule("p1 >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("p2 >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("blanks >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("p1a >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("p1b >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("p1c >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("p2a >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("p2b >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("p2c >= 0", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT bills < blanks", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT bills < p1", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT p1a > p1", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT p1b > p1", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT p1c > p1", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT bills < p2", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT p2a > p2", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT p2b > p2", contractors3),
                SigmaRuleParser.parseSigmaRule("NOT p2c > p2", contractors3)
        ));

    }

    @Test
    public void singleMCCETest1() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 1)
                .setInteger("b", 2)
                .setInteger("c", 3);

        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());

        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);

        Map<Integer, Map<Integer, Integer>> costMapForB = new HashMap<>();
        costMapForB.put(2, new HashMap<>());
        costMapForB.put(3, new HashMap<>());

        costMapForB.get(2).put(3, 1);
        costMapForB.get(2).put(4, 3);
        costMapForB.get(2).put(1, 3);

        costMapForB.get(3).put(2, 1);
        costMapForB.get(3).put(4, 1);
        costMapForB.get(3).put(1, 1);

        Map<Integer, Map<Integer, Integer>> costMapForC = new HashMap<>();
        costMapForC.put(3, new HashMap<>());

        costMapForC.get(3).put(1, 1);
        costMapForC.get(3).put(2, 3);
        costMapForC.get(3).put(4, 2);

        NonConstantCostModel costModel = new NonConstantCostModel();
        costModel.addCostFunction("a", new SimpleIterableCostFunction<>(costMapForA));
        costModel.addCostFunction("b", new SimpleIterableCostFunction<>(costMapForB));
        costModel.addCostFunction("c", new SimpleIterableCostFunction<>(costMapForC));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("a == 2 & c == 1", contractors12)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void singleMCCERepairTest1() throws RepairException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 1)
                .setInteger("b", 2)
                .setInteger("c", 3);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        Map<Integer, Map<Integer, Integer>> costMapForA = new HashMap<>();
        costMapForA.put(1, new HashMap<>());

        costMapForA.get(1).put(2, 1);
        costMapForA.get(1).put(3, 5);
        costMapForA.get(1).put(4, 5);

        Map<Integer, Map<Integer, Integer>> costMapForB = new HashMap<>();
        costMapForB.put(2, new HashMap<>());
        costMapForB.put(3, new HashMap<>());

        costMapForB.get(2).put(3, 1);
        costMapForB.get(2).put(4, 3);
        costMapForB.get(2).put(1, 3);

        costMapForB.get(3).put(2, 1);
        costMapForB.get(3).put(4, 1);
        costMapForB.get(3).put(1, 1);

        Map<Integer, Map<Integer, Integer>> costMapForC = new HashMap<>();
        costMapForC.put(3, new HashMap<>());

        costMapForC.get(3).put(1, 1);
        costMapForC.get(3).put(2, 3);
        costMapForC.get(3).put(4, 2);

        NonConstantCostModel costModel = new NonConstantCostModel();
        costModel.addCostFunction("a", new SimpleIterableCostFunction<>(costMapForA));
        costModel.addCostFunction("b", new SimpleIterableCostFunction<>(costMapForB));
        costModel.addCostFunction("c", new SimpleIterableCostFunction<>(costMapForC));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(ruleset1.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void singleMCCETest2() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setInteger("bills", 100)
                .setInteger("blanks", 10)
                .setInteger("p1", 30)
                .setInteger("p1a", 50)
                .setInteger("p1b", 40)
                .setInteger("p1c", 10)
                .setInteger("p2", 60)
                .setInteger("p2a", 21)
                .setInteger("p2b", 15)
                .setInteger("p2c", 9);

        NonConstantCostModel costModel = new NonConstantCostModel();

        costModel.addCostFunction("bills", new IterableCostFunctionWrapper(new ErrorFunction<>(new SwapError<>("p1", "p2"))));
        costModel.addCostFunction("blanks", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1a", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1b", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1c", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2a", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2b", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2c", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset3, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("p1 IN {50, 60, 70, 80, 90}", contractors3)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void singleMCCERepairTest2() throws RepairException {

        DataObject dataObject = new DataObject()
                .setInteger("bills", 100)
                .setInteger("blanks", 10)
                .setInteger("p1", 30)
                .setInteger("p1a", 50)
                .setInteger("p1b", 40)
                .setInteger("p1c", 10)
                .setInteger("p2", 60)
                .setInteger("p2a", 21)
                .setInteger("p2b", 15)
                .setInteger("p2c", 9);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        NonConstantCostModel costModel = new NonConstantCostModel();

        costModel.addCostFunction("bills", new IterableCostFunctionWrapper(new ErrorFunction<>(new SwapError<>("p1", "p2"))));
        costModel.addCostFunction("blanks", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1a", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1b", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p1c", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2a", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2b", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));
        costModel.addCostFunction("p2c", new IterableCostFunctionWrapper(new ErrorFunction<>(IntegerErrors.SINGLE_DIGIT)));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset3, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(ruleset1.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void multipleMCCETest1() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 1)
                .setInteger("b", 2)
                .setInteger("c", 3);

        NonConstantCostModel costModel = new NonConstantCostModel();

        costModel.addCostFunction("a", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("b", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("c", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("b == 1", contractors12)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void multipleMCCERepairTest1() throws RepairException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 1)
                .setInteger("b", 2)
                .setInteger("c", 3);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        NonConstantCostModel costModel = new NonConstantCostModel();

        costModel.addCostFunction("a", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("b", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("c", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset1, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(ruleset1.isSatisfied(repair.getDataObjects().get(0)));

    }

    @Test
    public void multipleMCCETest2() throws RepairException, ParseException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 1)
                .setInteger("b", 2)
                .setInteger("c", 3);

        NonConstantCostModel costModel = new NonConstantCostModel();

        costModel.addCostFunction("a", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("b", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("c", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));

        costModel.addBounding("a", new RangedBounding<>(5))
                .addBounding("b", new RangedBounding<>(5))
                .addBounding("c", new RangedBounding<>(5));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset2, costModel, new CPFRandomRepairSelection());

        Set<CPF> minCostChangeExpressions = repairEngine.getMinCostChangeExpressions(dataObject);

        Set<CPF> expected = Set.of(
                CPFParser.parseCPF("a == 4", contractors12),
                CPFParser.parseCPF("b IN {1,3} & c == 1", contractors12),
                CPFParser.parseCPF("a == 3 & c == 2", contractors12),
                CPFParser.parseCPF("b IN {1,3} & a == 2 & c == 2", contractors12)
        );

        Assert.assertEquals(expected, minCostChangeExpressions);

    }

    @Test
    public void multipleMCCERepairTest2() throws RepairException {

        DataObject dataObject = new DataObject()
                .setInteger("a", 1)
                .setInteger("b", 2)
                .setInteger("c", 3);

        Dataset dataset = new SimpleDataset();
        dataset.addDataObject(dataObject);

        NonConstantCostModel costModel = new NonConstantCostModel();

        costModel.addCostFunction("a", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("b", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));
        costModel.addCostFunction("c", new DistanceCostFunction<>(SigmaContractorFactory.INTEGER));

        costModel.addBounding("a", new RangedBounding<>(5))
                .addBounding("b", new RangedBounding<>(5))
                .addBounding("c", new RangedBounding<>(5));

        NonConstantCostRepairEngine repairEngine = new NonConstantCostRepairEngine(ruleset2, costModel, new CPFRandomRepairSelection());

        Dataset repair = repairEngine.repair(dataset);

        Assert.assertTrue(ruleset2.isSatisfied(repair.getDataObjects().get(0)));

    }

}
