package be.ugent.ledc.sigma.test.contractors;

import be.ugent.ledc.core.dataset.contractors.TypeContractorFactory;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class StringContractorTest
{
    @Test
    public void nameTest()
    {
        assertEquals(TypeContractorFactory.STRING.name(), SigmaContractorFactory.STRING.name());
    }

    @Test
    public void printConstantTest()
    {
        assertEquals("'Hello world!'", SigmaContractorFactory.STRING.printConstant("Hello world!"));
    }
    
    @Test
    public void printConstantWithAmpersandTest()
    {
        assertEquals("'Hello \\& world!'", SigmaContractorFactory.STRING.printConstant("Hello & world!"));
    }
    
    @Test
    public void parseConstantWithAmpersandTest()
    {
        assertEquals("Hello & world!", SigmaContractorFactory.STRING.parseConstant("'Hello \\& world!'"));
    }
}
