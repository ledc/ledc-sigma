package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.fd.PartialKey;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.ParkerRepair;
import be.ugent.ledc.sigma.repair.cost.models.ParkerModel;
import be.ugent.ledc.sigma.repair.selection.RandomRepairSelection;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import static be.ugent.ledc.sigma.test.repair.ParkerRepairFlightTest.repairEngine;
import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

public class ParkerPartialKeyTest
{
    @Test
    public void partialKeyWithConsistentKeyPartTest() throws RepairException, ParseException
    {
        Dataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", null));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", null));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", "Yes"));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", null));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", null));

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        //Attributes are string
        contractors.put("controlled", SigmaContractorFactory.STRING);
        contractors.put("placebo", SigmaContractorFactory.STRING);
        contractors.put("active_comparator", SigmaContractorFactory.STRING);

        Set<SigmaRule> rules = Stream
            .of(
                SigmaRuleParser.parseSigmaRule("NOT controlled notin {'Yes', 'No'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT placebo notin {'Yes', 'No'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT active_comparator notin {'Yes', 'No'}", contractors),
                    
                //Interactions
                SigmaRuleParser.parseSigmaRule("NOT controlled in {'No'} & placebo in {'Yes'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT controlled in {'No'} & active_comparator in {'Yes'}", contractors)
            )
            .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);
        
        SufficientSigmaRuleset eudractRules = SufficientSigmaRuleset.create(ruleset, new FCFGenerator());
        
        PartialKey partialkey = new PartialKey
        (
            Stream.of("key").collect(Collectors.toSet()),
            Stream.of("controlled").collect(Collectors.toSet())
        );
        
        //Build a parker model
        ParkerModel<?> costModel = new ParkerModel<>(partialkey, eudractRules);
        
        repairEngine = new ParkerRepair<>(
            costModel,
            new RandomRepairSelection()
        );
        
        Dataset repair = repairEngine.repair(data);
        
        assertEquals(5, repair.getSize()); //The repair should contain 5 objects
        assertEquals(1, repair.project("controlled").distinct().getSize()); //All objects must have the same value for controlled
        assertEquals("Yes", repair.head(1).getDataObjects().get(0).getString("controlled")); //We expect the minimal cost repairs to have values "Yes" for controlled

      
    }
    
    @Test
    public void partialKeyWithInconsistentKeyPartTest() throws RepairException, ParseException
    {
        Dataset data = new SimpleDataset();
        
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("single_blind", "Yes")
                .set("double_blind", "Yes")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", null));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("single_blind", "Yes")
                .set("double_blind", "Yes")
                .set("placebo", "Yes")
                .set("active_comparator", null));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", "Yes"));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("placebo", "Yes")
                .set("active_comparator", null));
        data.addDataObject(new DataObject()
                .set("key", "1")
                .set("controlled", "No")
                .set("single_blind", "Yes")
                .set("double_blind", "Yes")
                .set("placebo", "Yes")
                .set("active_comparator", null));

        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        
        //Attributes are string
        contractors.put("controlled", SigmaContractorFactory.STRING);
        contractors.put("placebo", SigmaContractorFactory.STRING);
        contractors.put("active_comparator", SigmaContractorFactory.STRING);
        contractors.put("single_blind", SigmaContractorFactory.STRING);
        contractors.put("double_blind", SigmaContractorFactory.STRING);

        Set<SigmaRule> rules = Stream
            .of(
                SigmaRuleParser.parseSigmaRule("NOT controlled notin {'Yes', 'No'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT placebo notin {'Yes', 'No'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT active_comparator notin {'Yes', 'No'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT single_blind notin {'Yes', 'No'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT double_blind notin {'Yes', 'No'}", contractors),
                    
                //Interactions

                SigmaRuleParser.parseSigmaRule("NOT controlled in {'No'} & placebo in {'Yes'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT controlled in {'No'} & active_comparator in {'Yes'}", contractors),
                SigmaRuleParser.parseSigmaRule("NOT single_blind == double_blind", contractors)
            )
            .collect(Collectors.toSet());
        
        SigmaRuleset ruleset = new SigmaRuleset(contractors, rules);
        
        SufficientSigmaRuleset eudractRules = SufficientSigmaRuleset.create(ruleset, new FCFGenerator());
        
        PartialKey partialkey = new PartialKey
        (
            Stream.of("key").collect(Collectors.toSet()),
            Stream.of("controlled", "single_blind", "double_blind").collect(Collectors.toSet())
        );
        
        //Build a parker model
        ParkerModel<?> costModel = new ParkerModel<>(partialkey, eudractRules);
        
        repairEngine = new ParkerRepair<>(
            costModel,
            new RandomRepairSelection()
        );
        
        Dataset repair = repairEngine.repair(data);
        
        assertEquals(5, repair.getSize()); //The repair should contain 5 objects
        assertEquals(1, repair.project("controlled", "single_blind", "double_blind").distinct().getSize()); //All objects must have the same value for controlled, single blind and double blind
        assertEquals("Yes", repair.head(1).getDataObjects().get(0).getString("controlled")); //We expect the minimal cost repairs to have values "Yes" for controlled
        assertNotEquals(repair.head(1).getDataObjects().get(0).getString("single_blind"), repair.head(1).getDataObjects().get(0).getString("double_blind"));

      
    }
}
