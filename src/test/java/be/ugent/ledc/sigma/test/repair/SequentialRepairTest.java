package be.ugent.ledc.sigma.test.repair;

import be.ugent.ledc.core.ParseException;
import be.ugent.ledc.core.RepairException;
import be.ugent.ledc.core.dataset.DataObject;
import be.ugent.ledc.core.dataset.Dataset;
import be.ugent.ledc.core.dataset.SimpleDataset;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractor;
import be.ugent.ledc.sigma.datastructures.contracts.SigmaContractorFactory;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRule;
import be.ugent.ledc.sigma.datastructures.rules.SigmaRuleset;
import be.ugent.ledc.sigma.datastructures.rules.SufficientSigmaRuleset;
import be.ugent.ledc.sigma.io.SigmaRuleParser;
import be.ugent.ledc.sigma.repair.NullBehavior;
import be.ugent.ledc.sigma.repair.RepairEngine;
import be.ugent.ledc.sigma.repair.SequentialRepair;
import be.ugent.ledc.sigma.repair.cost.models.ConstantCostModel;
import be.ugent.ledc.sigma.sufficientsetgeneration.FCFGenerator;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.BeforeClass;
import org.junit.Test;

/**
 * This test class contains a set of tests for the Sequantial repair engine 
 * build around the Adult dataset.
 * @author abronsel
 */
public class SequentialRepairTest
{
    private static SufficientSigmaRuleset adultRules;
    
    @BeforeClass
    public static void setUpClass() throws ParseException
    {
        Map<String, SigmaContractor<?>> contractors = new HashMap<>();
        contractors.put("sex", SigmaContractorFactory.STRING);
        contractors.put("marital_status", SigmaContractorFactory.STRING);
        contractors.put("relationship", SigmaContractorFactory.STRING);
        contractors.put("age", SigmaContractorFactory.INTEGER);

        Set<SigmaRule> rules = Stream.of(
            SigmaRuleParser.parseSigmaRule("relationship in {'Own-child', 'Husband', 'Not-in-family', 'Unmarried', 'Wife', 'Other-relative'}", contractors),
            SigmaRuleParser.parseSigmaRule("sex in {'Female', 'Male'}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT marital_status notin {'Divorced', 'Married-spouse-absent', 'Widowed', 'Separated', 'Married-civ-spouse', 'Married-AF-spouse', 'Never-married'}", contractors),
            SigmaRuleParser.parseSigmaRule("NOT age > 90", contractors),
            SigmaRuleParser.parseSigmaRule("NOT age < 17", contractors),
            SigmaRuleParser.parseSigmaRule("NOT relationship == 'Husband' & sex != 'Male'", contractors),
            SigmaRuleParser.parseSigmaRule("NOT relationship == 'Wife' & sex != 'Female'", contractors),
            SigmaRuleParser.parseSigmaRule("NOT age < 18 & marital_status != 'Never-married'", contractors),
            SigmaRuleParser.parseSigmaRule("NOT marital_status in {'Married-civ-spouse', 'Married-AF-spouse', 'Married-spouse-absent'} & relationship == 'Unmarried'", contractors),
            SigmaRuleParser.parseSigmaRule("NOT age < 18 &  relationship in {'Husband', 'Wife'}", contractors)
        )
        .collect(Collectors.toSet());
        
        adultRules = SufficientSigmaRuleset.create(
            new SigmaRuleset(contractors, rules),
            new FCFGenerator()
        );
        
    }
    
    @Test
    public void sequentialRepairTestMarStatError() throws RepairException
    {
        SimpleDataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("marital_status", "Widowed")
            .set("sex", "Male")
            .set("relationship", "Own-child")
            .set("age", 17)
        );
        
        //Build a cost model with constant cost functions
        ConstantCostModel costModel = new ConstantCostModel()
            .addConstantCostFunction("marital_status", 1)
            .addConstantCostFunction("sex", 2)
            .addConstantCostFunction("age", 2)
            .addConstantCostFunction("relationship", 1);
        
        //Build a sequential repair engine
        RepairEngine<?> engine = new SequentialRepair(
            adultRules,
            costModel,
            NullBehavior.CONSTRAINED_REPAIR,
            new SimpleDataset()
        );
        
        Dataset repair = engine.repair(dirty);
        
        //Repair should contain one DataObject
        assertEquals(1, repair.getSize());
        
        DataObject ro = repair.getDataObjects().get(0);
        
        assertEquals("Never-married", ro.getString("marital_status"));
        assertEquals(ro.project("age", "sex", "relationship"), dirty.getDataObjects().get(0).project("age", "sex", "relationship"));
    }
    
    @Test
    public void sequentialRepairTestMarStatNull() throws RepairException
    {
        SimpleDataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("marital_status", null)
            .set("sex", "Male")
            .set("relationship", "Own-child")
            .set("age", 17)
        );
        
        //Build a cost model with constant cost functions
        ConstantCostModel costModel = new ConstantCostModel()
            .addConstantCostFunction("marital_status", 1)
            .addConstantCostFunction("sex", 1)
            .addConstantCostFunction("age", 1)
            .addConstantCostFunction("relationship", 1);
        
        //Build a sequential repair engine
        RepairEngine<?> engine = new SequentialRepair(
            adultRules,
            costModel,
            NullBehavior.CONSTRAINED_REPAIR,
            new SimpleDataset()
        );
        
        Dataset repair = engine.repair(dirty);
        
        //Repair should contain one DataObject
        assertEquals(1, repair.getSize());
        
        DataObject ro = repair.getDataObjects().get(0);
        
        assertEquals("Never-married", ro.getString("marital_status"));
        assertEquals(ro.project("age", "sex", "relationship"), dirty.getDataObjects().get(0).project("age", "sex", "relationship"));
    }
    
    @Test
    public void sequentialRepairTestGenderError() throws RepairException
    {
        SimpleDataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("marital_status", "Married-civ-spouse")
            .set("sex", "Female")
            .set("relationship", "Husband")
            .set("age", 34)
        );
        
        //Build a cost model with constant cost functions
        ConstantCostModel costModel = new ConstantCostModel()
            .addConstantCostFunction("marital_status", 1)
            .addConstantCostFunction("sex", 1)
            .addConstantCostFunction("age", 1)
            .addConstantCostFunction("relationship", 2);
        
        //Build a sequential repair engine
        RepairEngine<?> engine = new SequentialRepair(
            adultRules,
            costModel,
            NullBehavior.CONSTRAINED_REPAIR,
            new SimpleDataset()
        );
        
        Dataset repair = engine.repair(dirty);
        
        //Repair should contain one DataObject
        assertEquals(1, repair.getSize());
        
        DataObject ro = repair.getDataObjects().get(0);
        
        assertEquals("Male", ro.getString("sex"));
        assertEquals(ro.project("age", "marital_status", "relationship"), dirty.getDataObjects().get(0).project("age", "marital_status", "relationship"));
    }
    
    @Test
    public void sequentialRepairTestRelationshipError() throws RepairException
    {
        SimpleDataset dirty = new SimpleDataset();
        
        dirty.addDataObject(new DataObject()
            .set("marital_status", "Married-civ-spouse")
            .set("sex", "Male")
            .set("relationship", "Husband")
            .set("age", 17)
        );
        
        //Build a cost model with constant cost functions
        ConstantCostModel costModel = new ConstantCostModel()
            .addConstantCostFunction("marital_status", 1)
            .addConstantCostFunction("sex", 1)
            .addConstantCostFunction("age", 1)
            .addConstantCostFunction("relationship", 1);
        
        //Build a sequential repair engine
        RepairEngine<?> engine = new SequentialRepair(
            adultRules,
            costModel,
            NullBehavior.CONSTRAINED_REPAIR,
            new SimpleDataset()
        );
        
        Dataset repair = engine.repair(dirty);
        
        //Repair should contain one DataObject
        assertEquals(1, repair.getSize());
        
        DataObject ro = repair.getDataObjects().get(0);

        assertTrue(ro.getInteger("age") >= 18);
    }
}
