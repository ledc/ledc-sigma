# The Allergen example

The Allergen example describes a cleaning scenario of data about the allergen information of food products.

## Dataset

The data were collected by consulting two websites: the webshop [Alnatura](https://www.alnatura.de/de-de/) and the [Open Food Facts](https://world.openfoodfacts.org/) registry.
Information about allergens was available as arrays of tags or labels.
Products were matched for identical barcodes in order to link them together.
In total, a match was found for 580 products, so the dataset has 1160 rows in total.
The information about allergens was transformed into 21 different columns, where each column contains information about one specific allergen.
All columns with information about an allergen contain an integer number from the set $`\{0,1,2\}`$ and should be interpreted as follows:

- Code 0 means 'Does not contain'
- Code 1 means 'May contain traces'
- Code 2 means 'Contains'

In total, the dataset contains 22 columns: one barcode + 21 columns with information about allergen for that product.
The data can be found in the [data folder](examples/data/allergens_dataset.csv).

For the sake of experimentation, the dataset has been provided with a [golden standard](examples/data/allergens_golden_standard.csv).
The golden standard was composed by drawing a random sample of about 100 products from the dataset.
Two annotators independently looked for pictures of those products on the web from which the allergen information could be observed.
After the collection of all annotations, any discrepancies were resolved after mutual discussion and further inspection.
Some products were discarded from the initial selection either because no good picture of the product was available, or because there were actually different pictures of the same product with different allergen information.
The latter is probably due to changes in the production process over time.
After discarding these products, the sample was extended with more products which were again processed in the same way.


## Constraints

The constraints used in this example can be found [here](examples/rules/allergens.rules).
The file declares the attributes and their contractors, plus some domain constraints.
Finally, the following rules are defined.

``` 
NOT nuts < almondnuts
NOT nuts < brazil_nuts
NOT nuts < macadamia_nuts
NOT nuts < hazelnut
NOT nuts < pistachio
NOT nuts < walnut
NOT nuts < cashew
```

In addition to these rules, we require that the information of a product is the same in both sources.
That means we declare the barcode to be a full key: it functionally determines all information about allergens.

## The repair scenario

The first steps are as before: we read the data and rules and make sure the rules are transformed into
a sufficient set of rules by using the FCF generator.

```java
//Prepare a binding
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(true, ",", '"'),
    new File("data/allergen_dataset.csv"));
        
//Read the data from csv
Dataset dataset = new CSVDataReader(csvBinder).readDataWithTypeInference(100);


//Read the rules
SigmaRuleset allergenRules = ConstraintIo.readSigmaRuleSet(new File("rules/allergen.rules"));
        
//Build a sufficient set with the FCF generator
SufficientSigmaRuleset sufficientAllergenRules = SufficientSigmaRuleset.create(allergenRules, new FCFGenerator());
```

Next, we define a key constraint in a similar way as with the Eudract example.

```java
Set<String> allergenAttributes = Stream.of(
    "nuts",
    "almondnuts",
    "brazil_nuts",
    "macadamia_nuts",
    "hazelnut",
    "pistachio",
    "walnut",
    "cashew",
    "celery",
    "crustaceans",
    "eggs",
    "fish",
    "gluten",
    "lupin",
    "milk",
    "molluscs",
    "mustard",
    "peanut",
    "sesame",
    "soy",
    "sulfite"
).collect(Collectors.toSet());
        
PartialKey partialkey = new PartialKey
(
    Stream.of("code").collect(Collectors.toSet()),
    allergenAttributes
);
```
The next step is the definition of a cost model.
Similar to the Eudract example, we will construct a ParkerModel.
But rather than considering a simple constant cost model, something more refined is used here.
Imagine that we find a conflict in the dataset: source 1 says product A contains nuts while source 2 says product A does not contain nuts at all.
There are only two sources, so the resolution of this conflict can be done either way by changing source 1 or source 2.
However, the *risk* of correcting source 1 and making a mistake is much higher than that of correcting source 2 and making a mistake.
Indeed, it seems here that if one source claims to have information that some product contains (or may contain) some allergen, this should not be neglected.
As such, when choosing between alternatives, we should always be inclined to choose the safer option.
Non-constant cost models come to the rescue.

```java
Map<String, CostFunction<?>> costFunctions = new HashMap<>();
        
Map<Integer, Map<Integer, Integer>> costMap = new HashMap<>();

//Change 0 into...
costMap.put(0, new HashMap<>());
costMap.get(0).put(1,1);
costMap.get(0).put(2,1);

//Change 1 into...
costMap.put(1, new HashMap<>());
costMap.get(1).put(0,3);
costMap.get(1).put(2,1);

//Change 2 into...
costMap.put(2, new HashMap<>());
costMap.get(2).put(0,3);
costMap.get(2).put(1,3);
        
//Build cost functions.
for(String a: allergenAttributes)
{
    costFunctions.put(a, new SimpleIterableCostFunction<>(costMap));
}
        
ParkerModel costModel = new ParkerModel(
    partialkey,
    costFunctions,
    new DefaultObjectCost(),
    sufficientRules
);
```
In the code above, we first make a mapping of costs for making particular changes.
For example, the change from `0` into `1` or `2` is considered low risk and is assigned a baseline cost of 1.
However, the change from `2` into `0` or `1` is considered high risk and is assigned a higher cost of 3.
This mapping is then used to create cost functions for each of the allergen attributes.
This preference-based cost model can be visualized by the following cost matrix.

<table class="tg">
<thead>
  <tr>
    <th class="tg-0lax" colspan="2" rowspan="2"></th>
    <th class="tg-baqh" colspan="3">Repair</th>
  </tr>
  <tr>
    <th class="tg-1wig">0</th>
    <th class="tg-1wig">1</th>
    <th class="tg-1wig">2</th>
  </tr>
</thead>
<tbody>
  <tr>
    <th class="tg-wa1i" rowspan="3">Original</th>
    <th class="tg-1wig">0</th>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">1</td>
    <td class="tg-0lax">1</td>
  </tr>
  <tr>
    <th class="tg-1wig">1</th>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">0</td>
    <td class="tg-0lax">1</td>
  </tr>
  <tr>
    <th class="tg-1wig">2</th>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">3</td>
    <td class="tg-0lax">0</td>
  </tr>
</tbody>
</table>

Note in the constructor that it also accepts as an argument `new DefaultObjectCost()`.
The object cost is a multiplier for costs that are calculated on a per-object basis.
It can be used to indicate to parker that some objects are more or less costly to change.
This is for example useful if you want to model that some sources are more reliable than others.
Here, with default object costs, there is no differentiation and all multipliers are equal to 1.

The final step is the definition of the parker engine.
We now choose a simple Random selector to choose among minimal cost repairs.

```java
ParkerRepair repairEngine = new ParkerRepair(
    costModel,
    new RandomRepairSelection()
);

Dataset repaired = repairEngine.repair(dataset);
```

## Some example repairs

We illustrate some data in the table below, which shows the allergen information for two different products.
The problematic attribute values are marked in bold font.

| code          | nuts | almondnuts | brazil_nuts | macadamia_nuts | hazelnut | pistachio | walnut | cashew | celery | crustaceans | eggs | fish | gluten | lupin | milk | molluscs | mustard | peanut | sesame | soy | sulfite |
|---------------|------|------------|-------------|----------------|----------|-----------|--------|--------|--------|-------------|------|------|--------|-------|------|----------|---------|--------|--------|-----|---------|
| 4104420145566 | 1    | 0          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 1    | 0    | **2**      | 0     | 1    | 0        | 0       | **1**      | 2      | 1   | 0       |
| 4104420145566 | 1    | 0          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 1    | 0    | **0**      | 0     | 1    | 0        | 0       | **0**      | 2      | 1   | 0       |
| 4104420053786 | **1**    | **2**          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 2    | 0    | 2      | 0     | 2    | 0        | 0       | 0      | 1      | 1   | 0       |
| 4104420053786 | **2**    | **0**          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 2    | 0    | 2      | 0     | 2    | 0        | 0       | 0      | 1      | 1   | 0       |

A minimal repair is shown below, with changes marked in bold.
It can be seen that there is a preference towards increasing the values for allergen-related attributes to avoid risk.

| code          | nuts | almondnuts | brazil_nuts | macadamia_nuts | hazelnut | pistachio | walnut | cashew | celery | crustaceans | eggs | fish | gluten | lupin | milk | molluscs | mustard | peanut | sesame | soy | sulfite |
|---------------|------|------------|-------------|----------------|----------|-----------|--------|--------|--------|-------------|------|------|--------|-------|------|----------|---------|--------|--------|-----|---------|
| 4104420145566 | 1    | 0          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 1    | 0    | 2      | 0     | 1    | 0        | 0       | 1      | 2      | 1   | 0       |
| 4104420145566 | 1    | 0          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 1    | 0    | **2**      | 0     | 1    | 0        | 0       | **1**      | 2      | 1   | 0       |
| 4104420053786 | **2**    | 2          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 2    | 0    | 2      | 0     | 2    | 0        | 0       | 0      | 1      | 1   | 0       |
| 4104420053786 | 2    | **2**          | 0           | 0              | 0        | 0         | 0      | 0      | 0      | 0           | 2    | 0    | 2      | 0     | 2    | 0        | 0       | 0      | 1      | 1   | 0       |
