In order to get started with ledc-sigma, we provide some concrete examples, from the data to start with up to the code to configure the repair model.
To keep things simple, information about a single example is bundled on a separate page.
On each of these pages, we first go over the data, then discuss the constraints that are in place and finally describe the code to read data and constraints and compile a repair engine for that particular case.
An overview of the examples that you can learn from is given below.
It is advisable to start with the Adult use case and work your way up from there.

| Case     | Atoms          | Cost model       | Repair engine | Gold standard |
|----------|----------------|------------------|---------------|---------------|
| [Adult](adult.md)    | Constant & Set | Constant         | Constant      | No            |
| [Eudract](eudract.md)  | Constant       | Constant         | Parker        | Yes           |
| [Allergen](allergen.md) | Variable       | Preference-based | Parker        | Yes           |
| [Flight](flight.md)   | Variable       | Error-based      | Parker        | Yes           |
