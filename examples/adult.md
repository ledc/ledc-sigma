# The Adult example

The Adult example describes a cleaning scenario of Census data with some simple constraints and provides a simple constant-cost repair scenario.

## Dataset

The dataset itself is a classical dataset from the UCI repository and can be found [here](https://archive.ics.uci.edu/ml/datasets/adult).
It contains 14 attributes and 48842 rows.
Most attributes are categorical, although some integer-valued attributes are present as well.

## Constraints

The constraints used in this example can be found [here](rules/adult.rules).
The file first declares the 14 attributes and their contractors.
Next, some domain constraints are provided, which are derived from the actual dataset itself.
Finally, the following rules are defined.

``` 
NOT age < 18 & income != '<=50K'
NOT age < 18 &  relationship in {'Husband', 'Wife'}
NOT age < 18 & marital_status != 'Never-married'
NOT relationship == 'Husband' & sex != 'Male'
NOT relationship == 'Wife' & sex != 'Female'
NOT marital_status in {'Married-civ-spouse', 'Married-AF-spouse', 'Married-spouse-absent'} & relationship == 'Unmarried'
```

## The repair scenario

We now show how a simple repair scenario can be developed for this use case.
The first step is to read the data, which we assume to reside in a csv file.
The data is read as a [Dataset](https://gitlab.com/ddcm/ledc-core/-/blob/master/docs/data-model.md).
We read the csv file with type inference such that integer-valued datatypes are properly read.

```java
//Prepare a binding
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(true, ",", '"'),
    new File("adult.csv"));
        
//Read the data from csv
Dataset dataset = new CSVDataReader(csvBinder).readDataWithTypeInference(1000);
```

The next step is to read the rules and convert them into a sufficient set with the FCF algorithm.

```java
//Read the rules
SigmaRuleset adultRules = ConstraintIo.readSigmaRuleSet(new File("rules/adult.rules"));
        
//Build a sufficient set with the FCF generator
SufficientSigmaRuleset sufficientAdultRules = SufficientSigmaRuleset.create(adultRules, new FCFGenerator());
```

In the final step, we are ready to configure the repair engine.
In this example, we will use [constant cost functions](../docs/repair.md#constant-cost-functions).
That means the cost to change an attribute is some fixed positive integer.
For simplicity, we assume that all attributes have equal cost.
The code below shows how the cost model is defined.
Note that for some default cases, there are some short-hand constructors, but we provide the full code here for the sake of the example.

```java
//The cost model assumes constant cost for each attribute
Map<String, ConstantCostFunction> costMap = new HashMap<>();
        
costMap.put("income", new ConstantCostFunction(1));
costMap.put("education", new ConstantCostFunction(1));
costMap.put("education_num", new ConstantCostFunction(1));
costMap.put("occupation", new ConstantCostFunction(1));
costMap.put("race", new ConstantCostFunction(1));
costMap.put("sex", new ConstantCostFunction(1));
costMap.put("hours_per_week", new ConstantCostFunction(1));
costMap.put("fnlwgt", new ConstantCostFunction(1));
costMap.put("capital_loss", new ConstantCostFunction(1));
costMap.put("capital_gain", new ConstantCostFunction(1));
costMap.put("native_country", new ConstantCostFunction(1));
costMap.put("marital_status", new ConstantCostFunction(1));
costMap.put("workclass", new ConstantCostFunction(1));
costMap.put("relationship", new ConstantCostFunction(1));
costMap.put("age", new ConstantCostFunction(1));
        
ConstantCostModel costModel = new ConstantCostModel(costMap);
```

With the cost model defined, we are now ready to build a repair engine.
There are several [repair engines](../docs/repair.md#repair-engines) that deal with constant cost models.
We demonstrate here a simple sequential repair strategy.
To define this engine, we need to pass a few arguments.
The engine needs to know the sufficient set of rules and of course also the cost model, which we both have defined already.
In addition, we can inform the repair engine about how it should treat missing data (i.e., NULL-values).
Finally, the repair engine also requires a selection of *clean* data to be passed.
This clean data will be used to estimate frequencies of repair values and helps to maintain the (marginal) distributions in the dataset.
When the repair engine is defined, we can simply apply it to the dataset

```java
//Build the repair engine
RepairEngine engine = new SequentialRepair(
    sufficientAdultRules,
    costModel,
    NullBehavior.CONSTRAINED_REPAIR,
    adultDataset.select(o -> adultRules.isSatisfied(o))
);

//Apply the repair engine to the dataset        
Dataset repaired = engine.repair(dataset);
```
## Some example repairs

The table below shows some rows selected from the Adult dataset, projected over five attributes.
In this table, each attribute value marked in bold font, indicates a minimal set cover (all set covers are singletons here) and thus a possible location of an error.
Note that the second row in this example fails two rules and as `age` appears in both these failing rules, there is only one minimal set cover.
All the other rows fail a single rule that each time involves two attributes.
Therefore, there are two minimal set covers and thus two ways of repairing.

| income | marital_status        | sex    | relationship   | age |
|--------|-----------------------|--------|----------------|-----|
| <=50K  | Married-civ-spouse    | **Female** | **Husband**    | 34  |
| <=50K  | Married-civ-spouse    | Male   | Husband        | **17**  |
| <=50K  | **Married-civ-spouse**    | Female | Own-child  | **17**  |
| <=50K  | Married-civ-spouse    | **Male**   | **Wife**   | 36  |
| <=50K  | **Widowed**         | Male   | Own-child      | **17** |

When we apply our sequential repair engine to this dataset, the table shown below is a possible repair.
In this table, the *changes* that are introduced in the repair, are marked in bold font.
We note that, except for the second row, the choice about which attribute to change, is taken completely at random by the sequential repair engine.
In most cases, this is reasonable.
However, the repair of third row is a bit peculiar.
For the third row, the engine chose to change the value for `age`, leading to a repair in which a person is married, aged 33 and lives with her parents.
This is a rare case and in fact, a repair where `relationship' is changed, would lead to a more common scenario.
This is clear example where the conditional sequential repair engine would make a better choice among the possible minimal set covers.

| income | marital_status        | sex    | relationship   | age |
|--------|-----------------------|--------|----------------|-----|
| <=50K  | Married-civ-spouse    | Female | **Not-in-family**    | 34  |
| <=50K  | Married-civ-spouse    | Male   | Husband        | **22**  |
| <=50K  | Married-civ-spouse    | Female | Own-child  | **33**  |
| <=50K  | Married-civ-spouse    | Male   | **Not-in-family**   | 36  |
| <=50K  | **Never-married**     | Male   | Own-child      | 17 |
