# The Flight example

The Flight example describes a cleaning scenario of data about departure and arrival of flights collected across different web sources.

## Dataset

The Flight dataset and the golden standard used for this example, are a modification of the [original Flight dataset](http://lunadong.com/fusionDataSets.htm).
The authors of the original dataset collected information about the departure and arrival times of flights as reported across multiple websites.
Both the expected and the actual times are collected in the dataset.

An issue with the original dataset, is all temporal information is provided as text.
Because there is a large variation in the *patterns* used to encode temporal information as text, it is difficult to compare two temporal values.
In addition to that, the temporal information is given in the local timezone of the airport where the flight departs or arrives.
The following table shows a few rows from the original dataset.

| flight_number   | date_collected | source              | actual_departure         | actual_arrival           |
|-----------------|----------------|---------------------|--------------------------|--------------------------|
| AA-1007-MIA-PHX | 2011-12-01     | aa                  | 12/01/2011 05:08 PM      | 12/01/2011 07:55 PM      |
| AA-1007-MIA-PHX | 2011-12-01     | airtravelcenter     | 12/1/11 5:09 PM (-05:00) | 12/1/11 7:43 PM (-07:00) |
| AA-1007-MIA-PHX | 2011-12-01     | allegiantair        | 5:09 PMDec 01            | 7:53 PMDec 01            |
| AA-1007-MIA-PHX | 2011-12-01     | boston              | 5:09 PMDec 01            | 7:53 PMDec 01            |
| AA-1007-MIA-PHX | 2011-12-01     | businesstravellogue | 2011-12-01 05:08 PM      | 2011-12-01 07:55 PM      |

The snippet of data shown above demonstrates that although rows 2, 3 and 4 agree on the actual departure, the representation of the timestamp differs and hence looking at equal values will fail.
The same is true for rows 1 and 5.
Also note that this particular flight departed at MIA (Miami International Airport) and arrived at PHX (Phoenix Sky Harbor International Airport).In December, when these data were collected, these airports are respectively in timezones UTC-5 and UTC-7.
If we want to verify whether or not some constraints are satisfied (e.g., departure must be before arrival), we must account for these timezones.

To resolves these issues, the original dataset was transformed.
The data were loaded in a PostgreSQL database and a SQL script was used to parse the temporal information as timestamps.
Next, for each flight, the airport codes for the departing and arriving airport were extracted from the flight number and a mapping table between airport codes and timezones was made.
This lookup table was used to adjust all temporal information into the same timezone (UTC-0).
The snippet of data shown above is then transformed to the following data.

| flight_number   | date_collected | source              | actual_departure | actual_arrival   |
|-----------------|----------------|---------------------|------------------|------------------|
| AA-1007-MIA-PHX | 2011-12-01     | aa                  | 2011-12-01 22:08 | 2011-12-02 02:55 |
| AA-1007-MIA-PHX | 2011-12-01     | airtravelcenter     | 2011-12-01 22:09 | 2011-12-02 02:43 |
| AA-1007-MIA-PHX | 2011-12-01     | allegiantair        | 2011-12-01 22:09 | 2011-12-02 02:53 |
| AA-1007-MIA-PHX | 2011-12-01     | boston              | 2011-12-01 22:09 | 2011-12-02 02:53 |
| AA-1007-MIA-PHX | 2011-12-01     | businesstravellogue | 2011-12-01 22:08 | 2011-12-02 02:55 |

The resulting dataset can be found in the [data folder](data/flight.csv).
It contains 13 columns in total, but only seven are of interest here: flight_number, data_collected, source, actual_departure, actual_arrival, scheduled_departure and scheduled_arrival.
Among the other six columns, four columns include the original textual representations of the temporal information, for the purpose of traceability.
The other columns describe the departure gate and the arrival gate, but we omit these as our main focus of the example lies on the temporal information.
In total, the dataset has 776067 rows, which makes it an interesting dataset for scalibility experiments.

The original dataset comes with a golden standard, which we processed in the same way as described above.
The transformed golden standard can be found [here](data/flight_golden_standard.csv).

## Constraints

The constraints used in this example can be found [here](rules/flight.rules).
In constrast to the previous examples, a few interesting things can be said about these constraints.

* The four attributes that contain temporal information, are assigned the contractor `datetime_in_minutes`.
That means *all* temporal information is processed at the truncation level of minutes.
Shifting a given datetime with one unit, means adding/subtracting exactly one minute.

* The file contains *no* domain constraints.
As a result, the set of permitted values is theoretically unlimited and this requires attention when the repair engine is designed.

The constraint themselves (shown below) also deserve some attention.
The first rule states that if the scheduled departure lies after the scheduled arrival of less than 15 minutes before, data are erroneous.
The 15 minutes are introduced to ensure that during repair departures and arrivals are not too close to each other as the take-off and landing of an airplane requires a minimal amount of time.
The second rule basically states the same, but for the actual departure and arrival.

The third and fourth rule state that the amount of time between the scheduled and actual departure should be within reasonable limits, chosen here as 12 hours (720 minutes).

``` 
NOT scheduled_departure >= S^-15(scheduled_arrival)
NOT actual_departure >= S^-15(actual_arrival)
NOT actual_departure >= S^720(scheduled_departure)
NOT actual_departure <= S^-720(scheduled_departure)
```

In addition to these rules, we require that the information of a flight is the same in all sources.
That means we declare the combination of the `flight_number` and the `date_collected` to be a full key: it functionally determines all temporal information.

## The repair scenario

As in our previous examples, we first have some code that reads the data and rules and does the necessary processing steps.

```java
//Prepare a binding
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(true, ",", '"'),
    new File("data/flight.csv"));
   
//Read the data from csv
Dataset dataset = new CSVDataReader(csvBinder).readData();

dataset
.asDateTime("actual_departure",     "yyyy-MM-dd HH:mm:ssX")
.asDateTime("actual_arrival",       "yyyy-MM-dd HH:mm:ssX")
.asDateTime("scheduled_departure",  "yyyy-MM-dd HH:mm:ssX")
.asDateTime("scheduled_arrival",    "yyyy-MM-dd HH:mm:ssX")

//Read the rules
SigmaRuleset flightRules = ConstraintIo.readSigmaRuleSet(new File("rules/flight.rules"));
        
//Build a sufficient set with the FCF generator
SufficientSigmaRuleset sufficientFlightRules = SufficientSigmaRuleset.create(flightRules, new FCFGenerator());
```
Next, we define the key constraint.

```java
PartialKey partialkey = new PartialKey
(
    Stream.of("date_collected", "flight_number").collect(Collectors.toSet()),
    Stream.of(
        "scheduled_departure",
        "actual_departure",
        "scheduled_arrival",
        "actual_arrival"
    ).collect(Collectors.toSet())
);
```
The next step is the definition of the cost model.
We will again construct a ParkerModel, but this time with a cost model that is able to account for some specific *error mechanism*.
To explain this, recall that in the construction procedure of the dataset, patterns are used to parse textual information into timestamps.
In these patterns, the indicators `AM` and `PM` are used sometimes to denote the time.
In some boundary cases, like `00:00 AM` or `12:00 PM`, there seems to be a difference in interpretation among the different web sources that were consulted.
This causes some offsets of exactly 24 hours in the dataset after transformation.
Apart from these boundary cases, the offset could also be explained by simple mistakes in date.
The exact cause of these errors remains somehow unclear, but it is a certainty that they are present in the dataset.

In the cost model below, a repair is assigned a cost of 1 if the difference between the original value and the repair, is exactly 24 hours.
This corresponds to the ONE_DAY_OFF error mechanism.
Else, a repair has cost 3.

```java  
//A mapping from attributes to cost functions    
Map<String, CostFunction<?>> costFunctions = new HashMap<>();
        
//We build a cost function by assigning a cost calculator to an error mechanisms
Map<ErrorMechanism<LocalDateTime>, CostCalculator<LocalDateTime>> emMapping = new HashMap<>();

//If differences in datetime can be explained by a difference of exactly 24 hours, we assign cost 1
emMapping.put(DatetimeErrors.ONE_DAY_OFF, (orig, fix) -> 1);

//The cost for non-explainable values is fixed to 3
//NULL values are repaired with fixed cost 1
ErrorBasedCostFunction<LocalDateTime> costFuction = new ErrorBasedCostFunction<>(
    emMapping,
    (orig, fix) -> 3,
    1
);

//We assign the same cost function for all involved attributes    
costFunctions.put("scheduled_departure", costFuction);
costFunctions.put("scheduled_arrival", costFuction);
costFunctions.put("actual_departure", costFuction);
costFunctions.put("actual_arrival", costFuction);
        
ParkerModel costModel = new ParkerModel(
    partialkey,
    costFunctions,
    new DefaultObjectCost(),
    sufficientRules,
    new RangedBounding(1440)
);
```
It can be seen in the code above that the cost model takes an additional argument `new RangedBounding(1440)`.
The reason why we do this, is because there are no domain constraints.
To limit the scope in which repairs can be searched, we can define a bounding strategy.
Such a bounding strategy will determine, for each individual case, dynamic bounds on the domain.
The ranged bounding strategy can be applied to ordinal datatypes and will look for the *minimal* and *maximal* values in the use case and then stretch the bounds by the given offset.
In the case above, the offset is 1440 minutes, which equals 24 hours.
The default behavior is that the bounding strategy is applied to attributes that have no domain constraints.
In this example, this happens to be all attributes that require repair.
If there is a need to apply the bounding strategy to *all* attributes regardless of the domain constraints, set the `forceBounding` parameter to `true`.

To illustrate this principle of ranged bounding, consider the example snippet of data we introduced above and take for example the attribute `actual_departure`.
The minimal and maximal values are respectively `2011-12-01 22:08` and `2011-12-01 22:09`.
We then apply the offsets of 1440 minutes, which means that in this case, repairs are searched between `2011-11-30 22:08` and `2011-12-02 22:09`, bounds inclusive.

The final step is again the definition of a repair engine, which is now simply done by passing the cost model and a repair selector.

```java
ParkerRepair repairEngine = new ParkerRepair(
    costModel,
    new RandomRepairSelection()
);

Dataset repaired = repairEngine.repair(dataset);
```

## Some example repairs

We illustrate the repair engine on an example taken from the dataset.
Because the dataset contains many source, that often provide the same information, we present for the case below the *unique* combinations of values and indicate in the last column how many times that particular combination occurs.

| date_collected | flight_number   | scheduled_departure    | actual_departure       | scheduled_arrival      | actual_arrival         | count |
|----------------|-----------------|------------------------|------------------------|------------------------|------------------------|-------|
| 2011-12-31     | AA-3823-LAX-DEN | 2012-01-01 05:00 | 2012-01-01 05:00 | 2011-12-31 07:15 | 2011-12-31 07:15| 3     |
| 2011-12-31     | AA-3823-LAX-DEN | 2012-01-01 05:00 |                  | 2012-01-01 07:15 |                 | 16    |
| 2011-12-31     | AA-3823-LAX-DEN |                  |                  | 2011-12-31 07:15 |                 | 1     |
| 2011-12-31     | AA-3823-LAX-DEN | 2012-01-01 05:00 |                  | 2012-01-01 06:46 |                 | 3     |
| 2011-12-31     | AA-3823-LAX-DEN | 2012-01-01 05:00 |                  | 2011-12-31 07:15 |                 | 2     |

This simple example shows some important findings with respect to the parker engine.

First of all, the example illustrates the error mechanism we explained earlier: some sources report date `2011-12-31` and some report date `2012-01-01` in the attribute `scheduled_arrival`.
Probably (we cannot be sure) this error is introduced because of misinterpretations of `12AM`.
Important to see is that the *differences* between most of these values, can be explained via the `ONE_DAY_OFF` mechanism.
So parker will be able to use this error mechanism in order to settle differences between the sources.

Second, note that there is only one value for `actual_arrival` and the date for this value is `2011-12-31`.
However, most sources seem to indicate that the date should be `2011-01-01`.
So we actually want to consider the alternative date `2011-01-01` for `actual_arrival` and this is possible because of the ranged bounding we introduced above.

Third, as we introduced ranged bounding with an offset of 1440 minutes, each attribute has more than 2880 possible values which means there are in total more than 68 trillion combinations for this single use case.
However, only a few of those combinations lead to a minimal cost and the search strategy of parker manages to find these extremely fast.

If we apply the parker engine to these data, all rows will be assigned the following values:

| date_collected | flight_number   | scheduled_departure    | actual_departure       | scheduled_arrival| actual_arrival   | 
|----------------|-----------------|------------------------|------------------------|------------------|------------------|
| 2011-12-31     | AA-3823-LAX-DEN | 2012-01-01 05:00 | 2012-01-01 05:00 | 2012-01-01 07:15 | 2012-01-01 07:15 |
