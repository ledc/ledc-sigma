# The Eudract example

The Eudract example describes a cleaning scenario of data about the designs of clinical trials as reported on the [EudraCT register](https://www.clinicaltrialsregister.eu/ctr-search/search/).

## Dataset

The data were collected via the EudraCT register, where we collected the design of studies as reported by the different membership states involved.
A dump of the data is available in the [data folder](data/eudract_dataset.csv).
The dataset contains 13 attributes and has 86670 rows.
All attributes are categorical.

The dataset can be used for experiments and comes with a [golden standard](data/eudract_golden_standard.csv).
This golden standard was collected by searching for studies in two other registries: [clinicaltrials.gov](https://clinicaltrials.gov/) and the [Deutches Register Klinischen Studien](https://www.drks.de).
Links were made between studies via reported secondary identifiers.
We then looked for studies that appeared in all three registers and for which the two additional registers agree on *all* design attributes.
Those studies were selected as golden standard and the values reported in the two additional registers were recorded.

## Constraints

The constraints used in this example can be found [here](rules/eudract.rules).
The file declares the attributes and their contractors, plus some domain constraints.
Finally, the following rules are defined.

``` 
-- eudract rules for masking
NOT open == 'Yes' & single_blind == 'Yes'
NOT open == 'Yes' & double_blind == 'Yes'
NOT single_blind == 'Yes' & double_blind == 'Yes'
NOT open == 'No' & single_blind == 'No' & double_blind == 'No'

-- eudract rules for control
NOT controlled == 'No' & placebo == 'Yes'
NOT controlled == 'No' & active_comparator == 'Yes'

-- crossover and parallel cannot occur simultaneously
NOT parallel_group == 'Yes' & crossover == 'Yes'

-- arms check
NOT arms in {'0', '1'} & placebo == 'Yes'
NOT arms in {'0', '1'} & active_comparator == 'Yes'
```

In addition to these rules, the Eudract dataset may provide *multiple* rows for the *same* study.
This is due to the fact that the same study can be executed in multiple member states of the European Union.
However, it is then reasonable to expect that some parts of the reported design are the same across these different member states.
Therefore, we will consider a partial key constraint as follows:

$`eudract\_number \rightarrow single\_blind, double\_blind, open, controlled, randomised, crossover, parallel\_group, arms`$

## The repair scenario

The first steps are basically the same as with the Adult example: we read the data from a csv file into a Dataset object.
We read the rules and transform them into a sufficient set of rules by using the FCF generator.

```java
//Prepare a binding
CSVBinder csvBinder = new CSVBinder(
    new CSVProperties(true, ";", null),
    new File("data/eudract_dataset.csv"));
        
//Read the data from csv
Dataset dataset = new CSVDataReader(csvBinder).readData();

//Read the rules
SigmaRuleset eudractRules = ConstraintIo.readSigmaRuleSet(new File("rules/eudract.rules"));
        
//Build a sufficient set with the FCF generator
SufficientSigmaRuleset sufficientEudractRules = SufficientSigmaRuleset.create(eudractRules, new FCFGenerator());
```

Next, we need to define the additional partial key constraint.
Currently, .rules files can only model selection rules, so we need to define the partial key constraint in code, as shown below.

```java
PartialKey partialkey = new PartialKey
(
    Stream.of("eudract_number").collect(Collectors.toSet()),
    Stream.of(
        "single_blind",
        "double_blind",
        "open",
        "controlled",
        "randomised",
        "crossover",
        "parallel_group",
        "arms"
    ).collect(Collectors.toSet())
);
```
The next step is the definition of a cost model.
When we want to include the partial key constraint, we use a specialized cost model for parker.
In the ParkerModel, the basic cost model is a constant cost model with a fixed cost of 1 for each attribute. 
With the code below, all attributes involved in some rule will be assigned a constant cost function with fixed cost of 1.

```java
ParkerModel costModel = new ParkerModel(partialKey, sufficientEudractRules);
```

The final step is the definition of the parker engine.
Hereby, we need to pass a repair selector.
This is because parker will search for all minimal cost solutions and uses the repair selector to choose among the minimal cost solutions.
Just as with the Adult example, we use a selection strategy based on frequency estimation on the clean data.

```java
//Collect the clean data, but remove attributes that do not need repairs: eudract_number (partial key), protocol_country_code and source (attributes about provenance)
Dataset cleanData = data
    .select(o -> sufficientRules.isSatisfied(o))
    .inverseProject(
        "eudract_number",
        "protocol_country_code",
        "source"
    );

//Define a repair selector        
RepairSelection selector = new FrequencyRepairSelection(cleanData);

//Pass the cost model and the repair selector to make a parker engine
ParkerRepair repairEngine = new ParkerRepair(
    costModel,
    selector
);
```

## Some example repairs

### Example 1
A first example is the data shown below, where problematic cells are marked in bold.
First of all, the partial key constraint is violated for `parallel_group` and `single_blind`.
In addition, three rows state that the masking of the study is both single blinded and double blinded.

| eudract_number | protocol_country_code | randomised | crossover | parallel_group | open | single_blind | double_blind | controlled | arms | active_comparator | placebo |
|----------------|-----------------------|------------|-----------|----------------|------|--------------|--------------|------------|------|-------------------|---------|
| 2006-001913-13 | DE                    | Yes        | No        | **Yes**            | No   | **Yes**          | **Yes**          | Yes        |      | No                | Yes     |
| 2006-001913-13 | SE                    | Yes        | No        | **Yes**            | No   | **Yes**          | **Yes**          | Yes        |      | No                | Yes     |
| 2006-001913-13 | GB                    | Yes        | No        | **No**            | No   | **No**           | **Yes**          | Yes        |      | No                | Yes     |
| 2006-001913-13 | FR                    | Yes        | No        | **Yes**            | No   | **Yes**          | **Yes**          | Yes        |      | No                | Yes     |

A minimal repair is shown below.
The attribute values that are changed, are shown in bold font.
A first interesting observation is the interaction between selection rules and the partial key constraint.
Using *only* the partial key constraint, the minimal cost principle would enforce `single_blind='Yes'` as the majority of the tuples has value `Yes` for attribute `single_blind`.
However, this violates the selection rule `NOT single_blind == 'Yes' & double_blind == 'Yes'`.
The parker engine uses the principle set covers to inspect alternatives for `single_blind` and `double_blind` which satisfy all rules and have minimal cost.

| eudract_number | protocol_country_code | randomised | crossover | parallel_group | open | single_blind | double_blind | controlled | arms | active_comparator | placebo |
|----------------|-----------------------|------------|-----------|----------------|------|--------------|--------------|------------|------|-------------------|---------|
| 2006-001913-13 | DE                    | Yes        | No        | Yes            | No   | **No**          | Yes          | Yes        |  **2+**    | No                | Yes     |
| 2006-001913-13 | SE                    | Yes        | No        | Yes            | No   | **No**          | Yes          | Yes        |   **2+**   | No                | Yes     |
| 2006-001913-13 | GB                    | Yes        | No        | **Yes**            | No   | No           | Yes          | Yes        |  **2+**    | No                | Yes     |
| 2006-001913-13 | FR                    | Yes        | No        | Yes            | No   | **No**          | Yes          | Yes        |   **2+**   | No                | Yes     |

A second thing to notice in the repair above, is that the missing values for attribute `arms` are filled in.
Our strategy was to repair missing values only when the attribute was involved in at least one rule that also involved another attribute.
There are two such rules:
``` 
NOT arms in {'0', '1'} & placebo == 'Yes'
NOT arms in {'0', '1'} & active_comparator == 'Yes'
```
The example shows the power of this strategy: because some rows have `placebo='Yes'`, the rules above dictate that `arms` must be different from `0` and `1`.
This means it can only take the value `2+`.
Because of the partial key constraint, we can copy this value to all rows that have the same `eudract_number`.

### Example 2

In the previous example, all rows in the repair eventually have the same value.
This is a coincidence as the values for `placebo` and `active_comparator` (who are not determined by `eudract_number`) were already equal.
Let's take a look at another example, shown below.
Again, problematic values are shown in bold font.
There are some problems with the attributes `parallel_group` and `controlled`, who have both different values.
Some rows have `controlled='No'` while either `placebo='Yes'` or `active_comparator='Yes'`, which is a violation of the selection rules.
Finally, the value for `arms` is missing.

| eudract_number | protocol_country_code | randomised | crossover | parallel_group | open | single_blind | double_blind | controlled | arms | active_comparator | placebo |
|----------------|-----------------------|------------|-----------|----------------|------|--------------|--------------|------------|------|-------------------|---------|
| 2006-006497-17 | NL                    | Yes        | No        | **No**           | No   | No           | Yes          | **No**         |      | **Yes**             | **Yes**    |
| 2006-006497-17 | DE                    | Yes        | No        | **Yes**            | No   | No           | Yes          | **Yes**        |      | Yes               | Yes     |
| 2006-006497-17 | IT                    | Yes        | No        | **Yes**          | No   | No           | Yes          | **Yes**        |      | Yes               | No      |
| 2006-006497-17 | HU                    | Yes        | No        |**No**           | No   | No           | Yes          | **No**        |      | **Yes**              | **Yes**    |
| 2006-006497-17 | GR                    | Yes        | No        |**No**          | No   | No           | Yes          | **Yes**        |      | Yes               | Yes     |

A minimal cost repair is shown below, where changes are marked in bold font.
The problem for `parallel_group` is settled by choosing `No` for all rows, because of a simple majority voting.
The problem with `controlled` is settled by choosing value `Yes` as this solves the violation of the partial key constraint as well as the violation of the selection rules.
Note that `placebo` still takes different values in the repair as it is not part of the partial key constraint, even though it occurs in some rules.
Finally, in a similar way as in the previous example, the value for `arms` must be equal to `2+`.

| eudract_number | protocol_country_code | randomised | crossover | parallel_group | open | single_blind | double_blind | controlled | arms | active_comparator | placebo |
|----------------|-----------------------|------------|-----------|----------------|------|--------------|--------------|------------|------|-------------------|---------|
| 2006-006497-17 | NL                    | Yes        | No        | No           | No   | No           | Yes          | **Yes**         |  **2+**    | Yes             | Yes    |
| 2006-006497-17 | DE                    | Yes        | No        | **No**            | No   | No           | Yes          | Yes        | **2+**     | Yes               | Yes     |
| 2006-006497-17 | IT                    | Yes        | No        | **No**          | No   | No           | Yes          | Yes        |  **2+**    | Yes               | No      |
| 2006-006497-17 | HU                    | Yes        | No        |No           | No   | No           | Yes          | **Yes**        |  **2+**    | Yes              | Yes    |
| 2006-006497-17 | GR                    | Yes        | No        |No          | No   | No           | Yes          | Yes        |  **2+**    | Yes               | Yes     |

