# ledc-sigma

## Intro

Welcome to the main page of ledc-sigma, a tool for verification and repair of data based on a type of tuple-level constraints called $`\sigma`$-rules or simply *selection* rules.
If you have a dataset with errors and you can express rules on the level of individual *rows*, then keep reading and find out how ledc-sigma can solve your problem.

## What

This repository is part of ledc, which is short for *Lightweight Engine for Data quality Control*. 
It is a modular framework designed for data quality monitoring and improvement in several different ways.
Here, you can find ledc-sigma which has a focus on *error detection* and *repair* on the level of indiviual rows. 
Its main features are:

* **Expressiveness**: [selection rules](docs/sigma-rules.md) are a highly expressive type of rules for categorical data.
They are more expressive than the (generalized) selection operator of the relational algebra.

* **Easy configuration**: selection rules can be denoted in a simple, text-based [file format](docs/sigma-rules.md#file-format).

* **Repairing**: ledc-sigma comes with a wide variety of [repair strategies](docs/repair.md) that propose minimal-cost changes in an efficient way.
To do that, it uses positive definite cost functions.
Besides the traditional "fixed-cost" repairing, these cost functions allow to model error mechanisms (e.g., typographical errors), a priori preferences and magnitude of error.

* **Partial key constraints:** If you have multiple rows describing the same entity, you can further increase the expressiveness of ledc-sigma by adding partial key constraints.
The [Parker repair engine](docs/repair.md#parker-repair) offers a very powerful tool to repair inconsistencies in this case.

## Getting started

As all current and future components of ledc, ledc-sigma is written in Java.
To run it, as from version 1.2, you require Java 17 (or higher) and Maven.
Just pull the code from this repository and build it with Maven.
Alternatively, have a look at the packages & registries section to find your Maven snippet.
To understand the mechanics of ledc-sigma, have a look at the [documentation](docs/introduction.md) or dive directly into the [examples](examples/index.md).

## References

The code you can find in ledc-sigma is based on the results of several scientific publications.
Below you can find relevant publications.

* Antoon Bronselaer and Maribel Acosta, [Parker: Data fusion through consistent repairs using edit rules under partial keys](https://www.sciencedirect.com/science/article/abs/pii/S1566253523002580), Information Fusion, (**2023**)

* Toon Boeckling, Guy De Tré and Antoon Bronselaer, [Efficient edit rule implication for nominal and ordinal data](https://www.sciencedirect.com/science/article/pii/S0020025521013360), Information Sciences, (**2022**)

* Toon Boeckling, Guy De Tré and Antoon Bronselaer, [Cleaning data with selection rules](https://doi.org/10.1109/ACCESS.2022.3222786), IEEE Access, vol. 10, pp. 125212-125229 (**2022**).
